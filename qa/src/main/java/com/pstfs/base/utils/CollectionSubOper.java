package com.pstfs.base.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.hutool.core.util.ReflectUtil;

/**
 * 集合的差集操作
 * @author jiajian
 */
public final class CollectionSubOper {

	/**
	 * 集合减集合 c1-c2，不会改变参数 c1
	 * @param <T>
	 * @param c1
	 * @param c2
	 * @param fields 扣减依据
	 * @return 返回扣减后的集合 c1=c1-c2
	 */
	public static <T> List<T> sub(List<T> c1,List<T> c2,String ...fields) {
		HashSet<List<Object>> c2Set = new HashSet<>();
		for (int i=0,len_i = c2.size();i<len_i;i++) {
			T c2r = c2.get(i);
			List<Object> vals = new ArrayList<>(fields.length);
			for (int j=0,len_j = fields.length;j<len_j;j++) {
				vals.add(ReflectUtil.invoke(c2r, fields[j]));
			}
			c2Set.add(vals);
		}
		List<T> retC1 = new ArrayList<T>(c1);
		for (int i = retC1.size()-1;i>-1;i--) {
			T c2r = retC1.get(i);
			List<Object> vals = new ArrayList<>(fields.length);
			for (int j=0,len_j = fields.length;j<len_j;j++) {
				vals.add(ReflectUtil.invoke(c2r, fields[j]));
			}
			if(c2Set.contains(vals)) {
				retC1.remove(i);
			}
		}
		return retC1;
	}
}