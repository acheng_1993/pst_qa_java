package com.pstfs.base.utils;
import java.util.HashMap;
/**
 * 创建 hashMap 工具类，使得 hashMap 更简洁。
 * @author jiajian
 */
public final class MapUtils {
	
	/**
	 * 简单地创建 HashMap<String, String>
	 * @param params
	 * @return
	 */
	public final static HashMap<String,String> hms(final String... params){
		HashMap<String, String> retMap = new HashMap<String, String>();
		for(int i=0,len_i=params.length;i<len_i;i+=2) {
			retMap.put(params[i].toString(), params[i+1]);
		}
		return retMap;
	}
	
	/**
	 * 简单地创建 HashMap<String, String>
	 * @param params
	 * @return
	 */
	public final static HashMap<String,Object> hmo(final Object... params){
		HashMap<String, Object> retMap = new HashMap<>();
		for(int i=0,len_i=params.length;i<len_i;i+=2) {
			retMap.put(params[i].toString(), params[i+1]);
		}
		return retMap;
	}
}
