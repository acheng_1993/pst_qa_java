package com.pstfs.base.utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class I18nUtils {

	@Autowired
	private MessageSource messageSource;
	
	/**
	 * 把编码翻译成对应的语言
	 * @param code
	 * @param args
	 * @return
	 */
	public String t(String code, Object... args) {
		return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
	}
}