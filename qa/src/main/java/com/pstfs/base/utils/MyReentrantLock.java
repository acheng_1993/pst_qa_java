package com.pstfs.base.utils;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 自定义的锁，支持对数字、字符串加锁。
 * @author jiajian
 */
public class MyReentrantLock {
	
	private static final Map<Number,ReentrantLock> NUM_LOCK_MAP;
	private static final AtomicBoolean A_LOCK1;
	
	private static final Map<String,ReentrantLock> STR_LOCK_MAP;
	private static final AtomicBoolean A_LOCK2;
	static {
		NUM_LOCK_MAP = new HashMap<>();
		A_LOCK1 = new AtomicBoolean(false);
		
		STR_LOCK_MAP = new HashMap<>();
		A_LOCK2 = new AtomicBoolean(false);
	}
	
	/**
	 * 按照数字锁定
	 * @param num
	 */
	public static void lockNum(Number num) {
		for(;;) {
			if(A_LOCK1.compareAndSet(false, true)) {
				break;
			}
		}
		ReentrantLock lock = NUM_LOCK_MAP.get(num);
		if(lock == null) {
			NUM_LOCK_MAP.put(num, lock = new ReentrantLock());
		}
		A_LOCK1.compareAndSet(true, false);
		lock.lock();
    }
	
	/**
	 * 尝试获取锁，如果获取成功，则返回 true，否则返回 false
	 * @param num
	 * @return
	 */
	public static boolean tryLockNum(Number num) {
		for(;;) {
			if(A_LOCK1.compareAndSet(false, true)) {
				break;
			}
		}
		ReentrantLock lock = NUM_LOCK_MAP.get(num);
		if(lock == null) {
			NUM_LOCK_MAP.put(num, lock = new ReentrantLock());
		}
		A_LOCK1.compareAndSet(true, false);
		return lock.tryLock();
	}
	
	/**
	 * 按照数字解锁
	 * @param num
	 */
	public static void unlockNum(Number num) {
		for(;;) {
			if(A_LOCK1.compareAndSet(false, true)) {
				break;
			}
		}
		ReentrantLock lock = NUM_LOCK_MAP.remove(num);
		A_LOCK1.compareAndSet(true, false);
		if(lock != null) {
			lock.unlock();
		}
	}
	
	
	/**
	 * 按照字符串锁定
	 * @param num
	 */
	public static void lockStr(String str) {
		for(;;) {
			if(A_LOCK2.compareAndSet(false, true)) {
				break;
			}
		}
		ReentrantLock lock = STR_LOCK_MAP.get(str);
		if(lock == null) {
			STR_LOCK_MAP.put(str, lock = new ReentrantLock());
		}
		A_LOCK2.compareAndSet(true, false);
		lock.lock();
    }
	
	/**
	 * 按照字符串解锁
	 * @param num
	 */
	public static void unlockStr(String str) {
		for(;;) {
			if(A_LOCK2.compareAndSet(false, true)) {
				break;
			}
		}
		ReentrantLock lock = STR_LOCK_MAP.remove(str);
		A_LOCK2.compareAndSet(true, false);
		if(lock != null) {
			lock.unlock();
		}
	}
}