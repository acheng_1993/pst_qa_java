package com.pstfs.base.utils;
import java.util.HashSet;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.exception.RRException;

/**
 * Sql语句的简易解析器。
 * 为提高灵活性，允许前端传入部分 sql，为了方便业务类解析这些 sql，设计该解析器，经过解析的 sql 不会产生 sql 注入漏洞
 * @author jiajian
 */
public class SqlParserHelper {
	
	/**
	 * 检测查询的列是否符合: xxx1,xxx2,xxx3 的格式, 即只指定查询的字段名
	 * @param selectColumns 查询的字段名
	 * @param sensitiveColumns 敏感字段, 如果 selectColumns 含有敏感字段,则去除敏感字段
	 * @return
	 */
	public static String selectColumnsFilter(String selectColumns,String ...sensitiveColumns) {
		HashSet<String> ret = new HashSet<>();
		if (StringUtils.isNotEmpty(selectColumns)) {
			SelectColumnsParser selectColumnsParser = new SelectColumnsParser(selectColumns);
			for(;;) {
				String field = selectColumnsParser.next();
				if (field == null) {
					break;
				}
				ret.add(field.toLowerCase());
			}
			for (int i=0,len_i = selectColumns.length();i<len_i;i++) {
				ret.remove(sensitiveColumns[i].toLowerCase());
			}
			return String.join(",", ret);
		} else {
			return "";
		}
	}
	
	private static class SelectColumnsParser {
		private char[] selectColumnsCharArr;
		
		private SelectColumnsParser(String selectColumns) {
			selectColumnsCharArr = selectColumns.toCharArray();
		}
		
		//初始状态
		private final static int INIT = 0;
		//字段解析状态
		private final static int FIELD_ANALYSIS = 1;
		//字段解析状态，遇到 . 后的首个字符
		private final static int FIELD_ANALYSIS_AFTER_POINT_FIRST = 2;
		//字段解析状态，遇到 . 后的首个字符的后继字符
		private final static int FIELD_ANALYSIS_AFTER_POINT_FIRST_AFTER = 3;
		//寻找分隔符
		private final static int FIND_SPLIT = 4;
		
		private int idx = -1;
		
		private int curStatus;

		public String next() {
			int curStatus = this.curStatus;
			int i = idx + 1;
			char[] selectColumnsCharArr = this.selectColumnsCharArr;
			int j = 0;
			try {
				for(int len_i = selectColumnsCharArr.length;i < len_i;i++) {
					char c = selectColumnsCharArr[i];
					switch (curStatus) {
					case INIT:
						if (' ' == c) {
							continue;
						} else if((c>='a' && c<='z') || (c>='A' && c<='Z') || c == '_') {
							selectColumnsCharArr[j++] = c;
							curStatus = FIELD_ANALYSIS;
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIELD_ANALYSIS:
						if ((c>='a' && c<='z') || (c>='A' && c<='Z') || c == '_' || (c >= '0' && c <= '9')) {
							selectColumnsCharArr[j++] = c;
						} else if(c == '.') {
							selectColumnsCharArr[j++] = c;
							curStatus = FIELD_ANALYSIS_AFTER_POINT_FIRST;
						} else if(c == ' ') {
							curStatus = FIND_SPLIT;
							return new String(selectColumnsCharArr,0,j);
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIELD_ANALYSIS_AFTER_POINT_FIRST:
						if ((c>='a' && c<='z') || (c>='A' && c<='Z') || c == '_') {
							selectColumnsCharArr[j++] = c;
							curStatus = FIELD_ANALYSIS_AFTER_POINT_FIRST_AFTER;
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIELD_ANALYSIS_AFTER_POINT_FIRST_AFTER:
						if ((c>='a' && c<='z') || (c>='A' && c<='Z') || c == '_' || (c >= '0' && c <= '9')) {
							selectColumnsCharArr[j++] = c;
						} else if(c == ' ') {
							curStatus = FIND_SPLIT;
							return new String(selectColumnsCharArr,0,j);
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIND_SPLIT:
						if (' ' == c) {
							continue;
						} else if(c == ',') {
							curStatus = INIT;
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					}
				}
				if (j>0) {
					return new String(selectColumnsCharArr,0,j);
				}
				return null;
			} finally {
				idx = i;
			}
		}
	}
	
	/**
	 * 创建 order by 解析器
	 * @param orderSql
	 * @return
	 */
	public static <E> void setOrderRule(QueryWrapper<E> queryWrapper,String orderSql) {
		if (StringUtils.isNotEmpty(orderSql)) {
			OrderParser orderParser = new OrderParser(orderSql);
			for(;;) {
				String orderField = orderParser.next();
				if(orderField == null) {
					break;
				}
				orderParser.next();
				if(orderParser.token == orderParser.ASC_ORDER_TOKEN) {
					queryWrapper.orderByAsc(orderField);
				} else if(orderParser.token == orderParser.DESC_ORDER_TOKEN) {
					queryWrapper.orderByDesc(orderField);
				}
			}
		}
	}
	
	/**
	 * order by 解析器
	 * @author jiajian
	 */
	private static class OrderParser {
		
		private char[] orderSqlCharArr;
		
		private OrderParser(String orderSql) {
			orderSqlCharArr = orderSql.toCharArray();
		}
		
		//字段符号
		public final static int FIELD_TOKEN = 0;
		//升序排序符号
		public final static int ASC_ORDER_TOKEN = 1;
		//降序排序符号
		public final static int DESC_ORDER_TOKEN = 2;
		
		/**
		 * 当前语句的符号
		 */
		public int token;
		
		//初始状态
		private final static int INIT = 0;
		//字段解析状态
		private final static int FIELD_ANALYSIS = 1;
		//字段解析状态，遇到 . 后的首个字符
		private final static int FIELD_ANALYSIS_AFTER_POINT_FIRST = 2;
		//字段解析状态，遇到 . 后的首个字符的后继字符
		private final static int FIELD_ANALYSIS_AFTER_POINT_FIRST_AFTER = 3;
		//寻找排序规则状态
		private final static int FIND_ORDER = 4;
		//寻找排序规则的分隔符
		private final static int FIND_ORDER_SPLIT = 5;
		
		private int idx = -1;
		
		private int curStatus;
		
		public String next() {
			int curStatus = this.curStatus;
			int i = idx + 1;
			try {
				char[] orderSqlCharArr = this.orderSqlCharArr;
				int j = 0;
				for(int len_i = orderSqlCharArr.length;i < len_i;i++) {
					char c = orderSqlCharArr[i];
					switch (curStatus) {
					case INIT:
						if (' ' == c) {
							continue;
						} else if((c>='a' && c<='z') || (c>='A' && c<='Z') || c == '_') {
							orderSqlCharArr[j++] = c; 
							curStatus = FIELD_ANALYSIS;
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIELD_ANALYSIS:
						if ((c>='a' && c<='z') || (c>='A' && c<='Z') || c == '_' || (c >= '0' && c <= '9')) {
							orderSqlCharArr[j++] = c;
						} else if(c == '.') {
							orderSqlCharArr[j++] = c;
							curStatus = FIELD_ANALYSIS_AFTER_POINT_FIRST;
						} else if(c == ' ') {
							curStatus = FIND_ORDER;
							token = FIELD_TOKEN;
							return new String(orderSqlCharArr,0,j);
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIELD_ANALYSIS_AFTER_POINT_FIRST:
						if ((c>='a' && c<='z') || (c>='A' && c<='Z') || c == '_') {
							orderSqlCharArr[j++] = c;
							curStatus = FIELD_ANALYSIS_AFTER_POINT_FIRST_AFTER;
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIELD_ANALYSIS_AFTER_POINT_FIRST_AFTER:
						if ((c>='a' && c<='z') || (c>='A' && c<='Z') || c == '_' || (c >= '0' && c <= '9')) {
							orderSqlCharArr[j++] = c;
						} else if(c == ' ') {
							curStatus = FIND_ORDER;
							token = FIELD_TOKEN;
							return new String(orderSqlCharArr,0,j);
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIND_ORDER:
						if (' ' == c) {
							continue;
						} else if(c == 'a' || c == 'A') {
							orderSqlCharArr[j++] = c;
							c = orderSqlCharArr[++i];
							if (c == 's' || c == 'S') {
								orderSqlCharArr[j++] = c;
							} else {
								throw new RRException("解析异常，请确认代码是否有误！");
							}
							c = orderSqlCharArr[++i];
							if (c == 'c' || c == 'C') {
								orderSqlCharArr[j++] = c;
							} else {
								throw new RRException("解析异常，请确认代码是否有误！");
							}
							c = orderSqlCharArr[++i];
							if (c == ' ') {
								token = ASC_ORDER_TOKEN;
								curStatus = FIND_ORDER_SPLIT;
								return new String(orderSqlCharArr,0,j);
							} else if(c == ',') {
								token = ASC_ORDER_TOKEN;
								curStatus = INIT;
								return new String(orderSqlCharArr,0,j);
							}
						} else if(c == 'd' || c == 'D') {
							orderSqlCharArr[j++] = c;
							c = orderSqlCharArr[++i];
							if (c == 'e' || c == 'E') {
								orderSqlCharArr[j++] = c;
							} else {
								throw new RRException("解析异常，请确认代码是否有误！");
							}
							c = orderSqlCharArr[++i];
							if (c == 's' || c == 'S') {
								orderSqlCharArr[j++] = c;
							} else {
								throw new RRException("解析异常，请确认代码是否有误！");
							}
							c = orderSqlCharArr[++i];
							if (c == 'c' || c == 'C') {
								orderSqlCharArr[j++] = c;
							} else {
								throw new RRException("解析异常，请确认代码是否有误！");
							}
							c = orderSqlCharArr[++i];
							if (c == ' ') {
								token = DESC_ORDER_TOKEN;
								curStatus = FIND_ORDER_SPLIT;
								return new String(orderSqlCharArr,0,j);
							} else if(c == ',') {
								token = DESC_ORDER_TOKEN;
								curStatus = INIT;
								return new String(orderSqlCharArr,0,j);
							}
						} else if(c == ',') {
							curStatus = INIT;
							token = ASC_ORDER_TOKEN;
							return "ASC";
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					case FIND_ORDER_SPLIT:
						if (' ' == c) {
							continue;
						} else if(c == ',') {
							curStatus = INIT;
						} else {
							throw new RRException("解析异常，请确认代码是否有误！");
						}
						break;
					}
				}
				return null;
			} finally {
				this.curStatus = curStatus;
				idx = i;
			}
		}
	}
}