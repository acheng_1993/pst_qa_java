package com.pstfs.base.utils;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.oauth2.SecretKeyTable.SecretKeyTableMgr;
import com.pstfs.base.service.RoleService;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

/**
 * 令牌工具
 * 
 * @author jiajian
 */
@Component
public class AuthorizationUtils {

	/**
	 * 密码表管理类
	 */
	@Autowired
	private SecretKeyTableMgr secretKeyTableMgr;
	
	@Autowired
	private I18nUtils i;

	/**
	 * 根据请求报文获取令牌 Authorization
	 * @param request
	 * @return
	 */
	public String getAuthorization(HttpServletRequest request) {
		//在报文头传入 Authorization
		String authorization = request.getHeader("Authorization");
		//如果报文头没有 Authorization，则只能从 Cookie 中寻找 Authorization
		if (StringUtils.isEmpty(authorization)) {
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (int i=0,len_i = cookies.length;i<len_i;i++) {
					Cookie cookie = cookies[i];
					if ("Authorization".equals(cookie.getName())) {
						authorization = cookie.getValue();
						break;
					}
				}
			}
		}
		return authorization;
	}
	
	/**
	 * 创建token，token 组成有: 用户名长度+用户名+登录时间+用户拥有的角色
	 * 
	 * @param username
	 * @param userHasRole
	 * @return
	 */
	public String createAuthorization(String username, String userHasRole) {
		byte[] key = secretKeyTableMgr.getSecretKeyTable().getSecretKey(username.toCharArray());
		byte[] usernameByte = username.getBytes();
		byte[] userHasRoleByte = userHasRole == null ? new byte[0] : Base64.decode(userHasRole);
		// 用户名长度+用户名+登录时间+用户拥有的角色
		byte[] data = new byte[1 + usernameByte.length + 4 + userHasRoleByte.length];
		byte[] time = new byte[4];
		//当前小时数
		long currTimeHours = System.currentTimeMillis()/(3600*1000)+2;
		for (int i = 0; i < 4; i++) {
			time[i] = (byte) (currTimeHours >> (8 * i));
		}
		data[0] = (byte) usernameByte.length;
		System.arraycopy(usernameByte, 0, data, 1, usernameByte.length);
		System.arraycopy(time, 0, data, 1 + usernameByte.length, time.length);
		System.arraycopy(userHasRoleByte, 0, data, 1 + usernameByte.length + 4, userHasRoleByte.length);

		SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
		return Base64.encode(data) + "." + aes.encryptBase64(data);
	}

	/**
	 * 把开关位的 base64 转化为具体的开关位
	 * 
	 * @param indexBase64
	 * @return
	 */
	public List<Integer> getIndexList(String indexBase64) {
		List<Integer> ret = new ArrayList<>();
		if (StringUtils.isNotEmpty(indexBase64)) {
			byte[] indexBytes = Base64.decode(indexBase64);
			for (int i = 0, len_i = indexBytes.length; i < len_i; i++) {
				byte indexByte = indexBytes[i];
				for (int j = 0; j < 8; j++) {
					if (((indexByte >> j) & 1) == 1) {
						ret.add(j + i * 8);
					}
				}
			}
		}
		return ret;
	}

	/**
	 * 获取装载信息的对象
	 * @param request
	 * @return
	 */
	public AuthorizationObj getAuthorizationObj(HttpServletRequest request) {
		//在报文头传入 Authorization
		String authorization = request.getHeader("Authorization");
		//如果报文头没有 Authorization，则只能从 Cookie 中寻找 Authorization
		if (StringUtils.isEmpty(authorization)) {
			Cookie[] cookies = request.getCookies();
			if(cookies!=null) {
				for (int i=0,len_i = cookies.length;i<len_i;i++) {
					Cookie cookie = cookies[i];
					if ("Authorization".equals(cookie.getName())) {
						authorization = cookie.getValue();
						break;
					}
				}
			}
		}
		if(StringUtils.isEmpty(authorization)) {
//			方法调用失败，该方法只能登录后才能被调用！
			throw new RRException(i.t("AuthorizationUtils.getAuthorizationObj.authorizationIsEmpty"));
		}
		return new AuthorizationObj(authorization,i);
	}
	
	/**
	 * 获取装载信息的对象
	 * @param authorization
	 * @return
	 */
	public AuthorizationObj getAuthorizationObj(String authorization) {
		return new AuthorizationObj(authorization,i);
	}

	/**
	 * 内部类
	 * 
	 * @author jiajian
	 */
	public static class AuthorizationObj {
		private String username;
		private long validity;
		private byte[] data;
		private String authorization;

		public AuthorizationObj(String authorization,I18nUtils i18nUtils) {
			this.authorization = authorization;
			int splitPoint = authorization.indexOf(".");
			if (splitPoint == -1) {
				//Authorization格式错误！
				throw new RRException(i18nUtils.t("AuthorizationObj.authorizationFormatError"));
			}
			String authorizationData = authorization.substring(0, splitPoint);
			data = Base64.decode(authorizationData);
			byte usernameLength = data[0];
			username = new String(data, 1, usernameLength);

			long validity = 0;
			for (int i = usernameLength + 1, j = 0; j < 4; i++, j++) {
				long temp = data[i] & 0xff;
				validity += temp << (j * 8);
			}
			this.validity = validity * 3600 * 1000;
		}

		/**
		 * 获取角色的开关位
		 * @param indexBase64
		 * @return
		 */
		public List<Integer> getRoleIndexList() {
			List<Integer> ret = new ArrayList<>();
			int i = data[0] + 1 + 4; // 先获取角色位起始位置
			for (int len_i=data.length,h=0;i<len_i;i++,h++) {
				byte roleIdxByte = data[i];
				for (int j=0;roleIdxByte>0;j++,roleIdxByte>>=1) {
					if ((roleIdxByte & 1) == 1) {
						ret.add(h*8+j);
					}
				}
			}
			return ret;
		}
		
		/**
		 * 数据
		 * @return
		 */
		public byte[] getData() {
			return data;
		}

		/**
		 * 校验角色位是否存在
		 * @param index
		 * @return
		 */
		public boolean checkIndex(int index) {
			int i = data[0] + 1 + 4; // 先获取角色位起始位置
			for (int len_i=data.length,h=0;i<len_i;i++,h++) {
				byte roleIdxByte = data[i];
				for (int j=0;roleIdxByte>0;j++,roleIdxByte>>=1) {
					if ((roleIdxByte & 1) == 1) {
						byte[] urlIdxByte = RoleService.ROLE_IDX_URL_MAP.get(h*8+j);
						if (urlIdxByte != null) {
							boolean ret = false;
							int rest = (index + 1) % 8;
							if (rest == 0) { //如果余位是0
								int tempIdx = (index + 1) / 8 - 1; //计算偏移位置
								if (tempIdx<urlIdxByte.length) {
									ret = ((urlIdxByte[tempIdx]>>>7) & 1) == 1; 
								}
							} else {
								int tempIdx = (index + 1 - rest) / 8;
								if (tempIdx<urlIdxByte.length) {
									ret = ((urlIdxByte[tempIdx]>>>(rest-1)) & 1) == 1;
								}
							}
							if (ret) {
								return true;
							}
						}
					}
				}
			}
			return false;
		}

		/**
		 * 获取用户名
		 * 
		 * @param authorization
		 * @return
		 */
		public String getUsername() {
			return username;
		}

		/**
		 * 获取有效期
		 * 
		 * @param authorization
		 * @return
		 */
		public long getValidity() {
			return validity;
		}

		public String getAuthorization() {
			return authorization;
		}
	}
}