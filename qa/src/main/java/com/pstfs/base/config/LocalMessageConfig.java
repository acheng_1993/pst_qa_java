package com.pstfs.base.config;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * 如果国际化文件名的格式如下:
 * xxx模块_messages_ja_JP.properties
 * xxx模块_messages_zh_CN.properties
 * xxx模块_messages.properties
 * 这时内置的国际化配置加载器将不会去整合这些分模块的配置，而该配置是使得
 * 国际化配置加载器自动整合这些分模块的配置
 * 
 * @author jiajian
 *
 */
@Configuration
public class LocalMessageConfig {

	@Bean(name = "messageSource")
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        String[] strings = scanModelsForI18nFolders();
        messageSource.setBasenames(strings);
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

	private String[] scanModelsForI18nFolders() {
        try {
            // 通配规则 直接获取 Resources中的内容
            String resourcePattern = "classpath*:i18n/*_messages.properties";
            ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
            Resource[] resources = resourcePatternResolver.getResources(resourcePattern);
            String[] paths = new String[resources.length];
            for (int i = 0; i < resources.length; i++) {
                Resource resource = resources[i];
                String[] urlStrArr = resource.getURL().toString().split("/");
                int index = urlStrArr.length - 1;
                String moduleName = urlStrArr[index];
                // 通配符加+ 模块名 + /国际化数据目录
                paths[i] = "classpath:i18n/" + moduleName.substring(0, moduleName.length() - 11);
            }
            return paths;
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return null;
    }
}