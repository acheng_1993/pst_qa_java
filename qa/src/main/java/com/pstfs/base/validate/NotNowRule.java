package com.pstfs.base.validate;
import java.util.Date;

import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 不能为现在值，至少也是24小时以后
 * @author jiajian
 */
public class NotNowRule implements IDataValidRule {
	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if(data == null) {
			return true;
		}
		long time = ((Date)data).getTime();
		return time > System.currentTimeMillis() + 24*3600*1000;
	}
}