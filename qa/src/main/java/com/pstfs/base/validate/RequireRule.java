package com.pstfs.base.validate;
import java.util.Collection;
import java.util.Date;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 必填校验
 * @author Administrator
 */
public class RequireRule implements IDataValidRule{

	
	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if(data == null) {
			return false;
		}
		if (data instanceof String) {
			return StringUtils.isNotEmpty((String)data);
		} else if(data instanceof Collection) {
			return data != null && !((Collection)data).isEmpty();
		} else if(data instanceof Number) {
			return data != null;
		} else if (data instanceof Date) {
			return data != null;
		}
		return false;
	}

}