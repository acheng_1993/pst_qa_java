package com.pstfs.base.validate;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 枚举校验，例如: in:1,2,3 则字段只能是 1,2,3 其中一种
 * @author jiajian
 */
public class InRule implements IDataValidRule {

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if(data == null) {
			return true;
		}
		if(data instanceof String) {
			for (int i=0,len_i = params.length;i<len_i;i++) {
				Object param = params[i];
				if(String.valueOf(param).equals(data)) {
					return true;
				}
			}
		} else if(data instanceof Number) {
			Comparable<Number> data2 = (Comparable<Number>)data;
			for (int i=0,len_i = params.length;i<len_i;i++) {
				Number param = (Number)params[i];
				if(data2.compareTo(param) == 0) {
					return true;
				}
			}
		}
		return false;
	}
}