package com.pstfs.base.validate;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 文件名校验
 * @author jiajian
 */
public class FilenameRule implements IDataValidRule{

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (StringUtils.isEmpty((String)data)) {
			return true;
		}
		String filename = String.valueOf(data);
		return filename.length()<=255 && filename.matches("^([^ \\\\:*?\"<>|/][^\\\\:*?\"<>|/]*[^ \\\\:*?\"<>|/])|[^ \\\\:*?\"<>|/]$");
	}
}