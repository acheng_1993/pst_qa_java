package com.pstfs.base.validate;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 验证是否为标准的url，支持传入数组批量校验
 * @author jiajian
 */
public class UrlRule implements IDataValidRule {

	
	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (data == null) {
			return true;
		}
		if(data instanceof String) {
			if (StringUtils.isEmpty((String)data)) {
				return true;
			}
			try {
				new URL(String.valueOf(data));
				return true;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		} else if(data instanceof Iterable) {
			Collection it = (Collection)data;
			if (it.isEmpty()) {
				return true;
			}
			for(Object url:it) {
				try {
					new URL(String.valueOf(url));
				} catch (MalformedURLException e) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}