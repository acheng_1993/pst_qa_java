package com.pstfs.base.validate;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 不等校验
 * @author Administrator
 */
public class NotEqualToRule implements IDataValidRule {
	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (data == null) {
			return true;
		}
		return !data.equals(params[0]);
	}
}