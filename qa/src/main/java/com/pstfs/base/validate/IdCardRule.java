package com.pstfs.base.validate;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;
import cn.hutool.core.util.IdcardUtil;
/**
 * 身份证校验
 * @author jiajian
 */
public class IdCardRule implements IDataValidRule{

	public final static String ID_CARD_15 = "ID_CARD_15";
	public final static String ID_CARD_18 = "ID_CARD_18";
	public final static String ID_CARD_15_OR_18 = "ID_CARD_15_OR_18";
	
	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (StringUtils.isEmpty((String)data)) {
			return true;
		}
		String cardType = (String)params[0];
		String data2 = (String)data;
		if (ID_CARD_15_OR_18.equals(cardType)) {
			return IdcardUtil.isValidCard(data2) || IdcardUtil.isValidCard(data2);
		} else if(ID_CARD_15.equals(cardType)) {
			return IdcardUtil.isValidCard(data2);
		} else if(ID_CARD_18.equals(cardType)) {
			return IdcardUtil.isValidCard(data2);
		} else {			
			return false;
		}
	}
}