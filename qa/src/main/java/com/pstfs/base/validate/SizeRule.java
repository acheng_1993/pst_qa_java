package com.pstfs.base.validate;

import java.util.Collection;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 验证对象的长度，例如：集合长度、字符串长度
 * @author Administrator
 */
public class SizeRule implements IDataValidRule{

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (data == null) {
			return true;
		}
		int rangeStart = (int)params[0];
		int rangeEnd = (int)params[1];
		int size = -1;
		if(data instanceof CharSequence) {
			if(StringUtils.isEmpty((CharSequence)data)) {
				return true;
			}
			size = ((CharSequence)data).length();
		} else if(data instanceof Collection) {
			if(((Collection)data).isEmpty()) {
				return true;
			}
			size = ((Collection<?>)data).size();
		}
		return size>=rangeStart && size<=rangeEnd;
	}
}