package com.pstfs.base.validate;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.pstfs.base.exception.RRException;

/**
 * 数据校验工具
 * 
 * @author Administrator
 */
public class DataValidateUtils {

	private static final Map<String, IDataValidRule> RULES_MAP;
	static {
		RULES_MAP = new HashMap<>();
		try {
			Class.forName("com.pstfs.base.validate.DataValidateFactory");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void registerRule(String ruleName, IDataValidRule dataValidRule) {
		RULES_MAP.put(ruleName, dataValidRule);
	}
	
	public static IDataValidRule useRule(String ruleName) {
		return RULES_MAP.get(ruleName);
	}

	/**
	 * 校验键值对
	 * 
	 * @param data       需要校验的键值对
	 * @param keyAndRule 校验的字段和规则
	 */
	public static void validate(Map<String, String> errMsgMap, Map<String, ?> data, Object... keyAndRule) {
		RuleAnalysis ruleAnalysis = new RuleAnalysis();
		ArrayList<Object> params = new ArrayList<Object>(10);
		for (int i = 0, len_i = keyAndRule.length; i < len_i; i += 2) {
			String key = (String) keyAndRule[i];
			Object val = data.get(key);
			Object rule = keyAndRule[i + 1];
			String strRule = (String) rule;
			ruleAnalysis.setStrRule(strRule);
			
			String word = null;
			for(boolean withParamName = false;;) {
				if(withParamName) {
					withParamName = false;
				} else {
					word = ruleAnalysis.word();					
				}
				if(word == null) {
					break;
				}
				IDataValidRule dataValidRule = RULES_MAP.get(word);
				if (dataValidRule == null) {
					throw new RRException("“"+word+"”规则不存在！");
				}
				String ruleName = word;
				if (DataValidateFactory.isNoParamsRule(word)) {
					if (!dataValidRule.valid(data, val, params.toArray())) {
						String msg = errMsgMap.get(key + "." + word);
						if (msg == null) {
							throw new RRException("未对'" + key + "." + word + "'指定错误消息");
						} else {
							for (int j = 0, len_j = params.size(); j < len_j; j++) {
								msg = msg.replaceAll("\\{" + j + "\\}", params.get(j).toString());
							}
							throw new RRException(msg);
						}
					}
				} else {
					for(;;) {
						word = ruleAnalysis.word();
						if (ruleAnalysis.currentToken == ruleAnalysis.RULE_NUM_PARAMS) {
							if (word.indexOf(".") > -1 || word.indexOf("e") > -1) {
								params.add(Double.parseDouble(word));
							} else {
								params.add(Integer.parseInt(word));
							}
						} else if (ruleAnalysis.currentToken == ruleAnalysis.RULE_STRING_PARAMS) {
							params.add(word);
						} else {
							break;
						}
					}
					if (!dataValidRule.valid(data, val, params.toArray())) {
						String msg = errMsgMap.get(key + "." + ruleName);
						if (msg == null) {
							throw new RRException("未对'" + key + "." + ruleName + "'指定错误消息");
						} else {
							for (int j = 0, len_j = params.size(); j < len_j; j++) {
								msg = msg.replaceAll("\\{" + j + "\\}", params.get(j).toString());
							}
							throw new RRException(msg);
						}
					}
					params.clear();
					if (word == null) {
						break;
					} else {
						withParamName = true;
					}
				}
			}
		}
	}

	/**
	 * 校验实体
	 * 
	 * @param data       需要校验的实体
	 * @param keyAndRule 校验的字段和规则
	 */
	public static void validate(Map<String, String> errMsgMap, Object data, Object... keyAndRule) {
		RuleAnalysis ruleAnalysis = new RuleAnalysis();
		ArrayList<Object> params = new ArrayList<Object>(10);
		Class clazz = data.getClass();
		for (int i = 0, len_i = keyAndRule.length; i < len_i; i += 2) {
			String key = (String) keyAndRule[i];
			Object val = null;
			try {
				Field field = clazz.getDeclaredField(key);
				field.setAccessible(true);
				val = field.get(data);
			} catch (NoSuchFieldException e) {
				throw new RRException("字段" + key + "不存在！");
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			Object rule = keyAndRule[i + 1];
			String strRule = (String) rule;
			ruleAnalysis.setStrRule(strRule);
			String word = null;
			for(boolean withParamName = false;;) {
				if(withParamName) {
					withParamName = false;
				} else {
					word = ruleAnalysis.word();					
				}
				if(word == null) {
					break;
				}
				IDataValidRule dataValidRule = RULES_MAP.get(word);
				if (dataValidRule == null) {
					throw new RRException("“"+word+"”规则不存在！");
				}
				String ruleName = word;
				if (DataValidateFactory.isNoParamsRule(word)) {
					if (!dataValidRule.valid(data, val, params.toArray())) {
						String msg = errMsgMap.get(key + "." + word);
						if (msg == null) {
							throw new RRException("未对'" + key + "." + word + "'指定错误消息");
						} else {
							for (int j = 0, len_j = params.size(); j < len_j; j++) {
								msg = msg.replaceAll("\\{" + j + "\\}", params.get(j).toString());
							}
							throw new RRException(msg);
						}
					}
				} else {
					for(;;) {
						word = ruleAnalysis.word();
						if (ruleAnalysis.currentToken == ruleAnalysis.RULE_NUM_PARAMS) {
							if (word.indexOf(".") > -1 || word.indexOf("e") > -1) {
								params.add(Double.parseDouble(word));
							} else {
								params.add(Integer.parseInt(word));
							}
						} else if (ruleAnalysis.currentToken == ruleAnalysis.RULE_STRING_PARAMS) {
							params.add(word);
						} else {
							break;
						}
					}
					if (!dataValidRule.valid(data, val, params.toArray())) {
						String msg = errMsgMap.get(key + "." + ruleName);
						if (msg == null) {
							throw new RRException("未对'" + key + "." + ruleName + "'指定错误消息");
						} else {
							for (int j = 0, len_j = params.size(); j < len_j; j++) {
								msg = msg.replaceAll("\\{" + j + "\\}", params.get(j).toString());
							}
							throw new RRException(msg);
						}
					}
					params.clear();
					if (word == null) {
						break;
					} else {
						withParamName = true;
					}
				}
			}
		}
	}

	/**
	 * 消息校验规则接口
	 * 
	 * @author Administrator
	 */
	public static interface IDataValidRule {
		/**
		 * 校验
		 * 
		 * @param entity 校验的实体或map
		 * @param data   校验的数据
		 * @param params 校验参数
		 * @return
		 */
		public boolean valid(Object entity, Object data, Object[] params);
	}

	/**
	 * 规则解析器
	 * 
	 * @author jiajian
	 */
	private static class RuleAnalysis {
		/**
		 * 规则符号
		 */
		// 规则名
		public final static int RULE_NAME = 0;
		// 规则数字参数
		public final static int RULE_NUM_PARAMS = 1;
		// 规则字符串参数
		public final static int RULE_STRING_PARAMS = 2;
		// 规则结束
		public final static int RULE_FINISH = 4;

		/**
		 * 解析状态
		 */
		// 开始解析规则对
		public final static int INIT = 0;
		// 解析规则名
		public final static int RULE_NAME_STATUS = 1;
		// 解析规则名完毕，寻找分隔符
		public final static int RULE_NAME_TO_SPLIT_STATUS = 2;
		// 找到规则参数分隔符，开始找规则参数
		public final static int SPLIT_TO_RULE_PARAMS_STATUS = 3;
		// 解析字符规则参数
		public final static int STRING_RULE_PARAMS_STATUS = 4;
		// 解析字符规则时遇到斜杠
		public final static int SLASH_STRING_RULE_PARAMS_STATUS = 5;
		// 从字符规则参数解析完毕后，去寻找分割符
		public final static int STRING_RULE_PARAMS_TO_SPLIT_STATUS = 6;
		// 解析含正负号数字规则参数
		public final static int PLUS_OR_MINUS_NUM_RULE_PARAMS_STATUS = 7;
		// 解析数字规则参数
		public final static int NUM_RULE_PARAMS_STATUS = 8;
		// 首次解析直接用 . 开头的数字规则参数
		public final static int POINT_FIRST_NUM_RULE_PARAMS_STATUS = 9;
		// 解析直接用 . 开头的数字规则参数
		public final static int POINT_NUM_RULE_PARAMS_STATUS = 10;
		// 解析数字规则参数时遇到e，并紧跟有符号数字
		public final static int E_PLUS_OR_MINUS_NUM_RULE_PARAMS_STATUS = 11;
		// 解析数字规则参数时遇到e，并紧跟无符号数字
		public final static int E_UNSIGNED_NUM_RULE_PARAMS_STATUS = 12;
		// 解析规则参数完毕，寻找分隔符
		public final static int RULE_PARAMS_TO_SPLIT_STATUS = 13;

		private char[] strRule;

		public void setStrRule(String strRule) {
			char[] strRule1 = new char[strRule.length() + 1];
			System.arraycopy(strRule.toCharArray(), 0, strRule1, 0, strRule.length());
			strRule1[strRule.length()] = Character.MIN_VALUE;
			this.strRule = strRule1;
			lastIndex = -1;
			currentToken = -1;
			currentStatus = INIT;
		}

		private int lastIndex = -1;

		/**
		 * 当前符号
		 */
		public int currentToken = -1;

		/**
		 * 当前状态
		 */
		public int currentStatus = INIT;

		public String word() {
			int lastIndex = this.lastIndex;
			int currentStatus = this.currentStatus;
			char[] strRule = this.strRule;
			int retIndex = 0;
			try {
				lastIndex++;
				for (int len = strRule.length; lastIndex < len; lastIndex++) {
					char c = strRule[lastIndex];
					switch (currentStatus) {
					case INIT:
						if (c == ' ') {
							continue;
						} else if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_') {
							strRule[retIndex++] = c;
							currentStatus = RULE_NAME_STATUS;
						} else {
							throw new RRException("规则中不允许出现字符'" + c + "'！");
						}
						break;
					case RULE_NAME_STATUS:
						if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_' || (c >= '0' && c <= '9')) {
							strRule[retIndex++] = c;
						} else if (c == ' ') {// 遇到空格，则寻找分隔符
							currentToken = RULE_NAME;
							currentStatus = RULE_NAME_TO_SPLIT_STATUS;
							return new String(strRule, 0, retIndex);
						} else if (c == ':') {// 遇到分号，则寻找参数
							currentToken = RULE_NAME;
							currentStatus = SPLIT_TO_RULE_PARAMS_STATUS;
							return new String(strRule, 0, retIndex);
						} else if (c == '|') {
							currentToken = RULE_NAME;
							currentStatus = INIT;
							return new String(strRule, 0, retIndex);
						} else if (c == Character.MIN_VALUE) {
							currentToken = RULE_NAME;
							return new String(strRule, 0, retIndex);
						} else {
							throw new RRException("规则中不允许出现字符'" + c + "'！");
						}
						break;
					case RULE_NAME_TO_SPLIT_STATUS:
						if (c == ' ') {
							continue;
						} else if (c == ':') {
							currentStatus = SPLIT_TO_RULE_PARAMS_STATUS;
						} else if (c == '|') {
							currentStatus = INIT;
						} else {
							throw new RRException("规则中不允许出现字符'" + c + "'！");
						}
						break;
					case SPLIT_TO_RULE_PARAMS_STATUS:
						if (c == ' ') {
							continue;
						} else if (c == '+' || c == '-') {
							currentStatus = PLUS_OR_MINUS_NUM_RULE_PARAMS_STATUS;
							strRule[retIndex++] = c;
						} else if (c >= '0' && c <= '9') {
							strRule[retIndex++] = c;
							currentStatus = NUM_RULE_PARAMS_STATUS;
						} else if (c == '.') {
							strRule[retIndex++] = c;
							currentStatus = POINT_FIRST_NUM_RULE_PARAMS_STATUS;
						} else if (c == '\'') {
							currentStatus = STRING_RULE_PARAMS_STATUS;
						} else {
							throw new RRException("规则中不允许出现字符'" + c + "'！");
						}
						break;
					case PLUS_OR_MINUS_NUM_RULE_PARAMS_STATUS:
						if (c == ' ') {
							strRule[retIndex++] = c;
						} else if (c >= '0' && c <= '9') {
							strRule[retIndex++] = c;
							currentStatus = NUM_RULE_PARAMS_STATUS;
						} else if (c == '.') {
							strRule[retIndex++] = c;
							currentStatus = POINT_FIRST_NUM_RULE_PARAMS_STATUS;
						} else {
							throw new RRException("规则中不允许出现字符'" + c + "'！");
						}
						break;
					case NUM_RULE_PARAMS_STATUS:
						if (c >= '0' && c <= '9') {
							strRule[retIndex++] = c;
						} else if (c == '.') {
							strRule[retIndex++] = c;
							currentStatus = POINT_FIRST_NUM_RULE_PARAMS_STATUS;
						} else if (c == 'e') {
							strRule[retIndex++] = c;
							currentStatus = E_PLUS_OR_MINUS_NUM_RULE_PARAMS_STATUS;
						} else if (c == ' ') {
							currentStatus = RULE_PARAMS_TO_SPLIT_STATUS;
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else if (c == ',') {
							currentStatus = SPLIT_TO_RULE_PARAMS_STATUS;
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else if (c == '|') {
							currentStatus = INIT;
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else if (c == Character.MIN_VALUE) {
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else {
							throw new RRException("规则中不允许出现字符'" + c + "'！");
						}
						break;
					case POINT_FIRST_NUM_RULE_PARAMS_STATUS:
						if (c >= '0' && c <= '9') {
							strRule[retIndex++] = c;
							currentStatus = POINT_NUM_RULE_PARAMS_STATUS;
						} else {
							throw new RRException("在数字中'.'后面不能是'" + c + "'");
						}
						break;
					case POINT_NUM_RULE_PARAMS_STATUS:
						if (c >= '0' && c <= '9') {
							strRule[retIndex++] = c;
						} else if (c == ' ') {
							currentStatus = RULE_PARAMS_TO_SPLIT_STATUS;
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else if (c == 'e') {
							currentStatus = E_PLUS_OR_MINUS_NUM_RULE_PARAMS_STATUS;
							strRule[retIndex++] = c;
						} else if (c == Character.MIN_VALUE) {
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else {
							throw new RRException("在数字中'.'后面不能出现'" + c + "'");
						}
						break;
					case E_PLUS_OR_MINUS_NUM_RULE_PARAMS_STATUS:
						if ((c >= '0' && c <= '9') || c == '+' || c == '-') {
							currentStatus = E_UNSIGNED_NUM_RULE_PARAMS_STATUS;
							strRule[retIndex++] = c;
						} else {
							throw new RRException("在数字中'.'后面不能出现'" + c + "'");
						}
						break;
					case E_UNSIGNED_NUM_RULE_PARAMS_STATUS:
						if (c >= '0' && c <= '9') {
							strRule[retIndex++] = c;
						} else if (c == ' ') {
							currentStatus = RULE_PARAMS_TO_SPLIT_STATUS;
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else if (c == ',') {
							currentStatus = SPLIT_TO_RULE_PARAMS_STATUS;
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else if (c == '|') {
							currentStatus = INIT;
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else if (c == Character.MIN_VALUE) {
							currentToken = RULE_NUM_PARAMS;
							return new String(strRule, 0, retIndex);
						} else {
							throw new RRException("在数字中'.'后面不能出现'" + c + "'");
						}
						break;
					case STRING_RULE_PARAMS_STATUS:
						if (c == '\'') {
							currentStatus = STRING_RULE_PARAMS_TO_SPLIT_STATUS;
							currentToken = RULE_STRING_PARAMS;
							return new String(strRule, 0, retIndex);
						} else if (c == '\\') {
							currentStatus = SLASH_STRING_RULE_PARAMS_STATUS;
							strRule[retIndex++] = c;
						} else {
							strRule[retIndex++] = c;
						}
						break;
					case SLASH_STRING_RULE_PARAMS_STATUS:
						currentStatus = STRING_RULE_PARAMS_STATUS;
						strRule[retIndex++] = c;
						break;
					case STRING_RULE_PARAMS_TO_SPLIT_STATUS:
						if (c == ' ') {
							continue;
						} else if (c == '|') {
							currentStatus = INIT;
						} else if (c == ',') {
							currentStatus = SPLIT_TO_RULE_PARAMS_STATUS;
						} else if (c == Character.MIN_VALUE) {
							currentToken = RULE_FINISH;
							return null;
						} else {
							throw new RRException("在字符串后面不能出现'" + c + "'");
						}
						break;
					}
				}
				if(currentStatus == STRING_RULE_PARAMS_STATUS) {
					throw new RRException("“"+new String(strRule, 0, retIndex)+"”字符串未结束异常");
				}
				currentToken = RULE_FINISH;
				return null;
			} finally {
				this.lastIndex = lastIndex;
				this.currentStatus = currentStatus;
			}
		}
	}
}