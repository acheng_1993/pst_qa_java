package com.pstfs.base.validate;

import java.util.ArrayList;

public class DataValidateFactory {
	public static final String REQUIRE = "require";
	public static final String RANGE = "range";
	public static final String SIZE = "size";
	public static final String ID_CARD = "idCard";
	public static final String REGEX = "regex";
	public static final String NOT_EQUAL_TO = "notEqualTo";
	public static final String MAX = "max";
	public static final String IN = "in";
	public static final String NOT_A_FUTURE = "notAFuture";
	public static final String BEFORE = "before";
	public static final String BEFORE_OR_EQUAL = "beforeOrEqual";
	public static final String AFTER = "after";
	public static final String AFTER_OR_EQUAL = "afterOrEqual";
	public static final String URI = "uri";
	public static final String URL = "url";
	public static final String FILENAME = "filename";
	public static final String XSS = "xss";
	public static final String EMAIL = "email";
	public static final String NOT_NOW = "notNow";

	/**
	 * 无参数校验规则
	 */
	private static final ArrayList<String> NO_PARAMS_RULE;
	
	static {
		NO_PARAMS_RULE = new ArrayList<String>();
		NO_PARAMS_RULE.add(REQUIRE);
		NO_PARAMS_RULE.add(XSS);
		NO_PARAMS_RULE.add(URI);
		NO_PARAMS_RULE.add(URL);
		NO_PARAMS_RULE.add(NOT_A_FUTURE);
		NO_PARAMS_RULE.add(FILENAME);
		NO_PARAMS_RULE.add(EMAIL);
		NO_PARAMS_RULE.add(NOT_NOW);
		
		DataValidateUtils.registerRule(REQUIRE, new RequireRule());
		DataValidateUtils.registerRule(RANGE, new RangeRule());
		DataValidateUtils.registerRule(SIZE, new SizeRule());
		DataValidateUtils.registerRule(ID_CARD, new IdCardRule());
		DataValidateUtils.registerRule(REGEX, new RegexRule());
		DataValidateUtils.registerRule(NOT_EQUAL_TO, new NotEqualToRule());
		DataValidateUtils.registerRule(MAX, new MaxRule());
		DataValidateUtils.registerRule(IN, new InRule());
		DataValidateUtils.registerRule(NOT_A_FUTURE, new NotAFutureRule());
		DataValidateUtils.registerRule(BEFORE, new BeforeRule());
		DataValidateUtils.registerRule(BEFORE_OR_EQUAL, new BeforeOrEqualRule());
		DataValidateUtils.registerRule(AFTER, new AfterRule());
		DataValidateUtils.registerRule(AFTER_OR_EQUAL, new AfterOrEqualRule());
		DataValidateUtils.registerRule(URI, new UriRule());
		DataValidateUtils.registerRule(URL, new UrlRule());
		DataValidateUtils.registerRule(FILENAME, new FilenameRule());
		DataValidateUtils.registerRule(XSS, new XSSRule());
		DataValidateUtils.registerRule(EMAIL, new EmailRule());
		DataValidateUtils.registerRule(NOT_NOW, new NotNowRule());
	}
	
	/**
	 * 检测是否为无参校验规则
	 * @param word
	 * @return
	 */
	public static boolean isNoParamsRule(String word) {
		return NO_PARAMS_RULE.indexOf(word)>-1;
	}
}