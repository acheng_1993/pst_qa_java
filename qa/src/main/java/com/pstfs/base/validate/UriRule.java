package com.pstfs.base.validate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 验证是否为标准的uri，支持传入数组批量校验
 * @author jiajian
 */
public class UriRule implements IDataValidRule {

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (data == null) {
			return true;
		}
		if(data instanceof String) {
			if (StringUtils.isEmpty((String)data)) {
				return true;
			}
			try {
				new URI(String.valueOf(data));
				return true;
			} catch (URISyntaxException e) {
				return false;
			}
		} else if(data instanceof Iterable) {
			Collection it = (Collection)data;
			if (it.isEmpty()) {
				return true;
			}
			for(Object uri:it) {
				try {
					new URI(String.valueOf(uri));
				} catch (URISyntaxException e) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}