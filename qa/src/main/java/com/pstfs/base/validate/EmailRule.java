package com.pstfs.base.validate;
import java.util.regex.Pattern;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 邮箱格式校验
 * @author jiajian
 */
public class EmailRule implements IDataValidRule{

	private static final Pattern PATTERN = Pattern.compile("^[A-Za-z0-9\\u4e00-\\u9fa5.-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$");

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (StringUtils.isEmpty((String)data)) {
			return true;
		}
		return PATTERN.matcher((CharSequence)data).matches();
	}

}