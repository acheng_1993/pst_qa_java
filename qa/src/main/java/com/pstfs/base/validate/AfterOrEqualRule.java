package com.pstfs.base.validate;
import java.util.Date;
import java.util.Map;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;
import cn.hutool.core.util.ReflectUtil;

/**
 * 针对日期值，日期值必须在指定的字段之后或相同
 * @author jiajian
 */
public class AfterOrEqualRule implements IDataValidRule {

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if(data == null) {
			return true;
		}
		String fieldName = (String)params[0];
		Date afterDate = null;
		if (entity instanceof Map) {
			Map<String,Object> map = (Map<String,Object>)entity;
			afterDate = (Date)map.get(fieldName);
		} else {
			afterDate = (Date)ReflectUtil.invoke(entity, "get"+fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1));
		}
		if (afterDate == null) {
			return true;
		}
		long time = ((Date)data).getTime();
		long afterTime = afterDate.getTime();
		return time >= afterTime;
	}
}