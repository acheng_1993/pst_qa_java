package com.pstfs.base.validate;
import java.util.regex.Pattern;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;
/**
 * 正则校验
 * @author Administrator
 */
public class RegexRule implements IDataValidRule{

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (StringUtils.isEmpty((String)data)) {
			return true;
		}
		return Pattern.matches((String)params[0], (CharSequence)data);
	}

}
