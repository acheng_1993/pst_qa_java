package com.pstfs.base.validate;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;
/**
 * 文本内容最大长度校验
 * @author jiajian
 */
public class MaxRule implements IDataValidRule {

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (data == null) {
			return true;
		}
		Integer max = (Integer)params[0];
		String data2 = String.valueOf(data);
		return data2.length()<=max;
	}
}