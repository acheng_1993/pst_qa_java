package com.pstfs.base.validate;
import java.util.Date;

import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;
/**
 * 不能为将来值的校验
 * @author jiajian
 */
public class NotAFutureRule implements IDataValidRule {

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if(data == null) {
			return true;
		}
		long time = ((Date)data).getTime();
		return time < System.currentTimeMillis();
	}

}
