package com.pstfs.base.validate;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 数字范围校验
 * @author Administrator
 */
public class RangeRule implements IDataValidRule {

	@Override
	public boolean valid(Object entity, Object data, Object[] params) {
		if (data == null) {
			return true;
		}
		Number[] rangeParams = new Number[params.length];
		rangeParams[0] = (Number)params[0];
		rangeParams[1] = (Number)params[1];
		Comparable<Number> data2 = (Comparable<Number>)data;
		if(data2.compareTo(rangeParams[0])<0 || data2.compareTo(rangeParams[1])>0) {			
			return false;
		} else {
			return true;
		}	
	}
}
