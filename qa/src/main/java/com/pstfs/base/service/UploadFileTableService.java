package com.pstfs.base.service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.UploadFileTableEntity;
import com.pstfs.base.mapper.UploadFileTableMapper;

@Service("uploadFileTableService")
public class UploadFileTableService extends ServiceImpl<UploadFileTableMapper, UploadFileTableEntity> {

	
}