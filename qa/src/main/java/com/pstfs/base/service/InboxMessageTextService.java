package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.InboxEmailMessageEntity;
import com.pstfs.base.entity.InboxMessageEntity;
import com.pstfs.base.entity.InboxMessageTextEntity;
import com.pstfs.base.entity.UserEntity;
import com.pstfs.base.envm.MessageType;
import com.pstfs.base.envm.SendedEmail;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.mapper.InboxMessageTextMapper;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.validate.DataValidateFactory;
import com.pstfs.base.validate.DataValidateUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

import cn.hutool.http.HtmlUtil;

@Service("inboxMessageTextService")
public class InboxMessageTextService extends BaseService<InboxMessageTextMapper, InboxMessageTextEntity> {

	@Autowired
	private UserService userService;

	@Autowired
	private InboxMessageService inboxMessageService;
	
	@Autowired
	private InboxEmailMessageService inboxEmailMessageService;
	
	/**
	 * 批量创建站内信（直接发送到邮箱，不会在站内信表中发送）
	 * @param inboxEmailMessageList
	 * @param receiverUsernameList
	 * @return
	 */
	@Transactional
	public boolean saveBatchForEmail(List<InboxMessageTextEntity> inboxMessageTextList,String... receiverEmailList) {
		I18nUtils i1 = i;
		for (int i=0,len_i = inboxMessageTextList.size();i<len_i;i++) {
			InboxMessageTextEntity inboxMessageText = inboxMessageTextList.get(i);
			DataValidateUtils.validate(
			MapUtils.hms(
				"title.max", i1.t("InboxMessageTextService.validate.title.max"),//标题不能超过{0}个字符！
				"content.require", i1.t("InboxMessageTextService.validate.content.require"),//内容不能为空！
				"content.max", i1.t("InboxMessageTextService.validate.content.max"),//内容长度不能超过16MB！
				"creatorUsername.require",i1.t("InboxMessageTextService.validate.creatorUsername.require"),//创建人不能为空！
				"creatorUsername.max", i1.t("InboxMessageTextService.validate.creatorUsername.max"),//创建人长度不能超过{0}个字符！
				"messageType.require", i1.t("InboxMessageTextService.validate.messageType.require"),//消息类型不能为空！
				"messageType.in", i1.t("InboxMessageTextService.validate.messageType.in"),//消息类型异常！
				"sendEmail.require", i1.t("InboxMessageTextService.validate.sendEmail.require"),//消息是否发送到邮箱不能为空！
				"sendEmail.in",i1.t("InboxMessageTextService.validate.sendEmail.in")//消息是否发送到邮箱格式错误！
			),
			inboxMessageText,
				"title","require|max:128",
				"content","require|max:16777215",
				"creatorUsername","require|max:25",
				"messageType","require|in:"+enumVals(MessageType.values()),
				"sendEmail","require|in:"+enumVals(SendedEmail.values())
			);
//			if (inboxMessageText.getMessageType() == MessageType.html.getV() 
//					&& !DataValidateUtils.useRule(DataValidateFactory.XSS).valid(null, inboxMessageText.getContent(), null)) {
//				throw new RRException(i1.t("InboxMessageTextService.saveBatch.xss"));//消息内容中发现疑似 XSS 攻击，保存失败！
//			}
		}
		List<InboxMessageTextEntity> saveList = new ArrayList<>();
		for (int i=0,len_i = inboxMessageTextList.size();i<len_i;i++) {
			InboxMessageTextEntity inboxMessageText = inboxMessageTextList.get(i);
			InboxMessageTextEntity saveEntity = new InboxMessageTextEntity();
			saveEntity.setContent(inboxMessageText.getContent());
			saveEntity.setCreatorUsername(inboxMessageText.getCreatorUsername());
			saveEntity.setMessageType(inboxMessageText.getMessageType());
			saveEntity.setTitle(inboxMessageText.getTitle());
			saveEntity.setSendEmail(inboxMessageText.getSendEmail());
			String content = inboxMessageText.getContent();
			//提取内容的前 20 个字生成预览文本，如果是 html 则需要去除标签后提取前 20 个字。
			if (inboxMessageText.getMessageType() == MessageType.html.getV()) {
				content = HtmlUtil.cleanHtmlTag(content);
			}
			if (StringUtils.isNotEmpty(content)) {
				saveEntity.setSimpleContent(content.substring(0, Math.min(40, content.length())));
			}
			saveList.add(saveEntity);
		}
		boolean ret = saveBatch(saveList);
		//如果没有指定邮箱则报错
		if(receiverEmailList == null || receiverEmailList.length == 0) {
			throw new RRException(i.t("common.commitException")); //提交的数据异常，保存失败！
		} else {
			List<InboxEmailMessageEntity> inboxEmailMessageList = new ArrayList<>();
			for (int h=0,len_h = saveList.size();h<len_h;h++) {
				InboxMessageTextEntity inboxMessageText = saveList.get(h);
				for (int i=0,len_i = receiverEmailList.length;i<len_i;i++) {
					InboxEmailMessageEntity inboxEmailMessage = inboxEmailMessageList.get(i);
					inboxEmailMessage.setInboxMessageTextId(inboxMessageText.getId());
					inboxEmailMessage.setSendedEmail(SendedEmail.是.getV());
					inboxEmailMessage.setReceiverEmail(receiverEmailList[i]);
					inboxEmailMessageList.add(inboxEmailMessage);
				}
			}
			inboxEmailMessageService.saveBatch(inboxEmailMessageList);
		}
		return ret;
	}

	/**
	 * 批量创建站内信
	 * @param inboxMessageTextList
	 * @param receiverUsernameList
	 * @return
	 */
	@Transactional
	public boolean saveBatch(List<InboxMessageTextEntity> inboxMessageTextList,String... receiverUsernameList) {
		I18nUtils i1 = i;
		for (int i=0,len_i = inboxMessageTextList.size();i<len_i;i++) {
			InboxMessageTextEntity inboxMessageText = inboxMessageTextList.get(i);
			DataValidateUtils.validate(
			MapUtils.hms(
				"title.max", i1.t("InboxMessageTextService.validate.title.max"),//标题不能超过{0}个字符！
				"content.require", i1.t("InboxMessageTextService.validate.content.require"),//内容不能为空！
				"content.max", i1.t("InboxMessageTextService.validate.content.max"),//内容长度不能超过16MB！
				"creatorUsername.require",i1.t("InboxMessageTextService.validate.creatorUsername.require"),//创建人不能为空！
				"creatorUsername.max", i1.t("InboxMessageTextService.validate.creatorUsername.max"),//创建人长度不能超过{0}个字符！
				"messageType.require", i1.t("InboxMessageTextService.validate.messageType.require"),//消息类型不能为空！
				"messageType.in", i1.t("InboxMessageTextService.validate.messageType.in"),//消息类型异常！
				"sendEmail.require", i1.t("InboxMessageTextService.validate.sendEmail.require"),//消息是否发送到邮箱不能为空！
				"sendEmail.in",i1.t("InboxMessageTextService.validate.sendEmail.in")//消息是否发送到邮箱格式错误！
			),
			inboxMessageText,
				"title","require|max:128",
				"content","require|max:16777215",
				"creatorUsername","require|max:25",
				"messageType","require|in:"+enumVals(MessageType.values()),
				"sendEmail","require|in:"+enumVals(SendedEmail.values())
			);
//			if (inboxMessageText.getMessageType() == MessageType.html.getV() 
//					&& !DataValidateUtils.useRule(DataValidateFactory.XSS).valid(null, inboxMessageText.getContent(), null)) {
//				throw new RRException(i1.t("InboxMessageTextService.saveBatch.xss"));//消息内容中发现疑似 XSS 攻击，保存失败！
//			}
		}
		List<InboxMessageTextEntity> saveList = new ArrayList<>();
		for (int i=0,len_i = inboxMessageTextList.size();i<len_i;i++) {
			InboxMessageTextEntity inboxMessageText = inboxMessageTextList.get(i);
			InboxMessageTextEntity saveEntity = new InboxMessageTextEntity();
			saveEntity.setContent(inboxMessageText.getContent());
			saveEntity.setCreatorUsername(inboxMessageText.getCreatorUsername());
			saveEntity.setMessageType(inboxMessageText.getMessageType());
			saveEntity.setTitle(inboxMessageText.getTitle());
			saveEntity.setSendEmail(inboxMessageText.getSendEmail());
			String content = inboxMessageText.getContent();
			//提取内容的前 20 个字生成预览文本，如果是 html 则需要去除标签后提取前 20 个字。
			if (inboxMessageText.getMessageType() == MessageType.html.getV()) {
				content = HtmlUtil.cleanHtmlTag(content);
			}
			if (StringUtils.isNotEmpty(content)) {
				saveEntity.setSimpleContent(content.substring(0, Math.min(40, content.length())));
			}
			saveList.add(saveEntity);
		}
		boolean ret = saveBatch(saveList);
		List<InboxMessageEntity> inboxMessageList = new ArrayList<>();
		//不指定用户则全员发送
		if(receiverUsernameList==null || receiverUsernameList.length == 0) {
			QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.select("username");
			List<UserEntity> userList = userService.list(queryWrapper);
			for (int h=0,len_h = saveList.size();h<len_h;h++) {
				InboxMessageTextEntity saveEntity = saveList.get(h);
				Integer inboxMessageTextId = saveEntity.getId();
				int sendedEmail = -1;
				//对于需要发送到邮箱的消息，在每个接收者的已发送记录标记为：未发送
				if(saveEntity.getSendEmail() == SendedEmail.是.getV()) {
					sendedEmail = SendedEmail.否.getV();
				} else {
					//对于不需要发送到邮箱的消息，在每个接收者的已发送记录标记为：不需要发送
					sendedEmail = SendedEmail.不需要发送.getV();
				}
				for (int i=0,len_i=userList.size();i<len_i;i++) {
					InboxMessageEntity inboxMessageEntity = new InboxMessageEntity();
					inboxMessageEntity.setInboxMessageTextId(inboxMessageTextId);
					inboxMessageEntity.setReceiverUsername(userList.get(i).getUsername());
					inboxMessageEntity.setSendedEmail(sendedEmail);
					inboxMessageList.add(inboxMessageEntity);
				}
			}
		//指定了用户则不全员发送
		} else {
			for (int h=0,len_h = saveList.size();h<len_h;h++) {
				InboxMessageTextEntity saveEntity = saveList.get(h);
				Integer inboxMessageTextId = saveEntity.getId();
				int sendedEmail = -1;
				//对于需要发送到邮箱的消息，在每个接收者的已发送记录标记为：未发送
				if(saveEntity.getSendEmail() == SendedEmail.是.getV()) {
					sendedEmail = SendedEmail.否.getV();
				} else {
					//对于不需要发送到邮箱的消息，在每个接收者的已发送记录标记为：不需要发送
					sendedEmail = SendedEmail.不需要发送.getV();
				}
				for (int i=0,len_i = receiverUsernameList.length;i < len_i;i++) {
					InboxMessageEntity inboxMessageEntity = new InboxMessageEntity();
					inboxMessageEntity.setInboxMessageTextId(inboxMessageTextId);
					inboxMessageEntity.setReceiverUsername(receiverUsernameList[i]);
					inboxMessageEntity.setSendedEmail(sendedEmail);
					inboxMessageList.add(inboxMessageEntity);
				}
			}
		}
		inboxMessageService.saveBatch(inboxMessageList);
		return ret;
	}
	
	/**
	 * 创建站内信(直接发送到邮箱, 不在站内推送)
	 * @param title
	 * @param content
	 * @param creatorUsername
	 * @param messageType
	 * @param receiverEmailList
	 * @return
	 */
	@Transactional
	public boolean saveForEmail(String title,String content,String creatorUsername,Integer messageType,String... receiverEmailList) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"title.max",i.t("InboxMessageTextService.validate.title.max"),//标题不能超过{0}个字符！
			"content.require", i.t("InboxMessageTextService.validate.content.require"),//内容不能为空！
			"content.max", i.t("InboxMessageTextService.validate.content.max"),//内容长度不能超过16MB！
			"creatorUsername.require", i.t("InboxMessageTextService.validate.creatorUsername.require"),//创建人不能为空！
			"creatorUsername.max", i.t("InboxMessageTextService.validate.creatorUsername.max"),//创建人长度不能超过{0}个字符！
			"messageType.require", i.t("InboxMessageTextService.validate.messageType.require"),//消息类型不能为空！
			"messageType.in", i.t("InboxMessageTextService.validate.messageType.in") //消息类型异常！
		),
		MapUtils.hmo(
			"title",title,
			"content",content,
			"creatorUsername",creatorUsername,
			"messageType",messageType
		),
			"title","require|max:128",
			"content","require|max:16777215",
			"creatorUsername","require|max:25",
			"messageType","require|in:"+enumVals(MessageType.values())
		);
//		if (messageType == MessageType.html.getV() && !DataValidateUtils.useRule(DataValidateFactory.XSS).valid(null, content, null)) {
//			throw new RRException(i.t("InboxMessageTextService.saveBatch.xss"));//消息内容中发现疑似 XSS 攻击，保存失败！
//		}
		IDataValidRule emailRule = DataValidateUtils.useRule(DataValidateFactory.EMAIL);
		I18nUtils i_ = i;
		for (int i=0,len_i = receiverEmailList.length;i<len_i;i++) {
			if(!emailRule.valid(null, receiverEmailList[i], null)) {
				throw new RRException(i_.t("InboxMessageTextService.saveBatch.email"));//邮箱地址格式错误
			}
		}
		InboxMessageTextEntity inboxMessageTextEntity = new InboxMessageTextEntity();
		inboxMessageTextEntity.setContent(content);
		inboxMessageTextEntity.setCreatorUsername(creatorUsername);
		inboxMessageTextEntity.setMessageType(messageType);
		inboxMessageTextEntity.setTitle(title);
		inboxMessageTextEntity.setSendEmail(SendedEmail.是.getV());
		//提取内容的前 20 个字生成预览文本，如果是 html 则需要去除标签后提取前 20 个字。
		if (messageType == MessageType.html.getV()) {
			content = HtmlUtil.cleanHtmlTag(content);
		}
		if(StringUtils.isNotEmpty(content)) {
			inboxMessageTextEntity.setSimpleContent(content.substring(0, Math.min(40, content.length())));
		}
		boolean ret = save(inboxMessageTextEntity);
		//如果没有指定邮箱则报错
		if(receiverEmailList == null || receiverEmailList.length == 0) {
			throw new RRException(i.t("common.commitException")); //提交的数据异常，保存失败！
		} else {
			List<InboxEmailMessageEntity> inboxEmailMessageList = new ArrayList<>();
			for (int i=0,len_i = receiverEmailList.length;i < len_i;i++) {
				InboxEmailMessageEntity inboxEmailMessage = new InboxEmailMessageEntity();
				inboxEmailMessage.setInboxMessageTextId(inboxMessageTextEntity.getId());
				inboxEmailMessage.setReceiverEmail(receiverEmailList[i]);
				inboxEmailMessage.setSendedEmail(SendedEmail.否.getV());
				inboxEmailMessageList.add(inboxEmailMessage);
			}
			inboxEmailMessageService.saveBatch(inboxEmailMessageList);
		}
		return ret;
	}

	/**
	 * 创建站内信
	 * @param title
	 * @param content
	 * @param creatorUsername
	 * @param messageType
	 * @param receiverUsernameList
	 * @return
	 */
	@Transactional
	public boolean save(String title,String content,String creatorUsername,Integer messageType,Integer sendEmail,String... receiverUsernameList) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"title.max",i.t("InboxMessageTextService.validate.title.max"),//标题不能超过{0}个字符！
			"content.require", i.t("InboxMessageTextService.validate.content.require"),//内容不能为空！
			"content.max", i.t("InboxMessageTextService.validate.content.max"),//内容长度不能超过16MB！
			"creatorUsername.require", i.t("InboxMessageTextService.validate.creatorUsername.require"),//创建人不能为空！
			"creatorUsername.max", i.t("InboxMessageTextService.validate.creatorUsername.max"),//创建人长度不能超过{0}个字符！
			"messageType.require", i.t("InboxMessageTextService.validate.messageType.require"),//消息类型不能为空！
			"messageType.in", i.t("InboxMessageTextService.validate.messageType.in"),//消息类型异常！
			"sendEmail.require",i.t("InboxMessageTextService.validate.sendEmail.require"),//消息是否发送到邮箱不能为空！
			"sendEmail.in",i.t("InboxMessageTextService.validate.sendEmail.in")//消息是否发送到邮箱格式错误！
		),
		MapUtils.hmo(
			"title",title,
			"content",content,
			"creatorUsername",creatorUsername,
			"messageType",messageType,
			"sendEmail", sendEmail,
			"receiverUsernameList",receiverUsernameList
		),
			"title","require|max:128",
			"content","require|max:16777215",
			"creatorUsername","require|max:25",
			"messageType","require|in:"+enumVals(MessageType.values()),
			"sendEmail","require|in:"+enumVals(SendedEmail.values())
		);
//		if (messageType == MessageType.html.getV() && !DataValidateUtils.useRule(DataValidateFactory.XSS).valid(null, content, null)) {
//			throw new RRException(i.t("InboxMessageTextService.saveBatch.xss"));//消息内容中发现疑似 XSS 攻击，保存失败！
//		}
		InboxMessageTextEntity inboxMessageTextEntity = new InboxMessageTextEntity();
		inboxMessageTextEntity.setContent(content);
		inboxMessageTextEntity.setCreatorUsername(creatorUsername);
		inboxMessageTextEntity.setMessageType(messageType);
		inboxMessageTextEntity.setTitle(title);
		inboxMessageTextEntity.setSendEmail(sendEmail);
		//提取内容的前 20 个字生成预览文本，如果是 html 则需要去除标签后提取前 20 个字。
		if (messageType == MessageType.html.getV()) {
			content = HtmlUtil.cleanHtmlTag(content);
		}
		if(StringUtils.isNotEmpty(content)) {
			inboxMessageTextEntity.setSimpleContent(content.substring(0, Math.min(40, content.length())));
		}
		boolean ret = save(inboxMessageTextEntity);
		List<InboxMessageEntity> inboxMessageList = new ArrayList<>();
		
		int sendedEmail = -1;
		//对于需要发送到邮箱的消息，在每个接收者的已发送记录标记为：未发送
		if(inboxMessageTextEntity.getSendEmail() == SendedEmail.是.getV()) {
			sendedEmail = SendedEmail.否.getV();
		} else {
			//对于不需要发送到邮箱的消息，在每个接收者的已发送记录标记为：不需要发送
			sendedEmail = SendedEmail.不需要发送.getV();
		}
		
		//不指定用户则全员发送
		if(receiverUsernameList==null || receiverUsernameList.length == 0) {
			QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.select("username");
			List<UserEntity> userList = userService.list(queryWrapper);
			for (int i=0,len_i=userList.size();i<len_i;i++) {
				InboxMessageEntity inboxMessageEntity = new InboxMessageEntity();
				inboxMessageEntity.setInboxMessageTextId(inboxMessageTextEntity.getId());
				inboxMessageEntity.setReceiverUsername(userList.get(i).getUsername());
				inboxMessageEntity.setSendedEmail(sendedEmail);
				inboxMessageList.add(inboxMessageEntity);
			}
		//指定了用户则不全员发送
		} else {
			for (int i=0,len_i = receiverUsernameList.length;i < len_i;i++) {
				InboxMessageEntity inboxMessageEntity = new InboxMessageEntity();
				inboxMessageEntity.setInboxMessageTextId(inboxMessageTextEntity.getId());
				inboxMessageEntity.setReceiverUsername(receiverUsernameList[i]);
				inboxMessageEntity.setSendedEmail(sendedEmail);
				inboxMessageList.add(inboxMessageEntity);
			}
		}
		inboxMessageService.saveBatch(inboxMessageList);
		return ret;
	}
}