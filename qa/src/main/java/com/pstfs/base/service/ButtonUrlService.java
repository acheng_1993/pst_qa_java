package com.pstfs.base.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.ButtonUrlEntity;
import com.pstfs.base.mapper.ButtonUrlMapper;

@Service("buttonUrlService")
public class ButtonUrlService extends ServiceImpl<ButtonUrlMapper, ButtonUrlEntity> {

}