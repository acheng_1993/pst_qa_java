package com.pstfs.base.service;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.MstItmAllEntity;
import com.pstfs.base.mapper.MstItmAllMapper;

@DS("new_mes")
@Service("mstItmAllService")
public class MstItmAllService extends ServiceImpl<MstItmAllMapper, MstItmAllEntity> {

	/**
	 * 获取机种品番
	 * @param params
	 * @param field
	 * @param limit
	 * @return
	 */
	public List<MstItmAllEntity> list(Map<String,Object> params,String field,Integer limit) {
		QueryWrapper<MstItmAllEntity> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			List<Integer> itmTypeCodeList = (List<Integer>)params.get("itmTypeCodeList");
			if(itmTypeCodeList!=null && !itmTypeCodeList.isEmpty()) {
				queryWrapper.in("itm_type_code", itmTypeCodeList);
			}
		}
		queryWrapper.select(field);
		if (limit>0) {
			queryWrapper.last(" limit "+limit);
		}
		return list(queryWrapper);
	}
}