package com.pstfs.base.service;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.GrievanceMgrEntity;
import com.pstfs.base.entity.MgmNoAllocationEntity;
import com.pstfs.base.envm.YesOrNo;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.mapper.MgmNoAllocationMapper;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.validate.DataValidateUtils;

@Service("mgmNoAllocationService")
public class MgmNoAllocationService extends ServiceImpl<MgmNoAllocationMapper, MgmNoAllocationEntity> {

	/**
	 * 校验管理No.的使用状态
	 * @param mgmNo
	 * @return -1:管理No.不存在，0:管理No.未使用，1:管理No.已使用
	 */
	public int checkMgmNoUsed(String mgmNo) {
		QueryWrapper<MgmNoAllocationEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("mgm_no", mgmNo);
		queryWrapper.select("used");
		MgmNoAllocationEntity mgmNoAllocation = getOne(queryWrapper);
		return mgmNoAllocation == null?-1:mgmNoAllocation.getUsed();
	}

	
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 创建一个新的管理No.
	 * @param type 管理No.类型
	 * @return
	 */
	public String createNewMgmNo(String creatorUsername, String type) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"type.require","管理No.类型不能为空",
			"type.in","管理No.类型格式错误"
		),
		MapUtils.hmo(
			"type", type
		),
			"type", "require|in:'F','M'"
		);
		LocalDate localDate = LocalDate.now();
		String yyMM = localDate.format(DateTimeFormatter.ofPattern("yyMM"));
		QueryWrapper<MgmNoAllocationEntity> queryWrapper1 = new QueryWrapper<>();
		queryWrapper1.eq("yy_mm", yyMM);
		queryWrapper1.eq("type", type);
		queryWrapper1.eq("creator_username", creatorUsername);
		queryWrapper1.select("max(create_time) create_time");

		QueryWrapper<MgmNoAllocationEntity> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.eq("yy_mm", yyMM);
		queryWrapper2.eq("type", type);
		queryWrapper2.select("max(mgm_no) mgm_no");
		synchronized (this) {
			MgmNoAllocationEntity createTimeEntity = getOne(queryWrapper1);
			//每个用户每隔 10 秒钟操作一次
			if ("pro".equals(springProfilesActive) && createTimeEntity!=null && System.currentTimeMillis() - createTimeEntity.getCreateTime().getTime() < 10*1000) {
				throw new RRException("操作过于频繁，请稍候重试！");
			}
			MgmNoAllocationEntity mgmNoEntity = getOne(queryWrapper2);
			int currMonthMaxNo = 0;
			if (mgmNoEntity != null) {
				String mgmNo = mgmNoEntity.getMgmNo();
				currMonthMaxNo = Integer.parseInt(mgmNo.substring(5));
			}
			if (currMonthMaxNo == 9999) {
				throw new RRException("当月可分配的管理No.已经用尽，请联系管理员！");
			} else {
				currMonthMaxNo++;
				MgmNoAllocationEntity mgmNoAllocationEntity = new MgmNoAllocationEntity();
				mgmNoAllocationEntity.setMgmNo(type + yyMM + String.format("%04d", currMonthMaxNo));
				mgmNoAllocationEntity.setType(type);
				mgmNoAllocationEntity.setUsed(YesOrNo.否.getV());
				mgmNoAllocationEntity.setYyMm(yyMM);
				mgmNoAllocationEntity.setCreatorUsername(creatorUsername);
				save(mgmNoAllocationEntity);
				return mgmNoAllocationEntity.getMgmNo();
			}
		}
	}
}