package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.ButtonEntity;
import com.pstfs.base.entity.MenuEntity;
import com.pstfs.base.entity.RoleButtonEntity;
import com.pstfs.base.entity.RoleEntity;
import com.pstfs.base.entity.RoleMenuEntity;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.extend.RoleExtend;
import com.pstfs.base.mapper.RoleMapper;
import com.pstfs.base.utils.CollectionSubOper;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.utils.SQLFilter;
import com.pstfs.base.validate.DataValidateUtils;

@Lazy
@Service("roleService")
public class RoleService extends BaseService<RoleMapper, RoleEntity> {

	@Autowired
	private MenuService menuService;
	
	@Autowired
	private ButtonService buttonService;
	
	@Autowired
	private RoleMenuService roleMenuService;
	
	@Autowired
	private RoleButtonService roleButtonService;

	@Lazy
	@Autowired
	private UserService userService;

	/**
	 * 克隆一份角色出来
	 * @param id
	 * @return
	 */
	@Transactional
	public boolean copy(Integer id) {
		RoleEntity roleFromCache = ROLE_ID_MAP.get(id);
		if(roleFromCache !=null) {
			HashSet<Integer> roleMenuIdSet = ROLE_MENU_ID_MAP.get(id);
			HashSet<Integer> roleButtonIdSet = ROLE_BUTTON_ID_MAP.get(id);
			String newRoleName = null;
			for(int i=1;;i++) {
				newRoleName = roleFromCache.getRoleName()+"("+i+")";
				if(!ROLE_NAME_MAP.containsKey(newRoleName)) {
					break;
				}
			}
			RoleEntity saveEntity = new RoleEntity();
			saveEntity.setRoleName(newRoleName);
			saveEntity.setRoleDesc(roleFromCache.getRoleDesc()+"(这是复制出来的角色)");
			saveEntity.setRoleIndex(getNextRoleIndex());
			boolean ret = save(saveEntity);
			if(roleMenuIdSet!=null) {
				List<RoleMenuEntity> roleMenuList = new ArrayList<>();
				for(Integer menuId:roleMenuIdSet) {
					RoleMenuEntity roleMenuEntity = new RoleMenuEntity();
					roleMenuEntity.setRoleId(saveEntity.getId());
					roleMenuEntity.setMenuId(menuId);
					roleMenuList.add(roleMenuEntity);
				}
				roleMenuService.saveBatch(roleMenuList);
			}
			if(roleButtonIdSet!=null) {
				List<RoleButtonEntity> roleButtonList = new ArrayList<>();
				for(Integer buttonId:roleButtonIdSet) {
					RoleButtonEntity roleButtonEntity = new RoleButtonEntity();
					roleButtonEntity.setRoleId(saveEntity.getId());
					roleButtonEntity.setButtonId(buttonId);
					roleButtonList.add(roleButtonEntity);
				}
				roleButtonService.saveBatch(roleButtonList);
			}
			if(ret) {
				saveCache(saveEntity.getId(), saveEntity.getRoleIndex(), saveEntity.getRoleName(), saveEntity.getRoleDesc(), roleMenuIdSet, roleButtonIdSet);
			}
			return ret;
		}
		return false;
	}
	
	/**
	 * 获取符合条件的角色数据
	 * @param params
	 * @return
	 */
	public List<RoleEntity> list(Map<String,Object> params,String field) {
		QueryWrapper<RoleEntity> queryWrapper = new QueryWrapper<>();
		if (params!=null) {
			Object userId = params.get("userId");
			if (userId != null && userId instanceof Number) {
				queryWrapper.inSql("id", "select role_id from user_role where user_id="+userId);
			}
			String username = (String)params.get("username");
			if(StringUtils.isNotEmpty(username)) {
				queryWrapper.inSql("id", "select role_id from user_role where user_id=(select id from user where username='"+SQLFilter.sqlInject(username)+"')");
			}
			List<Integer> userIdList = (List<Integer>)params.get("userIdList");
			if (userIdList != null && !userIdList.isEmpty()) {
				queryWrapper.in("id", userIdList);
			}
		}
		queryWrapper.select(field);
		return list(queryWrapper);
	}
	
	/**
	 * 获取符合条件的角色数据
	 * @param params
	 * @param field
	 * @return
	 */
	public List<RoleExtend> listExtend(Map<String,Object> params,String field) {
		QueryWrapper<RoleExtend> queryWrapper = new QueryWrapper<>();
		if (params!=null) {
			Object userId = params.get("userId");
			if (userId != null && userId instanceof Number) {
				queryWrapper.eq("ur.user_id", userId);
			}
			List<Integer> userIdList = (List<Integer>)params.get("userIdList");
			if (userIdList != null && !userIdList.isEmpty()) {
				queryWrapper.in("ur.user_id", userIdList);
			}
		}
		queryWrapper.select(field);
		if (StringUtils.isEmpty(queryWrapper.getCustomSqlSegment())) {
			queryWrapper.apply("0 = 0");
		}
		return baseMapper.listExtend1(queryWrapper);
	}
	
	/**
	 * 滚动式加载角色信息
	 * @param params
	 * @return
	 */
	public List<RoleEntity> scrollLoad(Map<String,Object> params) {
		QueryWrapper<RoleEntity> queryWrapper = new QueryWrapper<>();
		if(params!=null) {
			String roleNameLike = String.valueOf(params.getOrDefault("roleNameLike",""));
			if (StringUtils.isNotEmpty(roleNameLike)) {
				queryWrapper = queryWrapper.like("role_name", roleNameLike+"%");
			}
		}
		scrollLoadSplit(params, queryWrapper);
		return list(queryWrapper);
	}
	
	/**
	 * 删除角色，如果角色被使用，则删除失败
	 * @param id
	 * @return
	 */
	@Transactional
	public boolean removeById(Integer id) {
		UpdateWrapper<RoleEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.notExists("select role_id from user_role where role_id = role.id");
		boolean ret = remove(updateWrapper);
		if (ret) {
			UpdateWrapper<RoleButtonEntity> updateWrapper2 = new UpdateWrapper<>();
			updateWrapper2.eq("role_id", id);
			roleButtonService.remove(updateWrapper2);
			
			UpdateWrapper<RoleMenuEntity> updateWrapper3 = new UpdateWrapper<>();
			updateWrapper3.eq("role_id", id);
			roleMenuService.remove(updateWrapper3);
			//删除缓存
			removeCache(id);
		}
		return ret;
	}
	
	/**
	 * 更新角色
	 * @param id
	 * @param roleName 角色名称
	 * @param roleDesc 角色描述
	 * @param menuIdList 角色菜单
	 * @param buttonIdList 角色按钮
	 * @return
	 */
	@Transactional
	public boolean update(Integer id,String roleName,String roleDesc,List<Integer> menuIdList,List<Integer> buttonIdList) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"id.require",i.t("RoleService.validate.id.require"),//主键id不能为空！
			"roleName.require",i.t("RoleService.validate.roleName.require"),//角色名称不能为空！
			"roleName.size",i.t("RoleService.validate.roleName.size"),//角色名称长度必须在{0}-{1}之间！
			"roleDesc.max", i.t("RoleService.validate.roleDesc.max"),//角色描述长度不能超过{0}个字符！
			"menuIdList.require", i.t("RoleService.validate.menuIdList.require"),//菜单、表单不能为空
			"buttonIdList.require",i.t("RoleService.validate.buttonIdList.require")//按钮不能为空
		),
		MapUtils.hmo(
			"id",id,
			"roleName",roleName,
			"roleDesc",roleDesc,
			"menuIdList",menuIdList,
			"buttonIdList",buttonIdList
		),
			"id","require",
			"roleName","require|size:1,50",
			"roleDesc","max:255",
			"menuIdList","require",
			"buttonIdList","require"
		);
		HashSet<Integer> menuIdSet = new HashSet<>(menuIdList);
		QueryWrapper<MenuEntity> menuQueryWrapper = new QueryWrapper<>();
		menuQueryWrapper.in("id", menuIdSet);
		if(menuService.count(menuQueryWrapper)!=menuIdSet.size()) {
			throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！
		}
		HashSet<Integer> buttonIdSet = new HashSet<>(buttonIdList);
		QueryWrapper<ButtonEntity> buttonQueryWrapper = new QueryWrapper<>();
		buttonQueryWrapper.in("id", buttonIdSet);
		if(buttonService.count(buttonQueryWrapper)!=buttonIdSet.size()) {
			throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！
		}
		RoleEntity roleEntity = new RoleEntity();
		roleEntity.setId(id);
		roleEntity.setRoleDesc(roleDesc);
		roleEntity.setRoleName(roleName);
		boolean ret = updateById(roleEntity);
		
		List<RoleMenuEntity> roleMenuList = new ArrayList<>(menuIdSet.size());
		for(Integer menuId:menuIdSet) {
			RoleMenuEntity roleMenuEntity = new RoleMenuEntity();
			roleMenuEntity.setRoleId(id);
			roleMenuEntity.setMenuId(menuId);
			roleMenuList.add(roleMenuEntity);
		}
		List<RoleButtonEntity> roleButtonList = new ArrayList<>(buttonIdSet.size());
		for(Integer buttonId:buttonIdSet) {
			RoleButtonEntity roleButtonEntity = new RoleButtonEntity();
			roleButtonEntity.setRoleId(id);
			roleButtonEntity.setButtonId(buttonId);
			roleButtonList.add(roleButtonEntity);
		}
		
		QueryWrapper<RoleMenuEntity> roleMenuQueryWrapper = new QueryWrapper<>();
		roleMenuQueryWrapper.eq("role_id", id);
		List<RoleMenuEntity> roleMenuListFromDb = roleMenuService.list(roleMenuQueryWrapper);
		List<RoleMenuEntity> saveRoleMenuList = CollectionSubOper.sub(roleMenuList, roleMenuListFromDb, "getRoleId","getMenuId");
		List<RoleMenuEntity> delRoleMenuList = CollectionSubOper.sub(roleMenuListFromDb, roleMenuList, "getRoleId","getMenuId");
		List<Integer> delRoleMenuIdList = new ArrayList<>(delRoleMenuList.size());
		for(RoleMenuEntity roleMenuEntity:delRoleMenuList) {
			delRoleMenuIdList.add(roleMenuEntity.getId());
		}
		if (!saveRoleMenuList.isEmpty()) {
			roleMenuService.saveBatch(saveRoleMenuList);
		}
		if (!delRoleMenuIdList.isEmpty()) {
			roleMenuService.removeByIds(delRoleMenuIdList);
		}
		
		QueryWrapper<RoleButtonEntity> roleButtonQueryWrapper = new QueryWrapper<>();
		roleButtonQueryWrapper.eq("role_id", id);
		roleButtonQueryWrapper.select("role_id,button_id");
		List<RoleButtonEntity> roleButtonListFromDb = roleButtonService.list(roleButtonQueryWrapper);
		List<RoleButtonEntity> saveRoleButtonList = CollectionSubOper.sub(roleButtonList, roleButtonListFromDb, "getRoleId","getButtonId");
		List<RoleButtonEntity> delRoleButtonList = CollectionSubOper.sub(roleButtonListFromDb,roleButtonList,"getRoleId","getButtonId");
		List<Integer> delRoleButtonIdList = new ArrayList<>(delRoleButtonList.size());
		for(RoleButtonEntity roleButtonEntity:delRoleButtonList) {
			delRoleButtonIdList.add(roleButtonEntity.getId());
		}
		if (!saveRoleButtonList.isEmpty()) {
			roleButtonService.saveBatch(saveRoleButtonList);
		}
		if (!delRoleButtonIdList.isEmpty()) {
			roleButtonService.removeByIds(delRoleButtonIdList);
		}
		if (ret) {
			updateCache(id, roleName, roleDesc, menuIdSet, buttonIdSet);
		}
		return ret;
	}
	
	/**
	 * 新增角色
	 * @param roleName 角色名称
	 * @param roleDesc 角色描述
	 * @param menuIdList 角色菜单
	 * @param buttonIdList 角色按钮
	 * @return
	 */
	@Transactional
	public boolean save(String roleName,String roleDesc,List<Integer> menuIdList,List<Integer> buttonIdList) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"roleName.require", i.t("RoleService.validate.roleName.require"),//角色名称不能为空！
			"roleName.size", i.t("RoleService.validate.roleName.size"),//角色名称长度必须在{0}-{1}之间！
			"roleDesc.max", i.t("RoleService.validate.roleDesc.max"),//角色描述长度不能超过{0}个字符！
			"menuIdList.require",i.t("RoleService.validate.menuIdList.require"),//菜单、表单不能为空
			"buttonIdList.require",i.t("RoleService.validate.buttonIdList.require")//按钮不能为空
		),
		MapUtils.hmo(
			"roleName",roleName,
			"roleDesc",roleDesc,
			"menuIdList",menuIdList,
			"buttonIdList",buttonIdList
		),
			"roleName","require|size:1,50",
			"roleDesc","max:255",
			"menuIdList","require",
			"buttonIdList","require"
		);
		HashSet<Integer> menuIdSet = new HashSet<>(menuIdList);
		QueryWrapper<MenuEntity> menuQueryWrapper = new QueryWrapper<>();
		menuQueryWrapper.in("id", menuIdSet);
		if(menuService.count(menuQueryWrapper)!=menuIdSet.size()) {
			throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！
		}
		HashSet<Integer> buttonIdSet = new HashSet<>(buttonIdList);
		QueryWrapper<ButtonEntity> buttonQueryWrapper = new QueryWrapper<>();
		buttonQueryWrapper.in("id", buttonIdSet);
		if(buttonService.count(buttonQueryWrapper)!=buttonIdSet.size()) {
			throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！
		}
		RoleEntity roleEntity = new RoleEntity();
		boolean ret;
		synchronized (this) {
			roleEntity.setRoleName(roleName);
			roleEntity.setRoleDesc(roleDesc);
			roleEntity.setRoleIndex(getNextRoleIndex());
			ret = save(roleEntity);
		}
		List<RoleMenuEntity> saveRoleMenuList = new ArrayList<>(menuIdSet.size());
		for(Integer menuId:menuIdSet) {
			RoleMenuEntity roleMenuEntity = new RoleMenuEntity();
			roleMenuEntity.setMenuId(menuId);
			roleMenuEntity.setRoleId(roleEntity.getId());
			saveRoleMenuList.add(roleMenuEntity);
		}
		roleMenuService.saveBatch(saveRoleMenuList);
		List<RoleButtonEntity> saveRoleButtonList = new ArrayList<>(buttonIdSet.size());
		for(Integer buttonId:buttonIdSet) {
			RoleButtonEntity roleButtonEntity = new RoleButtonEntity();
			roleButtonEntity.setButtonId(buttonId);
			roleButtonEntity.setRoleId(roleEntity.getId());
			saveRoleButtonList.add(roleButtonEntity);
		}
		roleButtonService.saveBatch(saveRoleButtonList);
		
		if (ret) {
			saveCache(roleEntity.getId(), roleEntity.getRoleIndex(), roleName, roleDesc, menuIdSet, buttonIdSet);
		}
		return ret;
	}
	
	/**
	 * 获取下一个角色下标
	 * @return
	 */
	private synchronized Integer getNextRoleIndex() {
		QueryWrapper<RoleEntity> roleQueryWrapper = new QueryWrapper<>();
		roleQueryWrapper.select("role_index");
		List<RoleEntity> roleEntityList = list(roleQueryWrapper);
		int[] roleIndexArray = new int[roleEntityList.size()];
		for(int i=0,len_i=roleEntityList.size();i<len_i;i++) {
			roleIndexArray[i] = roleEntityList.get(i).getRoleIndex();
		}
		Arrays.sort(roleIndexArray);
		if(roleIndexArray[0] > 0) {
			return roleIndexArray[0]-1;
		}
		int i=1,len_i=roleIndexArray.length;
		for(;i<len_i;i++) {
			if(roleIndexArray[i] - roleIndexArray[i-1]>1) {
				return roleIndexArray[i-1]+1;
			}
		}
		return roleIndexArray[len_i-1]+1;
	}
	
	/**
	 * 根据角色的下标获取菜单 id 列表。
	 * @param roleIndexList
	 * @return
	 */
	public HashSet<Integer> getMenuIdListByRoleIndex(List<Integer> roleIndexList) {
		HashSet<Integer> ret = null;
		for(;;) {
			if(ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				ret = new HashSet<>();
				for (int i=0,len_i = roleIndexList.size();i<len_i;i++) {
					RoleEntity role = ROLE_IDX_MAP.get(roleIndexList.get(i));
					HashSet<Integer> menuIdSet = ROLE_MENU_ID_MAP.get(role.getId());
					ret.addAll(menuIdSet);
				}
				ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
		return ret;
	}
	
	/**
	 * 根据角色的下标获取按钮 id 列表。
	 * @param roleIndexList
	 * @return
	 */
	public HashSet<Integer> getButtonIdListByRoleIndex(List<Integer> roleIndexList){
		HashSet<Integer> ret = null;
		for(;;) {
			if (ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				ret = new HashSet<>();
				for (int i=0,len_i = roleIndexList.size();i<len_i;i++) {
					RoleEntity role = ROLE_IDX_MAP.get(roleIndexList.get(i));
					HashSet<Integer> buttonIdSet = ROLE_BUTTON_ID_MAP.get(role.getId());
					ret.addAll(buttonIdSet);
				}
				ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
		return ret;
	}
	
	/**
	 * 所有角色的映射表
	 */
	final static Map<Integer,RoleEntity> ROLE_ID_MAP = new HashMap<>();
	
	/**
	 * 角色名称映射表
	 */
	private final static Map<String,RoleEntity> ROLE_NAME_MAP = new HashMap<>();
	
	/**
	 * 所有角色权限位映射表
	 */
	private final static Map<Integer,RoleEntity> ROLE_IDX_MAP = new HashMap<>();
	
	/**
	 * 所有角色持有的url映射表
	 */
	public final static Map<Integer, byte[]> ROLE_IDX_URL_MAP = new HashMap<>();

	/**
	 * 角色id菜单id关联映射表
	 */
	final static Map<Integer,HashSet<Integer>> ROLE_MENU_ID_MAP = new HashMap<>();
	
	/**
	 * 角色id按钮id关联映射表
	 */
	final static Map<Integer,HashSet<Integer>> ROLE_BUTTON_ID_MAP = new HashMap<>();
	
	/**
	 * 自旋锁
	 */
	final static AtomicBoolean ROLE_MENU_BUTTON_LOCK = new AtomicBoolean(Boolean.FALSE);
	
	
	@PostConstruct
	public void initRoleList() {
		QueryWrapper<RoleEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("id,role_name,role_index,role_desc");
		List<RoleEntity> roleList = list(queryWrapper);
		
		for (int i=0,len_i = roleList.size();i<len_i;i++) {
			RoleEntity roleEntity = roleList.get(i);
			ROLE_ID_MAP.put(roleEntity.getId(), roleEntity);
			ROLE_IDX_MAP.put(roleEntity.getRoleIndex(), roleEntity);
			ROLE_NAME_MAP.put(roleEntity.getRoleName(), roleEntity);
		}
		
		QueryWrapper<RoleMenuEntity> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.select("role_id,menu_id");
		List<RoleMenuEntity> roleMenuList = roleMenuService.list(queryWrapper2);
		
		for (;!menuService.isInitFinish();) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Map<Integer, HashSet<Integer>> menuIdUrlMap = menuService.MENU_ID_URL_MAP;
		for(int i=0,len_i = roleMenuList.size();i<len_i;i++) {
			RoleMenuEntity roleMenuEntity = roleMenuList.get(i);
			HashSet<Integer> roleMenuIdSet = ROLE_MENU_ID_MAP.get(roleMenuEntity.getRoleId());
			if(roleMenuIdSet == null) {
				ROLE_MENU_ID_MAP.put(roleMenuEntity.getRoleId(), roleMenuIdSet = new HashSet<Integer>());
			}
			roleMenuIdSet.add(roleMenuEntity.getMenuId());
			RoleEntity roleEntity = ROLE_ID_MAP.get(roleMenuEntity.getRoleId());
			byte[] urlByte = ROLE_IDX_URL_MAP.get(roleEntity.getRoleIndex());
			if (urlByte == null) {
				ROLE_IDX_URL_MAP.put(roleEntity.getRoleIndex(), urlByte = new byte[125]);
			}
			HashSet<Integer> menuIdUrlSet = menuIdUrlMap.get(roleMenuEntity.getMenuId());
			if (menuIdUrlSet!=null) {
				for (Integer urlIdx:menuIdUrlSet) {
					int idx2 = (urlIdx + 1)%8; //获取余数位
					int idx1;
					if (idx2 == 0) { //没有余数位
						idx1 = (urlIdx + 1)/8 - 1; //获取下标
						urlByte[idx1] |= 1 << 7;
					} else { // 有余数位
						idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
						idx2 -= 1;
						urlByte[idx1] |= 1 << idx2;
					}
				}
			}
		}
		
		QueryWrapper<RoleButtonEntity> queryWrapper3 = new QueryWrapper<>();
		queryWrapper3.select("role_id,button_id");
		List<RoleButtonEntity> roleButtonList = roleButtonService.list(queryWrapper3);
		
		for(;!buttonService.isInitFinish();) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Map<Integer, HashSet<Integer>> buttonIdUrlMap = buttonService.BUTTON_ID_URL_MAP;
		
		for (int i=0,len_i = roleButtonList.size();i<len_i;i++) {
			RoleButtonEntity roleButtonEntity = roleButtonList.get(i);
			HashSet<Integer> roleButtonIdSet = ROLE_BUTTON_ID_MAP.get(roleButtonEntity.getRoleId());
			if (roleButtonIdSet == null) {
				ROLE_BUTTON_ID_MAP.put(roleButtonEntity.getRoleId(), roleButtonIdSet = new HashSet<Integer>());
			}
			roleButtonIdSet.add(roleButtonEntity.getButtonId());
			RoleEntity roleEntity = ROLE_ID_MAP.get(roleButtonEntity.getRoleId());
			byte[] urlByte = ROLE_IDX_URL_MAP.get(roleEntity.getRoleIndex());
			if (urlByte == null) {
				ROLE_IDX_URL_MAP.put(roleEntity.getRoleIndex(), urlByte = new byte[125]);
			}
			HashSet<Integer> buttonIdUrlSet = buttonIdUrlMap.get(roleButtonEntity.getButtonId());
			if (buttonIdUrlSet!=null) {
				for (Integer urlIdx:buttonIdUrlSet) {
					int idx2 = (urlIdx + 1)%8; //获取余数位
					int idx1;
					if (idx2 == 0) { //没有余数位
						idx1 = (urlIdx + 1)/8 - 1; //获取下标
						urlByte[idx1] |= 1 << 7;
					} else { // 有余数位
						idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
						idx2 -= 1;
						urlByte[idx1] |= 1 << idx2;
					}
				}
			}
		}
	}
	
	/**
	 * 删除缓存内容
	 * @param id
	 */
	private void removeCache(Integer id) {
		for(;;) {
			if(ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				ROLE_MENU_ID_MAP.remove(id);
				ROLE_BUTTON_ID_MAP.remove(id);
				RoleEntity roleEntity = ROLE_ID_MAP.remove(id);
				if (roleEntity!=null) {
					ROLE_IDX_URL_MAP.remove(roleEntity.getRoleIndex());
					ROLE_IDX_MAP.remove(roleEntity.getRoleIndex());
					ROLE_NAME_MAP.remove(roleEntity.getRoleDesc());
				}
				ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 新增缓存
	 * @param id
	 * @param roleIndex
	 * @param roleName
	 * @param roleDesc
	 * @param menuIdList
	 * @param buttonIdList
	 */
	private void saveCache(Integer id,Integer roleIndex, String roleName, String roleDesc, HashSet<Integer> menuIdSet,HashSet<Integer> buttonIdSet) {
		RoleEntity roleEntity = new RoleEntity();
		roleEntity.setId(id);
		roleEntity.setRoleIndex(roleIndex);
		roleEntity.setRoleName(roleName);
		roleEntity.setRoleDesc(roleDesc);
		for (;;) {
			if (ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				ROLE_MENU_ID_MAP.put(id,menuIdSet);
				ROLE_BUTTON_ID_MAP.put(id,buttonIdSet);
				ROLE_ID_MAP.put(id, roleEntity);
				ROLE_IDX_MAP.put(roleIndex, roleEntity);
				ROLE_NAME_MAP.put(roleName, roleEntity);
				byte[] urlByte = new byte[125];
				ROLE_IDX_URL_MAP.put(roleIndex, urlByte);
				if (menuIdSet != null) {
					Map<Integer,HashSet<Integer>> menuIdUrlMap = menuService.MENU_ID_URL_MAP;
					for (Integer menuId:menuIdSet) {
						HashSet<Integer> menuIdUrlSet = menuIdUrlMap.get(menuId);
						//部分菜单属于目录，没有关联 url
						if (menuIdUrlSet != null) {
							for (Integer urlIdx:menuIdUrlSet) {
								int idx2 = (urlIdx + 1)%8; //获取余数位
								int idx1;
								if (idx2 == 0) { //没有余数位
									idx1 = (urlIdx + 1)/8 - 1; //获取下标
									urlByte[idx1] |= 1 << 7;
								} else { // 有余数位
									idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
									idx2 -= 1;
									urlByte[idx1] |= 1 << idx2;
								}
							}
						}
					}
				}
				if (buttonIdSet != null) {
					Map<Integer,HashSet<Integer>> buttonIdUrlMap = buttonService.BUTTON_ID_URL_MAP;
					for (Integer buttonId:buttonIdSet) {
						HashSet<Integer> buttonIdUrlSet = buttonIdUrlMap.get(buttonId);
						for(Integer urlIdx:buttonIdUrlSet) {
							int idx2 = (urlIdx + 1)%8; //获取余数位
							int idx1;
							if (idx2 == 0) { //没有余数位
								idx1 = (urlIdx + 1)/8 - 1; //获取下标
								urlByte[idx1] |= 1 << 7;
							} else { // 有余数位
								idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
								idx2 -= 1;
								urlByte[idx1] |= 1 << idx2;
							}
						}
					}
				}
				ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 更新缓存内容
	 * @param id
	 * @param roleName
	 * @param roleDesc
	 * @param menuIdSet
	 * @param buttonIdSet
	 */
	public void updateCache(Integer id,String roleName,String roleDesc,HashSet<Integer> menuIdSet,HashSet<Integer> buttonIdSet) {
		for (;;) {
			if (ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				ROLE_MENU_ID_MAP.put(id,menuIdSet);
				ROLE_BUTTON_ID_MAP.put(id,buttonIdSet);
				RoleEntity roleEntity = ROLE_ID_MAP.get(id);
				if (roleEntity != null) {
					//更新角色名映射
					ROLE_NAME_MAP.remove(roleEntity.getRoleName());
					ROLE_NAME_MAP.put(roleName, roleEntity);
					roleEntity.setRoleName(roleName);
					roleEntity.setRoleDesc(roleDesc);
				}
				byte[] urlByte = new byte[125];
				ROLE_IDX_URL_MAP.put(roleEntity.getRoleIndex(), urlByte);
				if (menuIdSet!=null) {
					Map<Integer, HashSet<Integer>> menuIdUrlMap = menuService.MENU_ID_URL_MAP;
					for (Integer menuId:menuIdSet) {
						HashSet<Integer> menuIdUrlSet = menuIdUrlMap.get(menuId);
						//部分菜单属于目录，没有关联 url
						if(menuIdUrlSet != null) {
							for(Integer urlIdx:menuIdUrlSet) {
								int idx2 = (urlIdx + 1)%8; //获取余数位
								int idx1;
								if (idx2 == 0) { //没有余数位
									idx1 = (urlIdx + 1)/8 - 1; //获取下标
									urlByte[idx1] |= 1 << 7;
								} else { // 有余数位
									idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
									idx2 -= 1;
									urlByte[idx1] |= 1 << idx2;
								}
							}
						}
					}
				}
				if (buttonIdSet!=null) {
					Map<Integer,HashSet<Integer>> buttonIdUrlMap = buttonService.BUTTON_ID_URL_MAP;
					for (Integer buttonId:buttonIdSet) {
						HashSet<Integer> buttonIdUrlSet = buttonIdUrlMap.get(buttonId);
						for(Integer urlIdx:buttonIdUrlSet) {
							int idx2 = (urlIdx + 1)%8; //获取余数位
							int idx1;
							if (idx2 == 0) { //没有余数位
								idx1 = (urlIdx + 1)/8 - 1; //获取下标
								urlByte[idx1] |= 1 << 7;
							} else { // 有余数位
								idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
								idx2 -= 1;
								urlByte[idx1] |= 1 << idx2;
							}
						}
					}
				}
				ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
}