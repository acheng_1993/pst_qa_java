package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.RequestUrlEntity;
import com.pstfs.base.mapper.RequestUrlMapper;
import com.pstfs.base.oauth2.UrlMap;
import com.pstfs.base.utils.CollectionSubOper;

@Service("requestUrlService")
public class RequestUrlService extends ServiceImpl<RequestUrlMapper, RequestUrlEntity> {

	/**
	 * 获取请求url列表
	 * @param params
	 * @param field
	 * @return
	 */
	public List<RequestUrlEntity> list(Map<String,Object> params,String field) {
		QueryWrapper<RequestUrlEntity> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			Object buttonId = params.get("buttonId");
			if (buttonId != null && buttonId instanceof Number) {
				queryWrapper.inSql("id", "select url_id from button_url where button_id="+buttonId);
			}
			Object menuId = params.get("menuId");
			if(menuId != null && menuId instanceof Number) {
				queryWrapper.inSql("id", "select url_id from menu_url where menu_id="+menuId);
			}
		}
		queryWrapper.select(field);
		return list(queryWrapper);
	}

	/**
	 * 删除 url
	 * @return
	 */
	public boolean removeById(Integer id) {
		UpdateWrapper<RequestUrlEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id);
		updateWrapper.apply("not exists(select id from button_url where url_id=request_url.id)");
		updateWrapper.apply("not exists(select id from menu_url where url_id=request_url.id)");
		return remove(updateWrapper);
	}

	@Autowired
	private UrlMap urlMap;
	
	/**
	 * 批量保存 url,并返回在表中 url 对应的主键 id 实体。
	 * @param urlList
	 * @return
	 */
	public List<RequestUrlEntity> saveBatch(List<String> urlList) {
		List<RequestUrlEntity> requestUrlEntityList = new ArrayList<>();
		for (int i=0,len_i=urlList.size();i<len_i;i++) {
			RequestUrlEntity requestUrlEntity = new RequestUrlEntity();
			requestUrlEntity.setUrl(urlList.get(i));
			requestUrlEntityList.add(requestUrlEntity);
		}
		QueryWrapper<RequestUrlEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("url", urlList);
		queryWrapper.select("id,url_index,url");
		List<RequestUrlEntity> requestUrlEntityListFromDb = list(queryWrapper);
		List<RequestUrlEntity> saveRequestUrlList = CollectionSubOper.sub(requestUrlEntityList, requestUrlEntityListFromDb, "getUrl");
		if (!saveRequestUrlList.isEmpty()) {
			synchronized (this) {
				List<Integer> nextIndexList = getNextIndexList(saveRequestUrlList.size());
				for(int i=0,len_i=nextIndexList.size();i<len_i;i++) {
					saveRequestUrlList.get(i).setUrlIndex(nextIndexList.get(i));
				}
				saveBatch(saveRequestUrlList);
			}
			requestUrlEntityListFromDb.addAll(saveRequestUrlList);
		}
		urlMap.putAll(requestUrlEntityListFromDb); //更新权限校验的url列表
		return requestUrlEntityListFromDb;
	}
	
	/**
	 * 把所有的 url 加入到 url 映射表中
	 */
	@PostConstruct
	private void initUrlMap() {
		QueryWrapper<RequestUrlEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("id,url,url_index");
		urlMap.putAll(list(queryWrapper));
	}
	
	/**
	 * 获取下一个下标
	 * @return
	 */
	private synchronized List<Integer> getNextIndexList(int count) {
		QueryWrapper<RequestUrlEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("url_index");
		List<RequestUrlEntity> requestUrlList = list(queryWrapper);
		List<Integer> ret = new ArrayList<>(count);
		if (requestUrlList.isEmpty()) {
			for (int i=0;i<count;i++) {
				ret.add(i);
			}
			return ret;
		} else {
			List<Integer> urlIndexList = new ArrayList<>();
			for (int i=0,len_i = requestUrlList.size();i < len_i;i++) {
				urlIndexList.add(requestUrlList.get(i).getUrlIndex());
			}
			Collections.sort(urlIndexList);
			int urlIndex = urlIndexList.get(0);
			for (int i=0;i<urlIndex && ret.size() < count;i++) {
				ret.add(i);	
			}
			for (int i=1,len_i = urlIndexList.size();i<len_i && ret.size() < count;i++) {
				int currUrlIndex = urlIndexList.get(i);
				int prevUrlIndex = urlIndexList.get(i-1);
				for(int j=prevUrlIndex+1;j<currUrlIndex && ret.size() < count;j++) {
					ret.add(j);
				}
			}
			for (int i=urlIndexList.size();ret.size() < count;i++) {
				ret.add(i);
			}
			return ret;
		}
	}
}