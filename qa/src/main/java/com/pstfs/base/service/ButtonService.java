package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.ButtonEntity;
import com.pstfs.base.entity.ButtonUrlEntity;
import com.pstfs.base.entity.MenuEntity;
import com.pstfs.base.entity.RequestUrlEntity;
import com.pstfs.base.entity.RoleEntity;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.extend.ButtonExtend;
import com.pstfs.base.extend.RequestUrlExtend;
import com.pstfs.base.mapper.ButtonMapper;
import com.pstfs.base.mapper.RequestUrlMapper;
import com.pstfs.base.utils.CollectionSubOper;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.validate.DataValidateUtils;

import cn.hutool.core.collection.CollUtil;

@Service("buttonService")
public class ButtonService extends BaseService<ButtonMapper, ButtonEntity> {

	@Lazy
	@Autowired
	private UserService userService;
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private RequestUrlService requestUrlService;
	
	@Lazy
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private ButtonUrlService buttonUrlService;
	
	/**
	 * 根据角色下标获取对应的按钮列表
	 * @param roleIndexList
	 * @return
	 */
	public List<ButtonEntity> getButtonListByRoleIndex(List<Integer> roleIndexList) {
		List<ButtonEntity> ret = new ArrayList<>();
		HashSet<Integer> buttonIdSet = roleService.getButtonIdListByRoleIndex(roleIndexList);
		for(;;) {
			if(RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				for(Integer buttonId:buttonIdSet) {
					ButtonEntity buttonEntity = BUTTON_ID_MAP.get(buttonId);
					if(buttonEntity != null) {
						ButtonEntity newEntity = new ButtonEntity();
						newEntity.setId(buttonEntity.getId());
						newEntity.setBtnName(buttonEntity.getBtnName());
						newEntity.setMenuId(buttonEntity.getMenuId());
						newEntity.setCreateTime(buttonEntity.getCreateTime());
						newEntity.setUpdateTime(buttonEntity.getUpdateTime());
						newEntity.setBtnIndex(buttonEntity.getBtnIndex());
						newEntity.setBtnDesc(buttonEntity.getBtnDesc());
						ret.add(newEntity);
					}
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
		return ret;
	}
	
	/**
	 * 根据主键id删除按钮
	 * @param id
	 * @return
	 */
	@Transactional
	public boolean removeById(Integer id) {
		boolean ret = super.removeById(id);
		UpdateWrapper<ButtonUrlEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("button_id", id);
		buttonUrlService.remove(updateWrapper);
		if (ret) {
			removeCache(id);
		}
		return ret;
	}
	
	/**
	 * 获取符合条件的按钮列表
	 * @param params
	 * @return
	 */
	public List<ButtonEntity> list(Map<String,Object> params,String field) {
		QueryWrapper<ButtonEntity> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			Object roleId = params.get("roleId");
			if (roleId !=null) {
				queryWrapper = queryWrapper.inSql("id", "select button_id from role_button where role_id = "+roleId);
			}
			List<Integer> roleIndexList = (List<Integer>)params.get("roleIndexList");
			if (roleIndexList != null && !roleIndexList.isEmpty()) {
				queryWrapper = queryWrapper.inSql("id","select button_id from role_button where role_id in"+
						"(select id from role where role_index in("+CollUtil.join(roleIndexList, ",")+"))");
			}
		}
		queryWrapper.select(field);
		return list(queryWrapper);
	}
	
	/**
	 * 获取符合条件的按钮列表
	 * @param params
	 * @param field
	 * @return
	 */
	public List<ButtonExtend> listExtend(Map<String,Object> params,String field){
		QueryWrapper<ButtonExtend> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			Object roleId = params.get("roleId");
			if (roleId !=null) {
				queryWrapper = queryWrapper.inSql("id", "select button_id from role_button where role_id = "+roleId);
			}
			List<Integer> roleIndexList = (List<Integer>)params.get("roleIndexList");
			if (roleIndexList != null && !roleIndexList.isEmpty()) {
				queryWrapper = queryWrapper.inSql("id","select button_id from role_button where role_id in"+
						"(select id from role where role_index in("+CollUtil.join(roleIndexList, ",")+"))");
			}
		}
		queryWrapper.select(field);
		if (StringUtils.isEmpty(queryWrapper.getCustomSqlSegment())) {
			queryWrapper.apply("0 = 0");
		}
		queryWrapper.orderByDesc("b.menu_id,b.id");
		return baseMapper.listExtend(queryWrapper);
	}
	

	/**
	 * 更新按钮信息
	 * @param id
	 * @param btnName
	 * @param btnDesc
	 * @param menuId
	 * @param urlList
	 * @return
	 */
	@Transactional
	public boolean update(Integer id,String btnName,String btnDesc,Integer menuId,List<String> urlList) {		
		DataValidateUtils.validate(
		MapUtils.hms(
			"id.require", i.t("ButtonService.validate.id.require"),//主键id不能为空
			"btnName.require", i.t("ButtonService.validate.btnName.require"),//按钮名称不能为空
			"btnName.regex", i.t("ButtonService.validate.btnName.regex"),//按钮名称只能是数字、字母、下划线组合
			"btnName.max", i.t("ButtonService.validate.btnName.max"),//按钮名称长度不能超过{0}个字符
			"btnDesc.require",i.t("ButtonService.validate.btnDesc.require"),//按钮功能描述不能为空
			"btnDesc.max",i.t("ButtonService.validate.btnDesc.max"),//按钮功能描述不能超过{0}个字符
			"menuId.require",i.t("ButtonService.validate.menuId.require"),//按钮所属菜单(表单)不能为空
			"urlList.require",i.t("ButtonService.validate.urlList.require")//按钮拥有的url不能为空
		),
		MapUtils.hmo(
			"id",id,
			"btnName",btnName,
			"btnDesc",btnDesc,
			"menuId",menuId,
			"urlList",urlList
		),
			"id","require",
			"btnName","require|regex:'^\\w+$'|max:55",
			"btnDesc","require|max:255",
			"menuId","require",
			"urlList","require"
		);
		//如果菜单不存在则报错
		if (!menuService.menuExists(menuId)) {
			throw new RRException(i.t("ButtonService.update.menuNotExists"));//菜单不存在，更新失败！
		}
		{
			QueryWrapper<ButtonEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("btn_name", btnName);
			queryWrapper.eq("menu_id", menuId);
			queryWrapper.ne("id", id);
			queryWrapper.select("id");
			queryWrapper.last(" limit 1");
			if (getOne(queryWrapper) != null) {
				throw new RRException(i.t("ButtonService.update.sameNameBtnExists")); //存在同名的按钮，更新失败！
			}
		}
		if (!urlList.isEmpty()) {
			//如果发现有新的url，则录入到数据库
			List<RequestUrlEntity> requestUrlEntityListFromDb = requestUrlService.saveBatch(urlList);
			//更改按钮和 url 的引用关系
			if (!requestUrlEntityListFromDb.isEmpty()) {
				List<ButtonUrlEntity> buttonUrlList = new ArrayList<>(requestUrlEntityListFromDb.size());
				for(int i=0,len_i=requestUrlEntityListFromDb.size();i<len_i;i++) {
					ButtonUrlEntity buttonUrlEntity = new ButtonUrlEntity();
					buttonUrlEntity.setUrlId(requestUrlEntityListFromDb.get(i).getId());
					buttonUrlEntity.setButtonId(id);
					buttonUrlList.add(buttonUrlEntity);
				}
				QueryWrapper<ButtonUrlEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("button_id", id);
				queryWrapper.select("id,url_id");
				List<ButtonUrlEntity> buttonUrlListFromDb = buttonUrlService.list(queryWrapper);
				List<ButtonUrlEntity> saveButtonUrlList = CollectionSubOper.sub(buttonUrlList, buttonUrlListFromDb, "getId","getUrlId");
				if (!saveButtonUrlList.isEmpty()) {
					buttonUrlService.saveBatch(saveButtonUrlList);
				}
				List<ButtonUrlEntity> delButtonUrlList = CollectionSubOper.sub(buttonUrlListFromDb,buttonUrlList,"getId","getUrlId");
				if (!delButtonUrlList.isEmpty()) {
					List<Integer> delButtonUrlIdList = new ArrayList<>(delButtonUrlList.size());
					for(int i=0,len_i=delButtonUrlList.size();i<len_i;i++) {
						delButtonUrlIdList.add(delButtonUrlList.get(i).getId());
					}
					buttonUrlService.removeByIds(delButtonUrlIdList);
				}
			}
		}
		ButtonEntity buttonEntity = new ButtonEntity();
		buttonEntity.setId(id);
		buttonEntity.setBtnName(btnName);
		buttonEntity.setBtnDesc(btnDesc);
		buttonEntity.setMenuId(menuId);
		boolean ret = updateById(buttonEntity);
		if(ret) {
			QueryWrapper<RequestUrlExtend> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("bu.button_id", id);
			queryWrapper.select("ru.url_index");
			List<RequestUrlExtend> requestUrlExtendList = requestUrlMapper.listExtend2(queryWrapper);
			updateCache(id, btnName, btnDesc, menuId, requestUrlExtendList);
		}
		return ret;
	}
	
	/**
	 * 新增按钮
	 * @param btnName
	 * @param btnDesc
	 * @param menuId
	 * @return
	 */
	@Transactional
	public boolean save(String btnName,String btnDesc,Integer menuId,List<String> urlList) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"btnName.require", i.t("ButtonService.validate.btnName.require"),//按钮名称不能为空
			"btnName.regex", i.t("ButtonService.validate.btnName.regex"),//按钮名称只能是数字、字母、下划线组合
			"btnName.max", i.t("ButtonService.validate.btnName.max"),//按钮名称长度不能超过{0}个字符
			"btnDesc.require",i.t("ButtonService.validate.btnDesc.require"),//按钮功能描述不能为空
			"btnDesc.max", i.t("ButtonService.validate.btnDesc.max"),//按钮功能描述不能超过{0}个字符
			"menuId.require",i.t("ButtonService.validate.menuId.require"),//按钮所属菜单(表单)不能为空",
			"urlList.require",i.t("ButtonService.validate.urlList.require")//按钮拥有的url不能为空"
		),
		MapUtils.hmo(
			"btnName",btnName,
			"btnDesc",btnDesc,
			"menuId",menuId,
			"urlList",urlList
		),
			"btnName","require|regex:'^\\w+$'|max:55",
			"btnDesc","require|max:255",
			"menuId","require",
			"urlList","require"
		);
		//如果菜单不存在则报错
		if (!menuService.menuExists(menuId)) {
			throw new RRException(i.t("ButtonService.update.menuNotExists"));//菜单不存在，新增失败！
		}
		{
			QueryWrapper<ButtonEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("menu_id", menuId);
			queryWrapper.eq("btn_name", btnName);
			queryWrapper.select("menu_id");
			if (getOne(queryWrapper) != null) {
				throw new RRException(i.t("ButtonService.save.sameNameBtnExists"));//该按钮已存在，新增失败！
			}
		}
		
		ButtonEntity buttonEntity = new ButtonEntity();
		buttonEntity.setMenuId(menuId);
		buttonEntity.setBtnName(btnName);
		buttonEntity.setBtnDesc(btnDesc);
		boolean ret;
		synchronized (this) {
			buttonEntity.setBtnIndex(getNextIndex());
			ret = save(buttonEntity);
		}
		if(urlList.isEmpty()) {
			if(ret) {
				saveCache(buttonEntity.getId(), btnName, menuId, buttonEntity.getBtnIndex(), btnDesc,null);
			}
		} else {
			//如果发现有新的url，则录入到数据库
			List<RequestUrlEntity> requestUrlEntityListFromDb = requestUrlService.saveBatch(urlList);
			//保存按钮和 url 的引用关系
			if (!requestUrlEntityListFromDb.isEmpty()) {
				List<ButtonUrlEntity> saveButtonUrlList = new ArrayList<>(requestUrlEntityListFromDb.size());
				for(int i=0,len_i=requestUrlEntityListFromDb.size();i<len_i;i++) {
					ButtonUrlEntity buttonUrlEntity = new ButtonUrlEntity();
					buttonUrlEntity.setUrlId(requestUrlEntityListFromDb.get(i).getId());
					buttonUrlEntity.setButtonId(buttonEntity.getId());
					saveButtonUrlList.add(buttonUrlEntity);
				}
				buttonUrlService.saveBatch(saveButtonUrlList);
				if(ret) {
					saveCache(buttonEntity.getId(), btnName, menuId, buttonEntity.getBtnIndex(), btnDesc,requestUrlEntityListFromDb);
				}
			}
		}
		return ret;
	}
	
	
	private synchronized Integer getNextIndex() {
		QueryWrapper<ButtonEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("btn_index");
		List<ButtonEntity> buttonList = list(queryWrapper);
		if (buttonList.isEmpty()) {
			return 0;
		} else {
			List<Integer> buttonIndexList = new ArrayList<>();
			for (int i=0,len_i = buttonList.size();i < len_i;i++) {
				buttonIndexList.add(buttonList.get(i).getBtnIndex());
			}
			Collections.sort(buttonIndexList);
			for (int i=0,len_i = buttonIndexList.size();i<len_i;i++) {
				if (buttonIndexList.get(i).intValue() != i) {
					return i;
				}
			}
			return buttonIndexList.size();
		}
	}
	
	
	/**
	 * 所有按钮的映射表
	 */
	private final static Map<Integer, ButtonEntity> BUTTON_ID_MAP = new HashMap<>();
	
	/**
	 * 所有按钮权限位映射表
	 */
	private final static Map<Integer, ButtonEntity> BUTTON_IDX_MAP = new HashMap<>();
	
	/**
	 * 所有按钮持有的url映射表
	 */
	final static Map<Integer, HashSet<Integer>> BUTTON_ID_URL_MAP = new HashMap<>();
	
	
	@Autowired
	private RequestUrlMapper requestUrlMapper;
	
	/**
	 * initButtonList 是否调用完毕状态
	 */
	private boolean initFinish = false;
	
	
	public boolean isInitFinish() {
		return initFinish;
	}
	
	@PostConstruct
	private void initButtonList() {
		QueryWrapper<ButtonEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("id,btn_name,menu_id,btn_index,btn_desc");
		List<ButtonEntity> buttonList = list(queryWrapper);

		QueryWrapper<RequestUrlExtend> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.select("ru.id,ru.url,ru.url_index,bu.button_id");
		queryWrapper2.apply("0 = 0");
		List<RequestUrlExtend> requestUrlExtendList = requestUrlMapper.listExtend2(queryWrapper2);

		for(int i=0,len_i=buttonList.size();i<len_i;i++) {
			ButtonEntity buttonEntity = buttonList.get(i);
			BUTTON_ID_MAP.put(buttonEntity.getId(), buttonEntity);
			BUTTON_IDX_MAP.put(buttonEntity.getBtnIndex(), buttonEntity);
		}
		for (int i=0,len_i = requestUrlExtendList.size();i<len_i;i++) {
			RequestUrlExtend requestUrlExtend = requestUrlExtendList.get(i);
			HashSet<Integer> requestUrlSet = BUTTON_ID_URL_MAP.get(requestUrlExtend.getButtonId());
			if (requestUrlSet == null) {
				BUTTON_ID_URL_MAP.put(requestUrlExtend.getButtonId(), requestUrlSet = new HashSet<Integer>());
			}
			requestUrlSet.add(requestUrlExtend.getUrlIndex());
		}

		initFinish = true;
	}
	
	
	/**
	 * 删除缓存内容
	 * @param id
	 */
	private void removeCache(Integer id) {
		for(;;) {
			if(RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				BUTTON_ID_URL_MAP.remove(id);
				ButtonEntity buttonEntity = BUTTON_ID_MAP.remove(id);
				if (buttonEntity!=null) {
					BUTTON_IDX_MAP.remove(buttonEntity.getBtnIndex());
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 更新缓存内容
	 * @param id
	 * @param menuName
	 */
	private void updateCache(Integer id,String btnName,String btnDesc,Integer menuId,List<RequestUrlExtend> requestUrlExtendList) {
		for (;;) {
			if (RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				ButtonEntity entity = BUTTON_ID_MAP.get(id);
				if (entity != null) {
					entity.setBtnName(btnName);
					entity.setBtnDesc(btnDesc);
					entity.setMenuId(menuId);
				}
				if (requestUrlExtendList!=null) {
					HashSet<Integer> requestUrlSet = new HashSet<Integer>();
					for (int i=0,len_i = requestUrlExtendList.size();i<len_i;i++) {
						requestUrlSet.add(requestUrlExtendList.get(i).getUrlIndex());
					}
					BUTTON_ID_URL_MAP.put(id, requestUrlSet);
				}
				Map<Integer,HashSet<Integer>> menuIdUrlMap = MenuService.MENU_ID_URL_MAP;
				Map<Integer, byte[]> roleIdxUrlMap = RoleService.ROLE_IDX_URL_MAP;
				Map<Integer,HashSet<Integer>> roleButtonIdMap = RoleService.ROLE_BUTTON_ID_MAP;
				
				HashSet<Integer> buttonIdSet;
				for (Entry<Integer,HashSet<Integer>> entry:roleButtonIdMap.entrySet()) {
					buttonIdSet = entry.getValue();
					if (buttonIdSet.contains(id)) {
						byte[] urlByte = new byte[125];
						for(Integer buttonId:buttonIdSet) {
							HashSet<Integer> buttonIdUrlSet = BUTTON_ID_URL_MAP.get(buttonId);
							for(Integer urlIdx:buttonIdUrlSet) {
								int idx2 = (urlIdx + 1)%8; //获取余数位
								int idx1;
								if (idx2 == 0) { //没有余数位
									idx1 = (urlIdx + 1)/8 - 1; //获取下标
									urlByte[idx1] |= 1 << 7;
								} else { // 有余数位
									idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
									idx2 -= 1;
									urlByte[idx1] |= 1 << idx2;
								}
							}
						}
						HashSet<Integer> menuIdSet = RoleService.ROLE_MENU_ID_MAP.get(entry.getKey());
						for(Integer menuId2:menuIdSet) {
							HashSet<Integer> menuIdUrlSet = menuIdUrlMap.get(menuId2);
							//部分菜单属于目录，没有关联 url
							if (menuIdUrlSet != null) {
								for(Integer urlIdx:menuIdUrlSet) {
									int idx2 = (urlIdx + 1)%8; //获取余数位
									int idx1;
									if (idx2 == 0) { //没有余数位
										idx1 = (urlIdx + 1)/8 - 1; //获取下标
										urlByte[idx1] |= 1 << 7;
									} else { // 有余数位
										idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
										idx2 -= 1;
										urlByte[idx1] |= 1 << idx2;
									}
								}
							}
						}
						RoleEntity roleEntity = RoleService.ROLE_ID_MAP.get(entry.getKey());
						roleIdxUrlMap.put(roleEntity.getRoleIndex(), urlByte);
						break;
					}
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 新增缓存
	 * @param id
	 * @param sortIndex
	 * @param menuName
	 * @param type
	 * @param parentId
	 */
	private void saveCache(Integer id,String btnName, Integer menuId, Integer btnIndex, String btnDesc,List<RequestUrlEntity> requestUrlEntityList) {
		ButtonEntity buttonEntity = new ButtonEntity();
		buttonEntity.setId(id);
		buttonEntity.setBtnName(btnName);
		buttonEntity.setMenuId(menuId);
		buttonEntity.setBtnIndex(btnIndex);
		buttonEntity.setBtnDesc(btnDesc);
		for (;;) {
			if (RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				BUTTON_ID_MAP.put(id, buttonEntity);
				BUTTON_IDX_MAP.put(btnIndex, buttonEntity);
				if (requestUrlEntityList!=null) {
					HashSet<Integer> requestUrlSet = new HashSet<Integer>();
					for(int i=0,len_i = requestUrlEntityList.size();i<len_i;i++) {
						requestUrlSet.add(requestUrlEntityList.get(i).getUrlIndex());
					}
					BUTTON_ID_URL_MAP.put(id, requestUrlSet);
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
}