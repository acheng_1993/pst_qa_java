package com.pstfs.base.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.UserRoleEntity;
import com.pstfs.base.mapper.UserRoleMapper;

@Service("userRoleService")
public class UserRoleService extends ServiceImpl<UserRoleMapper, UserRoleEntity> {
	
}