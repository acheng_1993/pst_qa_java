package com.pstfs.base.service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.InboxEmailMessageEntity;
import com.pstfs.base.mapper.InboxEmailMessageMapper;

@Service("inboxEmailMessageService")
public class InboxEmailMessageService extends ServiceImpl<InboxEmailMessageMapper, InboxEmailMessageEntity> {

	
}