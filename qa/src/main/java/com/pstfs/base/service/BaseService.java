package com.pstfs.base.service;
import java.lang.reflect.Method;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.utils.Constant;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.SQLFilter;
import cn.hutool.core.util.ReflectUtil;

/**
 * 基础业务类
 * @author jiajian
 * @param <M>
 * @param <T>
 */
public class BaseService<M extends BaseMapper<T>, T> extends ServiceImpl<M, T>{
	
	@Autowired
	protected I18nUtils i;
	
	/**
	 * 把枚举值遍历出来，得到一份列表，用于校验
	 * @param envms
	 * @return
	 */
	protected String enumVals(Enum[] envms) {
		Method method = ReflectUtil.getMethod(envms[0].getClass(), "getV");
		StringBuilder sb = new StringBuilder();
		String connChar = "";
		for(int i=0,len_i=envms.length;i<len_i;i++) {
			Object data = ReflectUtil.invoke(envms[i], method, null);
			sb.append(connChar).append(data);
			connChar = ",";
		}
		return sb.toString();
	}
	
	/**
	 * 滚动式加载查询
	 * @param params
	 * @param queryWrapper
	 */
	protected void scrollLoadSplit(Map<String, Object> params, QueryWrapper<? extends T> queryWrapper) {
		scrollLoadSplit(params, queryWrapper,"");
	}
	
	/**
	 * 滚动式加载查询
	 * @param queryWrapper
	 * @param params
	 * @param idPrefix id前缀,用于多表查询时的前缀
	 */
	protected void scrollLoadSplit(Map<String, Object> params, QueryWrapper<? extends T> queryWrapper,String idPrefix) {
		String idField = idPrefix + "id";
		Object lastId = params.get("lastId");
		Object firstId = params.get("firstId");
		String sidx = params.containsKey(Constant.ORDER_FIELD)? SQLFilter.sqlInject((String)params.get(Constant.ORDER_FIELD)):null;
		String order = String.valueOf(params.getOrDefault("order","ASC"));
		String idOrder = String.valueOf(params.getOrDefault("idOrder","DESC"));
		if (lastId == null && firstId == null) {
			if(sidx != null) {
				if (Constant.ASC.equalsIgnoreCase(order)) {
					queryWrapper.orderByAsc(sidx);
				} else {
					queryWrapper.orderByDesc(sidx);
				}
			}
		} else if(lastId != null) {
			if(sidx == null) {
				queryWrapper.lt(idField, lastId);
			} else {
				Object lastSidx = params.get("lastSidx");
				queryWrapper.and((queryWrapper2)->{
					queryWrapper2.or((queryWrapper3)->{
						queryWrapper3.eq(sidx, lastSidx).lt(idField, lastId);
					});
					queryWrapper2.or((queryWrapper3)->{
						if (Constant.ASC.equalsIgnoreCase(order)) {
							queryWrapper3.gt(sidx, lastSidx);
						} else {
							queryWrapper3.lt(sidx, lastSidx);
						}
					});
				});
				if (Constant.ASC.equalsIgnoreCase(order)) {
					queryWrapper.orderByAsc(sidx);
				} else {
					queryWrapper.orderByDesc(sidx);
				}
			}
		} else if(firstId != null) {
			if(sidx == null) {
				queryWrapper.gt(idField, firstId);
			} else {
				Object firstSidx = params.get("firstSidx");
				queryWrapper.and((queryWrapper2)->{
					queryWrapper2.or((queryWrapper3)->{
						queryWrapper3.eq(sidx, firstSidx).gt(idField, firstId);
					});
					queryWrapper2.or((queryWrapper3)->{
						if (Constant.ASC.equalsIgnoreCase(order)) {
							queryWrapper3.lt(sidx, firstSidx);
						} else {
							queryWrapper3.gt(sidx, firstSidx);
						}
					});
				});
				if (Constant.ASC.equalsIgnoreCase(order)) {
					queryWrapper.orderByAsc(sidx);
				} else {
					queryWrapper.orderByDesc(sidx);
				}
			}
		}
		if (Constant.ASC.equalsIgnoreCase(idOrder)) {
			queryWrapper.orderByAsc(idField);
		} else {
			queryWrapper.orderByDesc(idField);
		}
		Integer limit = (Integer)params.get("limit");
		if(limit == null) {
			limit = 20;
		}
		// limit 为 0 则没有限制，返回所有数据
		if(limit.intValue() != 0) {
			queryWrapper.last("limit "+limit);
		}
	}
}