package com.pstfs.base.service;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.RoleEntity;
import com.pstfs.base.entity.UserEntity;
import com.pstfs.base.entity.UserRoleEntity;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.mapper.UserMapper;
import com.pstfs.base.utils.CollectionSubOper;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.validate.DataValidateUtils;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.lang.UUID;
import cn.hutool.crypto.SecureUtil;

@Service("userService")
public class UserService extends BaseService<UserMapper, UserEntity> {
	
	/**
	 * 根据主键 id 获取用户信息
	 * @param id 主键 id
	 * @return
	 */
	public UserEntity getById(Integer id) {
		return getById(id, "id,username,email,name");
	}
	
	/**
	 * 根据主键 id 获取用户信息
	 * @param id 主键 id
	 * @param field 获取用户信息字段
	 * @return
	 */
	public UserEntity getById(Integer id,String field) {
		QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("id", id);
		queryWrapper.select(field);
		return getOne(queryWrapper);
	}

	/**
	 * 根据用户名获取用户信息
	 * @param username
	 * @param field
	 * @return
	 */
	public UserEntity getByUsername(String username,String field) {
		QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("username", username);
		queryWrapper.select(field);
		return getOne(queryWrapper);
	}

	/**
	 * 滚动式加载用户信息
	 * @param params
	 * @return
	 */
	public List<UserEntity> scrollLoad(Map<String,Object> params,String field) {
		QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
		if(params != null) {
			String usernameLike = (String)params.get("usernameLike");
			if (StringUtils.isNotEmpty(usernameLike)) {
				queryWrapper = queryWrapper.likeRight("username", usernameLike);
			}
			String nameLike = (String)params.get("nameLike");
			if (StringUtils.isNotEmpty(nameLike)) {
				queryWrapper = queryWrapper.likeRight("name", nameLike);
			}
			String emailLike = (String)params.get("emailLike");
			if (StringUtils.isNotEmpty(emailLike)) {
				queryWrapper = queryWrapper.likeRight("email", emailLike);
			}
		}
		queryWrapper.select(field);
		Integer limit = (Integer)params.get("limit");
		if (limit == null || limit> 20 || limit < 1) {
			limit = 20;
		}
		scrollLoadSplit(params, queryWrapper);
		List<UserEntity> tempList = list(queryWrapper);
		List<UserEntity> ret = new ArrayList<>(tempList.size());
		for(int i=0,len_i = tempList.size();i<len_i;i++) {
			UserEntity tempUser = tempList.get(i);
			if(tempUser != null) {
				ret.add(tempUser);
			}
		}
		return ret;
	}
	
	/**
	 * 用户解锁
	 * @param username
	 */
	public boolean userUnlock(Integer id) {
		UpdateWrapper<UserEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id);
		updateWrapper.set("last_login_fail_time", null);
		updateWrapper.set("login_fail_count", 0);
		return update(updateWrapper);
	}

	/**
	 * 用户删除
	 * @param id
	 * @return
	 */
	public boolean remove(Integer id) {
		UpdateWrapper<UserEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id);
		updateWrapper.eq("deletable", 1);
		return remove(updateWrapper);
	}

	/**
	 * 更新用户
	 * @param id
	 * @param password
	 * @param roleIdList
	 * @return
	 */
	@Transactional
	public boolean update(Integer id,String password,String name,String email,List<Integer> roleIdList) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"password.size",i.t("UserService.validate.password.size"),//密码长度必须在{0}-{1}之间！
			"password.regex",i.t("UserService.validate.password.regex"),//密码只能含有数字、字母、下划线的组合
			"roleIdList.require",i.t("UserService.validate.roleIdList.require"),//角色不能为空
			"name.require",i.t("UserService.validate.name.require"),//用户名称不能为空
			"name.max",i.t("UserService.validate.name.max"),//用户名称长度不能超过{0}个字符
			"email.email",i.t("UserService.validate.email.email"),//邮箱地址格式错误
			"email.max",i.t("UserService.validate.email.max")//邮箱地址长度不能超过{0}个字符
		),
		MapUtils.hmo(
			"password",password,
			"name",name,
			"email",email,
			"roleIdList",roleIdList
		),
			"password","size:6,25|regex:'^\\w+$'",
			"name","require|max:30",
			"roleIdList","require",
			"email","email|max:45"
		);
		
		HashSet<Integer> roleIdSet = new HashSet<>(roleIdList);
		
		Map<Integer,RoleEntity> roleIdMap = RoleService.ROLE_ID_MAP;
		for(Integer roleId:roleIdSet) {
			if(!roleIdMap.containsKey(roleId)) {
				throw new RRException(i.t("UserService.update.roleNotExists"));//角色不存在，保存失败！
			}
		}

		UserEntity userEntity = new UserEntity();
		userEntity.setId(id);
		userEntity.setName(name);
		userEntity.setEmail(email);
		{
			byte[] roleByte = new byte[10];
			int maxIdx1 = -1;
			for (Integer roleId:roleIdSet) {
				int currRoleIdx = roleIdMap.get(roleId).getRoleIndex();
				int idx2 = (currRoleIdx + 1)%8; //获取余数位
				int idx1;
				if (idx2 == 0) { //没有余数位
					idx1 = (currRoleIdx + 1)/8 - 1; //获取下标
					roleByte[idx1] += 1 << 7;
				} else { // 有余数位
					idx1 = (currRoleIdx + 1 - idx2)/8; //获取下标 
					idx2 -= 1;
					roleByte[idx1] += 1 << idx2;
				}
				if(idx1 > maxIdx1) {
					maxIdx1 = idx1;
				}
			}
			userEntity.setUserHasRole(Base64.encode(Arrays.copyOf(roleByte, maxIdx1 + 1)));
		}
		if (StringUtils.isNotEmpty(password)) {
			userEntity.setPassword(SecureUtil.sha1(password+userEntity.getSalt()));
			userEntity.setSalt(UUID.randomUUID().toString(true));
		}
		boolean ret = updateById(userEntity);
		List<UserRoleEntity> userRoleList = new ArrayList<>();
		for (Integer roleId:roleIdSet) {
			UserRoleEntity userRoleEntity = new UserRoleEntity();
			userRoleEntity.setUserId(userEntity.getId());
			userRoleEntity.setRoleId(roleId);
			userRoleList.add(userRoleEntity);
		}
		QueryWrapper<UserRoleEntity> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.eq("user_id", id);
		List<UserRoleEntity> userRoleListFromDb = userRoleService.list(queryWrapper2);
		List<UserRoleEntity> saveList = CollectionSubOper.sub(userRoleList, userRoleListFromDb, "getUserId", "getRoleId");
		if(!saveList.isEmpty()) {
			userRoleService.saveBatch(saveList);
		}
		List<UserRoleEntity> delList = CollectionSubOper.sub(userRoleListFromDb,userRoleList,"getUserId", "getRoleId");
		if(!delList.isEmpty()) {
			List<Integer> delIdList = new ArrayList<Integer>(delList.size());
			for (int i=0,len_i=delList.size();i<len_i;i++) {
				delIdList.add(delList.get(i).getId());
			}
			userRoleService.removeByIds(delIdList);
		}
		return ret;
	}

	@Autowired
	private UserRoleService userRoleService;
	
	/**
	 * 修改个人信息
	 * @param username 用户名
	 * @param email 邮箱
	 * @param oldPassword 旧密码
	 * @param newPassword 新密码
	 * @return
	 */
	public boolean updateSelf(String username, String name, String email, String oldPassword, String newPassword) {
		DataValidateUtils.validate(
		MapUtils.hms(
		"name.require",i.t("UserService.validate.name.require"),//用户名称不能为空
		"name.max",i.t("UserService.validate.name.max"),//用户名称长度不能超过{0}个字符
		"email.email",i.t("UserService.validate.email.email"),//邮箱地址格式错误
		"email.max",i.t("UserService.validate.email.max"),//邮箱地址长度不能超过{0}个字符
		"oldPassword.size",i.t("UserService.validate.oldPassword.size"),//旧密码长度必须是{0}-{1}个字符
		"oldPassword.regex",i.t("UserService.validate.oldPassword.regex"),//旧密码只能含有数字、大小写字母、下划线组合
		"oldPassword.notEqualTo",i.t("UserService.validate.newPassword.notEqualTo"),//新密码和旧密码不能相同
		"newPassword.size",i.t("UserService.validate.newPassword.size"),//新密码长度必须是{0}-{1}个字符
		"newPassword.regex", i.t("UserService.validate.newPassword.regex"),//新密码只能含有数字、大小写字母、下划线组合
		"newPassword.notEqualTo",i.t("UserService.validate.newPassword.notEqualTo")//新密码和旧密码不能相同
		), 
		MapUtils.hms(
		"name", name,
		"email", email,
		"oldPassword", oldPassword, 
		"newPassword", newPassword
		),
		"name","require|max:30",
		"email","email|max:45",
		"oldPassword","size:6,25|regex:'^\\w+$'|notEqualTo:'newPassword'",
		"newPassword","size:6,25|regex:'^\\w+$'|notEqualTo:'oldPassword'");
		
		QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("username", username);
		queryWrapper.select("id,username,password,salt");
		UserEntity userEntity = getOne(queryWrapper);
		if (userEntity == null) {
			throw new RRException(i.t("UserService.save.roleNotExists"));//用户不存在，保存失败
		}
		userEntity.setName(name);
		userEntity.setEmail(email);
		if(StringUtils.isNotEmpty(oldPassword) && StringUtils.isNotEmpty(newPassword)) {
			if (!userEntity.getPassword().equals(SecureUtil.sha1(oldPassword+userEntity.getSalt()))) {
				throw new RRException(i.t("UserService.updateSelf.oldPasswordError"));//旧密码错误，保存失败
			}
			userEntity.setSalt(UUID.randomUUID().toString(true));
			userEntity.setPassword(SecureUtil.sha1(newPassword+userEntity.getSalt()));
		}
        return updateById(userEntity);
	}
	
	/**
	 * 新增用户
	 * @param username 用户名
	 * @param password 密码
	 * @param name 用户名称
	 * @param roleIdList 角色id
	 * @return
	 */
	@Transactional
	public boolean save(String username,String password,String name,String email,List<Integer> roleIdList) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"username.require",i.t("UserService.validate.username.require"),//用户名不能为空！
			"username.size",i.t("UserService.validate.username.size"),//用户名长度必须在{0}-{1}之间！
			"username.regex",i.t("UserService.validate.username.regex"),//用户名只能含有数字、字母、下划线的组合
			"password.require",i.t("UserService.validate.password.require"),//密码不能为空！
			"password.size",i.t("UserService.validate.password.size"),//密码长度必须在{0}-{1}之间！
			"password.regex",i.t("UserService.validate.password.regex"),//密码只能含有数字、字母、下划线的组合
			"roleIdList.require",i.t("UserService.validate.roleIdList.require"),//角色不能为空
			"name.require",i.t("UserService.validate.name.require"),//用户名称不能为空
			"name.max", i.t("UserService.validate.name.max"),//用户名称长度不能超过{0}个字符
			"email.email",i.t("UserService.validate.email.email"),//邮箱地址格式错误
			"email.max",i.t("UserService.validate.email.max")//邮箱地址长度不能超过{0}个字符
		),
		MapUtils.hmo(
			"username",username,
			"password",password,
			"name",name,
			"email",email,
			"roleIdList",roleIdList
		),
			"username","require|size:3,25|regex:'^\\w+$'",
			"password","require|size:6,25|regex:'^\\w+$'",
			"name","require|max:30",
			"roleIdList","require",
			"email","email|max:45"
		);
		HashSet<Integer> roleIdSet = new HashSet<>(roleIdList);
		Map<Integer,RoleEntity> roleIdMap = RoleService.ROLE_ID_MAP;
		for(Integer roleId:roleIdSet) {
			if(!roleIdMap.containsKey(roleId)) {
				throw new RRException(i.t("UserService.update.roleNotExists"));//角色不存在，保存失败！
			}
		}		
		UserEntity userEntity = new UserEntity();
		userEntity.setSalt(UUID.randomUUID().toString(true));
		userEntity.setUsername(username);
		userEntity.setPassword(SecureUtil.sha1(password+userEntity.getSalt()));
		userEntity.setName(name);
		userEntity.setDeletable(1);
		userEntity.setEmail(email);
		{
			byte[] roleByte = new byte[10];
			int maxIdx1 = -1;
			for (Integer roleId:roleIdSet) {
				int currRoleIdx = roleIdMap.get(roleId).getRoleIndex();
				int idx2 = (currRoleIdx + 1)%8; //获取余数位
				int idx1;
				if (idx2 == 0) { //没有余数位
					idx1 = (currRoleIdx + 1)/8 - 1; //获取下标
					roleByte[idx1] += 1 << 7;
				} else { // 有余数位
					idx1 = (currRoleIdx + 1 - idx2)/8; //获取下标 
					idx2 -= 1;
					roleByte[idx1] += 1 << idx2;
				}
				if(idx1 > maxIdx1) {
					maxIdx1 = idx1;
				}
			}
			userEntity.setUserHasRole(Base64.encode(Arrays.copyOf(roleByte, maxIdx1 + 1)));
		}
		
		boolean ret = save(userEntity);
		List<UserRoleEntity> userRoleList = new ArrayList<>();
		for (int i=0,len_i=roleIdList.size();i<len_i;i++) {
			UserRoleEntity userRoleEntity = new UserRoleEntity();
			userRoleEntity.setUserId(userEntity.getId());
			userRoleEntity.setRoleId(roleIdList.get(i));
			userRoleList.add(userRoleEntity);
		}
		userRoleService.saveBatch(userRoleList);
		return ret;
	}
	
	/**
	 * 登录，当账号密码错误时，将进行错误次数累计，达到一定的次数后锁定不允许登录。
	 * @param username
	 * @param password
	 * @return
	 */
	public UserEntity login(String username,String password) {
		DataValidateUtils.validate(
		MapUtils.hms(
			"username.require",i.t("UserService.validate.username.require"),//用户名不能为空！
			"username.size",i.t("UserService.validate.username.size"),//用户名长度必须在{0}-{1}之间！
			"username.regex",i.t("UserService.validate.username.regex"),//用户名只能含有数字、字母、下划线的组合
			"password.require",i.t("UserService.validate.password.require"),//密码不能为空！
			"password.size",i.t("UserService.validate.password.size"),//密码长度必须在{0}-{1}之间！
			"password.regex",i.t("UserService.validate.password.regex")//密码只能含有数字、字母、下划线的组合
		),
		MapUtils.hms(
			"username", username,
			"password", password
		),
			"username","require|size:3,25|regex:'^\\w+$'",
			"password","require|size:6,25|regex:'^\\w+$'"
		);
		QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("username", username);
		queryWrapper.select("id,user_has_role,password,salt,last_login_fail_time,login_fail_count");
		UserEntity userEntity = getOne(queryWrapper);
		//用户不存在
		if(userEntity == null) {
			return null;
		}
		long currTimeMillis = System.currentTimeMillis();
		long nextOperTimeMillis = Long.MIN_VALUE;
		Date lastLoginFailTime = userEntity.getLastLoginFailTime();
		if (lastLoginFailTime!=null) {
			nextOperTimeMillis = 60000 + lastLoginFailTime.getTime(); //允许登录时间
			//登录错误次数达到3次或以上，必须间隔1分钟登录一次
			if (userEntity.getLoginFailCount()>=3 && nextOperTimeMillis>currTimeMillis) {
				LocalDateTime nextOperTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(nextOperTimeMillis), ZoneId.systemDefault());
//				登录出错次数超过3次，必须在{0}后才能登录！
				throw new RRException(i.t("UserService.login.loginFailCount",nextOperTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
			}
		}
		String sha1 = SecureUtil.sha1(password+userEntity.getSalt());
		//密码正确
		if(sha1.equals(userEntity.getPassword())) {
			//当登录成功的时间与上次登录失败的时间间隔足够长，则清除登录失败次数
			UpdateWrapper<UserEntity> updateWrapper = new UpdateWrapper<>();
			updateWrapper.eq("username", username);
			updateWrapper.set("login_fail_count", Math.max(userEntity.getLoginFailCount()-(currTimeMillis - nextOperTimeMillis)/60000, 0));
			update(updateWrapper);
			return userEntity;
		} else {
			//密码错误，记录登录失败次数
			UpdateWrapper<UserEntity> updateWrapper = new UpdateWrapper<>();
			updateWrapper.eq("username", username);
			updateWrapper.set("last_login_fail_time", new Date());
			updateWrapper.setSql("login_fail_count=login_fail_count+1");
			update(updateWrapper);
			return null;
		}
	}
}