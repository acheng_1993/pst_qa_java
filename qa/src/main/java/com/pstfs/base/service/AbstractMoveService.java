package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.intf.ISortable;
import com.pstfs.base.exception.RRException;

/**
 * 节点移动抽象业务类
 * @author jiajian
 */
public abstract class AbstractMoveService<M extends BaseMapper<T>, T extends ISortable> extends BaseService<M, T> {

    /**
     * 创建实体类
     * @return
     */
    protected abstract T createEntity();
    
    /**
     * 获取主键字段
     * @return 返回主键字段名
     */
    protected String getPk() {
        return "id";
    }
    
    /**
     * 获取排序字段
     * @return 返回排序字段
     */
    protected String getSortIndexField() {
        return "sort_index";
    }
    
    /**
     * 组件移动条件，确保节点在特定范围内的元素中移动，例如：
     * 1. 有若干组数据，但只允许组内数据移动。或者允许跨组移动。。。
     * @param 条件构造器
     */
    protected abstract QueryWrapper<T> moveCondition(QueryWrapper<T> queryWrapper,int id, int targetId, int moveType);
    
    /**
     * 节点移动
     * @param int id 被移动的节点id
     * @param int targetId 目标id
     */
    @Transactional
    public boolean move(int id, int targetId, int moveType) {
        if (nodeExist(id, targetId, moveType) == false) {
        	//移动失败，节点不存在！
            throw new RRException(i.t("AbstractMoveService.move.nodeNotExist"));
        }
        String sortIndexField = getSortIndexField();
        String pk = getPk();
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(pk, targetId).select(sortIndexField);
        //获取目标节点的排序号
        T tempObj = getOne(queryWrapper);
        Integer tempSortIndex = tempObj.getSortIndex();
        T data = createEntity();
        data.setId(id);
        try {
        	queryWrapper = new QueryWrapper<>();
        	queryWrapper = moveCondition(queryWrapper, id, targetId, moveType);
            if (moveType == -1) {
            	queryWrapper = queryWrapper.ge(sortIndexField, tempSortIndex).ne(pk, id).select(pk, sortIndexField);
                List<T> moveNodeList = list(queryWrapper);
                data.setSortIndex(tempSortIndex);
                List<T> saveList = new ArrayList<>(moveNodeList.size());
                for (int i = 0, len_i = moveNodeList.size(); i < len_i; i++) {
                    T moveNode = moveNodeList.get(i);
                    T saveNode = createEntity();
                    saveNode.setId(moveNode.getId());
                    saveNode.setSortIndex(moveNode.getSortIndex()+1);
                    saveList.add(saveNode);
                }
                updateBatchById(saveList);
            } else if (moveType == 1) {
            	queryWrapper = queryWrapper.gt(sortIndexField, tempSortIndex).select(pk,sortIndexField);
                List<T> moveNodeList = list(queryWrapper);
                if (moveNodeList.isEmpty()) {
                	data.setSortIndex(tempSortIndex+1);
                } else {
                	data.setSortIndex(moveNodeList.get(0).getSortIndex());
                    List<T> saveList = new ArrayList<>(moveNodeList.size());
                    for (int i = 0, len_i = moveNodeList.size(); i < len_i; i++) {
                        T moveNode = moveNodeList.get(i);
                        T saveNode = createEntity();
                        saveNode.setId(moveNode.getId());
                        saveNode.setSortIndex(moveNode.getSortIndex()+1);
                        saveList.add(saveNode);
                    }
                    updateBatchById(saveList);
                }
            }
            return updateById(data);
        } catch (Exception ex) {
            throw new RRException(ex.getMessage(),ex);
        }
    }

    /**
     * 拖拽节点，目标节点是否存在
     * @param int $id
     */
    private boolean nodeExist(int id, int targetId, int moveType) {
        String pk = getPk();
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        queryWrapper = moveCondition(queryWrapper, id, targetId, moveType);
        queryWrapper.in(pk, id,targetId);
        queryWrapper.select(pk);
        return count(queryWrapper) == 2;
    }
}