package com.pstfs.base.service;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.GrievanceMgrMsgAcceptEntity;
import com.pstfs.base.mapper.GrievanceMgrMsgAcceptMapper;


@Service("grievanceMgrMsgAcceptService")
public class GrievanceMgrMsgAcceptService extends ServiceImpl<GrievanceMgrMsgAcceptMapper, GrievanceMgrMsgAcceptEntity> {


}