package com.pstfs.base.service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.GrievanceMgrUploadFileEntity;
import com.pstfs.base.mapper.GrievanceMgrUploadFileMapper;

@Service("grievanceMgrUploadFileService")
public class GrievanceMgrUploadFileService extends ServiceImpl<GrievanceMgrUploadFileMapper, GrievanceMgrUploadFileEntity> {

}