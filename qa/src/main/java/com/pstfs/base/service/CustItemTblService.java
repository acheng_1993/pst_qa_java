package com.pstfs.base.service;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.CustItemTblEntity;
import com.pstfs.base.mapper.CustItemTblMapper;

@DS("time2")
@Service("custItemTblService")
public class CustItemTblService extends ServiceImpl<CustItemTblMapper, CustItemTblEntity> {

	/**
	 * 校验数据是否存在
	 * @param itemCd
	 * @return
	 */
	public boolean checkExists(String itemCd) {
		return checkExists(itemCd,null);
	}

	/**
	 * 校验数据是否存在
	 * @param itemCd
	 * @param custItemCd
	 * @return
	 */
	public boolean checkExists(String itemCd,String custItemCd) {
		QueryWrapper<CustItemTblEntity> queryWrapper = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(itemCd)) {
			queryWrapper.eq("item_cd", itemCd);
		}
		if(StringUtils.isNotEmpty(custItemCd)) {
			queryWrapper.eq("cust_item_cd", custItemCd);
		}
		queryWrapper.le("rownum",1);
		queryWrapper.select("fc_id");
		return getOne(queryWrapper) != null;
	}
	
	/**
	 * 获取符合条件的客户品目查询
	 * @param params
	 * @return
	 */
	public List<CustItemTblEntity> list(Map<String,Object> params,String field,int limit) {
		QueryWrapper<CustItemTblEntity> queryWrapper = new QueryWrapper<>();
		if (params!=null) {
			String itemCdLike = (String)params.get("itemCdLike");
			if(StringUtils.isNotEmpty(itemCdLike)) {
				queryWrapper.like("item_cd", itemCdLike+"%");
			}
			String itemCd = (String)params.get("itemCd");
			if(StringUtils.isNotEmpty(itemCd)) {
				queryWrapper.eq("item_cd", itemCd);
			}
			String custItemCdLike = (String)params.get("custItemCdLike");
			if(StringUtils.isNotEmpty(custItemCdLike)) {
				queryWrapper.like("cust_item_cd", custItemCdLike+"%");
			}
		}
		if (limit>0) {
			queryWrapper.le("rownum", limit);
		}
		queryWrapper.orderByDesc("upd_dt");
		queryWrapper.select(field);
		return list(queryWrapper);
	}
}