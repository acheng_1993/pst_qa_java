package com.pstfs.base.service;
import java.util.List;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.GrievanceMgrPartsDetailsEntity;
import com.pstfs.base.mapper.GrievanceMgrPartsDetailsMapper;

/**
 * 客户投诉管理部品详情业务类
 * @author jiajian
 */
@Service("grievanceMgrPartsDetailsService")
public class GrievanceMgrPartsDetailsService extends ServiceImpl<GrievanceMgrPartsDetailsMapper, GrievanceMgrPartsDetailsEntity> {

	/**
	 * 获取符合条件的客户投诉管理部品详情
	 * @param grievanceMgrIdList
	 * @return
	 */
	public List<GrievanceMgrPartsDetailsEntity> list(List<Integer> grievanceMgrIdList) {
		QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("grievance_mgr_id", grievanceMgrIdList);
		queryWrapper.select("grievance_mgr_id,maker_name,part_num,component_pos");
		return list(queryWrapper);
	}
}