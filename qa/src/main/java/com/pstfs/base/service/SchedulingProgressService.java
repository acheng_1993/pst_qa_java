package com.pstfs.base.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.SchedulingProgressEntity;
import com.pstfs.base.mapper.SchedulingProgressMapper;

@Service("schedulingProgressService")
public class SchedulingProgressService extends ServiceImpl<SchedulingProgressMapper, SchedulingProgressEntity> {

	
	/**
	 * 按照进度类型获取进度信息
	 * @param progressType
	 * @return
	 */
	public SchedulingProgressEntity getByProgressType(int progressType) {
		QueryWrapper<SchedulingProgressEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("progress_type", progressType);
		SchedulingProgressEntity schedulingProgressEntity = getOne(queryWrapper);
		if (schedulingProgressEntity == null) {
			schedulingProgressEntity = new SchedulingProgressEntity();
			schedulingProgressEntity.setProgressId(-1);
			schedulingProgressEntity.setProgressType(progressType);
			save(schedulingProgressEntity);
		}
		return schedulingProgressEntity;
	}
}