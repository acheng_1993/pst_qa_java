package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.I18nTranslateEntity;
import com.pstfs.base.mapper.I18nTranslateMapper;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.validate.DataValidateUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Service("i18nTranslateService")
public class I18nTranslateService extends ServiceImpl<I18nTranslateMapper, I18nTranslateEntity> {

	/**
	 * 按id增序排序，并且 id=null排在后面
	 */
	private Comparator<I18nTranslateEntity> compareById =  new Comparator<I18nTranslateEntity>() {
		@Override
		public int compare(I18nTranslateEntity o1, I18nTranslateEntity o2) {
			Integer id1 = o1.getId();
			Integer id2 = o2.getId();
			if(id1 == null) {
				if(id2 == null) {
					return 0;
				} else {
					return 1;
				}
			} else {
				if(id2 == null) {
					return -1;
				} else {
					return id1.compareTo(id2);
				}
			}
		}
	};
	
	/**
	 * 获取翻译值
	 * @param table
	 * @param col
	 * @param rowId
	 * @param langCode
	 * @return
	 */
	public String getValFromCache(String tableName, String col, Integer rowId,String langCode) {
		I18nTranslateKey1 key1 = new I18nTranslateKey1();
		key1.setTableName(tableName);
		key1.setCol(col);
		key1.setRowId(rowId);
		key1.setLangCode(langCode);
		I18nTranslateEntity i18nTranslate = I18N_TRANSLATE_KEY1_MAP.get(key1);
		return i18nTranslate == null?null:i18nTranslate.getVal();
	}
	
	/**
	 * 获取所有翻译值
	 * @param table
	 * @param col
	 * @param rowId
	 * @return
	 */
	public List<I18nTranslateEntity> list(String tableName, String col, Integer rowId) {
		I18nTranslateKey2 key2 = new I18nTranslateKey2();
		key2.setTableName(tableName);
		key2.setCol(col);
		key2.setRowId(rowId);
		List<I18nTranslateEntity> list = I18N_TRANSLATE_KEY2_MAP.get(key2);
		List<I18nTranslateEntity> ret = new ArrayList<>();
		if (list!=null) {
			for(int i=0,len_i = list.size();i<len_i;i++) {
				I18nTranslateEntity i18nTranslate = list.get(i);
				I18nTranslateEntity newEntity = new I18nTranslateEntity();
				newEntity.setId(i18nTranslate.getId());
				newEntity.setLangCode(i18nTranslate.getLangCode());
				newEntity.setVal(i18nTranslate.getVal());
				ret.add(newEntity);
			}
		}
		return ret;
	}
	
	/**
	 * 校验翻译数据，由调用方校验，这里的save、update方法不做校验
	 * @param i18nTranslateList
	 */
	public void validate(List<I18nTranslateEntity> i18nTranslateList) {
		i18nTranslateList.sort(compareById);
		HashSet<String> langCodeSet = new HashSet<>();
		for (int i=0,len_i=i18nTranslateList.size();i<len_i;) {
			I18nTranslateEntity i18nTranslate = i18nTranslateList.get(i);
			if (langCodeSet.contains(i18nTranslate.getLangCode())) {
				i18nTranslateList.remove(i);
			} else {
				DataValidateUtils.validate(
					MapUtils.hms(
						"langCode.require","语言标识码不能为空",
						"langCode.in","语言标识码错误",
						"val.require","翻译值不能为空",
						"val.max","翻译值长度不能超过255个字符"
					),
					i18nTranslate,
					"langCode","require|in:'zh','ja'",
					"val","require|max:255"
				);
				langCodeSet.add(i18nTranslate.getLangCode());
				i++;
			}
		}
	}

	/**
	 * 删除翻译值
	 * @param table
	 * @param col
	 * @param rowId
	 * @return
	 */
	public boolean remove(String table, String col, Integer rowId) {
		QueryWrapper<I18nTranslateEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("table", table);
		queryWrapper.eq("col", col);
		queryWrapper.eq("row_id", rowId);
		boolean ret = remove(queryWrapper);
		if(ret) {
			removeCache(table, col, rowId);
		}
		return ret;
	}
	
	/**
	 * 批量保存翻译值，使用该方法前必须先调用 validate 进行校验以确保数据正常。
	 * @param table
	 * @param col
	 * @param rowId
	 * @param i18nTranslateList
	 * @return
	 */
	public boolean saveBatch(String tableName,String col,Integer rowId, List<I18nTranslateEntity> i18nTranslateList) {
		I18nTranslateKey2 key2 = new I18nTranslateKey2();
		key2.setTableName(tableName);
		key2.setCol(col);
		key2.setRowId(rowId);
		List<I18nTranslateEntity> i18nTranslateListFromCache = I18N_TRANSLATE_KEY2_MAP.get(key2);
		if (i18nTranslateList == null || i18nTranslateList.isEmpty()) {
			if(i18nTranslateListFromCache == null || i18nTranslateListFromCache.isEmpty()) {
				return true;
			} else {
				List<Integer> delIdList = new ArrayList<>();
				for (int i=0,len_i = i18nTranslateListFromCache.size();i<len_i;i++) {
					delIdList.add(i18nTranslateListFromCache.get(i).getId());
				}
				boolean ret = removeByIds(delIdList);
				if (ret) {
					removeByIdsCache(tableName, col, rowId, delIdList);
				}
				return ret;
			}
		} else {
			//用二分查找，找到要删除的 id.（缓存中存在，但是要保存的数据中不存在，则认为是删除对象）
			List<Integer> delIdList = new ArrayList<>();
			if (i18nTranslateListFromCache != null && !i18nTranslateListFromCache.isEmpty()) {
				for (int i=0,len_i = i18nTranslateListFromCache.size();i<len_i;i++) {
					Integer id = i18nTranslateListFromCache.get(i).getId();
					int j = binarySearch(i18nTranslateList, id);
					//在缓存中存在，但在保存数据中不存在
					if(j == -1) {
						delIdList.add(id);
					}
				}
			}
			List<I18nTranslateEntity> saveList = new ArrayList<>();
			for (I18nTranslateEntity i18nTranslate:i18nTranslateList) {
				I18nTranslateEntity save = new I18nTranslateEntity();
				save.setId(i18nTranslate.getId());
				save.setTableName(tableName);
				save.setCol(col);
				save.setRowId(rowId);
				save.setLangCode(i18nTranslate.getLangCode());
				save.setVal(i18nTranslate.getVal());
				saveList.add(save);
			}
			boolean ret = saveOrUpdateBatch(saveList);
			if (ret) {
				saveBatchCache(tableName, col, rowId, saveList, delIdList);
			}
			return ret;
		}
	}
	
	/**
	 * 自旋锁
	 */
	private final static AtomicBoolean I18N_TRANSLATE_LOCK = new AtomicBoolean(Boolean.FALSE);
	
	/**
	 * 翻译表缓存
	 */
	private final static Map<Integer,I18nTranslateEntity> I18N_TRANSLATE_MAP = new HashMap<>();

	/**
	 * 翻译表缓存
	 */
	private final static Map<I18nTranslateKey1,I18nTranslateEntity> I18N_TRANSLATE_KEY1_MAP = new HashMap<>();
	private final static Map<I18nTranslateKey2,List<I18nTranslateEntity>> I18N_TRANSLATE_KEY2_MAP = new HashMap<>();
	
	/**
	 * 把翻译内容加载到内存缓存
	 */
	@PostConstruct
	private void initI18nTranslateList() {
		List<I18nTranslateEntity> i18nTranslateList = list();
		i18nTranslateList.sort(compareById);
		for (int i=0,len_i = i18nTranslateList.size();i<len_i;i++) {
			I18nTranslateEntity i18nTranslateEntity = i18nTranslateList.get(i);
			I18N_TRANSLATE_MAP.put(i18nTranslateEntity.getId(), i18nTranslateEntity);
			
			I18nTranslateKey1 i18nTranslateKey1 = new I18nTranslateKey1();
			i18nTranslateKey1.setTableName(i18nTranslateEntity.getTableName());
			i18nTranslateKey1.setCol(i18nTranslateEntity.getCol());
			i18nTranslateKey1.setRowId(i18nTranslateEntity.getRowId());
			i18nTranslateKey1.setLangCode(i18nTranslateEntity.getLangCode());
			I18N_TRANSLATE_KEY1_MAP.put(i18nTranslateKey1, i18nTranslateEntity);

			I18nTranslateKey2 i18nTranslateKey2 = new I18nTranslateKey2();
			i18nTranslateKey2.setTableName(i18nTranslateEntity.getTableName());
			i18nTranslateKey2.setCol(i18nTranslateEntity.getCol());
			i18nTranslateKey2.setRowId(i18nTranslateEntity.getRowId());
			List<I18nTranslateEntity> i18nTranslateList2 = I18N_TRANSLATE_KEY2_MAP.get(i18nTranslateKey2);
			if(i18nTranslateList2 == null) {
				I18N_TRANSLATE_KEY2_MAP.put(i18nTranslateKey2, i18nTranslateList2 = new ArrayList<>());
			}
			i18nTranslateList2.add(i18nTranslateEntity);
		}
	}
	
	/**
	 * 删除缓存
	 * @param table
	 * @param col
	 * @param rowId
	 */
	private void removeCache(String tableName, String col, Integer rowId) {
		for (;;) {
			if (I18N_TRANSLATE_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				I18nTranslateKey1 key1 = new I18nTranslateKey1();
				key1.setTableName(tableName);
				key1.setCol(col);
				key1.setRowId(rowId);
				
				I18nTranslateKey2 key2 = new I18nTranslateKey2();
				key2.setTableName(tableName);
				key2.setCol(col);
				key2.setRowId(rowId);
				List<I18nTranslateEntity> i18nTranslateList2 = I18N_TRANSLATE_KEY2_MAP.remove(key2);
				if(i18nTranslateList2!=null && !i18nTranslateList2.isEmpty()) {
					for (int i=0,len_i = i18nTranslateList2.size();i<len_i;i++) {
						I18nTranslateEntity i18nTranslate = i18nTranslateList2.get(i);
						I18N_TRANSLATE_MAP.remove(i18nTranslate.getId());
						
						key1.setLangCode(i18nTranslate.getLangCode());
						I18N_TRANSLATE_KEY1_MAP.remove(key1);
					}
				}
				I18N_TRANSLATE_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 按照 id 列表删除缓存
	 * @param idList
	 */
	private void removeByIdsCache(String tableName, String col, Integer rowId, List<Integer> delIdList) {
		for(;;) {
			if (I18N_TRANSLATE_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				I18nTranslateKey1 key1 = new I18nTranslateKey1();
				key1.setTableName(tableName);
				key1.setCol(col);
				key1.setRowId(rowId);
				
				I18nTranslateKey2 key2 = new I18nTranslateKey2();
				key2.setTableName(tableName);
				key2.setCol(col);
				key2.setRowId(rowId);
				
				List<I18nTranslateEntity> i18nTranslateList2 = I18N_TRANSLATE_KEY2_MAP.get(key2);
				if (i18nTranslateList2 == null || i18nTranslateList2.isEmpty()) {
					for (int i=0,len_i=delIdList.size();i<len_i;i++) {
						Integer id = delIdList.get(i);
						I18nTranslateEntity i18nTranslateEntity = I18N_TRANSLATE_MAP.remove(id);
						
						key1.setLangCode(i18nTranslateEntity.getLangCode());
						I18N_TRANSLATE_KEY1_MAP.remove(key1);
					}
				} else {
					for (int i=0,len_i=delIdList.size();i<len_i;i++) {
						Integer id = delIdList.get(i);
						I18nTranslateEntity i18nTranslateEntity = I18N_TRANSLATE_MAP.remove(id);

						key1.setLangCode(i18nTranslateEntity.getLangCode());
						I18N_TRANSLATE_KEY1_MAP.remove(key1);

						//用二分查找删除
						int j = binarySearch(i18nTranslateList2, id);
						//找到并删除
						if (j>-1) {
							i18nTranslateList2.remove(j);
						}
					}
				}
				I18N_TRANSLATE_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 二分查找
	 * @param i18nTranslateList
	 * @param id
	 */
	private int binarySearch(List<I18nTranslateEntity> i18nTranslateList,Integer id) {
		for (int j = i18nTranslateList.size()>>1,len_j = i18nTranslateList.size();;) {
			I18nTranslateEntity center = i18nTranslateList.get(j);
			int compareRet = id.compareTo(center.getId());
			if(compareRet == 0) {
				return j;
			} else if(compareRet>0) {
				int offset = (len_j-j)>>1;
				if(offset == 0) {
					break;
				}
				j += offset;
			} else if(compareRet<0) {
				int offset = (len_j-j)>>1;
				if(offset == 0) {
					break;
				}
				len_j = j;
				j -= offset;
			}
		}
		return -1;
	}
	
	/**
	 * 批量保存到缓存
	 * @param saveList
	 */
	private void saveBatchCache(String tableName,String col,Integer rowId,List<I18nTranslateEntity> saveList,List<Integer> delIdList) {
		for (;;) {
			if (I18N_TRANSLATE_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				I18nTranslateKey1 key1 = new I18nTranslateKey1();
				key1.setTableName(tableName);
				key1.setCol(col);
				key1.setRowId(rowId);
				
				I18nTranslateKey2 key2 = new I18nTranslateKey2();
				key2.setTableName(tableName);
				key2.setCol(col);
				key2.setRowId(rowId);
				List<I18nTranslateEntity> i18nTranslateList2 = I18N_TRANSLATE_KEY2_MAP.get(key2);
				if (i18nTranslateList2 == null) {
					I18N_TRANSLATE_KEY2_MAP.put(key2, i18nTranslateList2 = new ArrayList<>());
				}
				for (int i=0,len_i=delIdList.size();i<len_i;i++) {
					Integer id = delIdList.get(i);
					I18nTranslateEntity i18nTranslateEntity = I18N_TRANSLATE_MAP.remove(id);
					
					key1.setLangCode(i18nTranslateEntity.getLangCode());
					I18N_TRANSLATE_KEY1_MAP.remove(key1);

					if (!i18nTranslateList2.isEmpty()) {
						//用二分查找删除
						int j = binarySearch(i18nTranslateList2, id);
						if (j>-1) {
							i18nTranslateList2.remove(j);
						}
					}
				}
				
				if(i18nTranslateList2.isEmpty()) {
					for (int i=0,len_i = saveList.size();i<len_i;i++) {
						I18nTranslateEntity i18nTranslateEntity = saveList.get(i);
						I18N_TRANSLATE_MAP.put(i18nTranslateEntity.getId(), i18nTranslateEntity);
						
						key1 = new I18nTranslateKey1();
						key1.setTableName(tableName);
						key1.setCol(col);
						key1.setRowId(rowId);
						key1.setLangCode(i18nTranslateEntity.getLangCode());
						I18N_TRANSLATE_KEY1_MAP.put(key1, i18nTranslateEntity);
						
						i18nTranslateList2.add(i18nTranslateEntity);
					}
				} else {
					for (int i=0,len_i = saveList.size();i<len_i;i++) {
						I18nTranslateEntity i18nTranslateEntity = saveList.get(i);
						I18N_TRANSLATE_MAP.put(i18nTranslateEntity.getId(), i18nTranslateEntity);
						
						key1 = new I18nTranslateKey1();
						key1.setTableName(tableName);
						key1.setCol(col);
						key1.setRowId(rowId);
						key1.setLangCode(i18nTranslateEntity.getLangCode());
						I18N_TRANSLATE_KEY1_MAP.put(key1, i18nTranslateEntity);
						
						//用二分查找插入或更新数据
						Integer id = saveList.get(i).getId();
						for (int j = i18nTranslateList2.size()>>1,len_j = i18nTranslateList2.size();;) {
							I18nTranslateEntity center = i18nTranslateList2.get(j);
							int compareRet = id.compareTo(center.getId());
							//找到则更新缓存
							if(compareRet == 0) {
								center.setLangCode(i18nTranslateEntity.getLangCode());
								center.setVal(i18nTranslateEntity.getVal());
								break;
								//没有找到，并发现比缓存的 id 大
							} else if(compareRet > 0) {
								int offset = (len_j-j)>>1;
								//比所有缓存的数据的 id 都大，则直接追加
								if(offset == 0) {
									i18nTranslateList2.add(i18nTranslateEntity);
									break;
								} else {
									center = i18nTranslateList2.get(j+1);
									//尝试获取 j+1, 如果 id 比 j+1 的小，但是比 j 的大，则插入到 j 和 j+1 之间。
									if (id < center.getId()) {
										i18nTranslateList2.add(j+1, i18nTranslateEntity);
										break;
										//如果恰好找到，则更新
									} else if(id.equals(center.getId())) {
										center.setLangCode(i18nTranslateEntity.getLangCode());
										center.setVal(i18nTranslateEntity.getVal());
										break;
									}
									//其他情况则会按照二分查找继续查找合适的位置
								}
								j += offset;
							} else if(compareRet<0) {
								int offset = (len_j-j)>>1;
								if(offset == 0) {
									//比所有缓存的数据的 id 都小，则直接追加
									i18nTranslateList2.add(0, i18nTranslateEntity);
									break;
								} else {
									center = i18nTranslateList2.get(j - 1);
									//尝试获取 j-1, 如果 id 比 j-1 的大，但比 j 的小，则插入到 j-1 和 j 之间
									if (id > center.getId()) {
										i18nTranslateList2.add(j, i18nTranslateEntity);
										break;
										//如果恰好找到，则更新
									} else if(id.equals(center.getId())) {
										center.setLangCode(i18nTranslateEntity.getLangCode());
										center.setVal(i18nTranslateEntity.getVal());
										break;
									}
									//其他情况则会按照二分查找继续查找合适的位置
								}
								len_j = j;
								j -= offset;
							}
						}
					}	
				}
				I18N_TRANSLATE_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}

	@Data
	@EqualsAndHashCode
	private static class I18nTranslateKey2 {
		/**
		 * 表名
		 */
		private String tableName;
		/**
		 * 列名
		 */
		private String col;
		/**
		 * 行主键id
		 */
		private Integer rowId;
	}
	
	@Data
	@EqualsAndHashCode
	private static class I18nTranslateKey1 {
		/**
		 * 表名
		 */
		private String tableName;
		/**
		 * 列名
		 */
		private String col;
		/**
		 * 行主键id
		 */
		private Integer rowId;
		/**
		 * 语言标识码
		 */
		private String langCode;
	}
}