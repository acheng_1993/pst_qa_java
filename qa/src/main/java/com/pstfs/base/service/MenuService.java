package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.I18nTranslateEntity;
import com.pstfs.base.entity.MenuEntity;
import com.pstfs.base.entity.MenuUrlEntity;
import com.pstfs.base.entity.RequestUrlEntity;
import com.pstfs.base.entity.RoleEntity;
import com.pstfs.base.envm.MenuType;
import com.pstfs.base.envm.MoveType;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.extend.RequestUrlExtend;
import com.pstfs.base.mapper.MenuMapper;
import com.pstfs.base.mapper.RequestUrlMapper;
import com.pstfs.base.utils.CollectionSubOper;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.validate.DataValidateUtils;

import cn.hutool.core.collection.CollUtil;

@Service("menuService")
public class MenuService extends AbstractTreeMoveService<MenuMapper, MenuEntity> {

	@Autowired
	private RequestUrlService requestUrlService;
	
	@Autowired
	private MenuUrlService menuUrlService;

	@Lazy
	@Autowired
	private UserService userService;
	
	@Lazy
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private I18nTranslateService i18nTranslateService;
	
	/**
	 * 根据主键id删除菜单
	 * @param id
	 * @return
	 */
	@Transactional
	public boolean removeById(Integer id) {
		QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("parent_id", id);
		queryWrapper.select("id");
		if(getOne(queryWrapper) != null) {
			throw new RRException(i.t("MenuService.removeById.childNodeExists"));//该菜单存在下级菜单，删除失败！
		}
		boolean ret = super.removeById(id);
		UpdateWrapper<MenuUrlEntity> updateWrapper2 = new UpdateWrapper<>();
		updateWrapper2.eq("menu_id", id);
		menuUrlService.remove(updateWrapper2);
		if (ret) {
			//删除翻译表内的数据
			if(i18nTranslateService.remove("menu", "menu_desc", id)) {
				removeCache(id);
			}
		}
		return ret;
	}
	
	/**
	 * 菜单是否存在
	 * @param menuName
	 * @return
	 */
	public boolean menuExists(Integer id) {
		QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("id", id);
		queryWrapper.select("id");
		return getOne(queryWrapper)!=null;
	}
	
	/**
	 * 更新菜单
	 * @param id
	 * @param menuName
	 * @param menuDesc
	 * @param urlList
	 * @param i18nTranslateList
	 * @return
	 */
	@Transactional
	public boolean update(Integer id,String menuName,String menuDesc,List<String> urlList,List<I18nTranslateEntity> i18nTranslateList) {
		MenuEntity updateEntity = new MenuEntity();
		updateEntity.setId(id);
		updateEntity.setMenuName(menuName);
		updateEntity.setMenuDesc(menuDesc);
		DataValidateUtils.validate(
		MapUtils.hms(
		"id.require",i.t("MenuService.validate.id.require"),//主键id不能为空
		"menuName.require",i.t("MenuService.validate.menuName.require"),//菜单名称不能为空
		"menuName.max", i.t("MenuService.validate.menuName.max"),//菜单名称长度不能超过{0}个字符
		"menuName.regex",i.t("MenuService.validate.menuName.regex"),//菜单名称只能是数字、字母、下划线组合
		"menuDesc.require", i.t("MenuService.validate.menuDesc.require"),//菜单功能描述不能为空
		"menuDesc.max", i.t("MenuService.validate.menuDesc.max")//菜单功能描述长度不能超过{0}个字符
		),
		updateEntity,
		"id","require",
		"menuName","require|max:25|regex:'^\\w+$'",
		"menuDesc","require|max:255"
		);
		if (i18nTranslateList!=null && !i18nTranslateList.isEmpty()) {
			i18nTranslateService.validate(i18nTranslateList);
		}
		if (!urlList.isEmpty()) {
			urlList = new ArrayList<String>(new HashSet<String>(urlList));
		}
		{
			QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("menu_name", menuName);
			queryWrapper.ne("id", id);
			queryWrapper.select("id");
			if (getOne(queryWrapper) != null) {
				throw new RRException(i.t("MenuService.update.sameNameMenuExists"));//存在同名的菜单，更新失败！
			}
		}
		{
			QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("id", id);
			queryWrapper.select("type");
			MenuEntity menuEntity = getOne(queryWrapper);
			if (menuEntity == null) {
				throw new RRException(i.t("MenuService.update.menuNotExists"));//菜单不存在，更新失败！
			}
			if (menuEntity.getType() == MenuType.目录.getV()) {
				urlList.clear();
			} else {
				DataValidateUtils.validate(
				MapUtils.hms(
				"urlList.uri", i.t("MenuService.validate.urlList.uri")//url格式错误
				),
				MapUtils.hmo("urlList",urlList),
				"urlList","uri"
				);
			}
		}
		for(int i = urlList.size() - 1;i > -1;i--) {
			if (StringUtils.isEmpty(urlList.get(i))) {
				urlList.remove(i);
			}
		}
		if (!urlList.isEmpty()) {
			//如果发现有新的url，则录入到数据库
			List<RequestUrlEntity> requestUrlEntityListFromDb = requestUrlService.saveBatch(urlList);
			//更改菜单和 url 的引用关系
			if (!requestUrlEntityListFromDb.isEmpty()) {
				List<MenuUrlEntity> menuUrlList = new ArrayList<>(requestUrlEntityListFromDb.size());
				for(int i=0,len_i=requestUrlEntityListFromDb.size();i<len_i;i++) {
					MenuUrlEntity menuUrlEntity = new MenuUrlEntity();
					menuUrlEntity.setUrlId(requestUrlEntityListFromDb.get(i).getId());
					menuUrlEntity.setMenuId(id);
					menuUrlList.add(menuUrlEntity);
				}
				QueryWrapper<MenuUrlEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("menu_id", id);
				List<MenuUrlEntity> menuUrlListFromDb = menuUrlService.list(queryWrapper);
				List<MenuUrlEntity> saveMenuUrlList = CollectionSubOper.sub(menuUrlList, menuUrlListFromDb, "getMenuId","getUrlId");
				if (!saveMenuUrlList.isEmpty()) {
					menuUrlService.saveBatch(saveMenuUrlList);
				}
				List<MenuUrlEntity> delMenuUrlList = CollectionSubOper.sub(menuUrlListFromDb,menuUrlList,"getMenuId","getUrlId");
				if (!delMenuUrlList.isEmpty()) {
					List<Integer> delMenuUrlIdList = new ArrayList<>(delMenuUrlList.size());
					for(int i=0,len_i=delMenuUrlList.size();i<len_i;i++) {
						delMenuUrlIdList.add(delMenuUrlList.get(i).getId());
					}
					menuUrlService.removeByIds(delMenuUrlIdList);
				}
			}
		}
		boolean ret = updateById(updateEntity);
		if (ret) {
			boolean ret2 = true;
			if (i18nTranslateList!=null && !i18nTranslateList.isEmpty()) {
				ret2 = i18nTranslateService.saveBatch("menu", "menu_desc", id, i18nTranslateList);
			}
			if (ret2) {
				QueryWrapper<RequestUrlExtend> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("mu.menu_id", id);
				queryWrapper.select("ru.url_index");
				List<RequestUrlExtend> requestUrlExtendList = requestUrlMapper.listExtend1(queryWrapper);
				updateCache(id, menuName, requestUrlExtendList);
			}
		}
		return ret;
	}

	/**
	 * 新增菜单
	 * @param menuEntity
	 * @return
	 */
	@Transactional
	public boolean save(String menuName,Integer type,Integer parentId,String menuDesc,List<String> urlList,List<I18nTranslateEntity> i18nTranslateList) {
		DataValidateUtils.validate(
		MapUtils.hms(
		"menuName.require",i.t("MenuService.validate.menuName.require"),//菜单名称不能为空
		"menuName.max",i.t("MenuService.validate.menuName.max"),//菜单名称长度不能超过{0}个字符
		"menuName.regex",i.t("MenuService.validate.menuName.regex"),//菜单名称只能是数字、字母、下划线组合
		"type.require", i.t("MenuService.validate.type.require"),//菜单类型不能为空
		"type.in",i.t("MenuService.validate.type.in"),//菜单类型错误
		"menuDesc.require",i.t("MenuService.validate.menuDesc.require"),//菜单功能描述不能为空
		"menuDesc.max",i.t("MenuService.validate.menuDesc.max")//菜单功能描述长度不能超过{0}个字符"
		),
		MapUtils.hmo(
			"menuName",menuName,
			"type",type,
			"menuDesc",menuDesc
		),
		"menuName","require|max:25|regex:'^\\w+$'",
		"type","require|in:0,1,2",
		"menuDesc","require|max:255"
		);
		if (i18nTranslateList!=null && !i18nTranslateList.isEmpty()) {
			i18nTranslateService.validate(i18nTranslateList);
		}
		{
			QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("menu_name", menuName);
			queryWrapper.select("id");
			if (getOne(queryWrapper)!=null) {
				throw new RRException(i.t("MenuService.save.sameNameMenuExists"));//菜单名已存在，新增失败！
			}
		}
		MenuEntity saveEntity = new MenuEntity();
		saveEntity.setMenuName(menuName);
		saveEntity.setType(type);
		saveEntity.setParentId(parentId);
		saveEntity.setMenuDesc(menuDesc);
		if (!urlList.isEmpty()) {
			urlList = new ArrayList<String>(new HashSet<String>(urlList));
		}
		if (saveEntity.getParentId() == null) {
			if (saveEntity.getType() == MenuType.目录.getV()) {
				urlList.clear();
			} else {
				throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！
			}
			QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.isNull("parent_id");
			queryWrapper.select("max(sort_index) sort_index");
			MenuEntity tempEntity = getOne(queryWrapper);
			if (tempEntity == null) {
				saveEntity.setSortIndex(0);
			} else {
				saveEntity.setSortIndex(tempEntity.getSortIndex() + 1);
			}
		} else {
			if (saveEntity.getType() == MenuType.目录.getV()) {
				urlList.clear();
			} else {
				DataValidateUtils.validate(
				MapUtils.hms(
				"urlList.uri",i.t("MenuService.validate.urlList.uri")//url格式错误
				),
				MapUtils.hmo("urlList", urlList),
				"urlList","uri"
				);
			}
			QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("id", saveEntity.getParentId());
			queryWrapper.select("sort_index");
			MenuEntity tempEntity = getOne(queryWrapper);
			if (tempEntity == null) {
				throw new RRException(i.t("MenuService.save.parentNodeNotExists"));//上级节点不存在，保存失败！
			} else {
				saveEntity.setSortIndex(tempEntity.getSortIndex() + 1);
			}
		}
		boolean ret;
		synchronized (this) {
			saveEntity.setMenuIndex(getNextIndex());
			ret = save(saveEntity);
		}
		for(int i = urlList.size() - 1;i > -1;i--) {
			if (StringUtils.isEmpty(urlList.get(i))) {
				urlList.remove(i);
			}
		}
		List<RequestUrlEntity> requestUrlEntityListFromDb = null;
		if(!urlList.isEmpty()) {
			//如果发现有新的url，则录入到数据库
			requestUrlEntityListFromDb = requestUrlService.saveBatch(urlList);
			//保存菜单和 url 的引用关系
			if(!requestUrlEntityListFromDb.isEmpty()) {
				List<MenuUrlEntity> saveMenuUrlList = new ArrayList<>(requestUrlEntityListFromDb.size());
				for(int i=0,len_i=requestUrlEntityListFromDb.size();i<len_i;i++) {
					MenuUrlEntity menuUrlEntity = new MenuUrlEntity();
					menuUrlEntity.setUrlId(requestUrlEntityListFromDb.get(i).getId());
					menuUrlEntity.setMenuId(saveEntity.getId());
					saveMenuUrlList.add(menuUrlEntity);
				}
				menuUrlService.saveBatch(saveMenuUrlList);
			}
		}
		if(ret) {
			boolean ret2 = true;
			if (i18nTranslateList!=null && !i18nTranslateList.isEmpty()) {
				ret2 = i18nTranslateService.saveBatch("menu", "menu_desc", saveEntity.getId(), i18nTranslateList);
			}
			if(ret2) {
				saveCache(saveEntity.getId(),saveEntity.getMenuIndex(),saveEntity.getSortIndex(), menuName, menuDesc, type, parentId, requestUrlEntityListFromDb);
			}
		}
		return ret;
	}

	/**
	 * 获取下一个下标
	 * @return
	 */
	private synchronized Integer getNextIndex() {
		QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("menu_index");
		List<MenuEntity> menuList = list(queryWrapper);
		if (menuList.isEmpty()) {
			return 0;
		} else {
			List<Integer> menuIndexList = new ArrayList<>();
			for (int i=0,len_i = menuList.size();i < len_i;i++) {
				menuIndexList.add(menuList.get(i).getMenuIndex());
			}
			Collections.sort(menuIndexList);
			for (int i=0,len_i = menuIndexList.size();i<len_i;i++) {
				if (menuIndexList.get(i).intValue() != i) {
					return i;
				}
			}
			return menuIndexList.size();
		}
	}
	
	/**
	 * 获取符合条件的菜单列表
	 * @param params
	 * @param field
	 * @return
	 */
	public List<MenuEntity> list(Map<String,Object> params,String field) {
		return list(params,field,0);
	}
	
	/**
	 * 获取符合条件的菜单列表
	 * @param params
	 * @param field
	 * @param limit
	 * @return
	 */
	public List<MenuEntity> list(Map<String,Object> params,String field,Integer limit) {
		QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
		if(params!=null) {
			Object roleId = params.get("roleId");
			if (roleId !=null) {
				queryWrapper = queryWrapper.inSql("id", "select menu_id from role_menu where role_id = "+roleId);
			}
			List<Integer> roleIndexList = (List<Integer>)params.get("roleIndexList");
			if(roleIndexList != null && !roleIndexList.isEmpty()) {
				queryWrapper = queryWrapper.inSql("id", "select menu_id from role_menu where role_id in"+
						"(select id from role where role_index in("+CollUtil.join(roleIndexList, ",")+"))");
			}
			Object type = params.get("type");
			if(type!=null) {
				queryWrapper = queryWrapper.eq("type", type);
			}
			String menuDescLike = (String)params.get("menuDescLike");
			if (StringUtils.isNotEmpty(menuDescLike)) {
				queryWrapper = queryWrapper.like("menu_desc", menuDescLike+"%");
			}
		}
		queryWrapper.select(field);
		if (limit > 0) {
			queryWrapper.last(" limit "+limit);
		}
		return list(queryWrapper);
	}

	@Override
	protected MenuEntity createEntity() {
		return new MenuEntity();
	}

	/**
	 * 检测是否可以移动
	 */
	@Override
	protected boolean moveAble(int id, int targetId, int moveType) {
		if(id == targetId) {
			return false;
		}
		QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("id", id, targetId);
		queryWrapper.select("id,menu_name,type,parent_id");
		List<MenuEntity> tempList = list(queryWrapper);
		if (tempList.size() != 2) {
			return false;
		}
		MenuEntity currentMenu = null;
		MenuEntity targetMenu = null;
		for(int i = 0,len_i=tempList.size();i<len_i;i++) {
			MenuEntity menuEntity = tempList.get(i);
			if(menuEntity.getId() == id) {
				currentMenu = menuEntity;
			} else if(menuEntity.getId() == targetId) {
				targetMenu = menuEntity;
			}
		}
		if (moveType == MoveType.里面.getV()) {
			if (currentMenu.getType() == MenuType.菜单.getV() && targetMenu.getType() != MenuType.目录.getV()) {
				return false;
			} else if(currentMenu.getType() == MenuType.表单.getV() && targetMenu.getType() != MenuType.菜单.getV()) {
				return false;
			}
		} else if(moveType == MoveType.前面.getV() || moveType == MoveType.后面.getV()) {
			if (currentMenu.getType() != targetMenu.getType()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 根据角色下标获取对应的菜单列表
	 * @param roleIndexList
	 * @return
	 */
	public List<MenuEntity> getMenuListByRoleIndex(List<Integer> roleIndexList) {
		List<MenuEntity> ret = new ArrayList<>();
		HashSet<Integer> menuIdSet = roleService.getMenuIdListByRoleIndex(roleIndexList);
		for(;;) {
			if(RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				for(Integer menuId:menuIdSet) {
					MenuEntity menuEntity = MENU_ID_MAP.get(menuId);
					if(menuEntity != null) {
						MenuEntity newEntity = new MenuEntity();
						newEntity.setId(menuEntity.getId());
						newEntity.setMenuName(menuEntity.getMenuName());
						newEntity.setCreateTime(menuEntity.getCreateTime());
						newEntity.setUpdateTime(menuEntity.getUpdateTime());
						newEntity.setMenuIndex(menuEntity.getMenuIndex());
						newEntity.setType(menuEntity.getType());
						newEntity.setParentId(menuEntity.getParentId());
						newEntity.setSortIndex(menuEntity.getSortIndex());
						newEntity.setMenuDesc(menuEntity.getMenuDesc());
						ret.add(newEntity);
					}
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
		return ret;
	}
	
	/**
	 * 所有菜单的映射表
	 */
	private final static Map<Integer,MenuEntity> MENU_ID_MAP = new HashMap<>();
	
	/**
	 * 所有菜单权限位映射表
	 */
	private final static Map<Integer,MenuEntity> MENU_IDX_MAP = new HashMap<>();
	
	/**
	 * 所有菜单持有的url映射表
	 */
	final static Map<Integer, HashSet<Integer>> MENU_ID_URL_MAP = new HashMap<>();

	@Autowired
	private RequestUrlMapper requestUrlMapper;

	/**
	 * initMenuList 是否调用完毕状态
	 */
	private boolean initFinish = false;
	
	
	public boolean isInitFinish() {
		return initFinish;
	}

	@PostConstruct
	private void initMenuList() {
		QueryWrapper<MenuEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("id,menu_name,menu_desc,menu_index,type,parent_id,sort_index");
		List<MenuEntity> menuList = list(queryWrapper);

		QueryWrapper<RequestUrlExtend> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.select("ru.id,ru.url,ru.url_index,mu.menu_id");
		queryWrapper2.apply("0 = 0");
		List<RequestUrlExtend> requestUrlExtendList = requestUrlMapper.listExtend1(queryWrapper2);

		for(int i=0,len_i=menuList.size();i<len_i;i++) {
			MenuEntity menuEntity = menuList.get(i);
			MENU_ID_MAP.put(menuEntity.getId(), menuEntity);
			MENU_IDX_MAP.put(menuEntity.getMenuIndex(), menuEntity);
		}

		for (int i=0,len_i = requestUrlExtendList.size();i<len_i;i++) {
			RequestUrlExtend requestUrlExtend = requestUrlExtendList.get(i);
			HashSet<Integer> requestUrlSet = MENU_ID_URL_MAP.get(requestUrlExtend.getMenuId());
			if (requestUrlSet == null) {
				MENU_ID_URL_MAP.put(requestUrlExtend.getMenuId(), requestUrlSet = new HashSet<Integer>());
			}
			requestUrlSet.add(requestUrlExtend.getUrlIndex());
		}

		initFinish = true;
	}
	
	
	/**
	 * 删除缓存内容
	 * @param id
	 */
	private void removeCache(Integer id) {
		for(;;) {
			if(RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				MENU_ID_URL_MAP.remove(id);
				MenuEntity menuEntity = MENU_ID_MAP.remove(id);
				if (menuEntity!=null) {
					MENU_IDX_MAP.remove(menuEntity.getMenuIndex());
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 更新缓存内容
	 * @param id
	 * @param menuName
	 */
	private void updateCache(Integer id,String menuName,List<RequestUrlExtend> requestUrlExtendList) {
		for (;;) {
			if(RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				MenuEntity menuEntity = MENU_ID_MAP.get(id);
				if(menuEntity!=null) {
					menuEntity.setMenuName(menuName);
				}
				if (requestUrlExtendList != null) {
					HashSet<Integer> requestUrlSet = new HashSet<>();
					for (int i=0,len_i = requestUrlExtendList.size();i<len_i;i++) {
						requestUrlSet.add(requestUrlExtendList.get(i).getUrlIndex());
					}
					MENU_ID_URL_MAP.put(id, requestUrlSet);
				}
				Map<Integer,HashSet<Integer>> roleMenuIdMap = RoleService.ROLE_MENU_ID_MAP;
				Map<Integer, byte[]> roleIdxUrlMap = RoleService.ROLE_IDX_URL_MAP;
				Map<Integer,HashSet<Integer>> buttonIdUrlMap = ButtonService.BUTTON_ID_URL_MAP;
				
				HashSet<Integer> menuIdSet;
				for (Entry<Integer,HashSet<Integer>> entry:roleMenuIdMap.entrySet()) {
					menuIdSet = entry.getValue();
					if (menuIdSet.contains(id)) {
						byte[] urlByte = new byte[125];
						for(Integer menuId:menuIdSet) {
							HashSet<Integer> menuIdUrlSet = MENU_ID_URL_MAP.get(menuId);
							//部分菜单属于目录，没有关联 url
							if(menuIdUrlSet!=null) {
								for(Integer urlIdx:menuIdUrlSet) {
									int idx2 = (urlIdx + 1)%8; //获取余数位
									int idx1;
									if (idx2 == 0) { //没有余数位
										idx1 = (urlIdx + 1)/8 - 1; //获取下标
										urlByte[idx1] |= 1 << 7;
									} else { // 有余数位
										idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
										idx2 -= 1;
										urlByte[idx1] |= 1 << idx2;
									}
								}
							}
						}
						Map<Integer,HashSet<Integer>> roleButtonIdMap = RoleService.ROLE_BUTTON_ID_MAP;
						HashSet<Integer> buttonIdSet = roleButtonIdMap.get(entry.getKey());
						for(Integer buttonId:buttonIdSet) {
							HashSet<Integer> buttonIdUrlSet = buttonIdUrlMap.get(buttonId);
							for(Integer urlIdx:buttonIdUrlSet) {
								int idx2 = (urlIdx + 1)%8; //获取余数位
								int idx1;
								if (idx2 == 0) { //没有余数位
									idx1 = (urlIdx + 1)/8 - 1; //获取下标
									urlByte[idx1] |= 1 << 7;
								} else { // 有余数位
									idx1 = (urlIdx + 1 - idx2)/8 - 1 + 1; //获取下标 
									idx2 -= 1;
									urlByte[idx1] |= 1 << idx2;
								}
							}
						}
						RoleEntity roleEntity = RoleService.ROLE_ID_MAP.get(entry.getKey());
						roleIdxUrlMap.put(roleEntity.getRoleIndex(), urlByte);
						break;
					}
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 新增缓存
	 * @param id
	 * @param sortIndex
	 * @param menuName
	 * @param type
	 * @param parentId
	 */
	private void saveCache(Integer id,Integer menuIndex,Integer sortIndex, String menuName,String menuDesc,Integer type,Integer parentId,List<RequestUrlEntity> requestUrlEntityList) {
		MenuEntity menuEntity = new MenuEntity();
		menuEntity.setId(id);
		menuEntity.setMenuIndex(menuIndex);
		menuEntity.setSortIndex(sortIndex);
		menuEntity.setMenuName(menuName);
		menuEntity.setMenuDesc(menuDesc);
		menuEntity.setType(type);
		menuEntity.setParentId(parentId);
		for (;;) {
			if (RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				MENU_ID_MAP.put(id, menuEntity);
				MENU_IDX_MAP.put(menuIndex, menuEntity);
				if (requestUrlEntityList!=null) {
					HashSet<Integer> requestUrlSet = new HashSet<>();
					for (int i=0,len_i = requestUrlEntityList.size();i<len_i;i++) {
						requestUrlSet.add(requestUrlEntityList.get(i).getUrlIndex());
					}
					MENU_ID_URL_MAP.put(id, requestUrlSet);
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}

	@Override
	protected void moveCache(MenuEntity data, List<MenuEntity> saveList) {
		for (;;) {
			if(RoleService.ROLE_MENU_BUTTON_LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				MenuEntity entity = MENU_ID_MAP.get(data.getId());
				if(entity != null) {
					entity.setSortIndex(data.getSortIndex());
					entity.setParentId(data.getParentId());
					for (int i=0,len_i = saveList.size();i<len_i;i++) {
						MenuEntity menuEntity = saveList.get(i);
						entity = MENU_ID_MAP.get(menuEntity.getId());
						if (entity != null) {
							entity.setSortIndex(menuEntity.getSortIndex());
						}
					}
				}
				RoleService.ROLE_MENU_BUTTON_LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
}