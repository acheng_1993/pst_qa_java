package com.pstfs.base.service;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.MstRoutingStepAllEntity;
import com.pstfs.base.mapper.MstRoutingStepAllMapper;

@Service("mstRoutingStepAllService")
@DS("new_mes")
public class MstRoutingStepAllService extends ServiceImpl<MstRoutingStepAllMapper, MstRoutingStepAllEntity> {
	
	/**
	 * 检测品番是否存在
	 * @param itmCode
	 * @return
	 */
	public boolean checkItmCodeExists(String itmCode) {
		QueryWrapper<MstRoutingStepAllEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("itm_code", itmCode);
		queryWrapper.select("id");
		queryWrapper.last(" limit 1");
		return getOne(queryWrapper)!=null;
	}
	
	/**
	 * 获取机种品番
	 * @param params
	 * @param field
	 * @param limit
	 * @return
	 */
	public List<MstRoutingStepAllEntity> list(Map<String,Object> params,String field,Integer limit) {
		QueryWrapper<MstRoutingStepAllEntity> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			String itmCodeLike = (String)params.get("itmCodeLike");
			if (StringUtils.isNotEmpty(itmCodeLike)) {
				queryWrapper.like("itm_code", itmCodeLike+"%");
			}
		}
		queryWrapper.select(field);
		if (limit>0) {
			queryWrapper.last(" limit "+limit);
		}
		return list(queryWrapper);
	}
}