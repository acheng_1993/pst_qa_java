package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.EnumTableEntity;
import com.pstfs.base.entity.GrievanceMgrEntity;
import com.pstfs.base.entity.GrievanceMgrMsgAcceptEntity;
import com.pstfs.base.entity.GrievanceMgrPartsDetailsEntity;
import com.pstfs.base.entity.GrievanceMgrUploadFileEntity;
import com.pstfs.base.entity.MgmNoAllocationEntity;
import com.pstfs.base.entity.UploadFileEntity;
import com.pstfs.base.envm.GrievanceMgrUploadFileType;
import com.pstfs.base.envm.Status;
import com.pstfs.base.envm.YesOrNo;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.mapper.GrievanceMgrMapper;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.validate.DataValidateFactory;
import com.pstfs.base.validate.DataValidateUtils;
import com.pstfs.base.validate.DataValidateUtils.IDataValidRule;

/**
 * 客户投诉管理业务类
 * @author jiajian
 */
@Service("grievanceMgrService")
public class GrievanceMgrService extends BaseService<GrievanceMgrMapper, GrievanceMgrEntity> {

	/**
	 * 把数据复制一份，全部复制
	 * @param username
	 * @param id
	 * @return
	 */
	@Transactional
	public boolean copy(String username,Integer id) {
		GrievanceMgrEntity grievanceMgrEntity = getById(id);
		grievanceMgrEntity.setId(null);
		grievanceMgrEntity.setVersion(0);
		String mgmNo = mgmNoAllocationService.createNewMgmNo(username, grievanceMgrEntity.getFactoryType());
		grievanceMgrEntity.setMgmNo(mgmNo);
		grievanceMgrEntity.setCreatorUsername(username);
		grievanceMgrEntity.setEditorUsername(username);
		grievanceMgrEntity.setStatus(Status.启用.getV());
		boolean ret = save(grievanceMgrEntity);
		//把分配的编号标记为已使用
		if (ret) {
			UpdateWrapper<MgmNoAllocationEntity> updateWrapper = new UpdateWrapper<>();
			updateWrapper.eq("mgm_no", mgmNo);
			updateWrapper.set("used", YesOrNo.是.getV());
			mgmNoAllocationService.update(updateWrapper);
			int copyId = grievanceMgrEntity.getId();
			{
				QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("grievance_mgr_id", id);
				queryWrapper.select("component_pos,part_num,maker_name");
				List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(queryWrapper);
				if (!grievanceMgrPartsDetailsList.isEmpty()) {
					for (int i = 0,len_i= grievanceMgrPartsDetailsList.size();i<len_i;i++) {
						grievanceMgrPartsDetailsList.get(i).setGrievanceMgrId(copyId);
					}
					grievanceMgrPartsDetailsService.saveBatch(grievanceMgrPartsDetailsList);
				}
			}
			{
				QueryWrapper<GrievanceMgrUploadFileEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("grievance_mgr_id", id);
				queryWrapper.select("file_id,src_name,type");
				List<GrievanceMgrUploadFileEntity> grievanceMgrUploadFileList = grievanceMgrUploadFileService.list(queryWrapper);
				if (!grievanceMgrUploadFileList.isEmpty()) {
					for (int i = 0,len_i= grievanceMgrUploadFileList.size();i<len_i;i++) {
						grievanceMgrUploadFileList.get(i).setGrievanceMgrId(copyId);
					}
					grievanceMgrUploadFileService.saveBatch(grievanceMgrUploadFileList);
				}
			}
		}
		return ret;
	}
	
	@Autowired
	private GrievanceMgrUploadFileService grievanceMgrUploadFileService;

	@Autowired
	private GrievanceMgrMsgAcceptService grievanceMgrMsgAcceptService;
	
	/**
	 * 复制部分数据
	 * @param username
	 * @param id
	 * @return
	 */
	@Transactional
	public boolean copy(String username,Integer id,List<String> copyFieldList) {
		//允许可选的复制字段
		HashSet<String> allowCopyFieldSet = new HashSet<>();
		allowCopyFieldSet.add("customerFactory");
		allowCopyFieldSet.add("customerControlNo");
		allowCopyFieldSet.add("designatedAnswerDay");
		allowCopyFieldSet.add("lotLotNo2");
		allowCopyFieldSet.add("prodDay");
		allowCopyFieldSet.add("analysisRet");
		allowCopyFieldSet.add("factorsDetails");
		allowCopyFieldSet.add("componentPos");
		allowCopyFieldSet.add("partNum");
		allowCopyFieldSet.add("makerName");
		allowCopyFieldSet.add("anAnalysisReqBookFileId");
		allowCopyFieldSet.add("componentAnalysisRetFileId");
		allowCopyFieldSet.add("glamourPhotoFileId");
		allowCopyFieldSet.add("aqualityAbnormalityBookFileId");
		allowCopyFieldSet.add("oneAnsDay");
		allowCopyFieldSet.add("oneRespContent");
		allowCopyFieldSet.add("intermediateAnsDay");
		allowCopyFieldSet.add("intermediateAnsAttr");
		allowCopyFieldSet.add("finalRespDate");
		allowCopyFieldSet.add("finalAnsAttr");
		allowCopyFieldSet.add("custCompletDate");
		allowCopyFieldSet.add("afinalAnsBookFileId");
		allowCopyFieldSet.add("personInCharge");

		HashSet<String> copyFieldSet = new HashSet<>(copyFieldList);
		if (!allowCopyFieldSet.containsAll(copyFieldSet)) {
			throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！
		}
		GrievanceMgrEntity srcEntity = getById(id);

		GrievanceMgrEntity copyEntity = new GrievanceMgrEntity();
		copyEntity.setFactoryType(srcEntity.getFactoryType());
		copyEntity.setComplaintType(srcEntity.getComplaintType());
		copyEntity.setReceptionDate(srcEntity.getReceptionDate());
		copyEntity.setDemandReplyDate(srcEntity.getDemandReplyDate());
		copyEntity.setProdNum(srcEntity.getProdNum());
		copyEntity.setCustomerService(srcEntity.getCustomerService());
		copyEntity.setCompanyName(srcEntity.getCompanyName());
		copyEntity.setLocation(srcEntity.getLocation());
		copyEntity.setFailureDay(srcEntity.getFailureDay());
		copyEntity.setBadContent(srcEntity.getBadContent());
		copyEntity.setBadContentsDetails(srcEntity.getBadContentsDetails());
		copyEntity.setProdPlace(srcEntity.getProdPlace());
		copyEntity.setProdDivision(srcEntity.getProdDivision());
		copyEntity.setLotLotNo(srcEntity.getLotLotNo());
		copyEntity.setImportanceRank(srcEntity.getImportanceRank());
		copyEntity.setCause(srcEntity.getCause());
		copyEntity.setRespParty(srcEntity.getRespParty());
		copyEntity.setCreatorUsername(username);
		copyEntity.setEditorUsername(username);
		copyEntity.setStatus(Status.启用.getV());
		copyEntity.setVersion(0);

		if(!"解析中".equals(srcEntity.getCause())) {
			copyEntity.setAnalysisRet(srcEntity.getAnalysisRet());
		}
		List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList = null;
		if("部品".equals(srcEntity.getCause()) ||
		"部品破壊".equals(srcEntity.getCause()) ||
		"半導体破壊".equals(srcEntity.getCause())) {
			QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("grievance_mgr_id", id);
			queryWrapper.select("component_pos,part_num,maker_name");
			grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(queryWrapper);
		}
		List<Integer> copyUploadFileTypeList = new ArrayList<>();
		for(String copyField:copyFieldSet) {
			if("customerFactory".equals(copyField)) {
				copyEntity.setCustomerFactory(srcEntity.getCustomerFactory());
			} else if("customerControlNo".equals(copyField)) {
				copyEntity.setCustomerControlNo(srcEntity.getCustomerControlNo());
			} else if("designatedAnswerDay".equals(copyField)) {
				copyEntity.setDesignatedAnswerDay(srcEntity.getDesignatedAnswerDay());
			} else if("lotLotNo2".equals(copyField)) {
				copyEntity.setLotLotNo2(srcEntity.getLotLotNo2());
			} else if("prodDay".equals(copyField)) {
				copyEntity.setProdDay(srcEntity.getProdDay());
			} else if("analysisRet".equals(copyField)) {
				copyEntity.setAnalysisRet(srcEntity.getAnalysisRet());
			} else if("factorsDetails".equals(copyField)) {
				copyEntity.setFactorsDetails(srcEntity.getFactorsDetails());
			} else if("componentPos".equals(copyField) || "partNum".equals(copyField) || "makerName".equals(copyField)) {
				if (grievanceMgrPartsDetailsList == null) {
					QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
					queryWrapper.eq("grievance_mgr_id", id);
					queryWrapper.select("component_pos,part_num,maker_name");
					grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(queryWrapper);
				}
			} else if("oneAnsDay".equals(copyField)) {
				copyEntity.setOneAnsDay(srcEntity.getOneAnsDay());
				copyEntity.setOneRespContent(srcEntity.getOneRespContent());
			} else if("intermediateAnsDay".equals(copyField)) {
				copyEntity.setIntermediateAnsDay(srcEntity.getIntermediateAnsDay());
				copyEntity.setIntermediateAnsAttr(srcEntity.getIntermediateAnsAttr());
			} else if("finalRespDate".equals(copyField)) {
				copyEntity.setFinalRespDate(srcEntity.getFinalRespDate());
				copyEntity.setFinalAnsAttr(srcEntity.getFinalAnsAttr());
			} else if("custCompletDate".equals(copyField)) {
				copyEntity.setCustCompletDate(srcEntity.getCustCompletDate());
			} else if("personInCharge".equals(copyField)) {
				copyEntity.setPersonInCharge(srcEntity.getPersonInCharge());
			//需要复制的文件类型
			} else if("anAnalysisReqBookFileId".equals(copyField)) { 
				copyUploadFileTypeList.add(GrievanceMgrUploadFileType.解析依頼書.getV());
			} else if("componentAnalysisRetFileId".equals(copyField)) {
				copyUploadFileTypeList.add(GrievanceMgrUploadFileType.部品解析結果.getV());
			} else if("glamourPhotoFileId".equals(copyField)) {
				copyUploadFileTypeList.add(GrievanceMgrUploadFileType.不具合写真.getV());
			} else if("aqualityAbnormalityBookFileId".equals(copyField)) {
				copyUploadFileTypeList.add(GrievanceMgrUploadFileType.品質異常書.getV());
			} else if("afinalAnsBookFileId".equals(copyField)) {
				copyUploadFileTypeList.add(GrievanceMgrUploadFileType.最終回答書.getV());
			}
		}
		List<GrievanceMgrUploadFileEntity> copyGrievanceMgrUploadFileList = null;
		if(!grievanceMgrPartsDetailsList.isEmpty()) {
			QueryWrapper<GrievanceMgrUploadFileEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.select("file_id,src_name,type");
			queryWrapper.in("type", grievanceMgrPartsDetailsList);
			queryWrapper.eq("grievance_mgr_id", srcEntity.getId());
			copyGrievanceMgrUploadFileList = grievanceMgrUploadFileService.list(queryWrapper);
		}
		
		String mgmNo = mgmNoAllocationService.createNewMgmNo(username, srcEntity.getFactoryType());
		copyEntity.setMgmNo(mgmNo);
		boolean ret = save(copyEntity);
		//把分配的编号标记为已使用
		if (ret) {
			UpdateWrapper<MgmNoAllocationEntity> updateWrapper = new UpdateWrapper<>();
			updateWrapper.eq("mgm_no", mgmNo);
			updateWrapper.set("used", YesOrNo.是.getV());
			mgmNoAllocationService.update(updateWrapper);
			int copyId = copyEntity.getId();
			if(grievanceMgrPartsDetailsList!=null && !grievanceMgrPartsDetailsList.isEmpty()) {
				for (int i=0,len_i = grievanceMgrPartsDetailsList.size();i<len_i;i++) {
					grievanceMgrPartsDetailsList.get(i).setGrievanceMgrId(copyId);
				}
				grievanceMgrPartsDetailsService.saveBatch(grievanceMgrPartsDetailsList);
			}
			if(copyGrievanceMgrUploadFileList!=null && !copyGrievanceMgrUploadFileList.isEmpty()) {
				for (int i=0,len_i = copyGrievanceMgrUploadFileList.size();i<len_i;i++) {
					copyGrievanceMgrUploadFileList.get(i).setGrievanceMgrId(copyId);
				}
				grievanceMgrUploadFileService.saveBatch(copyGrievanceMgrUploadFileList);
			}
		}
		return ret;
	}
	
	
	/**
	 * 获取最小id、最大id
	 * @param params
	 * @return
	 */
	public Integer[] getMinIdMaxId(Map<String,Object> params) {
		QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			Object complaintType = params.get("complaintType");
			if (complaintType!=null) {
				queryWrapper.eq("complaint_type", complaintType);
			}
			String receptionDateRangeStart = (String)params.get("receptionDateRangeStart");
			if (StringUtils.isNotEmpty(receptionDateRangeStart)) {
				queryWrapper.ge("reception_date", receptionDateRangeStart);
			}
			String receptionDateRangeEnd = (String)params.get("receptionDateRangeEnd");
			if (StringUtils.isNotEmpty(receptionDateRangeEnd)) {
				queryWrapper.le("reception_date", receptionDateRangeEnd);
			}
			String factoryType = (String)params.get("factoryType");
			if (StringUtils.isNotEmpty(factoryType)) {
				queryWrapper.eq("factory_type", factoryType);
			}
			String mgmNoLike = (String)params.get("mgmNoLike");
			if (StringUtils.isNotEmpty(mgmNoLike)) {
				queryWrapper.likeRight("mgm_no", mgmNoLike);
			}
			String companyName = (String)params.get("companyName");
			if (StringUtils.isNotEmpty(companyName)) {
				queryWrapper.eq("company_name", companyName);
			}
			String customerFactoryLike = (String)params.get("customerFactoryLike");
			if (StringUtils.isNotEmpty(customerFactoryLike)) {
				queryWrapper.likeRight("customer_factory", customerFactoryLike);
			}
			String location = (String)params.get("location");
			if (StringUtils.isNotEmpty(location)) {
				queryWrapper.eq("location", location);
			}
			String prodPlace = (String)params.get("prodPlace");
			if (StringUtils.isNotEmpty(prodPlace)) {
				queryWrapper.eq("prod_place", prodPlace);
			}
			String prodDivision = (String)params.get("prodDivision");
			if (StringUtils.isNotEmpty(prodDivision)) {
				queryWrapper.eq("prod_division", prodDivision);
			}
			String prodNumLike = (String)params.get("prodNumLike");
			if (StringUtils.isNotEmpty(prodNumLike)) {
				queryWrapper.likeRight("prod_num", prodNumLike);
			}
			String cause = (String)params.get("cause");
			if (StringUtils.isNotEmpty(cause)) {
				queryWrapper.eq("cause", cause);
			}
		}
		queryWrapper.select("max(id),min(id)");
		Map<String, Object> map = getMap(queryWrapper);
		if(map == null) {
			return new Integer[] {null,null};
		} else {
			return new Integer[] {(Integer)map.get("min(id)"),(Integer)map.get("max(id)")};
		}
	}
	
	/**
	 * 滚动式加载用户信息
	 * @param params
	 * @return
	 */
	public List<GrievanceMgrEntity> scrollLoad(Map<String,Object> params) {
		QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			Object complaintType = params.get("complaintType");
			if (complaintType!=null) {
				queryWrapper.eq("complaint_type", complaintType);
			}
			String receptionDateRangeStart = (String)params.get("receptionDateRangeStart");
			if (StringUtils.isNotEmpty(receptionDateRangeStart)) {
				queryWrapper.ge("reception_date", receptionDateRangeStart);
			}
			String receptionDateRangeEnd = (String)params.get("receptionDateRangeEnd");
			if (StringUtils.isNotEmpty(receptionDateRangeEnd)) {
				queryWrapper.le("reception_date", receptionDateRangeEnd);
			}
			String factoryType = (String)params.get("factoryType");
			if (StringUtils.isNotEmpty(factoryType)) {
				queryWrapper.eq("factory_type", factoryType);
			}
			String mgmNoLike = (String)params.get("mgmNoLike");
			if (StringUtils.isNotEmpty(mgmNoLike)) {
				queryWrapper.likeRight("mgm_no", mgmNoLike);
			}
			String companyName = (String)params.get("companyName");
			if (StringUtils.isNotEmpty(companyName)) {
				queryWrapper.eq("company_name", companyName);
			}
			String customerFactoryLike = (String)params.get("customerFactoryLike");
			if (StringUtils.isNotEmpty(customerFactoryLike)) {
				queryWrapper.likeRight("customer_factory", customerFactoryLike);
			}
			String location = (String)params.get("location");
			if (StringUtils.isNotEmpty(location)) {
				queryWrapper.eq("location", location);
			}
			String prodPlace = (String)params.get("prodPlace");
			if (StringUtils.isNotEmpty(prodPlace)) {
				queryWrapper.eq("prod_place", prodPlace);
			}
			String prodDivision = (String)params.get("prodDivision");
			if (StringUtils.isNotEmpty(prodDivision)) {
				queryWrapper.eq("prod_division", prodDivision);
			}
			String prodNumLike = (String)params.get("prodNumLike");
			if (StringUtils.isNotEmpty(prodNumLike)) {
				queryWrapper.likeRight("prod_num", prodNumLike);
			}
			String cause = (String)params.get("cause");
			if (StringUtils.isNotEmpty(cause)) {
				queryWrapper.eq("cause", cause);
			}
			Object status = params.get("status");
			if (status != null) {
				queryWrapper.eq("status", status);
			}
		}
		Integer limit = (Integer)params.get("limit");
		if (limit != null) {
			if (limit<1 || limit>10000) {
				throw new RRException(i.t("GrievanceMgrService.scrollLoad.limitTooLarge"));//查询数据量过大，请求失败！
			}
		}
		scrollLoadSplit(params, queryWrapper);
		return list(queryWrapper);
	}

	@Autowired
	private CustItemTblService custItemTblService;
	
	@Autowired
	private MgmNoAllocationService mgmNoAllocationService;
	
	@Autowired
	private UploadFileService uploadFileService;

	/**
	 * 切换启用状态
	 * @param id
	 * @return
	 */
	public boolean changeStatus(Integer id) {
		UpdateWrapper<GrievanceMgrEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id);
		updateWrapper.setSql("status=status^1");
		return update(updateWrapper);
	}
	
	@Autowired
	private EnumTableService enumTableService;
	
	/**
	 * 更新数据
	 * @param entity
	 * @return
	 */
	@Transactional
	public boolean update(String username, GrievanceMgrEntity entity, List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList,
			List<GrievanceMgrUploadFileEntity> grievanceMgrUploadFileList, List<String> acceptEmailList) {
		Map<String,Object> params = new HashMap<>();
		params.put("enumGroupList", Arrays.asList("生産場所", "お客様社名F", "お客様社名M", "重要度ランク", "発生場所", "製品区分", "要因"));
		List<EnumTableEntity> enumTableList = enumTableService.list(params);
		Map<String,StringBuilder> inRuleMap = new HashMap<>();
		String connChar = "";
		for (int i=0,len_i = enumTableList.size();i<len_i;i++) {
			EnumTableEntity enumTable = enumTableList.get(i);
			StringBuilder sb = inRuleMap.get(enumTable.getEnumGroup());
			if (sb == null) {
				inRuleMap.put(enumTable.getEnumGroup(), sb = new StringBuilder());
				connChar = "";
			}
			sb.append(connChar).append("'").append(enumTable.getEnumDesc()).append("'");
			connChar = ",";
		}
		DataValidateUtils.validate(
		MapUtils.hms(
			"id.require",i.t("GrievanceMgrService.validate.id.require"),//主键id不能为空
			"complaintType.require",i.t("GrievanceMgrService.validate.complaintType.require"),//苦情タイプ不能为空
			"complaintType.in",i.t("GrievanceMgrService.validate.complaintType.in"),//苦情タイプ错误
			"receptionDate.require",i.t("GrievanceMgrService.validate.receptionDate.require"),//受付日付不能为空
			"receptionDate.notAFuture",i.t("GrievanceMgrService.validate.receptionDate.notAFuture"),//受付日付不能超过当前日期
			"prodNum.require",i.t("GrievanceMgrService.validate.prodNum.require"),//製品品番不能为空
			"customerService.require",i.t("GrievanceMgrService.validate.customerService.require"),//お客様品番不能为空
			"companyName.require",i.t("GrievanceMgrService.validate.companyName.require"),//お客様社名不能为空
			"companyName.in",i.t("GrievanceMgrService.validate.companyName.in"),//お客様社名错误
			"customerFactory.max",i.t("GrievanceMgrService.validate.customerFactory.max"),//お客様工場长度不能超过{0}个字符",
			"location.require",i.t("GrievanceMgrService.validate.location.require"),//発生場所不能为空
			"location.in",i.t("GrievanceMgrService.validate.location.in"),//発生場所错误
			"failureDay.require",i.t("GrievanceMgrService.validate.failureDay.require"),//不良発生日不能为空
//			"failureDay.beforeOrEqual",i.t("GrievanceMgrService.validate.failureDay.before"),//不良発生日不能在受付日付之后
			"badContent.require",i.t("GrievanceMgrService.validate.badContent.require"),//不良内容不能为空
			"badContent.max",i.t("GrievanceMgrService.validate.badContent.max"),//不良内容长度不能超过{0}个字符
			"badContentsDetails.require",i.t("GrievanceMgrService.validate.badContentsDetails.require"),//不良内容详情不能为空
			"badContentsDetails.max",i.t("GrievanceMgrService.validate.badContentsDetails.max"),//不良内容详情长度不能超过{0}个字符
			"customerControlNo.max",i.t("GrievanceMgrService.validate.customerControlNo.max"),//お客様管理No.长度不能超过{0}个字符
			"prodPlace.require",i.t("GrievanceMgrService.validate.prodPlace.require"),//生産場所不能为空
			"prodPlace.in",i.t("GrievanceMgrService.validate.prodPlace.in"),//生産場所错误
			"prodDivision.require",i.t("GrievanceMgrService.validate.prodDivision.require"),//製品区分不能为空
			"prodDivision.in",i.t("GrievanceMgrService.validate.prodDivision.in"),//製品区分错误
			"lotLotNo.require",i.t("GrievanceMgrService.validate.lotLotNo.require"),//製造ロットNo.不能为空
			"lotLotNo.max",i.t("GrievanceMgrService.validate.lotLotNo.max"),//製造ロットNo.长度不能超过{0}个字符
			"lotLotNo2.max",i.t("GrievanceMgrService.validate.lotLotNo2.max"),//製品DMC长度不能超过{0}个字符
			"importanceRank.require",i.t("GrievanceMgrService.validate.importanceRank.require"),//重要度ランク不能为空
			"importanceRank.in",i.t("GrievanceMgrService.validate.importanceRank.in"),//重要度ランク错误
			"cause.require",i.t("GrievanceMgrService.validate.cause.require"),//要因不能为空
			"cause.in",i.t("GrievanceMgrService.validate.cause.in"),//要因错误
			"respParty.max",i.t("GrievanceMgrService.validate.respParty.max"),//责任方长度不能超过{0}个字符
			"factorsDetails.max",i.t("GrievanceMgrService.validate.factorsDetails.max"),//要因詳細长度不能超过{0}个字符
			"personInCharge.max",i.t("GrievanceMgrService.validate.personInCharge.max"),//担当者长度不能超过{0}个字符
			"oneAnsDay.notAFuture",i.t("GrievanceMgrService.validate.oneAnsDay.notAFuture"),//１次回答日不能超过当前时间
			"intermediateAnsDay.notAFuture",i.t("GrievanceMgrService.validate.intermediateAnsDay.notAFuture"),//中間回答日不能超过当前时间
			"finalRespDate.notAFuture",i.t("GrievanceMgrService.validate.finalRespDate.notAFuture"),//最終回答内容不能超过当前时间
			"version.require",i.t("GrievanceMgrService.validate.version.require"),//版本号不能为空
		    "demandReplyDate.require",i.t("GrievanceMgrService.validate.demandReplyDate.require"),//要求回答日不能为空
		    "demandReplyDate.after",i.t("GrievanceMgrService.validate.demandReplyDate.after")//要求回答日必须在受付日付之后
		),
		entity,
			"id","require",
			"complaintType","require|in:0,1,2",
			"receptionDate","require|notAFuture",
			"prodNum","require",
			"customerService","require",
			"companyName","require|in:"+inRuleMap.get("お客様社名F").toString()+","+inRuleMap.get("お客様社名M").toString(),
			"customerFactory","max:200",
			"location","require|in:"+inRuleMap.get("発生場所").toString(),
			"failureDay","require",
			"badContent","require|max:255",
			"badContentsDetails","require|max:255",
			"customerControlNo","max:85",
			"prodPlace","require|in:"+inRuleMap.get("生産場所").toString(),
			"prodDivision","require|in:"+inRuleMap.get("製品区分").toString(),
			"lotLotNo","require|max:255",
			"lotLotNo2","max:255",
			"importanceRank","require|in:"+inRuleMap.get("重要度ランク").toString(),
			"cause","require|in:"+inRuleMap.get("要因").toString(),
			"respParty","max:45",
			"factorsDetails","max:355",
			"personInCharge","max:255",
			"oneAnsDay","notAFuture",
			"intermediateAnsDay","notAFuture",
			"finalRespDate","notAFuture",
			"version","require",
		    "demandReplyDate","require|after:'receptionDate'"
		);
		HashSet<String> acceptEmailSet = new HashSet<String>(acceptEmailList);
		IDataValidRule emailRule = DataValidateUtils.useRule(DataValidateFactory.EMAIL);
		for (String email:acceptEmailSet) {
			if (StringUtils.isEmpty(email)) {
				throw new RRException(i.t("GrievanceMgrService.validate.acceptEmail.require")); //用户邮箱地址不能为空
			}
			if (email.length() > 45) {
				throw new RRException(i.t("GrievanceMgrService.validate.acceptEmail.max")); //用户邮箱地址长度不能超过45个字符
			}
			if (!emailRule.valid(null, email, null)) {
				throw new RRException(i.t("GrievanceMgrService.validate.acceptEmail.email")); //用户邮箱地址格式错误！
			}
		}
		I18nUtils _i = i;
		for (int i=0,len_i = grievanceMgrUploadFileList.size();i<len_i;i++) {
			GrievanceMgrUploadFileEntity grievanceMgrUploadFile = grievanceMgrUploadFileList.get(i);
			DataValidateUtils.validate(
				MapUtils.hms(
					"srcName.require", _i.t("GrievanceMgrService.validate.srcName.require"),//文件名不能为空
					"srcName.filename", _i.t("GrievanceMgrService.validate.srcName.filename"),//文件名异常
					"srcName.max", _i.t("GrievanceMgrService.validate.srcName.max"),//文件名长度不能超过{0}个字符
					"type.require", _i.t("GrievanceMgrService.validate.type.require"),//文件类型不能为空
					"type.in", _i.t("GrievanceMgrService.validate.type.in"), //文件类型异常
					"fileId.require", _i.t("GrievanceMgrService.validate.fileId.require") //文件上传异常
				),
				grievanceMgrUploadFile,
				"srcName","require|filename|max:45",
				"type","require|in:0,1,2,3,4",
				"fileId","require"
			);
		}
		String cause = entity.getCause();
		if("解析中".equals(cause)) {
			DataValidateUtils.validate(
			MapUtils.hms("analysisRet.max",i.t("GrievanceMgrService.validate.analysisRet.max")),//解析結果／原因长度不能超过{0}个字符
			entity,
			"analysisRet","max:455");
		} else {
			DataValidateUtils.validate(
			MapUtils.hms(
			"analysisRet.require",i.t("GrievanceMgrService.validate.analysisRet.require"),//解析結果／原因不能为空
			"analysisRet.max", i.t("GrievanceMgrService.validate.analysisRet.max")//解析結果／原因长度不能超过{0}个字符
			),
			entity,
			"analysisRet","require|max:455");
		}
		for (int i = grievanceMgrPartsDetailsList.size() - 1;i>-1;i--) {
			GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
			if (StringUtils.isEmpty(grievanceMgrPartsDetails.getComponentPos()) &&
			StringUtils.isEmpty(grievanceMgrPartsDetails.getPartNum()) &&
			StringUtils.isEmpty(grievanceMgrPartsDetails.getMakerName())) {
				grievanceMgrPartsDetailsList.remove(i);
			}
		}
		if("半導体破壊".equals(cause) || "部品破壊".equals(cause) || "部品".equals(cause)) {
			if(grievanceMgrPartsDetailsList.isEmpty()) {
				throw new RRException(i.t("GrievanceMgrService.update.grievanceMgrPartsDetailsIsEmpty"));//“部品位置”、“部品品番”、“メーカ名”不能为空！
			}
			Map<String,String> msgMap = MapUtils.hms(
				"componentPos.require",i.t("GrievanceMgrService.validate.componentPos.require"),//部品位置不能为空
				"componentPos.max",i.t("GrievanceMgrService.validate.componentPos.max"),//部品位置长度不能超过{0}个字符
				"partNum.require",i.t("GrievanceMgrService.validate.partNum.require"),//部品品番不能为空
				"partNum.max",i.t("GrievanceMgrService.validate.partNum.max"),//部品品番长度不能超过{0}个字符
				"makerName.require",i.t("GrievanceMgrService.validate.makerName.require"),//メーカ名不能为空
				"makerName.max",i.t("GrievanceMgrService.validate.makerName.max")//メーカ名长度不能超过{0}个字符
			);
			for (int i = grievanceMgrPartsDetailsList.size() - 1;i>-1;i--) {
				GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
				DataValidateUtils.validate(
					msgMap,
					grievanceMgrPartsDetails,
					"componentPos","require|max:255",
					"partNum","require|max:255",
					"makerName","require|max:255"
				);
			}
		} else {
			Map<String,String> msgMap = MapUtils.hms(
				"componentPos.max",i.t("GrievanceMgrService.validate.componentPos.max"),//部品位置长度不能超过{0}个字符
				"partNum.max",i.t("GrievanceMgrService.validate.partNum.max"),//部品品番长度不能超过{0}个字符
				"makerName.max",i.t("GrievanceMgrService.validate.makerName.max")//メーカ名长度不能超过{0}个字符
			);
			for (int i = grievanceMgrPartsDetailsList.size() - 1;i>-1;i--) {
				GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
				DataValidateUtils.validate(
					msgMap,
					grievanceMgrPartsDetails,
					"componentPos","max:255",
					"partNum","max:255",
					"makerName","max:255"
				);
			}
		}
//		if(entity.getOneAnsDay()!=null) {
//			if(entity.getIntermediateAnsDay()!=null) {
//				if (entity.getFinalRespDate()!=null) {
//					DataValidateUtils.validate(
//					MapUtils.hms(
//					"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//					"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max"),//１次回答内容不能超过{0}个字符
//					"intermediateAnsAttr.require", i.t("GrievanceMgrService.validate.intermediateAnsAttr.require"),//中間回答内容不能为空
//					"intermediateAnsAttr.max",i.t("GrievanceMgrService.validate.intermediateAnsAttr.max"),//中間回答内容长度不能超过{0}个字符
//					"finalAnsAttr.require",i.t("GrievanceMgrService.validate.finalAnsAttr.require"),//最終回答内容不能为空
//					"finalAnsAttr.max",i.t("GrievanceMgrService.validate.finalAnsAttr.max")//最終回答内容长度不能超过{0}个字符
//					),
//					entity,
//					"oneRespContent","require|max:255",
//					"intermediateAnsAttr","require|max:255",
//					"finalAnsAttr","require|max:255");
//				} else {
//					DataValidateUtils.validate(
//					MapUtils.hms(
//						"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//						"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max"),//１次回答内容不能超过{0}个字符
//						"intermediateAnsAttr.require", i.t("GrievanceMgrService.validate.intermediateAnsAttr.require"),//中間回答内容不能为空
//						"intermediateAnsAttr.max",i.t("GrievanceMgrService.validate.intermediateAnsAttr.max")//中間回答内容长度不能超过{0}个字符
//					),
//					entity,
//					"oneRespContent","require|max:255",
//					"intermediateAnsAttr","require|max:255");
//				}
//			} else {
//				DataValidateUtils.validate(
//				MapUtils.hms(
//					"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//					"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max")//１次回答内容不能超过{0}个字符
//				),
//				entity,
//				"oneRespContent","require|max:255");
//			}
//		} else {
//			DataValidateUtils.validate(
//			MapUtils.hms(
//				"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max")//１次回答内容不能超过{0}个字符
//			),
//			entity,
//				"oneRespContent","max:255"
//			);
//		}
		HashSet<Integer> uploadFileIdSet = new HashSet<>();
		for (int i=0,len_i = grievanceMgrUploadFileList.size();i<len_i;i++) {
			uploadFileIdSet.add(grievanceMgrUploadFileList.get(i).getFileId());
		}
		if (!uploadFileIdSet.isEmpty()) {
			QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.in("id", uploadFileIdSet);
			queryWrapper.select("id");
			if (uploadFileService.count(queryWrapper) != uploadFileIdSet.size()) {
				throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！");
			}
		}
		UpdateWrapper<GrievanceMgrEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", entity.getId());
		// updateWrapper.eq("mgmno", entity.getMgmNo()); 
		updateWrapper.eq("version", entity.getVersion()); //必须按照传入的版本号更新，实现乐观锁
		updateWrapper.set("complaint_type", entity.getComplaintType());
		updateWrapper.set("version", entity.getVersion()+1);
		updateWrapper.set("reception_date", entity.getReceptionDate());
		updateWrapper.set("demand_reply_date", entity.getDemandReplyDate());
		updateWrapper.set("prod_num", entity.getProdNum());
		updateWrapper.set("customer_service", entity.getCustomerService());
		updateWrapper.set("company_name", entity.getCompanyName());
		updateWrapper.set("customer_factory", entity.getCustomerFactory());
		updateWrapper.set("location", entity.getLocation());
		updateWrapper.set("failure_day", entity.getFailureDay());
		updateWrapper.set("bad_content", entity.getBadContent());
		updateWrapper.set("bad_contents_details", entity.getBadContentsDetails());
		updateWrapper.set("customer_control_no", entity.getCustomerControlNo());
		updateWrapper.set("designated_answer_day", entity.getDesignatedAnswerDay());
		updateWrapper.set("prod_place", entity.getProdPlace());
		updateWrapper.set("prod_division", entity.getProdDivision());
		updateWrapper.set("lot_lot_no", entity.getLotLotNo());
		updateWrapper.set("lot_lot_no2", entity.getLotLotNo2());
		updateWrapper.set("prod_day", entity.getProdDay());
		updateWrapper.set("importance_rank", entity.getImportanceRank());
		updateWrapper.set("cause", entity.getCause());
		updateWrapper.set("resp_party", entity.getRespParty());
		updateWrapper.set("analysis_ret", entity.getAnalysisRet());
		updateWrapper.set("factors_details", entity.getFactorsDetails());
		updateWrapper.set("one_ans_day", entity.getOneAnsDay());
		updateWrapper.set("one_resp_content", entity.getOneRespContent());
		updateWrapper.set("intermediate_ans_day", entity.getIntermediateAnsDay());
		updateWrapper.set("intermediate_ans_attr", entity.getIntermediateAnsAttr());
		updateWrapper.set("final_resp_date", entity.getFinalRespDate());
		updateWrapper.set("final_ans_attr", entity.getFinalAnsAttr());
		updateWrapper.set("cust_complet_date", entity.getCustCompletDate());
		updateWrapper.set("person_in_charge", entity.getPersonInCharge());
		updateWrapper.set("editor_username", username);

		boolean ret = update(updateWrapper);
		if(ret == false) {
			return false;
		}
		
		Integer updateId = entity.getId();
		
		{//保存客户投诉管理部品详情
			QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("grievance_mgr_id", entity.getId());
			List<GrievanceMgrPartsDetailsEntity> saveOrUpdateList = grievanceMgrPartsDetailsService.list(queryWrapper);
			List<Integer> delIdList = new ArrayList<>();
			int saveOrUpdateCount = saveOrUpdateList.size();
			int grievanceMgrPartsDetailsCount = grievanceMgrPartsDetailsList.size();
			int i=0;
			for(int len_i = Math.min(saveOrUpdateCount, grievanceMgrPartsDetailsCount);i<len_i;i++) {
				GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
				GrievanceMgrPartsDetailsEntity saveOrUpdate = saveOrUpdateList.get(i);
				saveOrUpdate.setGrievanceMgrId(updateId);
				saveOrUpdate.setComponentPos(StringUtils.isEmpty(grievanceMgrPartsDetails.getComponentPos())?"":grievanceMgrPartsDetails.getComponentPos());
				saveOrUpdate.setPartNum(StringUtils.isEmpty(grievanceMgrPartsDetails.getPartNum())?"":grievanceMgrPartsDetails.getPartNum());
				saveOrUpdate.setMakerName(StringUtils.isEmpty(grievanceMgrPartsDetails.getMakerName())?"":grievanceMgrPartsDetails.getMakerName());
			}
			if (saveOrUpdateCount < grievanceMgrPartsDetailsCount) {
				for (;i<grievanceMgrPartsDetailsCount;i++) {
					GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
					GrievanceMgrPartsDetailsEntity saveOrUpdate = new GrievanceMgrPartsDetailsEntity();
					saveOrUpdate.setGrievanceMgrId(entity.getId());
					saveOrUpdate.setComponentPos(grievanceMgrPartsDetails.getComponentPos());
					saveOrUpdate.setPartNum(grievanceMgrPartsDetails.getPartNum());
					saveOrUpdate.setMakerName(grievanceMgrPartsDetails.getMakerName());
					saveOrUpdateList.add(saveOrUpdate);
				}
			} else if(saveOrUpdateCount > grievanceMgrPartsDetailsCount) {
				for (;saveOrUpdateCount>i;saveOrUpdateCount--) {
					GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = saveOrUpdateList.remove(i);
					delIdList.add(grievanceMgrPartsDetails.getId());
				}
			}
			if(!saveOrUpdateList.isEmpty()) {
				grievanceMgrPartsDetailsService.saveOrUpdateBatch(saveOrUpdateList);
			}
			if(!delIdList.isEmpty()) {
				grievanceMgrPartsDetailsService.removeByIds(delIdList);
			}
		}
		
		//保存用户上传的文件
		if (!grievanceMgrUploadFileList.isEmpty()) {
			QueryWrapper<GrievanceMgrUploadFileEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("grievance_mgr_id", updateId);
			List<GrievanceMgrUploadFileEntity> saveOrUpdateList = grievanceMgrUploadFileService.list(queryWrapper);
			saveOrUpdateList.sort(sortRule);
			grievanceMgrUploadFileList.sort(sortRule);
			List<Integer> deleteIdList = new ArrayList<>();
			int i=0,j=0,len_i = saveOrUpdateList.size(),len_j = grievanceMgrUploadFileList.size();
			for(;i<len_i && j<len_j;) {
				GrievanceMgrUploadFileEntity updateBefore = saveOrUpdateList.get(i);
				GrievanceMgrUploadFileEntity updateAfter = grievanceMgrUploadFileList.get(j);
				Integer typeBefore = updateBefore.getType();
				Integer typeAfter = updateAfter.getType();
				if (typeAfter < typeBefore) {
					updateAfter.setGrievanceMgrId(updateId);
					saveOrUpdateList.add(i, updateAfter);
					i++;
					j++;
					len_i++;
				} else if(typeAfter > typeBefore) {
					GrievanceMgrUploadFileEntity grievanceMgrUploadFile = saveOrUpdateList.remove(i);
					deleteIdList.add(grievanceMgrUploadFile.getId());
					len_i--;
				} else if(typeAfter.equals(typeBefore)) {
					Integer fileIdBefore = updateBefore.getFileId();
					Integer fileIdAfter = updateAfter.getFileId();
					if (fileIdAfter < fileIdBefore) {
						updateAfter.setGrievanceMgrId(updateId);
						saveOrUpdateList.add(i, updateAfter);
						i++;
						j++;
						len_i++;
					} else if(fileIdAfter > fileIdBefore) {
						GrievanceMgrUploadFileEntity grievanceMgrUploadFile = saveOrUpdateList.remove(i);
						deleteIdList.add(grievanceMgrUploadFile.getId());
						len_i--;
					} else if(fileIdAfter.equals(fileIdBefore)) {
						updateBefore.setSrcName(updateAfter.getSrcName());
						i++;
						j++;
					}
				}
			}
			for (;i<len_i;i++) {
				GrievanceMgrUploadFileEntity updateBefore = saveOrUpdateList.remove(i);
				deleteIdList.add(updateBefore.getId());
			}
			for(;j<len_j;j++) {
				GrievanceMgrUploadFileEntity updateAfter = grievanceMgrUploadFileList.get(j);
				updateAfter.setGrievanceMgrId(updateId);
				saveOrUpdateList.add(updateAfter);
			}
			if (!saveOrUpdateList.isEmpty()) {
				grievanceMgrUploadFileService.saveOrUpdateBatch(saveOrUpdateList);
			}
			if (!deleteIdList.isEmpty()) {
				grievanceMgrUploadFileService.removeByIds(deleteIdList);
			}
		}
		//保存接收邮件的用户邮箱
		if (!acceptEmailSet.isEmpty()) {
			QueryWrapper<GrievanceMgrMsgAcceptEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("grievance_mgr_id", updateId);
			List<GrievanceMgrMsgAcceptEntity> grievanceMgrMsgAcceptList = grievanceMgrMsgAcceptService.list(queryWrapper);
			List<Integer> deleteIdList = new ArrayList<>();
			for (int i = 0,len_i = grievanceMgrMsgAcceptList.size();i<len_i;i++) {
				GrievanceMgrMsgAcceptEntity grievanceMgrMsgAccept = grievanceMgrMsgAcceptList.get(i);
				String email = grievanceMgrMsgAccept.getEmail();
				if (acceptEmailSet.contains(email)) {
					acceptEmailSet.remove(email);
				} else {
					deleteIdList.add(grievanceMgrMsgAccept.getId());
				}
			}
			List<GrievanceMgrMsgAcceptEntity> saveList = new ArrayList<>();
			for (String acceptEmail:acceptEmailSet) {
				GrievanceMgrMsgAcceptEntity grievanceMgrMsgAccept = new GrievanceMgrMsgAcceptEntity();
				grievanceMgrMsgAccept.setEmail(acceptEmail);
				grievanceMgrMsgAccept.setGrievanceMgrId(updateId);
				saveList.add(grievanceMgrMsgAccept);
			}
			if (!deleteIdList.isEmpty()) {
				grievanceMgrMsgAcceptService.removeByIds(deleteIdList);
			}
			if (!saveList.isEmpty()) {
				grievanceMgrMsgAcceptService.saveBatch(saveList);
			}
		}
		return ret;
	}
	
	private Comparator<GrievanceMgrUploadFileEntity> sortRule =  new Comparator<GrievanceMgrUploadFileEntity>() {
		@Override
		public int compare(GrievanceMgrUploadFileEntity o1, GrievanceMgrUploadFileEntity o2) {
			Integer type1 = o1.getType();
			Integer type2 = o2.getType();
			if(type1.equals(type2)) {
				Integer fileId1 = o1.getFileId();
				Integer fileId2 = o2.getFileId();
				return fileId1 - fileId2;
			} else {
				return type1 - type2;
			}
		}
	};
	
	/**
	 * 通过 excel 批量导入数据
	 * @param username
	 * @param grievanceMgrList
	 * @return
	 */
	public boolean saveBatch(String username, List<GrievanceMgrEntity> grievanceMgrList) {
		Map<String,Object> params = new HashMap<>();
		params.put("enumGroupList", Arrays.asList("生産場所", "お客様社名F", "お客様社名M", "重要度ランク", "発生場所", "製品区分", "要因"));
		List<EnumTableEntity> enumTableList = enumTableService.list(params);
		Map<String,StringBuilder> inRuleMap = new HashMap<>();
		String connChar = "";
		for (int i=0,len_i = enumTableList.size();i<len_i;i++) {
			EnumTableEntity enumTable = enumTableList.get(i);
			StringBuilder sb = inRuleMap.get(enumTable.getEnumGroup());
			if(sb == null) {
				inRuleMap.put(enumTable.getEnumGroup(), sb = new StringBuilder());
				connChar = "";
			}
			sb.append(connChar).append("'").append(enumTable.getEnumDesc()).append("'");
			connChar = ",";
		}
		Map<String,String> msgMap = MapUtils.hms(
			"complaintType.require",i.t("GrievanceMgrService.validate.complaintType.require"),//苦情タイプ不能为空
			"complaintType.in",i.t("GrievanceMgrService.validate.complaintType.in"),//苦情タイプ错误
			"mgmNo.require",i.t("GrievanceMgrService.validate.mgmNo.require"),//管理No.不能为空
			"mgmNo.max",i.t("GrievanceMgrService.validate.mgmNo.max"),//管理No.长度不能超过{0}个字符
			"mgmNo.regex",i.t("GrievanceMgrService.validate.mgmNo.regex"),//管理No.格式错误
			"receptionDate.require",i.t("GrievanceMgrService.validate.receptionDate.require"),//受付日付不能为空
			"receptionDate.notAFuture",i.t("GrievanceMgrService.validate.receptionDate.notAFuture"),//受付日付不能超过当前日期
			"prodNum.require",i.t("GrievanceMgrService.validate.prodNum.require"),//製品品番不能为空
			"customerService.require",i.t("GrievanceMgrService.validate.customerService.require"),//お客様品番不能为空
			"companyName.require",i.t("GrievanceMgrService.validate.companyName.require"),//お客様社名不能为空
			"companyName.in",i.t("GrievanceMgrService.validate.companyName.in"),//お客様社名错误
			"customerFactory.max",i.t("GrievanceMgrService.validate.customerFactory.max"),//お客様工場长度不能超过{0}个字符",
			"location.require",i.t("GrievanceMgrService.validate.location.require"),//発生場所不能为空
			"location.in",i.t("GrievanceMgrService.validate.location.in"),//発生場所错误
			"failureDay.require",i.t("GrievanceMgrService.validate.failureDay.require"),//不良発生日不能为空
//			"failureDay.beforeOrEqual",i.t("GrievanceMgrService.validate.failureDay.before"),//不良発生日不能在受付日付之后
			"badContent.require",i.t("GrievanceMgrService.validate.badContent.require"),//不良内容不能为空
			"badContent.max",i.t("GrievanceMgrService.validate.badContent.max"),//不良内容长度不能超过{0}个字符
			"badContentsDetails.require",i.t("GrievanceMgrService.validate.badContentsDetails.require"),//不良内容详情不能为空
			"badContentsDetails.max",i.t("GrievanceMgrService.validate.badContentsDetails.max"),//不良内容详情长度不能超过{0}个字符
			"customerControlNo.max",i.t("GrievanceMgrService.validate.customerControlNo.max"),//お客様管理No.长度不能超过{0}个字符
			"prodPlace.require",i.t("GrievanceMgrService.validate.prodPlace.require"),//生産場所不能为空
			"prodPlace.in",i.t("GrievanceMgrService.validate.prodPlace.in"),//生産場所错误
			"prodDivision.require",i.t("GrievanceMgrService.validate.prodDivision.require"),//製品区分不能为空
			"prodDivision.in",i.t("GrievanceMgrService.validate.prodDivision.in"),//製品区分错误
			"lotLotNo.require",i.t("GrievanceMgrService.validate.lotLotNo.require"),//製造ロットNo.不能为空
			"lotLotNo.max",i.t("GrievanceMgrService.validate.lotLotNo.max"),//製造ロットNo.长度不能超过{0}个字符
			"lotLotNo2.max",i.t("GrievanceMgrService.validate.lotLotNo2.max"),//製品DMC长度不能超过{0}个字符
			"importanceRank.require",i.t("GrievanceMgrService.validate.importanceRank.require"),//重要度ランク不能为空
			"importanceRank.in",i.t("GrievanceMgrService.validate.importanceRank.in"),//重要度ランク错误
			"cause.require",i.t("GrievanceMgrService.validate.cause.require"),//要因不能为空
			"cause.in",i.t("GrievanceMgrService.validate.cause.in"),//要因错误
			"respParty.max",i.t("GrievanceMgrService.validate.respParty.max"),//责任方长度不能超过{0}个字符
			"factorsDetails.max",i.t("GrievanceMgrService.validate.factorsDetails.max"),//要因詳細长度不能超过{0}个字符
			"personInCharge.max",i.t("GrievanceMgrService.validate.personInCharge.max"),//担当者长度不能超过{0}个字符
			"oneAnsDay.notAFuture",i.t("GrievanceMgrService.validate.oneAnsDay.notAFuture"),//１次回答日不能超过当前时间
			"intermediateAnsDay.notAFuture",i.t("GrievanceMgrService.validate.intermediateAnsDay.notAFuture"),//中間回答日不能超过当前时间
			"finalRespDate.notAFuture",i.t("GrievanceMgrService.validate.finalRespDate.notAFuture"),//最終回答内容不能超过当前时间
		    "demandReplyDate.require",i.t("GrievanceMgrService.validate.demandReplyDate.require"),//要求回答日不能为空
		    "demandReplyDate.after",i.t("GrievanceMgrService.validate.demandReplyDate.after")//要求回答日必须在受付日付之后
		);
		List<GrievanceMgrEntity> saveList = new ArrayList<>();
		for (int h=0,len_h = grievanceMgrList.size();h<len_h;h++) {
			GrievanceMgrEntity entity = grievanceMgrList.get(h);
			GrievanceMgrEntity saveData = new GrievanceMgrEntity();
			saveData.setComplaintType(entity.getComplaintType());
			saveData.setMgmNo(entity.getMgmNo());
			saveData.setReceptionDate(entity.getReceptionDate());
			saveData.setDemandReplyDate(entity.getDemandReplyDate());
			saveData.setProdNum(entity.getProdNum());
			saveData.setCustomerService(entity.getCustomerService());
			saveData.setCompanyName(entity.getCompanyName());
			saveData.setCustomerFactory(entity.getCustomerFactory());
			saveData.setLocation(entity.getLocation());
			saveData.setFailureDay(entity.getFailureDay());
			saveData.setBadContent(entity.getBadContent());
			saveData.setBadContentsDetails(entity.getBadContentsDetails());
			saveData.setCustomerControlNo(entity.getCustomerControlNo());
			saveData.setDesignatedAnswerDay(entity.getDesignatedAnswerDay());
			saveData.setProdPlace(entity.getProdPlace());
			saveData.setProdDivision(entity.getProdDivision());
			saveData.setLotLotNo(entity.getLotLotNo());
			saveData.setLotLotNo2(entity.getLotLotNo2());
			saveData.setProdDay(entity.getProdDay());
			saveData.setImportanceRank(entity.getImportanceRank());
			saveData.setCause(entity.getCause());
			saveData.setRespParty(entity.getRespParty());
			saveData.setAnalysisRet(entity.getAnalysisRet());
			saveData.setFactorsDetails(entity.getFactorsDetails());
			saveData.setOneAnsDay(entity.getOneAnsDay());
			saveData.setOneRespContent(entity.getOneRespContent());
			saveData.setIntermediateAnsDay(entity.getIntermediateAnsDay());
			saveData.setIntermediateAnsAttr(entity.getIntermediateAnsAttr());
			saveData.setFinalRespDate(entity.getFinalRespDate());
			saveData.setFinalAnsAttr(entity.getFinalAnsAttr());
			saveData.setCustCompletDate(entity.getCustCompletDate());
			saveData.setPersonInCharge(entity.getPersonInCharge());
			saveData.setCreatorUsername(username);
			saveData.setEditorUsername(username);
			saveData.setStatus(Status.启用.getV());
			DataValidateUtils.validate(
				msgMap,
				saveData,
				"complaintType","require|in:0,1,2",
				"mgmNo","require|max:10|regex:'^[FM]\\d{2}((0[1-9])|(1[012]))\\d{3,4}$'",
				"receptionDate","require|notAFuture",
				"prodNum","require",
				"customerService","require",
				"companyName","require|in:"+inRuleMap.get("お客様社名F").toString()+","+inRuleMap.get("お客様社名M").toString(),
				"customerFactory","max:200",
				"location","require|in:"+inRuleMap.get("発生場所").toString(),
				"failureDay","require",
				"badContent","require|max:255",
				"badContentsDetails","require|max:255",
				"customerControlNo","max:85",
				"prodPlace","require|in:"+inRuleMap.get("生産場所").toString(),
				"prodDivision","require|in:"+inRuleMap.get("製品区分").toString(),
				"lotLotNo","require|max:255",
				"lotLotNo2","max:255",
				"importanceRank","require|in:"+inRuleMap.get("重要度ランク").toString(),
				"cause","require|in:"+inRuleMap.get("要因").toString(),
				"respParty","max:45",
				"factorsDetails","max:355",
				"personInCharge","max:255",
				"oneAnsDay","notAFuture",
				"intermediateAnsDay","notAFuture",
				"finalRespDate","notAFuture",
			    "demandReplyDate","require|after:'receptionDate'"
			);
			saveData.setFactoryType(entity.getMgmNo().substring(0,1));
			String cause = saveData.getCause();
			if("解析中".equals(cause)) {
				DataValidateUtils.validate(
				MapUtils.hms("analysisRet.max",i.t("GrievanceMgrService.validate.analysisRet.max")),//解析結果／原因长度不能超过{0}个字符
				saveData,
				"analysisRet","max:455");
			} else {
				DataValidateUtils.validate(
				MapUtils.hms(
				"analysisRet.require",i.t("GrievanceMgrService.validate.analysisRet.require"),//解析結果／原因不能为空
				"analysisRet.max", i.t("GrievanceMgrService.validate.analysisRet.max")//解析結果／原因长度不能超过{0}个字符
				),
				saveData,
				"analysisRet","require|max:455");
			}
//			if(saveData.getOneAnsDay()!=null) {
//				if(saveData.getIntermediateAnsDay()!=null) {
//					if (saveData.getFinalRespDate()!=null) {
//						DataValidateUtils.validate(
//						MapUtils.hms(
//							"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//							"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max"),//１次回答内容不能超过{0}个字符
//							"intermediateAnsAttr.require", i.t("GrievanceMgrService.validate.intermediateAnsAttr.require"),//中間回答内容不能为空
//							"intermediateAnsAttr.max",i.t("GrievanceMgrService.validate.intermediateAnsAttr.max"),//中間回答内容长度不能超过{0}个字符
//							"finalAnsAttr.require",i.t("GrievanceMgrService.validate.finalAnsAttr.require"),//最終回答内容不能为空
//							"finalAnsAttr.max",i.t("GrievanceMgrService.validate.finalAnsAttr.max")//最終回答内容长度不能超过{0}个字符
//						),
//						saveData,
//						"oneRespContent","require|max:255",
//						"intermediateAnsAttr","require|max:255",
//						"finalAnsAttr","require|max:255");
//					} else {
//						DataValidateUtils.validate(
//						MapUtils.hms(
//							"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//							"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max"),//１次回答内容不能超过{0}个字符
//							"intermediateAnsAttr.require", i.t("GrievanceMgrService.validate.intermediateAnsAttr.require"),//中間回答内容不能为空
//							"intermediateAnsAttr.max",i.t("GrievanceMgrService.validate.intermediateAnsAttr.max")//中間回答内容长度不能超过{0}个字符
//						),
//						saveData,
//						"oneRespContent","require|max:255",
//						"intermediateAnsAttr","require|max:255");
//					}
//				} else {				
//					DataValidateUtils.validate(
//					MapUtils.hms(
//						"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//						"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max")//１次回答内容不能超过{0}个字符
//					),
//					saveData,
//					"oneRespContent","require|max:255");
//				}
//			} else {
//				DataValidateUtils.validate(
//				MapUtils.hms(
//					"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max")//１次回答内容不能超过{0}个字符
//				),
//				saveData,
//					"oneRespContent","max:255"
//				);
//			}
			saveData.setVersion(0);
//			if (!custItemTblService.checkExists(entity.getProdNum(), entity.getCustomerService())) {
//				throw new RRException(i.t("GrievanceMgrService.save.custItemTblNotExists"));//“製品品番” 或 “お客様品番” 错误，保存失败
//			}
			saveList.add(saveData);
		}
		return saveBatch2(username, saveList);
	}
	
	/**
	 * 为了保证 Mgm_No_Allocation、grievance_Mgr 在同一事务下保存, 使用手动事务
	 * @param username
	 * @param grievanceMgrList
	 * @return
	 */
	private boolean saveBatch2(String username,List<GrievanceMgrEntity> grievanceMgrList) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		boolean ret;
		try {
			List<String> mgmNoList = new ArrayList<>();
			for (int i=0,len_i = grievanceMgrList.size();i<len_i;i++) {
				String mgmNo = grievanceMgrList.get(i).getMgmNo();
				mgmNoList.add(mgmNo);
			}
			{ //跳过在数据库中已经存在的受付件
				HashSet<String> mgmNoSet = new HashSet<>();
				QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.in("mgm_no", mgmNoList);
				queryWrapper.select("mgm_no");
				List<GrievanceMgrEntity> tempGrievanceMgrList = list(queryWrapper);
				for (int i=0,len_i = tempGrievanceMgrList.size();i<len_i;i++) {
					mgmNoSet.add(tempGrievanceMgrList.get(i).getMgmNo());
				}
				for (int i = grievanceMgrList.size() - 1;i>-1;i--) {
					GrievanceMgrEntity grievanceMgr = grievanceMgrList.get(i);
					String mgmNo = grievanceMgr.getMgmNo();
					if(mgmNoSet.contains(mgmNo)) {
						grievanceMgrList.remove(i);
					}
				}
			}
			
			List<MgmNoAllocationEntity> mgmNoAllocationSaveOrUpdateList = new ArrayList<>();
			{ //检测需要标记为已使用的 mgmNo
				QueryWrapper<MgmNoAllocationEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.in("mgm_no", mgmNoList);
				List<MgmNoAllocationEntity> mgmNoAllocationList = mgmNoAllocationService.list(queryWrapper);
				HashMap<String,MgmNoAllocationEntity> mgmNoAllocationMap = new HashMap<>();
				for (int i=0,len_i = mgmNoAllocationList.size();i<len_i;i++) {
					MgmNoAllocationEntity mgmNoAllocation = mgmNoAllocationList.get(i);
					mgmNoAllocationMap.put(mgmNoAllocation.getMgmNo(), mgmNoAllocation);
				}
				for (int i=0,len_i = mgmNoList.size();i<len_i;i++) {
					String mgmNo = mgmNoList.get(i);
					MgmNoAllocationEntity mgmNoAllocation = mgmNoAllocationMap.get(mgmNo);
					if (mgmNoAllocation == null) {
						mgmNoAllocation = new MgmNoAllocationEntity();
						mgmNoAllocation.setMgmNo(mgmNo);
						mgmNoAllocation.setType(mgmNo.substring(0,1));
						mgmNoAllocation.setUsed(1);
						mgmNoAllocation.setYyMm(mgmNo.substring(1,5));
					}
					mgmNoAllocation.setCreatorUsername(username);
					mgmNoAllocation.setUsed(1);
					mgmNoAllocationSaveOrUpdateList.add(mgmNoAllocation);
				}
			}
			ret = saveBatch(grievanceMgrList);
			mgmNoAllocationService.saveOrUpdateBatch(mgmNoAllocationSaveOrUpdateList);
			dataSourceTransactionManager.commit(transactionStatus);
			return ret;
		} catch (Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}

	/**
	 * 新增客户投诉
	 * @param username
	 * @param entity
	 * @param grievanceMgrPartsDetailsList
	 * @return
	 */
	public boolean save(String username, GrievanceMgrEntity entity, List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList,
			List<GrievanceMgrUploadFileEntity> grievanceMgrUploadFileList,List<String> acceptEmailList) {
		Map<String,Object> params = new HashMap<>();
		params.put("enumGroupList", Arrays.asList("生産場所", "お客様社名F", "お客様社名M", "重要度ランク", "発生場所", "製品区分", "要因"));
		List<EnumTableEntity> enumTableList = enumTableService.list(params);
		Map<String,StringBuilder> inRuleMap = new HashMap<>();
		String connChar = "";
		for (int i=0,len_i = enumTableList.size();i<len_i;i++) {
			EnumTableEntity enumTable = enumTableList.get(i);
			StringBuilder sb = inRuleMap.get(enumTable.getEnumGroup());
			if(sb == null) {
				inRuleMap.put(enumTable.getEnumGroup(), sb = new StringBuilder());
				connChar = "";
			}
			sb.append(connChar).append("'").append(enumTable.getEnumDesc()).append("'");
			connChar = ",";
		}
		GrievanceMgrEntity saveData = new GrievanceMgrEntity();
		saveData.setFactoryType(entity.getFactoryType());
		saveData.setComplaintType(entity.getComplaintType());
		saveData.setMgmNo(entity.getMgmNo());
		saveData.setReceptionDate(entity.getReceptionDate());
		saveData.setDemandReplyDate(entity.getDemandReplyDate());
		saveData.setProdNum(entity.getProdNum());
		saveData.setCustomerService(entity.getCustomerService());
		saveData.setCompanyName(entity.getCompanyName());
		saveData.setCustomerFactory(entity.getCustomerFactory());
		saveData.setLocation(entity.getLocation());
		saveData.setFailureDay(entity.getFailureDay());
		saveData.setBadContent(entity.getBadContent());
		saveData.setBadContentsDetails(entity.getBadContentsDetails());
		saveData.setCustomerControlNo(entity.getCustomerControlNo());
		saveData.setDesignatedAnswerDay(entity.getDesignatedAnswerDay());
		saveData.setProdPlace(entity.getProdPlace());
		saveData.setProdDivision(entity.getProdDivision());
		saveData.setLotLotNo(entity.getLotLotNo());
		saveData.setLotLotNo2(entity.getLotLotNo2());
		saveData.setProdDay(entity.getProdDay());
		saveData.setImportanceRank(entity.getImportanceRank());
		saveData.setCause(entity.getCause());
		saveData.setRespParty(entity.getRespParty());
		saveData.setAnalysisRet(entity.getAnalysisRet());
		saveData.setFactorsDetails(entity.getFactorsDetails());
		saveData.setOneAnsDay(entity.getOneAnsDay());
		saveData.setOneRespContent(entity.getOneRespContent());
		saveData.setIntermediateAnsDay(entity.getIntermediateAnsDay());
		saveData.setIntermediateAnsAttr(entity.getIntermediateAnsAttr());
		saveData.setFinalRespDate(entity.getFinalRespDate());
		saveData.setFinalAnsAttr(entity.getFinalAnsAttr());
		saveData.setCustCompletDate(entity.getCustCompletDate());
		saveData.setPersonInCharge(entity.getPersonInCharge());
		saveData.setCreatorUsername(username);
		saveData.setEditorUsername(username);
		saveData.setStatus(Status.启用.getV());
		DataValidateUtils.validate(
		MapUtils.hms(
			"factoryType.require",i.t("GrievanceMgrService.validate.factoryType.require"),//工厂类型不能为空
			"factoryType.in",i.t("GrievanceMgrService.validate.factoryType.in"), //工厂类型错误
			"complaintType.require",i.t("GrievanceMgrService.validate.complaintType.require"),//苦情タイプ不能为空
			"complaintType.in",i.t("GrievanceMgrService.validate.complaintType.in"),//苦情タイプ错误
			"mgmNo.require",i.t("GrievanceMgrService.validate.mgmNo.require"),//管理No.不能为空
			"mgmNo.max",i.t("GrievanceMgrService.validate.mgmNo.max"),//管理No.长度不能超过{0}个字符
			"receptionDate.require",i.t("GrievanceMgrService.validate.receptionDate.require"),//受付日付不能为空
			"receptionDate.notAFuture",i.t("GrievanceMgrService.validate.receptionDate.notAFuture"),//受付日付不能超过当前日期
			"prodNum.require",i.t("GrievanceMgrService.validate.prodNum.require"),//製品品番不能为空
			"customerService.require",i.t("GrievanceMgrService.validate.customerService.require"),//お客様品番不能为空
			"companyName.require",i.t("GrievanceMgrService.validate.companyName.require"),//お客様社名不能为空
			"companyName.in",i.t("GrievanceMgrService.validate.companyName.in"),//お客様社名错误
			"customerFactory.max",i.t("GrievanceMgrService.validate.customerFactory.max"),//お客様工場长度不能超过{0}个字符",
			"location.require",i.t("GrievanceMgrService.validate.location.require"),//発生場所不能为空
			"location.in",i.t("GrievanceMgrService.validate.location.in"),//発生場所错误
			"failureDay.require",i.t("GrievanceMgrService.validate.failureDay.require"),//不良発生日不能为空
//			"failureDay.beforeOrEqual",i.t("GrievanceMgrService.validate.failureDay.before"),//不良発生日不能在受付日付之后
			"badContent.require",i.t("GrievanceMgrService.validate.badContent.require"),//不良内容不能为空
			"badContent.max",i.t("GrievanceMgrService.validate.badContent.max"),//不良内容长度不能超过{0}个字符
			"badContentsDetails.require",i.t("GrievanceMgrService.validate.badContentsDetails.require"),//不良内容详情不能为空
			"badContentsDetails.max",i.t("GrievanceMgrService.validate.badContentsDetails.max"),//不良内容详情长度不能超过{0}个字符
			"customerControlNo.max",i.t("GrievanceMgrService.validate.customerControlNo.max"),//お客様管理No.长度不能超过{0}个字符
			"prodPlace.require",i.t("GrievanceMgrService.validate.prodPlace.require"),//生産場所不能为空
			"prodPlace.in",i.t("GrievanceMgrService.validate.prodPlace.in"),//生産場所错误
			"prodDivision.require",i.t("GrievanceMgrService.validate.prodDivision.require"),//製品区分不能为空
			"prodDivision.in",i.t("GrievanceMgrService.validate.prodDivision.in"),//製品区分错误
			"lotLotNo.require",i.t("GrievanceMgrService.validate.lotLotNo.require"),//製造ロットNo.不能为空
			"lotLotNo.max",i.t("GrievanceMgrService.validate.lotLotNo.max"),//製造ロットNo.长度不能超过{0}个字符
			"lotLotNo2.max",i.t("GrievanceMgrService.validate.lotLotNo2.max"),//製品DMC长度不能超过{0}个字符
			"importanceRank.require",i.t("GrievanceMgrService.validate.importanceRank.require"),//重要度ランク不能为空
			"importanceRank.in",i.t("GrievanceMgrService.validate.importanceRank.in"),//重要度ランク错误
			"cause.require",i.t("GrievanceMgrService.validate.cause.require"),//要因不能为空
			"cause.in",i.t("GrievanceMgrService.validate.cause.in"),//要因错误
			"respParty.max",i.t("GrievanceMgrService.validate.respParty.max"),//责任方长度不能超过{0}个字符
			"factorsDetails.max",i.t("GrievanceMgrService.validate.factorsDetails.max"),//要因詳細长度不能超过{0}个字符
			"personInCharge.max",i.t("GrievanceMgrService.validate.personInCharge.max"),//担当者长度不能超过{0}个字符
			"oneAnsDay.notAFuture",i.t("GrievanceMgrService.validate.oneAnsDay.notAFuture"),//１次回答日不能超过当前时间
			"intermediateAnsDay.notAFuture",i.t("GrievanceMgrService.validate.intermediateAnsDay.notAFuture"),//中間回答日不能超过当前时间
			"finalRespDate.notAFuture",i.t("GrievanceMgrService.validate.finalRespDate.notAFuture"),//最終回答内容不能超过当前时间
		    "demandReplyDate.require",i.t("GrievanceMgrService.validate.demandReplyDate.require"),//要求回答日不能为空
		    "demandReplyDate.after",i.t("GrievanceMgrService.validate.demandReplyDate.after")//要求回答日必须在受付日付之后
		),
		saveData,
			"factoryType","require|in:'F','M'",
			"complaintType","require|in:0,1,2",
			"mgmNo","require|max:10",
			"receptionDate","require|notAFuture",
			"prodNum","require",
			"customerService","require",
			"companyName","require|in:"+inRuleMap.get("お客様社名F").toString()+","+inRuleMap.get("お客様社名M").toString(),
			"customerFactory","max:200",
			"location","require|in:"+inRuleMap.get("発生場所").toString(),
			"failureDay","require",
			"badContent","require|max:255",
			"badContentsDetails","require|max:255",
			"customerControlNo","max:85",
			"prodPlace","require|in:"+inRuleMap.get("生産場所").toString(),
			"prodDivision","require|in:"+inRuleMap.get("製品区分").toString(),
			"lotLotNo","require|max:255",
			"lotLotNo2","max:255",
			"importanceRank","require|in:"+inRuleMap.get("重要度ランク").toString(),
			"cause","require|in:"+inRuleMap.get("要因").toString(),
			"respParty","max:45",
			"factorsDetails","max:355",
			"personInCharge","max:255",
			"oneAnsDay","notAFuture",
			"intermediateAnsDay","notAFuture",
			"finalRespDate","notAFuture",
		    "demandReplyDate","require|after:'receptionDate'"
		);
		HashSet<String> acceptEmailSet = new HashSet<String>(acceptEmailList);
		IDataValidRule emailRule = DataValidateUtils.useRule(DataValidateFactory.EMAIL);
		for (String email:acceptEmailSet) {
			if (StringUtils.isEmpty(email)) {
				throw new RRException(i.t("GrievanceMgrService.validate.acceptEmail.require")); //用户邮箱地址不能为空
			}
			if (email.length() > 45) {
				throw new RRException(i.t("GrievanceMgrService.validate.acceptEmail.max",45)); //用户邮箱地址长度不能超过{0}个字符
			}
			if (!emailRule.valid(null, email, null)) {
				throw new RRException(i.t("GrievanceMgrService.validate.acceptEmail.email")); //用户邮箱地址格式错误！
			}
		}
		I18nUtils _i = i;
		for (int i=0,len_i = grievanceMgrUploadFileList.size();i<len_i;i++) {
			GrievanceMgrUploadFileEntity grievanceMgrUploadFile = grievanceMgrUploadFileList.get(i);
			DataValidateUtils.validate(
				MapUtils.hms(
					"srcName.require", _i.t("GrievanceMgrService.validate.srcName.require"),//文件名不能为空
					"srcName.filename", _i.t("GrievanceMgrService.validate.srcName.filename"),//文件名异常
					"srcName.max", _i.t("GrievanceMgrService.validate.srcName.max"),//文件名长度不能超过{0}个字符
					"type.require", _i.t("GrievanceMgrService.validate.type.require"),//文件类型不能为空
					"type.in", _i.t("GrievanceMgrService.validate.type.in"), //文件类型异常
					"fileId.require", _i.t("GrievanceMgrService.validate.fileId.require") //文件上传异常
				),
				grievanceMgrUploadFile,
				"srcName","require|filename|max:45",
				"type","require|in:0,1,2,3,4",
				"fileId","require"
			);
		}
		for (int i = grievanceMgrPartsDetailsList.size() - 1;i>-1;i--) {
			GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
			if (StringUtils.isEmpty(grievanceMgrPartsDetails.getComponentPos()) &&
			StringUtils.isEmpty(grievanceMgrPartsDetails.getPartNum()) &&
			StringUtils.isEmpty(grievanceMgrPartsDetails.getMakerName())) {
				grievanceMgrPartsDetailsList.remove(i);
			}
		}
		String cause = saveData.getCause();
		if("解析中".equals(cause)) {
			DataValidateUtils.validate(
			MapUtils.hms("analysisRet.max",i.t("GrievanceMgrService.validate.analysisRet.max")),//解析結果／原因长度不能超过{0}个字符
			saveData,
			"analysisRet","max:455");
		} else {
			DataValidateUtils.validate(
			MapUtils.hms(
			"analysisRet.require",i.t("GrievanceMgrService.validate.analysisRet.require"),//解析結果／原因不能为空
			"analysisRet.max", i.t("GrievanceMgrService.validate.analysisRet.max")//解析結果／原因长度不能超过{0}个字符
			),
			saveData,
			"analysisRet","require|max:455");
		}
		if ("半導体破壊".equals(cause) || "部品破壊".equals(cause) || "部品".equals(cause)) {
			if(grievanceMgrPartsDetailsList.isEmpty()) {
				throw new RRException(i.t("GrievanceMgrService.update.grievanceMgrPartsDetailsIsEmpty"));//“部品位置”、“部品品番”、“メーカ名”不能为空！
			}
			Map<String,String> msgMap = MapUtils.hms(
				"componentPos.require",i.t("GrievanceMgrService.validate.componentPos.require"),//部品位置不能为空
				"componentPos.max",i.t("GrievanceMgrService.validate.componentPos.max"),//部品位置长度不能超过{0}个字符
				"partNum.require",i.t("GrievanceMgrService.validate.partNum.require"),//部品品番不能为空
				"partNum.max",i.t("GrievanceMgrService.validate.partNum.max"),//部品品番长度不能超过{0}个字符
				"makerName.require",i.t("GrievanceMgrService.validate.makerName.require"),//メーカ名不能为空
				"makerName.max",i.t("GrievanceMgrService.validate.makerName.max")//メーカ名长度不能超过{0}个字符
			);
			for (int i = grievanceMgrPartsDetailsList.size() - 1;i>-1;i--) {
				GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
				DataValidateUtils.validate(
					msgMap,
					grievanceMgrPartsDetails,
					"componentPos","require|max:255",
					"partNum","require|max:255",
					"makerName","require|max:255"
				);
			}
		} else {
			Map<String,String> msgMap = MapUtils.hms(
				"componentPos.max",i.t("GrievanceMgrService.validate.componentPos.max"),//部品位置长度不能超过{0}个字符
				"partNum.max",i.t("GrievanceMgrService.validate.partNum.max"),//部品品番长度不能超过{0}个字符
				"makerName.max",i.t("GrievanceMgrService.validate.makerName.max")//メーカ名长度不能超过{0}个字符
			);
			for (int i = grievanceMgrPartsDetailsList.size() - 1;i>-1;i--) {
				GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
				DataValidateUtils.validate(
					msgMap,
					grievanceMgrPartsDetails,
					"componentPos","max:255",
					"partNum","max:255",
					"makerName","max:255"
				);
			}
		}
//		if(saveData.getOneAnsDay()!=null) {
//			if(saveData.getIntermediateAnsDay()!=null) {
//				if (saveData.getFinalRespDate()!=null) {
//					DataValidateUtils.validate(
//					MapUtils.hms(
//						"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//						"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max"),//１次回答内容不能超过{0}个字符
//						"intermediateAnsAttr.require", i.t("GrievanceMgrService.validate.intermediateAnsAttr.require"),//中間回答内容不能为空
//						"intermediateAnsAttr.max",i.t("GrievanceMgrService.validate.intermediateAnsAttr.max"),//中間回答内容长度不能超过{0}个字符
//						"finalAnsAttr.require",i.t("GrievanceMgrService.validate.finalAnsAttr.require"),//最終回答内容不能为空
//						"finalAnsAttr.max",i.t("GrievanceMgrService.validate.finalAnsAttr.max")//最終回答内容长度不能超过{0}个字符
//					),
//					saveData,
//					"oneRespContent","require|max:255",
//					"intermediateAnsAttr","require|max:255",
//					"finalAnsAttr","require|max:255");
//				} else {
//					DataValidateUtils.validate(
//					MapUtils.hms(
//						"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//						"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max"),//１次回答内容不能超过{0}个字符
//						"intermediateAnsAttr.require", i.t("GrievanceMgrService.validate.intermediateAnsAttr.require"),//中間回答内容不能为空
//						"intermediateAnsAttr.max",i.t("GrievanceMgrService.validate.intermediateAnsAttr.max")//中間回答内容长度不能超过{0}个字符
//					),
//					saveData,
//					"oneRespContent","require|max:255",
//					"intermediateAnsAttr","require|max:255");
//				}
//			} else {				
//				DataValidateUtils.validate(
//				MapUtils.hms(
//					"oneRespContent.require",i.t("GrievanceMgrService.validate.oneRespContent.require"),//１次回答内容不能为空
//					"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max")//１次回答内容不能超过{0}个字符
//				),
//				saveData,
//				"oneRespContent","require|max:255");
//			}
//		} else {
//			DataValidateUtils.validate(
//			MapUtils.hms(
//				"oneRespContent.max",i.t("GrievanceMgrService.validate.oneRespContent.max")//１次回答内容不能超过{0}个字符
//			),
//			saveData,
//				"oneRespContent","max:255"
//			);
//		}
		saveData.setVersion(0);
		int used = checkMgmNoUsed2(saveData.getMgmNo());
		if (used == -1) {
			//throw new RRException(i.t("GrievanceMgrService.save.mgmNoNotExists"));//管理No.不存在，保存失败！
		} else if(used == 1) {
			throw new RRException(i.t("GrievanceMgrService.save.mgmNoAlreadyUsed"));//管理No.已被使用，保存失败！
		}
		if(!custItemTblService.checkExists(entity.getProdNum(), entity.getCustomerService())) {
			throw new RRException(i.t("GrievanceMgrService.save.custItemTblNotExists"));//“製品品番” 或 “お客様品番” 错误，保存失败
		}
		HashSet<Integer> uploadFileIdSet = new HashSet<>();
		for(int i=0,len_i = grievanceMgrUploadFileList.size();i<len_i;i++) {
			uploadFileIdSet.add(grievanceMgrUploadFileList.get(i).getFileId());
		}
		if (!uploadFileIdSet.isEmpty()) {
			QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.in("id", uploadFileIdSet);
			queryWrapper.select("id");
			if (uploadFileService.count(queryWrapper) != uploadFileIdSet.size()) {
				throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！
			}
		}
		return save2(saveData, grievanceMgrPartsDetailsList, grievanceMgrUploadFileList, acceptEmailSet);
	}
	public int checkMgmNoUsed2(String mgmNo) {
		QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("mgmNo", mgmNo);
		queryWrapper.select("mgmNo");
		GrievanceMgrEntity mgmNoAllocation = getOne(queryWrapper);
		return mgmNoAllocation == null?0:1;
	}

	@Autowired
	private GrievanceMgrPartsDetailsService grievanceMgrPartsDetailsService;
	
	@Autowired
	private DataSourceTransactionManager dataSourceTransactionManager;

	@Autowired
	private TransactionDefinition transactionDefinition;
	
	/**
	 * 这是只能由 save 调用的方法，这时因为避免在多数据源的情况下，开启事务后，
	 * 数据源切换不了的问题。所以使用手动开启事务, 不用注解事务！
	 * @param saveData
	 * @return
	 */
	private boolean save2(GrievanceMgrEntity saveData,List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList,
			List<GrievanceMgrUploadFileEntity> grievanceMgrUploadFileList, HashSet<String> acceptEmailSet) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		try {
			boolean ret = save(saveData);
			if(ret) {
				UpdateWrapper<MgmNoAllocationEntity> updateWrapper = new UpdateWrapper<>();
				updateWrapper.eq("mgm_no", saveData.getMgmNo());
				updateWrapper.set("used", YesOrNo.是.getV());
				mgmNoAllocationService.update(updateWrapper);
				Integer newId = saveData.getId();
				if (!grievanceMgrPartsDetailsList.isEmpty()) {
					List<GrievanceMgrPartsDetailsEntity> saveGrievanceMgrPartsDetailsList = new ArrayList<>();
					for (int i=0,len_i = grievanceMgrPartsDetailsList.size();i<len_i;i++) {
						GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
						GrievanceMgrPartsDetailsEntity saveGrievanceMgrPartsDetails = new GrievanceMgrPartsDetailsEntity();
						saveGrievanceMgrPartsDetails.setGrievanceMgrId(newId);
						saveGrievanceMgrPartsDetails.setComponentPos(grievanceMgrPartsDetails.getComponentPos());
						saveGrievanceMgrPartsDetails.setPartNum(grievanceMgrPartsDetails.getPartNum());
						saveGrievanceMgrPartsDetails.setMakerName(grievanceMgrPartsDetails.getMakerName());
						saveGrievanceMgrPartsDetailsList.add(saveGrievanceMgrPartsDetails);
					}
					grievanceMgrPartsDetailsService.saveBatch(saveGrievanceMgrPartsDetailsList);
				}
				if (!grievanceMgrUploadFileList.isEmpty()) {
					for (int i=0,len_i = grievanceMgrUploadFileList.size();i<len_i;i++) {
						GrievanceMgrUploadFileEntity grievanceMgrUploadFile = grievanceMgrUploadFileList.get(i);
						grievanceMgrUploadFile.setGrievanceMgrId(newId);
					}
					grievanceMgrUploadFileService.saveBatch(grievanceMgrUploadFileList);
				}
				if (!acceptEmailSet.isEmpty()) {
					List<GrievanceMgrMsgAcceptEntity> saveList = new ArrayList<>();
					for(String acceptEmail:acceptEmailSet) {
						GrievanceMgrMsgAcceptEntity grievanceMgrMsgAccept = new GrievanceMgrMsgAcceptEntity();
						grievanceMgrMsgAccept.setEmail(acceptEmail);
						grievanceMgrMsgAccept.setGrievanceMgrId(newId);
						saveList.add(grievanceMgrMsgAccept);
					}
					grievanceMgrMsgAcceptService.saveBatch(saveList);
				}
			}
			dataSourceTransactionManager.commit(transactionStatus);
			return ret;
		} catch (Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}
}