package com.pstfs.base.service;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.InboxMessageEntity;
import com.pstfs.base.esUtils.ElasticsearchConnPool;
import com.pstfs.base.esUtils.ElasticsearchIndex;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.extend.InboxMessageExtend;
import com.pstfs.base.mapper.InboxMessageMapper;
import com.pstfs.base.scheduling.InboxMessageElasticsearch;
@Service("inboxMessageService")
public class InboxMessageService extends BaseService<InboxMessageMapper, InboxMessageEntity> {

	@Autowired
	private InboxMessageElasticsearch inboxMessageElasticsearch;
	
	/**
	 * 更新站内信阅读状态
	 * @param id
	 * @param receiverUsername
	 * @return
	 */
	public boolean updateReaded(Integer id, String receiverUsername) {
		UpdateWrapper<InboxMessageEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.setSql("readed_time=now()");
		updateWrapper.eq("id", id);
		updateWrapper.eq("receiver_username", receiverUsername);
		updateWrapper.isNull("readed_time");
		boolean ret = update(updateWrapper);
		if (ret) {
			inboxMessageElasticsearch.dataSync();
		}
		return ret;
	}
	
	/**
	 * 删除站内信
	 * @param id
	 * @return
	 */
	public boolean remove(Integer id,String receiverUsername) {
		UpdateWrapper<InboxMessageEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.setSql("delete_time=now()");
		updateWrapper.eq("id", id);
		updateWrapper.eq("receiver_username", receiverUsername);
		updateWrapper.isNull("delete_time");
		boolean ret = update(updateWrapper);
		if (ret) {
			inboxMessageElasticsearch.dataSync();
		}
		return ret;
	}
	
	/**
	 * 统计符合条件的数据总量，最多统计前100条，超过100则返回100，低于100则返回实际数量
	 * @param params
	 * @param field
	 * @return
	 */
	public int count(Map<String,Object> params) {
		QueryWrapper<InboxMessageEntity> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			String receiverUsername = (String)params.get("receiverUsername");
			if(StringUtils.isNotEmpty(receiverUsername)) {
				queryWrapper.eq("receiver_username", receiverUsername);
			}
			String delete = (String)params.get("delete");
			if ("NULL".equals(delete)) {
				queryWrapper.isNull("delete_time");
			} else if("NOT_NULL".equals(delete)) {
				queryWrapper.isNotNull("delete_time");
			}
			String readed = (String)params.get("readed");
			if ("NULL".equals(readed)) {
				queryWrapper.isNull("readed_time");
			} else if("NOT_NULL".equals(readed)) {
				queryWrapper.isNotNull("readed_time");
			}
		}
		return baseMapper.topCount(queryWrapper);
	}
	
	/**
	 * 滚动式加载消息内容
	 * @param params
	 * @param field
	 * @return
	 */
	public List<InboxMessageExtend> scrollLoad(Map<String,Object> params,String field) {
		QueryWrapper<InboxMessageExtend> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			String receiverUsername = (String)params.get("receiverUsername");
			if(StringUtils.isNotEmpty(receiverUsername)) {
				queryWrapper.eq("im.receiver_username", receiverUsername);
			}
			String delete = (String)params.get("delete");
			if ("NULL".equals(delete)) {
				queryWrapper.isNull("im.delete_time");
			} else if("NOT_NULL".equals(delete)) {
				queryWrapper.isNotNull("im.delete_time");
			}
			String readed = (String)params.get("readed");
			if ("NULL".equals(readed)) {
				queryWrapper.isNull("im.readed_time");
			} else if("NOT_NULL".equals(readed)) {
				queryWrapper.isNotNull("im.readed_time");
			}
			String createTimeRangeStart = (String)params.get("createTimeRangeStart");
			if (StringUtils.isNotEmpty(createTimeRangeStart)) {
				queryWrapper.ge("imt.create_time", createTimeRangeStart);
			}
			String createTimeRangeEnd = (String)params.get("createTimeRangeEnd");
			if (StringUtils.isNotEmpty(createTimeRangeEnd)) {
				queryWrapper.le("imt.create_time", createTimeRangeEnd);
			}
		}
		queryWrapper.select(field);
		scrollLoadSplit(params, queryWrapper, "im.");
		return baseMapper.listExtend(queryWrapper);
	}
	
	@Autowired
	private ElasticsearchConnPool elasticsearchConnPool;

	/**
	 * 从 es 中滚动式加载数据
	 * @param params
	 * @param field
	 * @return
	 */
	public List<Map<String, Object>> scrollLoadFromEs(Map<String,Object> params){
		SearchRequest request = new SearchRequest();
	    request.indices(ElasticsearchIndex.INBOX_MESSAGE_INDEX);
	    BoolQueryBuilder bqb = QueryBuilders.boolQuery();
	    //由客户端传入的查询参数
		if (params != null) {
		    RangeQueryBuilder createTimeRangeQuery = QueryBuilders.rangeQuery("createTime");
		    String createTimeRangeStart = (String)params.get("createTimeRangeStart");
		    boolean createTimeRangeStartEmpty = StringUtils.isNotEmpty(createTimeRangeStart); 
			if (createTimeRangeStartEmpty) {
				LocalDateTime createTimeRangeStartTime = LocalDateTime.parse(createTimeRangeStart, DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm"));
				createTimeRangeQuery.gte(createTimeRangeStartTime.toEpochSecond(ZoneOffset.UTC));
			}
			String createTimeRangeEnd = (String)params.get("createTimeRangeEnd");
			boolean createTimeRangeEndEmpty = StringUtils.isNotEmpty(createTimeRangeEnd);
			if (createTimeRangeEndEmpty) {
				LocalDateTime createTimeRangeEndTime = LocalDateTime.parse(createTimeRangeEnd, DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm"));
				createTimeRangeQuery.lte(createTimeRangeEndTime.toEpochSecond(ZoneOffset.UTC));
			}
			if (createTimeRangeStartEmpty || createTimeRangeEndEmpty) {
				bqb.must(createTimeRangeQuery);
			}
			String receiverUsername = (String)params.get("receiverUsername");
			if(StringUtils.isNotEmpty(receiverUsername)) {
				bqb.must(QueryBuilders.termQuery("receiverUsername", receiverUsername));
			}
			String titleLike = (String)params.get("titleLike");
			if(StringUtils.isNotEmpty(titleLike)) {
				bqb.must(QueryBuilders.matchQuery("title", titleLike));
			}
			String contentLike = (String)params.get("contentLike");
			if (StringUtils.isNotEmpty(contentLike)) {
				bqb.must(QueryBuilders.matchQuery("content", contentLike));	
			}
			String delete = (String)params.get("delete");
			if ("NULL".equals(delete)) {
				bqb.mustNot(QueryBuilders.existsQuery("deleteTime"));
			} else if("NOT_NULL".equals(delete)) {
				bqb.must(QueryBuilders.existsQuery("deleteTime"));
			}
			String readed = (String)params.get("readed");
			if ("NULL".equals(readed)) {
				bqb.mustNot(QueryBuilders.existsQuery("readedTime"));
			} else if("NOT_NULL".equals(readed)) {
				bqb.must(QueryBuilders.existsQuery("readedTime"));
			}
			
			//滚动式加载
			Object lastId = params.get("lastId");
			Object firstId = params.get("firstId");
			RangeQueryBuilder idRangeQuery = QueryBuilders.rangeQuery("id");
			if(lastId != null) {
				idRangeQuery.lt(lastId);
			} else if(firstId != null) {
				idRangeQuery.gt(firstId);
			} else {
				idRangeQuery.gt(-1);
			}
			bqb.must(idRangeQuery);
		}
		SearchSourceBuilder ssb = new SearchSourceBuilder();
		String[] includes = {"readed","title","simpleContent","creatorUsername","createTime","id","readedTime","inboxMessageTextId"};
		String[] excludes = {};
		ssb.fetchSource(includes, excludes);
		ssb.sort("id", SortOrder.DESC); 
		ssb.from(0);
		Integer limit = (Integer)params.get("limit");
		if(limit == null) {
			limit = 20;
		}
		ssb.size(limit);
		ssb.query(bqb);
		request.source(ssb);
		
		RestHighLevelClient esClient = elasticsearchConnPool.getEsClient();
		if (esClient == null) {
			throw new RRException(i.t("common.sysBusy"));//系统繁忙，请稍候重试！
		}
		try {
			SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);
			elasticsearchConnPool.returnEsClient(esClient);
			SearchHits hits = response.getHits();
			ArrayList<Map<String, Object>> ret = new ArrayList<>();
			for (SearchHit hit:hits) {
				Map<String, Object> dataMap = hit.getSourceAsMap();
				ret.add(dataMap);
			}
			return ret;
		} catch (IOException e) {
			elasticsearchConnPool.dropEsClient(esClient);
			log.error(e.getMessage(), e);
			throw new RRException(e.getMessage());
		}
	}
}