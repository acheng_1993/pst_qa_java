package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.EnumTableEntity;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.mapper.EnumTableMapper;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.validate.DataValidateUtils;

/**
 * 枚举表业务类
 * @author jiajian
 */
@Service("enumTableService")
public class EnumTableService extends BaseService<EnumTableMapper, EnumTableEntity> {
	
	/**
	 * 获取所有枚举组
	 * @return
	 */
	public List<String> getEnumGroupList(){
		List<String> ret = new ArrayList<>();
		ret.addAll(ENUM_TABLE_MAP.keySet());
		return ret;
	}
	
	/**
	 * 获取枚举数据
	 * @param params
	 * @param field
	 * @return
	 */
	public List<EnumTableEntity> list(Map<String,Object> params){
		List<EnumTableEntity> ret = new ArrayList<>();
		if (params == null || params.isEmpty()) {
			return ret;
		} else {
			List<String> enumGroupList = (List<String>)params.get("enumGroupList");
			if (enumGroupList == null) {
				enumGroupList = new ArrayList<String>();
			}
			String enumGroup = (String)params.get("enumGroup");
			if (StringUtils.isNotEmpty(enumGroup)) {
				enumGroupList.add(enumGroup);
			}
			Integer status = (Integer)params.get("status");
			boolean statusParamExists = status != null;
			if(enumGroupList.isEmpty()) {
				for (EnumTableEntity enumTable:ENUM_TABLE_ID_MAP.values()) {
					if(statusParamExists) {
						if (status.equals(enumTable.getStatus())) {
							ret.add(copy(enumTable));
						}
					} else {
						ret.add(copy(enumTable));
					}
				}
			} else {
				for (String tempEnumGroup:enumGroupList) {
					Map<String,EnumTableEntity> enumTableMap2 = ENUM_TABLE_MAP.get(tempEnumGroup);
					if(enumTableMap2 != null) {
						for (EnumTableEntity enumTable:enumTableMap2.values()) {
							if(statusParamExists) {
								if (status.equals(enumTable.getStatus())) {
									ret.add(copy(enumTable));
								}
							} else {
								ret.add(copy(enumTable));
							}
						}
					}
				}
			}
		}
		return ret;
	}
	
	/**
	 * 复制一份数据
	 * @param enumTable
	 * @return
	 */
	private EnumTableEntity copy(EnumTableEntity enumTable) {
		EnumTableEntity copyEnumTable = new EnumTableEntity();
		copyEnumTable.setId(enumTable.getId());
		copyEnumTable.setStatus(enumTable.getStatus());
		copyEnumTable.setEnumDesc(enumTable.getEnumDesc());
		copyEnumTable.setEnumGroup(enumTable.getEnumGroup());
		return copyEnumTable;
	}
	
	/**
	 * 保存枚举值
	 * @param enumGroup
	 * @param enumDesc
	 * @return
	 */
	@Transactional
	public boolean save(String enumGroup,List<String> enumDescList,List<Integer> statusList) {
		if (enumDescList.isEmpty()) {
			throw new RRException(i.t("EnumTableService.save.enumDescIsEmpty"));//枚举描述不能为空
		}
		if (enumDescList.size()>1000) {
			throw new RRException(i.t("EnumTableService.save.enumDescLen"));//枚举描述不能超过1000个
		}
		TreeSet<String> enumDescSet = new TreeSet<>(enumDescList);
		if (enumDescSet.size()!=enumDescList.size()) {
			throw new RRException(i.t("common.commitException"));//提交的数据异常，保存失败！
		}
		DataValidateUtils.validate(
		MapUtils.hms(
			"enumGroup.require",i.t("EnumTableService.validate.enumGroup.require"),//枚举组不能为空
			"enumGroup.max",i.t("EnumTableService.validate.enumGroup.max")//枚举组长度不能超过{0}个字符
		),
		MapUtils.hms(
			"enumGroup",enumGroup
		),
			"enumGroup","require|max:45"
		);
		Map<String,String> msgs = MapUtils.hms(
			"enumDesc.require",i.t("EnumTableService.validate.enumDesc.require"),//枚举描述不能为空
			"enumDesc.max",i.t("EnumTableService.validate.enumDesc.max")//枚举描述长度不能超过{0}个字符
		);
		for(String enumDesc:enumDescSet) {
			DataValidateUtils.validate(
					msgs,
			MapUtils.hms(
				"enumGroup",enumGroup,
				"enumDesc",enumDesc
			),
				"enumDesc","require|max:45"
			);
		}
		List<EnumTableEntity> saveList = new ArrayList<>();
		List<EnumTableEntity> updateList = new ArrayList<>();
		Map<String,EnumTableEntity> enumTableMap2 = ENUM_TABLE_MAP.get(enumGroup);
		if (enumTableMap2 == null) {
			for (int i=0,len_i=enumDescList.size();i<len_i;i++) {
				String enumDesc = enumDescList.get(i);
				EnumTableEntity entity = new EnumTableEntity();
				entity.setEnumGroup(enumGroup);
				entity.setEnumDesc(enumDesc);
				saveList.add(entity);
			}
		} else {
			for (int i=0,len_i=enumDescList.size();i<len_i;i++) {
				String enumDesc = enumDescList.get(i);
				EnumTableEntity enumTable = enumTableMap2.get(enumDesc);
				if (enumTable == null) {
					EnumTableEntity entity = new EnumTableEntity();
					entity.setEnumGroup(enumGroup);
					entity.setEnumDesc(enumDesc);
					saveList.add(entity);
				} else {
					EnumTableEntity entity = new EnumTableEntity();
					entity.setId(enumTable.getId());
					entity.setEnumGroup(enumGroup);
					entity.setEnumDesc(enumDesc);
					if (statusList.size() > i) {
						entity.setStatus(statusList.get(i));
					}
					updateList.add(entity);
				}
			}
		}
		boolean saveRet = false;
		if(!saveList.isEmpty()) {
			saveRet = saveBatch(saveList);
		}
		boolean updateRet = false;
		if(!updateList.isEmpty()) {
			updateRet = updateBatchById(updateList);
		}
		if(saveRet) {
			saveCache(saveList);
		}
		if(updateRet) {
			updateCache(updateList);
		}
		return saveRet || updateRet;
	}

	/**
	 * 按照枚举组、枚举描述分组
	 */
	private final static Map<String,Map<String,EnumTableEntity>> ENUM_TABLE_MAP = new HashMap<>();
	
	/**
	 * 所有枚举数据的映射
	 */
	private final static Map<Integer,EnumTableEntity> ENUM_TABLE_ID_MAP = new HashMap<>();
	
	/**
	 * 自旋锁
	 */
	private final static AtomicBoolean LOCK = new AtomicBoolean(Boolean.FALSE);
	
	@PostConstruct
	public void initEnumTable() {
		List<EnumTableEntity> enumTableList = list();
		for(int i=0,len_i = enumTableList.size();i<len_i;i++) {
			EnumTableEntity entity = enumTableList.get(i);
			ENUM_TABLE_ID_MAP.put(entity.getId(), entity);
			String enumGroup = entity.getEnumGroup();
			Map<String,EnumTableEntity> enumTableMap2 = ENUM_TABLE_MAP.get(enumGroup);
			if(enumTableMap2 == null) {
				ENUM_TABLE_MAP.put(enumGroup, enumTableMap2 = new HashMap<>());
			}
			String enumDesc = entity.getEnumDesc();
			enumTableMap2.put(enumDesc, entity);
		}
	}

	/**
	 * 新增缓存数据
	 * @param saveList
	 */
	private void saveCache(List<EnumTableEntity> saveList) {
		for (;;) {
			if (LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				String enumGroup = saveList.get(0).getEnumGroup();
				Map<String,EnumTableEntity> enumTableMap2 = ENUM_TABLE_MAP.get(enumGroup);
				if(enumTableMap2 == null) {
					ENUM_TABLE_MAP.put(enumGroup, enumTableMap2 = new HashMap<>());
				}
				for(int i=0,len_i = saveList.size();i<len_i;i++) {
					EnumTableEntity enumTableEntity = saveList.get(i);
					ENUM_TABLE_ID_MAP.put(enumTableEntity.getId(), enumTableEntity);
					enumTableMap2.put(enumTableEntity.getEnumDesc(), enumTableEntity);
				}
				LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
	
	/**
	 * 更新缓存数据
	 * @param updateList
	 */
	private void updateCache(List<EnumTableEntity> updateList) {
		for (;;) {
			if (LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				for(int i=0,len_i = updateList.size();i<len_i;i++) {
					EnumTableEntity enumTableEntity = updateList.get(i);
					EnumTableEntity enumTableFromCache = ENUM_TABLE_ID_MAP.get(enumTableEntity.getId());
					if (enumTableEntity.getEnumDesc().equals(enumTableFromCache.getEnumDesc())) {
						ENUM_TABLE_ID_MAP.put(enumTableEntity.getId(), enumTableEntity);
					} else {
						Map<String,EnumTableEntity> enumTableMap2 = ENUM_TABLE_MAP.get(enumTableFromCache.getEnumGroup());
						enumTableMap2.remove(enumTableFromCache.getEnumDesc());
						enumTableMap2.put(enumTableEntity.getEnumDesc(), enumTableEntity);
					}
				}
				LOCK.set(Boolean.FALSE);
				break;
			}
		}
	}
}