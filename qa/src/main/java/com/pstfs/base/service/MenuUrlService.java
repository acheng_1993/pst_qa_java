package com.pstfs.base.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.MenuUrlEntity;
import com.pstfs.base.mapper.MenuUrlMapper;

@Service("menuUrlService")
public class MenuUrlService extends ServiceImpl<MenuUrlMapper, MenuUrlEntity> {

}