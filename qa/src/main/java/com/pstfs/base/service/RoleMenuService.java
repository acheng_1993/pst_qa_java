package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.RoleMenuEntity;
import com.pstfs.base.entity.UserEntity;
import com.pstfs.base.mapper.RoleMenuMapper;

@Service("roleMenuService")
public class RoleMenuService extends ServiceImpl<RoleMenuMapper, RoleMenuEntity> {

	@Lazy
	@Autowired
	private UserService userService;
  
	/**
	 * 修改角色对应的菜单
	 * @param roleId
	 * @param menuIdList
	 * @return
	 */
	@Transactional
	public boolean update(Integer roleId,List<Integer> menuIdList) {
		QueryWrapper<RoleMenuEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("role_id", roleId);
		queryWrapper.select("id,menu_id");
		List<RoleMenuEntity> roleMenuEntityList = list(queryWrapper);
		Map<Integer,Integer> menuIdMap = new HashMap<>();
		for (int i=0,len_i = roleMenuEntityList.size();i<len_i;i++) {
			RoleMenuEntity roleMenuEntity = roleMenuEntityList.get(i);
			menuIdMap.put(roleMenuEntity.getMenuId(), roleMenuEntity.getId());
		}
		List<RoleMenuEntity> saveList = new ArrayList<>();
		for (int i = 0,len_i=menuIdList.size();i<len_i;i++) {
			Integer menuId = menuIdList.get(i);
			if (menuIdMap.remove(menuId) != null) {
				RoleMenuEntity roleMenuEntity = new RoleMenuEntity();
				roleMenuEntity.setMenuId(menuId);
				roleMenuEntity.setRoleId(roleId);
				saveList.add(roleMenuEntity);
			}
		}
		removeByIds(menuIdMap.values());
		return saveBatch(saveList);
	}
}