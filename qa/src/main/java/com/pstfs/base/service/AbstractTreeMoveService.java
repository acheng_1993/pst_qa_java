package com.pstfs.base.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.intf.ISortable;
import com.pstfs.base.exception.RRException;

/**
 * 抽象的移动树节点类
 * @author jiajian
 * @param <M>
 * @param <T>
 */
public abstract class AbstractTreeMoveService <M extends BaseMapper<T>, T extends ISortable> extends BaseService<M, T>{

	/**
     * 创建实体类
     * @return
     */
    protected abstract T createEntity();
    
    /**
     * 获取主键字段
     * @return
     */
    protected String getPk() {
        return "id";
    }

    /**
     * 获取父级id的字段
     * @return
     */
    protected String getParentIdField() {
        return "parent_id";
    }

    /**
     * 获取排序字段
     * @return
     */
    protected String getSortIndexField() {
        return "sort_index";
    }
    
    /**
     * 是否可以移动
     * @return
     */
    protected abstract boolean moveAble(int id, int targetId, int moveType);
    
    /**
     * 节点移动
     * @param int $id 被移动的节点id
     * @param int $targetId 目标id
     * @param int $moveType 移动方式
     */
    @Transactional
    public boolean move(int id, int targetId, int moveType) {
        if (moveAble(id, targetId, moveType) == false) {
//        	不能这样移动节点，移动失败！
            throw new RRException(i.t("AbstractTreeMoveService.move.moveDisable"));
        }
        String parentIdField = getParentIdField();
        String sortIndexField = getSortIndexField();
        String pk = getPk();
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        T data = createEntity();
        data.setId(id);
        try {
        	List<T> saveList = new ArrayList<>();
            if (moveType == 0) {
            	queryWrapper = queryWrapper.eq(parentIdField, targetId).select("max("+sortIndexField+") "+sortIndexField);
            	T tempObj = getOne(queryWrapper);
            	if (tempObj == null) {
            		data.setSortIndex(0);
            	} else {
            		Integer maxSortIndex = tempObj.getSortIndex();
            		data.setSortIndex(maxSortIndex + 1);
            	}
                data.setParentId(targetId);
            } else if (moveType == -1) {
            	queryWrapper = queryWrapper.eq(pk, targetId).select(parentIdField,sortIndexField);
                T tempData = getOne(queryWrapper);
                
                data.setParentId(tempData.getParentId());
                data.setSortIndex(tempData.getSortIndex());
                
                queryWrapper = new QueryWrapper<>();
                queryWrapper = queryWrapper.eq(parentIdField, tempData.getParentId()).ne(pk, id)
                	.ge(sortIndexField,tempData.getSortIndex()).select(pk, sortIndexField);
                List<T> treeNodeList = list(queryWrapper);
                for (int i = 0, len_i = treeNodeList.size(); i < len_i; i++) {
                	T treeNode = treeNodeList.get(i);
                	T saveNode = createEntity();
                	saveNode.setId(treeNode.getId());
                	saveNode.setSortIndex(treeNode.getSortIndex()+1);
                    saveList.add(saveNode);
                }
                updateBatchById(saveList);
            } else if (moveType == 1) {
            	queryWrapper = queryWrapper.eq(pk, targetId).select(parentIdField, sortIndexField);
                T tempData = getOne(queryWrapper);
                data.setParentId(tempData.getParentId());
                
                queryWrapper = new QueryWrapper<>();
                queryWrapper = queryWrapper.eq(parentIdField, tempData.getParentId()).gt(sortIndexField, tempData.getSortIndex()).select(pk,sortIndexField);
                List<T> treeNodeList = list(queryWrapper);
                if (treeNodeList.isEmpty()) {
                	data.setSortIndex(tempData.getSortIndex()+1);
                } else {
                	data.setSortIndex(treeNodeList.get(0).getSortIndex());
                    for (int i = 0, len_i = treeNodeList.size(); i < len_i; i++) {
                        T treeNode = treeNodeList.get(i);
                        T saveNode = createEntity();
                        saveNode.setId(treeNode.getId());
                        saveNode.setSortIndex(treeNode.getSortIndex()+1);
                        saveList.add(saveNode);
                    }
                    updateBatchById(saveList);
                }
            }
            boolean ret = updateById(data);
            moveCache(data, saveList);
            return ret;
        } catch (Exception ex) {
            throw new RRException(ex.getMessage(),ex);
        }
    }
    
    /**
     * 把缓存的对象也进行移动
     * @param data
     */
    protected abstract void moveCache(T data,List<T> saveList);
}