package com.pstfs.base.service;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.pstfs.base.entity.BreakPointTransEntity;
import com.pstfs.base.entity.UploadFileEntity;
import com.pstfs.base.envm.BreakPointTransRetCode;
import com.pstfs.base.envm.BreakPointTransStatus;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.mapper.BreakPointTransMapper;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.utils.MyReentrantLock;
import com.pstfs.base.validate.DataValidateUtils;

import cn.hutool.crypto.SecureUtil;

@Service("breakPointTransService")
public class BreakPointTransService extends BaseService<BreakPointTransMapper, BreakPointTransEntity> {
	
	private String breakPointTransPath;
	
	private String uploadFilePath;
	
	public BreakPointTransService(@Value("${breakPointTransPath}") String breakPointTransPath,@Value("${uploadFilePath}") String uploadFilePath) {
		File file = new File(breakPointTransPath);
		file.mkdirs();
		this.breakPointTransPath = breakPointTransPath;
		file = new File(uploadFilePath);
		file.mkdirs();
		this.uploadFilePath = uploadFilePath;
	}
	
	/**
	 * 根据上次删除的进度 lastProgressId，然后以及本次删除的数据数 limit 获取本次需要
	 * 检阅的 upload_file 表的数据最后一条的 id 即 newLastProgressId，并返回。
	 * 实际检阅的文件是 [lastProgressId,newLastProgressId] 区间内的数据（仅限于 1:处理完成、2:中断 的数据）
	 * @param lastProgressId
	 * @param limit
	 * @return
	 */
	public Integer newLastProgressId(int lastProgressId,int limit) {
		return baseMapper.newLastProgressId(lastProgressId, limit);
	}
	
	/**
	 * 自动把超过一周时间内上传进度没有任何改变的，传输状态是 0:处理中 的文件标记为已中断 
	 */
	public void changeTransStatus() {
		UpdateWrapper<BreakPointTransEntity> queryWrapper = new UpdateWrapper<>();
		queryWrapper.le("update_time", new Date(System.currentTimeMillis()-7*24*3600*1000));
		queryWrapper.eq("trans_status", BreakPointTransStatus.处理中.getV());
		queryWrapper.set("trans_status", BreakPointTransStatus.中断.getV());
		update(queryWrapper);
	}

	/**
	 * 获取符合查询条件的断点续传数据
	 * @param params
	 * @param field
	 * @param limit
	 * @return
	 */
	public List<BreakPointTransEntity> list(Map<String,Object> params,String field,int limit) {
		QueryWrapper<BreakPointTransEntity> queryWrapper = new QueryWrapper<>();
		if (params != null) {
			Integer idRangeStart = (Integer)params.get("idRangeStart");
			if (idRangeStart != null) {
				queryWrapper.ge("id", idRangeStart);
			}
			Integer idRangeEnd = (Integer)params.get("idRangeEnd");
			if (idRangeEnd != null) {
				queryWrapper.le("id", idRangeEnd);
			}
			Integer transStatus = (Integer)params.get("transStatus");
			if (transStatus != null) {
				queryWrapper.eq("trans_status", transStatus);
			}
			List<Integer> transStatusList = (List<Integer>)params.get("transStatusList");
			if (transStatusList!=null && !transStatusList.isEmpty()) {
				queryWrapper.in("trans_status", transStatusList);
			}
		}
		queryWrapper.select(field);
		if (limit > 0) {
			queryWrapper.last(" limit "+limit);
		}
		return list(queryWrapper);
	}
	
	/**
	 * 按照创建者和sha1获取处理中的断点续传进度信息
	 * @param creatorUsername
	 * @param sha1
	 * @return
	 */
	public BreakPointTransEntity getByIdAndCreatorUsername(String creatorUsername,String sha1) {
		QueryWrapper<BreakPointTransEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("creator_username", creatorUsername);
		queryWrapper.eq("sha1", sha1);
		queryWrapper.eq("trans_status", BreakPointTransStatus.处理中.getV());
		queryWrapper.select("id,process");
		return getOne(queryWrapper);
	}
	
	/**
	 * 中断断点续传
	 * @param creatorUsername
	 * @param breakPointTransId
	 * @return
	 */
	public boolean cancelUpload(String creatorUsername, int breakPointTransId) {
		UpdateWrapper<BreakPointTransEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("creator_username", creatorUsername);
		updateWrapper.eq("id", breakPointTransId);
		updateWrapper.eq("trans_status", BreakPointTransStatus.处理中.getV());
		updateWrapper.set("trans_status", BreakPointTransStatus.中断.getV());
		return update(updateWrapper);
	}
	
	/**
	 * 更新断点续传的进度
	 * @param breakPointTransId
	 * @param prevProcess
	 * @param base64Data
	 * @param finish 结束标记
	 * @return
	 */
	@Transactional
	public int update(String creatorUsername, int breakPointTransId, int prevProcess, String base64Data,boolean[] finish) {
		//先获取正在上传中的文件进度、sha1、文件大小信息
		BreakPointTransEntity breakPointTransEntity;
		{
			QueryWrapper<BreakPointTransEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("id", breakPointTransId);
			queryWrapper.eq("creator_username", creatorUsername);
			queryWrapper.eq("trans_status", BreakPointTransStatus.处理中.getV());
			queryWrapper.select("process,file_size,sha1");
			breakPointTransEntity = getOne(queryWrapper);
		}
		if(breakPointTransEntity == null) {
			return BreakPointTransRetCode.文件上传进度异常.getV();
		} else {
			//客户端进度大于服务端进度，则判定为异常
			if (prevProcess > breakPointTransEntity.getProcess()) {
				return BreakPointTransRetCode.文件上传进度异常.getV();
			}
			//客户端上传进度+上传数据大小＞总大小，则判定为异常
			byte[] tempData = Base64.decodeBase64(base64Data);
			if (tempData.length + prevProcess > breakPointTransEntity.getFileSize()) {
				return BreakPointTransRetCode.文件上传进度异常.getV();
			}
			//计算实际写入的有效数据长度
			int effectLen = tempData.length + prevProcess - breakPointTransEntity.getProcess();
			//如果实际写入长度不到1字节，则无需写入
			if (effectLen<1) {
				return breakPointTransEntity.getProcess();
			}
			//如果实际有效写入长度比切片后的长度小，则截取有效长度
			if (effectLen < tempData.length) {
				tempData = Arrays.copyOfRange(tempData, tempData.length - effectLen, tempData.length);
			}
			UpdateWrapper<BreakPointTransEntity> updateWrapper = new UpdateWrapper<>();
			updateWrapper.eq("id", breakPointTransId);
			updateWrapper.eq("process", breakPointTransEntity.getProcess());
			updateWrapper.eq("trans_status", BreakPointTransStatus.处理中.getV());
			updateWrapper.set("process", prevProcess + tempData.length);
			boolean ret = update(updateWrapper);
			if (ret) {
				//按照断点续传的id创建锁，防止同时有两个线程写入到文件
				if(MyReentrantLock.tryLockNum(breakPointTransEntity.getId())) {
					//文件路径如下：根目录/sha1前2位/主键id
					File file = new File(breakPointTransPath+File.separator+breakPointTransEntity.getSha1().substring(0,2)+File.separator+breakPointTransEntity.getId());
					try (RandomAccessFile raf = new RandomAccessFile(file, "rw")){
						//临时文件容量不足，需要扩容
						if (prevProcess + tempData.length> raf.length()) {
							//最多扩容 50MB
							raf.setLength(Math.min(breakPointTransEntity.getFileSize(), raf.length()+50*1024*1024));
						}
						raf.seek(breakPointTransEntity.getProcess());
						raf.write(tempData);
						finish[0] = prevProcess + tempData.length == breakPointTransEntity.getFileSize();
						return prevProcess + tempData.length;
					} catch (IOException e) {
						log.error(e.getMessage(), e);
						return BreakPointTransRetCode.文件上传进度异常.getV();
					} finally {
						MyReentrantLock.unlockNum(breakPointTransEntity.getId());
					}
				} else {
					return BreakPointTransRetCode.禁止同时执行两个上传相同文件的上传任务.getV();
				}
			} else {
				return BreakPointTransRetCode.文件上传进度异常.getV();
			}
		}
	}
	
	@Autowired
	private UploadFileService uploadFileService;
	
	/**
	 * 当上传完毕时移动文件到上传文件夹中
	 * @param breakPointTransId
	 * @return
	 */
	@Transactional
	public int moveUploadFinishFile(String creatorUsername,int breakPointTransId) {
		QueryWrapper<BreakPointTransEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("id", breakPointTransId);
		queryWrapper.eq("creator_username", creatorUsername);
		queryWrapper.select("sha1");
		BreakPointTransEntity breakPointTransEntity = getOne(queryWrapper);
		if(breakPointTransEntity == null) {
			return -1;
		}
		File srcFile = new File(breakPointTransPath+File.separator+breakPointTransEntity.getSha1().substring(0,2)+File.separator+breakPointTransEntity.getId());
		String sha1 = SecureUtil.sha1(srcFile);
		UpdateWrapper<BreakPointTransEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", breakPointTransId);
		updateWrapper.eq("trans_status", BreakPointTransStatus.处理中.getV());
		updateWrapper.apply("process=file_size");
		updateWrapper.set("sha1", sha1);
		updateWrapper.set("trans_status", BreakPointTransStatus.处理完成.getV());
		if(update(updateWrapper)) {
			UploadFileEntity uploadFileEntity = uploadFileService.getBySha1(sha1, "id");
			if(uploadFileEntity == null) {
				int retId = uploadFileService.save(sha1);
				String targetPath = uploadFilePath + File.separator + sha1.substring(0, 2) + File.separator + sha1.substring(2, 4);
				File targetPathFile = new File(targetPath);
				targetPathFile.mkdirs();
				File targetFile = new File(targetPath+File.separator+sha1.substring(4));
				srcFile.renameTo(targetFile);
				return retId;
			} else {
				return uploadFileEntity.getId();
			}
		} else {
			return -1;
		}
	}
	
	/**
	 * 新增断点续传任务
	 * @param sha1
	 * @param fileSize
	 * @return
	 */
	@Transactional
	public BreakPointTransEntity save(String creatorUsername,String sha1, int fileSize) {
		BreakPointTransEntity saveEntity = new BreakPointTransEntity();
		saveEntity.setSha1(sha1);
		saveEntity.setFileSize(fileSize);
		saveEntity.setCreatorUsername(creatorUsername);
		DataValidateUtils.validate(
		MapUtils.hms(
			"sha1.require",i.t("BreakPointTransService.validate.sha1.require"),//sha1不能为空
			"sha1.regex", i.t("BreakPointTransService.validate.sha1.regex"),//sha1格式错误
			"fileSize.range",i.t("BreakPointTransService.validate.fileSize.range")//文件大小必须在{0}到{1}字节之间
		),
		saveEntity,
			"sha1","require|regex:'^[a-f0-9]{40}$'",
			"fileSize","range:1,"+Integer.MAX_VALUE
		);
		BreakPointTransEntity exists = getByIdAndCreatorUsername(creatorUsername, sha1);
		//如果发现已存在未上传完毕的任务，则直接返回该任务id
		if (exists != null) {
			return exists;
		}
		saveEntity.setTransStatus(BreakPointTransStatus.处理中.getV());
		saveEntity.setProcess(0);
		save(saveEntity);
		File tempPath = new File(breakPointTransPath+File.separator+sha1.substring(0,2));
		//创建目录
		tempPath.mkdirs();
		//文件路径如下：根目录/sha1前2位/主键id
		File file = new File(breakPointTransPath+File.separator+sha1.substring(0,2)+File.separator+saveEntity.getId());
		try {
			//创建空文件
			file.createNewFile();
		} catch (IOException e) {
			throw new RRException(e.getMessage(),e);
		}
		//如果断点续传的文件大于50MB 时创建 50MB 的文件
		if (fileSize > 50*1024*1024) {
			fileSize = 50*1024*1024;
		}
		try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
			raf.setLength(fileSize);
		} catch (Exception e) {
			throw new RRException(e.getMessage(),e);
		}
		return saveEntity;
	}	
}