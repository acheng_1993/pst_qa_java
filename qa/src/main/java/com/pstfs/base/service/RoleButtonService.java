package com.pstfs.base.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.base.entity.RoleButtonEntity;
import com.pstfs.base.mapper.RoleButtonMapper;

@Service("roleButtonService")
public class RoleButtonService extends ServiceImpl<RoleButtonMapper, RoleButtonEntity> {

}