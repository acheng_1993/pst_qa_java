package com.pstfs.base.service;
import java.io.File;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pstfs.base.entity.UploadFileEntity;
import com.pstfs.base.entity.UploadFileTableEntity;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.mapper.UploadFileMapper;

import cn.hutool.crypto.SecureUtil;

@Service("uploadFileService")
public class UploadFileService extends BaseService<UploadFileMapper, UploadFileEntity> {

	private String uploadFilePath;
	
	public UploadFileService(@Value("${uploadFilePath}") String uploadFilePath) {
		File file = new File(uploadFilePath);
		file.mkdirs();
		this.uploadFilePath = uploadFilePath;
	}
	
	/**
	 * 根据上次删除的进度 lastProgressId，然后以及本次删除的数据数 limit 获取本次需要
	 * 检阅的 upload_file 表的数据最后一条的 id 即 newLastProgressId，并返回。
	 * 实际检阅的文件是 [lastProgressId,newLastProgressId] 区间内的数据
	 * @param lastProgressId
	 * @param limit
	 * @return
	 */
	public Integer newLastProgressId(int lastProgressId,int limit) {
		return baseMapper.newLastProgressId(lastProgressId, limit);
	}
	
	/**
	 * 按照主键 id 获取上传文件信息
	 * @param id
	 * @param field
	 * @return
	 */
	public UploadFileEntity getById(Integer id,String field) {
		QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("id", id);
		queryWrapper.select(field);
		return getOne(queryWrapper);
	}
	
	/**
	 * 按照 sha1 获取上传文件信息
	 * @param sha1
	 * @param field
	 * @return
	 */
	public UploadFileEntity getBySha1(String sha1,String field) {
		QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("sha1", sha1);
		queryWrapper.select(field);
		return getOne(queryWrapper);
	}
	
	/**
	 * 保存上传文件
	 * @param sha1
	 * @param path
	 * @return 返回主键id
	 */
	public Integer save(String sha1) {
		UploadFileEntity uploadFileEntity = new UploadFileEntity();
		uploadFileEntity.setSha1(sha1);
		uploadFileEntity.setPath(sha1.substring(0, 2) + File.separator + sha1.substring(2, 4) + File.separator + sha1.substring(4));
		save(uploadFileEntity);
		return uploadFileEntity.getId();
	}
	
	/**
	 * 把脱离文件表管理的临时文件保存（相当于交由文件表来管理）
	 * @param tempFile
	 * @param delTempFile 保存后是否删除临时文件
	 * @return
	 */
	@Transactional
	public Integer saveTempFile(File tempFile,boolean delTempFile) {
		String sha1 = SecureUtil.sha1(tempFile);
		UploadFileEntity uploadFileEntity = getBySha1(sha1, "id");
		Integer id = uploadFileEntity == null?null:uploadFileEntity.getId();
		if(id == null) {
			id = save(sha1);
			if (delTempFile) {
				tempFile.renameTo(new File(uploadFilePath+File.separator+sha1.substring(0, 2) + File.separator + sha1.substring(2, 4) + File.separator + sha1.substring(4)));
			}
		} else {
			if (delTempFile) {
				tempFile.delete();
			}
		}
		return id;
	}
	
	@Autowired
	private UploadFileTableService uploadFileTableService;
	
	/**
	 * 从 [lastProgressId,newLastProgressId] 区间内的所有主键 id 中寻找未使用的文件列表
	 * @param lastProgressId
	 * @param newLastProgressId
	 * @return
	 */
    public List<UploadFileEntity> notUseUploadFileList(int lastProgressId,int newLastProgressId) {
    	List<com.pstfs.base.entity.UploadFileTableEntity> uploadFileTableEntityList = uploadFileTableService.list();
    	QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.select("id,path");
    	StringBuilder sb = new StringBuilder("where id > ").append(lastProgressId).append(" and id <= ").append(newLastProgressId).append(" and datediff(now(),create_time) >= 1 ");
    	if (uploadFileTableEntityList.isEmpty()) {
    		throw new RRException(i.t("UploadFileService.notUseUploadFileList.uploadFileTableIsEmpty"));//upload_file_table 表没有数据，停止删除！
    	}
    	for (UploadFileTableEntity uploadFileTableEntity:uploadFileTableEntityList) {
    		sb.append(" and not exists(select id from ").append(uploadFileTableEntity.getTableName())
    		.append(" where ").append(uploadFileTableEntity.getTableName()).append(".").append(uploadFileTableEntity.getFileIdField())
    		.append(" = upload_file.id)");
    	}
    	sb.append("order by id asc");
    	queryWrapper.last(sb.toString());
    	return list(queryWrapper);
    }
}