package com.pstfs.base.esUtils;
import java.io.IOException;
import java.util.HashMap;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.utils.I18nUtils;

/**
 * Elasticsearch 的链接池
 * @author jiajian
 */
@Component
public class ElasticsearchConnPool {

	/**
	 * 连接池
	 */
	private RestHighLevelClient[] connPoolArray;
	
	/**
	 * 连接的最近使用时间
	 */
	private long[] lastUseTimeArray;
	
	/**
	 * 最小连接数
	 */
	private final static int MIN_CONN_COUNT = 8;
	
	/**
	 * 现在连接数
	 */
	private static int CUR_CONN_COUNT = 0;
	
	/**
	 * 最大连接数
	 */
	private final static int MAX_CONN_COUNT = 32;
	
	/**
	 * 使用的连接
	 */
	private HashMap<RestHighLevelClient,Integer> usedConn;
	
	@Autowired
	private I18nUtils i;
	
	/**
	 * 使用的连接下标
	 */
	private int usedIdx;
	
	public ElasticsearchConnPool() {
		connPoolArray = new RestHighLevelClient[MAX_CONN_COUNT];
		lastUseTimeArray = new long[MAX_CONN_COUNT];
		for (int i=0; i<MIN_CONN_COUNT; i++) {
			connPoolArray[i] = new RestHighLevelClient(
				RestClient.builder(new HttpHost("localhost", 9200, "http")));
			lastUseTimeArray[i] = System.currentTimeMillis();
		}
		CUR_CONN_COUNT = MIN_CONN_COUNT;
		usedConn = new HashMap<RestHighLevelClient,Integer>();
	}
	
	/**
	 * 清除空闲的连接
	 */
	private synchronized void removeFreeConn() {
		RestHighLevelClient esConn = null;
		for (int i = MAX_CONN_COUNT - 1;CUR_CONN_COUNT > MIN_CONN_COUNT && i>-1; i--) {
			if ((esConn = connPoolArray[i]) != null && (usedIdx & ((long)1<<i)) == 0) {
				//超过 5 分钟没被使用都视为闲置连接
				if (System.currentTimeMillis() - lastUseTimeArray[i]>5*60*1000) {
					if (esConn != null) {
						try {
							esConn.close();
							connPoolArray[i] = null;
							CUR_CONN_COUNT--;
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	/**
	 * 归还连接
	 * @param ret
	 */
	public synchronized void returnEsClient(RestHighLevelClient esClient) {
		Integer idx = usedConn.remove(esClient);
		if (idx == null) {
//			归还连接失败，该连接并不是从连接池获取的！
			throw new RRException(i.t("ElasticsearchConnPool.returnEsClient.returnEsConnError"));
		} else {
			usedIdx -= ((long)idx);
			notifyAll();
		}
		removeFreeConn();
	}
	
	/**
	 * 把连接销毁,该连接连不上 Elasticsearch 的服务端，视为废弃连接
	 * @param esClient
	 */
	public synchronized void dropEsClient(RestHighLevelClient esClient) {
		Integer idx = usedConn.remove(esClient);
		if (idx == null) {
//			归还连接失败，该连接并不是从连接池获取的！
			throw new RRException(i.t("ElasticsearchConnPool.dropEsClient.dropEsConnError"));
		} else {
			usedIdx -= (((long)1)<<idx);
			connPoolArray[idx] = new RestHighLevelClient(
				RestClient.builder(new HttpHost("localhost", 9200, "http"))
			);
			notifyAll();
		}
		try {
			if(esClient != null) {
				esClient.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		removeFreeConn();
	}
	
	/**
	 * 从连接池获取连接，如果尝试两次都拿不到连接，则返回 null
	 * @return
	 */
	public synchronized RestHighLevelClient getEsClient() {
		int i=-1;
		RestHighLevelClient ret = null;
		try {
			for(int h=0;;h++) {
				//获取未使用的连接
				for (i=0;(usedIdx & (((long)1)<<i)) == 1;i++);
				//存在未使用的连接
				if (i < connPoolArray.length) {
					ret = connPoolArray[i];
					lastUseTimeArray[i] = System.currentTimeMillis();
					usedConn.put(ret, i);
					usedIdx += ((long)1)<<i;
					return ret;
				//不存在未使用的连接
				} else {
					//如果未超出最大连接数，
					if (i < MAX_CONN_COUNT) {
						RestHighLevelClient esClient = new RestHighLevelClient(
								RestClient.builder(new HttpHost("localhost", 9200, "http")));
						connPoolArray[i] = esClient;
						lastUseTimeArray[i] = System.currentTimeMillis();
						CUR_CONN_COUNT++;
						return ret = esClient;
					//如果超出最大连接数，则进行等待，并让出锁。
					} else {
						//h是尝试获取锁的次数，连续2次获取锁失败，直接报系统繁忙
						if (h > 0) {
							return null;
						}
						wait(1000);
						//等待结束后重新尝试获取连接
						continue;
					}
				}
			}
		} catch (Exception e) {
			throw new RRException(e.getMessage());
		} finally {
			if (ret != null) {
				usedConn.put(ret, i);
				usedIdx+=((long)1)<<i;
			}
		}
	}
}