package com.pstfs.base.esUtils;

/**
 * Elasticsearch 索引
 * @author jiajian
 */
public class ElasticsearchIndex {

	/**
	 * 站内信索引
	 * 删除：curl -XDELETE "http://localhost:9200/inbox_message_index"
	 */
	public final static String INBOX_MESSAGE_INDEX = "inbox_message_index";
}
