package com.pstfs.base.scheduling;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.pstfs.base.Main;
import com.pstfs.base.service.InboxEmailMessageService;
import com.pstfs.base.service.InboxMessageService;
import com.pstfs.base.service.InboxMessageTextService;
import com.pstfs.base.service.UserService;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class InboxMessageSendEmail {
	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	/**
	 * task2的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task2Lock;

	/**
	 * 发送邮件的用户
	 */
	private final String MAIL_FROM = "pstfs_system03@pst-hk.com";

	/**
	 * 发送邮件的用户密码
	 */
	private final String MAIL_FROM_PASSWORD = "System010";

	private Properties pro;
	
	public InboxMessageSendEmail() {
		task1Lock = new ReentrantLock();
		task2Lock = new ReentrantLock();
		Main.LOCK_LIST.add(task1Lock);
		Main.LOCK_LIST.add(task2Lock);
		
		pro = new Properties();
		pro.setProperty("mail.host", "smtp.mxhichina.com");
		pro.setProperty("mail.from", MAIL_FROM);
		pro.setProperty("mail.smtp.host", "smtp.mxhichina.com");
		pro.setProperty("mail.transport.protocol", "smtp");
		pro.setProperty("mail.smtp.starttls.enable", "true");
		pro.setProperty("mail.smtp.auth", "true");
		pro.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // 使用JSSE的SSL
		pro.setProperty("mail.smtp.socketFactory.fallback", "false");
		pro.setProperty("mail.smtp.port", "465");
		pro.setProperty("mail.smtp.socketFactory.port", "465");
	}

	@Value("${spring.profiles.active}")
	private String springProfilesActive;

	@Autowired
	private InboxMessageService inboxMessageService;
	
	@Autowired
	private InboxEmailMessageService inboxEmailMessageService;

	@Autowired
	private InboxMessageTextService inboxMessageTextService;

	@Autowired
	private UserService userService;

	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task2() {
		boolean trySuccess = task2Lock.tryLock();
		if (trySuccess) {
			log.info("task2()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
//				try {
//					long lastExecuteTime = System.currentTimeMillis();
//					for (;;) {
//						if (System.currentTimeMillis() - lastExecuteTime > 5*60*1000) {
//							log.info("自动把站内信发送到邮箱的功能，运行时长超过5分钟，马上退出...");
//							break;
//						}
//						// 查询未发送的邮箱信息
//						List<InboxEmailMessageEntity> inboxEmailMessageList;
//						{
//							QueryWrapper<InboxEmailMessageEntity> queryWrapper = new QueryWrapper<>();
//							queryWrapper.eq("sended_email", SendedEmail.否.getV());
//							queryWrapper.select("id,inbox_message_text_id,receiver_email");
//							inboxEmailMessageList = inboxEmailMessageService.list(queryWrapper);
//							if (inboxEmailMessageList.isEmpty()) {
//								break;
//							}
//						}
//						//整理发送的站内信对应要发送的目标邮箱
//						Map<Integer, List<InboxEmailMessageEntity>> sendEmailMap = new HashMap<>();
//						// 整理要发送的站内信
//						Map<Integer, InboxMessageTextEntity> inboxMessageTextMap = new HashMap<>();
//						{
//							HashSet<Integer> inboxMessageTextIdSet = new HashSet<>();
//							for (int i=0,len_i = inboxEmailMessageList.size();i<len_i;i++) {
//								InboxEmailMessageEntity inboxEmailMessage = inboxEmailMessageList.get(i);
//								Integer inboxMessageTextId = inboxEmailMessage.getInboxMessageTextId();
//								inboxMessageTextIdSet.add(inboxMessageTextId);
//								
//								List<InboxEmailMessageEntity> emailList = sendEmailMap.get(inboxMessageTextId);
//								if (emailList == null) {
//									sendEmailMap.put(inboxMessageTextId, emailList = new ArrayList<>());
//								}
//								emailList.add(inboxEmailMessage);
//							}
//							QueryWrapper<InboxMessageTextEntity> queryWrapper = new QueryWrapper<>();
//							queryWrapper.in("id", inboxMessageTextIdSet);
//							queryWrapper.select("id,title,content,message_type");
//							List<InboxMessageTextEntity> inboxMessageTextList = inboxMessageTextService.list(queryWrapper);
//							for (int i = 0, len_i = inboxMessageTextList.size(); i < len_i; i++) {
//								InboxMessageTextEntity inboxMessageText = inboxMessageTextList.get(i);
//								inboxMessageTextMap.put(inboxMessageText.getId(), inboxMessageText);
//							}
//						}
//						
//						for (Entry<Integer, InboxMessageTextEntity> entry : inboxMessageTextMap.entrySet()) {
//							List<InboxEmailMessageEntity> emailList = sendEmailMap.get(entry.getKey());
//							Transport transport = null;
//							try {
//								Session session = Session.getDefaultInstance(pro);
//								MimeMessage message = new MimeMessage(session);
//								message.setFrom(new InternetAddress(MAIL_FROM));
//								
//								List<Integer> inboxEmailMessageIdList = new ArrayList<>();
//								Address[] tos = new Address[emailList.size()];
//								for (int i = 0, len_i = emailList.size(); i < len_i; i++) {
//									InboxEmailMessageEntity inboxEmailMessage = emailList.get(i);
//									tos[i] = new InternetAddress(inboxEmailMessage.getReceiverEmail());
//									inboxEmailMessageIdList.add(inboxEmailMessage.getId());
//								}
//								InboxMessageTextEntity inboxMessageText = entry.getValue();
//								message.addRecipients(Message.RecipientType.TO, tos);
//								message.setSubject(inboxMessageText.getTitle());
//								Multipart multipart = new MimeMultipart();
//								BodyPart bodyPart = new MimeBodyPart();
//								if (inboxMessageText.getMessageType() == MessageType.文本.getV()) {
//									bodyPart.setText(inboxMessageText.getContent());
//								} else if (inboxMessageText.getMessageType() == MessageType.html.getV()) {
//									bodyPart.setContent(inboxMessageText.getContent(), "text/html;charset=UTF-8");
//								}
//								multipart.addBodyPart(bodyPart);
//								message.setContent(multipart);
//								message.saveChanges();
//								
//								transport = session.getTransport("smtp");
//								transport.connect("smtp.mxhichina.com", MAIL_FROM, MAIL_FROM_PASSWORD);
//								transport.sendMessage(message, message.getAllRecipients());
//								
//								// 针对已发送的邮箱，把是否发送状态更新为:是
//								UpdateWrapper<InboxEmailMessageEntity> updateWrapper = new UpdateWrapper<>();
//								updateWrapper.eq("inbox_message_text_id", entry.getKey());
//								updateWrapper.eq("sended_email", SendedEmail.否.getV());
//								updateWrapper.in("id", inboxEmailMessageIdList);
//								updateWrapper.set("sended_email", SendedEmail.是.getV());
//								inboxEmailMessageService.update(updateWrapper);
//							} catch (AddressException e) {
//								InboxEmailMessageEntity inboxEmailMessage = emailList.get(e.getPos());
//								//针对邮箱地址设置异常的用户，把是否发送状态更新为:暂无用户邮箱信息无法发送
//								UpdateWrapper<InboxEmailMessageEntity> updateWrapper = new UpdateWrapper<>();
//								updateWrapper.eq("receiver_email", inboxEmailMessage.getReceiverEmail());
//								updateWrapper.eq("inbox_message_text_id", entry.getKey());
//								updateWrapper.eq("sended_email", SendedEmail.否.getV());
//								updateWrapper.set("sended_email", SendedEmail.邮箱地址有误发送失败.getV());
//								inboxEmailMessageService.update(updateWrapper);
//								log.error(e.getMessage(), e);
//							} catch(Exception e) {
//								log.error(e.getMessage(), e);
//							} finally {
//								if (transport != null) {
//									transport.close();
//								}
//							}
//						}
//					}
//				} catch (Exception e) {
//					log.error(e.getMessage(), e);
//				}
			}
			log.info("task2()：任务结束...");
			task2Lock.unlock();
		}
	}
	
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
//				try {
//					long lastExecuteTime = System.currentTimeMillis();
//					for (;;) {
//						if (System.currentTimeMillis() - lastExecuteTime > 5*60*1000) {
//							log.info("自动把站内信发送到邮箱的功能，运行时长超过5分钟，马上退出...");
//							break;
//						}
//						// 查询未发送的消息
//						List<InboxMessageEntity> inboxMessageList;
//						{
//							QueryWrapper<InboxMessageEntity> queryWrapper = new QueryWrapper<>();
//							queryWrapper.eq("sended_email", SendedEmail.否.getV());
//							queryWrapper.select("id,inbox_message_text_id,receiver_username");
//							// 注意:由于阿里邮箱最大发送人数是300人，因此每次最多取300人进行发送
//							queryWrapper.last(" limit 300");
//							inboxMessageList = inboxMessageService.list(queryWrapper);
//							if (inboxMessageList.isEmpty()) {
//								break;
//							}
//						}
//						// 整理这些消息的用户对应的邮箱地址
//						Map<String, UserEntity> userMap = new HashMap<>();
//						{
//							HashSet<String> usernameSet = new HashSet<>();
//							for (int i = 0, len_i = inboxMessageList.size(); i < len_i; i++) {
//								InboxMessageEntity inboxMessage = inboxMessageList.get(i);
//								usernameSet.add(inboxMessage.getReceiverUsername());
//							}
//							QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
//							queryWrapper.in("username", usernameSet);
//							queryWrapper.select("username,email");
//							List<UserEntity> userList = userService.list(queryWrapper);
//							for (int i = 0, len_i = userList.size(); i < len_i; i++) {
//								UserEntity user = userList.get(i);
//								userMap.put(user.getUsername(), user);
//							}
//						}
//						// 整理这些消息要发送的邮箱列表
//						Map<Integer, List<UserEntity>> sendUsersMap = new HashMap<>();
//						{
//							for (int i = 0, len_i = inboxMessageList.size(); i < len_i; i++) {
//								InboxMessageEntity inboxMessage = inboxMessageList.get(i);
//								UserEntity user = userMap.get(inboxMessage.getReceiverUsername());
//								if (StringUtils.isNotEmpty(user.getEmail())) {
//									List<UserEntity> userList = sendUsersMap.get(inboxMessage.getInboxMessageTextId());
//									if (userList == null) {
//										userList = new ArrayList<UserEntity>();
//										sendUsersMap.put(inboxMessage.getInboxMessageTextId(), userList);
//									}
//									userList.add(user);
//								}
//							}
//						}
//						// 整理要发送的站内信
//						Map<Integer, InboxMessageTextEntity> inboxMessageTextMap = new HashMap<>();
//						{
//							QueryWrapper<InboxMessageTextEntity> queryWrapper = new QueryWrapper<>();
//							queryWrapper.in("id", sendUsersMap.keySet());
//							queryWrapper.select("id,title,content,message_type");
//							List<InboxMessageTextEntity> inboxMessageTextList = inboxMessageTextService.list(queryWrapper);
//							for (int i = 0, len_i = inboxMessageTextList.size(); i < len_i; i++) {
//								InboxMessageTextEntity inboxMessageText = inboxMessageTextList.get(i);
//								inboxMessageTextMap.put(inboxMessageText.getId(), inboxMessageText);
//							}
//						}
//						for (Entry<Integer, InboxMessageTextEntity> entry : inboxMessageTextMap.entrySet()) {
//							List<UserEntity> userList = sendUsersMap.get(entry.getKey());
//							Transport transport = null;
//							try {
//								Session session = Session.getDefaultInstance(pro);
//								MimeMessage message = new MimeMessage(session);
//								message.setFrom(new InternetAddress(MAIL_FROM));
//								
//								List<String> usernameList = new ArrayList<>();
//								Address[] tos = new Address[userList.size()];
//								for (int i = 0, len_i = userList.size(); i < len_i; i++) {
//									UserEntity user = userList.get(i);
//									tos[i] = new InternetAddress(user.getEmail());
//									usernameList.add(user.getUsername());
//								}
//								InboxMessageTextEntity inboxMessageText = entry.getValue();
//								message.addRecipients(Message.RecipientType.TO, tos);
//								message.setSubject(inboxMessageText.getTitle());
//								Multipart multipart = new MimeMultipart();
//								BodyPart bodyPart = new MimeBodyPart();
//								if (inboxMessageText.getMessageType() == MessageType.文本.getV()) {
//									bodyPart.setText(inboxMessageText.getContent());
//								} else if (inboxMessageText.getMessageType() == MessageType.html.getV()) {
//									bodyPart.setContent(inboxMessageText.getContent(), "text/html;charset=UTF-8");
//								}
//								multipart.addBodyPart(bodyPart);
//								message.setContent(multipart);
//								message.saveChanges();
//								
//								transport = session.getTransport("smtp");
//								transport.connect("smtp.mxhichina.com", MAIL_FROM, MAIL_FROM_PASSWORD);
//								transport.sendMessage(message, message.getAllRecipients());
//								
//								// 针对已发送的用户，把是否发送状态更新为:是
//								UpdateWrapper<InboxMessageEntity> updateWrapper = new UpdateWrapper<>();
//								updateWrapper.eq("inbox_message_text_id", entry.getKey());
//								updateWrapper.eq("sended_email", SendedEmail.否.getV());
//								updateWrapper.in("receiver_username", usernameList);
//								updateWrapper.set("sended_email", SendedEmail.是.getV());
//								inboxMessageService.update(updateWrapper);
//								
//								// 针对未设置邮箱的用户，把是否发送状态更新为:暂无用户邮箱信息无法发送
//								updateWrapper = new UpdateWrapper<>();
//								updateWrapper.eq("inbox_message_text_id", entry.getKey());
//								updateWrapper.eq("sended_email", SendedEmail.否.getV());
//								updateWrapper.notIn("receiver_username", usernameList);
//								updateWrapper.set("sended_email", SendedEmail.暂无用户邮箱信息无法发送.getV());
//								inboxMessageService.update(updateWrapper);
//							} catch (AddressException e) {
//								UserEntity userEntity = userList.get(e.getPos());
//								//针对邮箱地址设置异常的用户，把是否发送状态更新为:暂无用户邮箱信息无法发送
//								UpdateWrapper<InboxMessageEntity> updateWrapper = new UpdateWrapper<>();
//								updateWrapper.eq("receiver_username", userEntity.getUsername());
//								updateWrapper.eq("inbox_message_text_id", entry.getKey());
//								updateWrapper.eq("sended_email", SendedEmail.否.getV());
//								updateWrapper.set("sended_email", SendedEmail.邮箱地址有误发送失败.getV());
//								inboxMessageService.update(updateWrapper);
//								log.error(e.getMessage(), e);
//							} catch(Exception e) {
//								log.error(e.getMessage(), e);
//							} finally {
//								if (transport != null) {
//									transport.close();
//								}
//							}
//						}
//					}
//				} catch (Exception e) {
//					log.error(e.getMessage(), e);
//				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}
}