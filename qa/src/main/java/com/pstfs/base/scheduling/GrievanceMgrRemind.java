package com.pstfs.base.scheduling;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.Main;
import com.pstfs.base.entity.GrievanceMgrEntity;
import com.pstfs.base.entity.GrievanceMgrPartsDetailsEntity;
import com.pstfs.base.entity.UserEntity;
import com.pstfs.base.envm.MessageType;
import com.pstfs.base.envm.YesOrNo;
import com.pstfs.base.extend.GrievanceMgrMsgAcceptExtend;
import com.pstfs.base.mapper.GrievanceMgrMsgAcceptMapper;
import com.pstfs.base.service.GrievanceMgrPartsDetailsService;
import com.pstfs.base.service.GrievanceMgrService;
import com.pstfs.base.service.InboxMessageTextService;
import com.pstfs.base.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GrievanceMgrRemind {

	/**
	 * task1,2,3,4,5的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	private Lock task2Lock;
	private Lock task3Lock;
	private Lock task4Lock;
	private Lock task5Lock;

	public GrievanceMgrRemind() {
		task1Lock = new ReentrantLock();
		task2Lock = new ReentrantLock();
		task3Lock = new ReentrantLock();
		task4Lock = new ReentrantLock();
		task5Lock = new ReentrantLock();
		Main.LOCK_LIST.add(task1Lock);
		Main.LOCK_LIST.add(task2Lock);
		Main.LOCK_LIST.add(task3Lock);
		Main.LOCK_LIST.add(task4Lock);
		Main.LOCK_LIST.add(task5Lock);
	}

	@Autowired
	private GrievanceMgrService grievanceMgrService;

	@Autowired
	private InboxMessageTextService inboxMessageTextService;

	@Value("${spring.profiles.active}")
	private String springProfilesActive;

	@Autowired
	private InboxMessageElasticsearch inboxMessageElasticsearch;
	
	@Autowired
	private GrievanceMgrPartsDetailsService grievanceMgrPartsDetailsService;

	@Autowired
	private DataSourceTransactionManager dataSourceTransactionManager;

	@Autowired
	private TransactionDefinition transactionDefinition;
	
	@Autowired
	private UserService userService;

	/**
	 * 每隔 10 分钟检测 如果お客様完了日没有填写，则每天报警一次。
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task5() {
		boolean trySuccess = task5Lock.tryLock();
		if (trySuccess) {
			log.info("task5()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					long lastExecuteTime = System.currentTimeMillis();
					String[] factoryTypeArray = {"F","M"};
					// 标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					boolean handleData = false;
					String url = "";
					for (int h=0,len_h = factoryTypeArray.length;h<len_h;h++) {
						String factoryType = factoryTypeArray[h];
						url = getUrl(factoryType);
						for (;;) {
							if (System.currentTimeMillis() - lastExecuteTime > 5 * 60 * 1000) {
								log.info("运行时长超过5分钟，马上退出...");
								break;
							}
							QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.isNull("cust_complet_date");
							queryWrapper.eq("factory_type", factoryType);
							queryWrapper.apply("(last_remind_date5 is null or datediff(now(),last_remind_date5)>=1)");
							queryWrapper.select("mgm_no,creator_username,editor_username");
							List<GrievanceMgrEntity> grievanceMgrList = grievanceMgrService.list(queryWrapper);
							if (grievanceMgrList.isEmpty()) {
								break;
							}
							//整理受付件数信息列表
							Map<String,GrievanceMgrEntity> grievanceMgrMap = new HashMap<>();
							{
								for (int i=0,len_i = grievanceMgrList.size();i<len_i;i++) {
									GrievanceMgrEntity grievanceMgr = grievanceMgrList.get(i);
									grievanceMgrMap.put(grievanceMgr.getMgmNo(), grievanceMgr);
								}
							}
							Map<String,List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap = getAllMgrMsgAccept(grievanceMgrList);
							sendMsg5(url, grievanceMgrMsgAcceptExtendMap);
						}
					}
					// 检测标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					if (handleData) {
						inboxMessageElasticsearch.dataSync();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task5()：任务结束...");
			task5Lock.unlock();
		}
	}

	/**
	 * 把发送消息和设置发送日期使用同一个事务，使用手动事务
	 * @param title
	 * @param content
	 * @param creatorUsername
	 * @param messageType
	 * @param sendEmail
	 * @param receiverUsername
	 * @param mgmNoList
	 */
	private void sendMsg5(String url, Map<String,List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		try {
			String content = "<a href=\"" + url + "\">点击这里填写</a>";
			String creatorUsername = "admin";
			Integer messageType = MessageType.html.getV();
			Integer sendEmail = YesOrNo.是.getV();
			for(Entry<String,List<GrievanceMgrMsgAcceptExtend>> entry:grievanceMgrMsgAcceptExtendMap.entrySet()) {
				String mgmNo = entry.getKey();
				List<GrievanceMgrMsgAcceptExtend> grievanceMgrMsgAcceptExtendList = entry.getValue();
				String title = "发现有单号“" + mgmNo + "”的受付件的お客様完了日没有填写，请及时填写。";
				HashSet<String> receiverUsernameSet = new HashSet<>();
				HashSet<String> receiverEmailSet = new HashSet<>();
				for (int i = 0,len_i = grievanceMgrMsgAcceptExtendList.size();i < len_i;i++) {
					GrievanceMgrMsgAcceptExtend grievanceMgrMsgAcceptExtend = grievanceMgrMsgAcceptExtendList.get(i);
					String username = grievanceMgrMsgAcceptExtend.getUsername();
					String email = grievanceMgrMsgAcceptExtend.getEmail();
					if(StringUtils.isEmpty(username) && !StringUtils.isEmpty(email)) {
						receiverEmailSet.add(email);
					} else {
						receiverUsernameSet.add(username);
					}
				}
				if (!receiverUsernameSet.isEmpty()) {
					inboxMessageTextService.save(title, content, creatorUsername, messageType, sendEmail, receiverUsernameSet.toArray(new String[receiverUsernameSet.size()]));
				}
				if (!receiverEmailSet.isEmpty()) {
					inboxMessageTextService.saveForEmail(title, content.toString(), creatorUsername, messageType, receiverEmailSet.toArray(new String[receiverEmailSet.size()]));
				}
				UpdateWrapper<GrievanceMgrEntity> updateWrapper = new UpdateWrapper<>();
				updateWrapper.eq("mgm_no", mgmNo);
				updateWrapper.setSql("last_remind_date5=now()");
				grievanceMgrService.update(updateWrapper);
			}
			dataSourceTransactionManager.commit(transactionStatus);
		} catch(Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}
	
	/**
	 * 每隔 10 分钟检测 “受付件数管理” 中如果某个品番在过去 1 年内有 3 个或以上相同位置、品番的苦情物品，每次新增一份
	 * 则报警一次。
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task4() {
		boolean trySuccess = task4Lock.tryLock();
		if (trySuccess) {
			log.info("task4()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					long lastExecuteTime = System.currentTimeMillis();
					String[] factoryTypeArray = {"F","M"};
					// 标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					boolean handleData = false;
					String url = "";
					for (int h=0,len_h = factoryTypeArray.length;h<len_h;h++) {
						String factoryType = factoryTypeArray[h];
						url = getUrl(factoryType);
						for (;;) {
							if (System.currentTimeMillis() - lastExecuteTime > 5 * 60 * 1000) {
								log.info("运行时长超过5分钟，马上退出...");
								break;
							}
							/**
							 * 获取最近一年同一份苦情中出现相同品番，位置3个以上的苦情对应的品番
							 */
							List<String> partNumList = new ArrayList<>();
							{//找出最近一年内，相同位置、品番三件以上的受付。
								QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
								queryWrapper.select("grievance_mgr_id,part_num");
								//保证数据是一年内产生的数据
								queryWrapper.apply("grievance_mgr_id>=(select min(id) from grievance_mgr where create_time >= DATE_SUB(NOW(), INTERVAL 1 YEAR))");
								queryWrapper.groupBy("grievance_mgr_id,component_pos,part_num");
								queryWrapper.having("count(*)>=3");
								List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(queryWrapper);
								for (int i=0,len_i = grievanceMgrPartsDetailsList.size();i<len_i;i++) {
									GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
									partNumList.add(grievanceMgrPartsDetails.getPartNum());
								}
							}
							if(partNumList.isEmpty()) {
								break;
							}
							/**
							 * 根据这些品番查询一年内新出现的苦情品番，作为消息发送给用户
							 */
							List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList;
							{
								QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
								queryWrapper.select("distinct grievance_mgr_id,part_num,component_pos");
								queryWrapper.exists("select id from grievance_mgr where id=grievance_mgr_id and last_remind_date4 is null and factory_type='"+factoryType+"'");
								queryWrapper.in("part_num", partNumList);
								//保证数据是一年内产生的数据
								queryWrapper.apply("grievance_mgr_id>=(select min(id) from grievance_mgr where create_time >= DATE_SUB(NOW(), INTERVAL 1 YEAR))");
								grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(queryWrapper);
							}
							if (grievanceMgrPartsDetailsList.isEmpty()) {
								break;
							} else {
								handleData = true;
							}
							List<GrievanceMgrEntity> grievanceMgrList;
							{
								List<Integer> idList = new ArrayList<>();
								for (int i=0,len_i=grievanceMgrPartsDetailsList.size();i<len_i;i++) {
									idList.add(grievanceMgrPartsDetailsList.get(i).getGrievanceMgrId());
								}
								QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
								queryWrapper.select("id,mgm_no,creator_username,editor_username");
								queryWrapper.in("id", idList);
								grievanceMgrList = grievanceMgrService.list(queryWrapper);
							}
							//整理受付件数信息列表
							Map<String,GrievanceMgrEntity> grievanceMgrMap = new HashMap<>();
							{
								for (int i=0,len_i = grievanceMgrList.size();i<len_i;i++) {
									GrievanceMgrEntity grievanceMgr = grievanceMgrList.get(i);
									grievanceMgrMap.put(grievanceMgr.getMgmNo(), grievanceMgr);
								}
							}
							//整理 grievanceMgrPartsDetailsList
							Map<Integer,GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsMap = new HashMap<>();
							for (int i=0,len_i = grievanceMgrPartsDetailsList.size();i<len_i;i++) {
								GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
								Integer grievanceMgrId = grievanceMgrPartsDetails.getGrievanceMgrId();
								grievanceMgrPartsDetailsMap.put(grievanceMgrId, grievanceMgrPartsDetails);
							}
							Map<String,List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap = getAllMgrMsgAccept(grievanceMgrList);
							sendMsg4(url, grievanceMgrMap, grievanceMgrPartsDetailsMap, grievanceMgrMsgAcceptExtendMap);
						}
					}
					// 检测标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					if (handleData) {
						inboxMessageElasticsearch.dataSync();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task4()：任务结束...");
			task4Lock.unlock();
		}
	}
	
	/**
	 * 把发送消息和设置发送日期使用同一个事务，使用手动事务
	 * @param title
	 * @param content
	 * @param creatorUsername
	 * @param messageType
	 * @param sendEmail
	 * @param receiverUsername
	 * @param idList
	 */
	private void sendMsg4(String url, Map<String,GrievanceMgrEntity> grievanceMgrMap, Map<Integer,GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsMap, Map<String,List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		try {
			String cellStyle = " style=\"border-right:1px solid;border-bottom:1px solid;\"";
			String creatorUsername = "admin";
			Integer messageType = MessageType.html.getV();
			Integer sendEmail = YesOrNo.是.getV();
			for (Entry<String,List<GrievanceMgrMsgAcceptExtend>> entry:grievanceMgrMsgAcceptExtendMap.entrySet()) {
				String mgmNo = entry.getKey();
				GrievanceMgrEntity grievanceMgr = grievanceMgrMap.get(mgmNo);
				GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsMap.get(grievanceMgr.getId());
				List<GrievanceMgrMsgAcceptExtend> grievanceMgrMsgAcceptExtendList = entry.getValue();
				String title = "发现有单号“"+mgmNo+"”的受付件的品番, 这些品番曾经在一年内有不低于 3 件异常, 请及时确认！";
				StringBuilder content = new StringBuilder("<table style=\"border-collapse:collapse;border-top:1px solid;border-left:1px solid;color:red;font-size:20px;\">")
						.append("<thead><tr><th").append(cellStyle).append(">管理No.</th><th").append(cellStyle).append(">部品品番</th><th").append(cellStyle).append(">部品位置</th></tr></thead>")
						.append("<tbody>").append("<tr><td").append(cellStyle).append(">").append(mgmNo)
						.append("</td><td").append(cellStyle).append(">").append(grievanceMgrPartsDetails.getPartNum())
						.append("</td><td").append(cellStyle).append(">").append(grievanceMgrPartsDetails.getComponentPos())
						.append("</td></tr>").append("</tbody></table>")
						.append("<a style=\"font-size:20px;\" href=\"").append(url).append("\">点击这里回答</a></div>");
				HashSet<String> receiverUsernameSet = new HashSet<>();
				HashSet<String> receiverEmailSet = new HashSet<>();
				for (int i = 0,len_i = grievanceMgrMsgAcceptExtendList.size();i < len_i;i++) {
					GrievanceMgrMsgAcceptExtend grievanceMgrMsgAcceptExtend = grievanceMgrMsgAcceptExtendList.get(i);
					String username = grievanceMgrMsgAcceptExtend.getUsername();
					String email = grievanceMgrMsgAcceptExtend.getEmail();
					if(StringUtils.isEmpty(username) && !StringUtils.isEmpty(email)) {
						receiverEmailSet.add(email);
					} else {
						receiverUsernameSet.add(username);
					}
				}
				if(!receiverUsernameSet.isEmpty()) {
					inboxMessageTextService.save(title, content.toString(), creatorUsername, messageType, sendEmail, receiverUsernameSet.toArray(new String[receiverUsernameSet.size()]));
				}
				if(!receiverEmailSet.isEmpty()) {
					inboxMessageTextService.saveForEmail(title, content.toString(), creatorUsername, messageType, receiverEmailSet.toArray(new String[receiverEmailSet.size()]));
				}
				UpdateWrapper<GrievanceMgrEntity> updateWrapper = new UpdateWrapper<>();
				updateWrapper.eq("mgm_no", mgmNo);
				updateWrapper.setSql("last_remind_date4=now()");
				grievanceMgrService.update(updateWrapper);
			}
			dataSourceTransactionManager.commit(transactionStatus);
		} catch(Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}

	/**
	 * 每隔 10 分钟检测 “受付件数管理” 中同一条记录有 3 个或以上相同位置、品番的苦情物品都会报警。
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task3() {
		boolean trySuccess = task3Lock.tryLock();
		if (trySuccess) {
			log.info("task3()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					long lastExecuteTime = System.currentTimeMillis();
					// 标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					boolean handleData = false;
					String[] factoryTypeArray = {"F","M"};
					String url = "";
					for (int h=0,len_h = factoryTypeArray.length;h<len_h;h++) {
						String factoryType = factoryTypeArray[h];
						url = getUrl(factoryType);
						for (;;) {
							if (System.currentTimeMillis() - lastExecuteTime > 5 * 60 * 1000) {
								log.info("运行时长超过5分钟，马上退出...");
								break;
							}
							Map<Integer,GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsMap;
							{
								QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
								queryWrapper.select("grievance_mgr_id,component_pos,part_num");
								queryWrapper.exists("(select id from grievance_mgr where id=grievance_mgr_id and last_remind_date3 is null and factory_type='"+factoryType+"')");
								queryWrapper.apply("grievance_mgr_id>=(select min(t.id) from (select id from grievance_mgr order by id desc limit 1000) t)");
								queryWrapper.groupBy("grievance_mgr_id,component_pos,part_num");
								queryWrapper.having("count(*)>2");
								List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(queryWrapper);
								if (grievanceMgrPartsDetailsList.isEmpty()) {
									break;
								} else {
									handleData = true;
								}
								grievanceMgrPartsDetailsMap = new HashMap<>();
								for (int i=0,len_i = grievanceMgrPartsDetailsList.size();i<len_i;i++) {
									GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsList.get(i);
									grievanceMgrPartsDetailsMap.put(grievanceMgrPartsDetails.getGrievanceMgrId(), grievanceMgrPartsDetails);
								}
							}
							List<GrievanceMgrEntity> grievanceMgrList;
							{
								QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
								queryWrapper.select("id,mgm_no,creator_username,editor_username");
								queryWrapper.in("id", grievanceMgrPartsDetailsMap.keySet());
								grievanceMgrList = grievanceMgrService.list(queryWrapper);
							}
							//整理受付件数信息列表
							Map<String,GrievanceMgrEntity> grievanceMgrMap = new HashMap<>();
							{
								for (int i=0,len_i = grievanceMgrList.size();i<len_i;i++) {
									GrievanceMgrEntity grievanceMgr = grievanceMgrList.get(i);
									grievanceMgrMap.put(grievanceMgr.getMgmNo(), grievanceMgr);
								}
							}
							Map<String,List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap = getAllMgrMsgAccept(grievanceMgrList);
							sendMsg3(url, grievanceMgrMap, grievanceMgrPartsDetailsMap, grievanceMgrMsgAcceptExtendMap);
						}
					}
					// 检测标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					if (handleData) {
						inboxMessageElasticsearch.dataSync();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task3()：任务结束...");
			task3Lock.unlock();
		}
	}
	
	/**
	 * 把发送消息和设置发送日期使用同一个事务，使用手动事务
	 * @param title
	 * @param content
	 * @param creatorUsername
	 * @param messageType
	 * @param sendEmail
	 * @param receiverUsername
	 * @param mgmNoList
	 */
	public void sendMsg3(String url, Map<String,GrievanceMgrEntity> grievanceMgrMap, Map<Integer,GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsMap, Map<String,List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		try {
			String cellStyle = " style=\"border-right:1px solid;border-bottom:1px solid;\"";
			String creatorUsername = "admin";
			Integer messageType = MessageType.html.getV();
			Integer sendEmail = YesOrNo.是.getV();
			for (Entry<String, List<GrievanceMgrMsgAcceptExtend>> entry:grievanceMgrMsgAcceptExtendMap.entrySet()) {
				String mgmNo = entry.getKey();
				GrievanceMgrEntity grievanceMgr = grievanceMgrMap.get(mgmNo);
				GrievanceMgrPartsDetailsEntity grievanceMgrPartsDetails = grievanceMgrPartsDetailsMap.get(grievanceMgr.getId());
				List<GrievanceMgrMsgAcceptExtend> grievanceMgrMsgAcceptExtendList = entry.getValue();
				String title = "发现有单号“"+mgmNo+"”的受付件有不低于3个相同品番和3个相同位置出现问题,请确认";
				StringBuilder content = new StringBuilder()
				.append("<table style=\"border-collapse:collapse;border-top:1px solid;border-left:1px solid;color:red;font-size:20px;\">")
				.append("<thead><tr><th").append(cellStyle).append(">管理No.</th><th").append(cellStyle).append(">部品品番</th><th").append(cellStyle).append(">部品位置</th></tr></thead>")
				.append("<tbody>").append("<tr><td").append(cellStyle).append(">").append(mgmNo)
				.append("</td><td").append(cellStyle).append(">").append(grievanceMgrPartsDetails.getPartNum())
				.append("</td><td").append(cellStyle).append(">").append(grievanceMgrPartsDetails.getComponentPos())
				.append("</td></tr>").append("</tbody></table>")
				.append("<a style=\"font-size:20px;\" href=\"").append(url).append("\">点击这里回答</a></div>");
				HashSet<String> receiverUsernameSet = new HashSet<>();
				HashSet<String> receiverEmailSet = new HashSet<>();
				for (int i = 0,len_i = grievanceMgrMsgAcceptExtendList.size();i < len_i;i++) {
					GrievanceMgrMsgAcceptExtend grievanceMgrMsgAcceptExtend = grievanceMgrMsgAcceptExtendList.get(i);
					String username = grievanceMgrMsgAcceptExtend.getUsername();
					String email = grievanceMgrMsgAcceptExtend.getEmail();
					if (StringUtils.isEmpty(username) && !StringUtils.isEmpty(email)) {
						receiverEmailSet.add(email);
					} else {
						receiverUsernameSet.add(username);
					}
				}
				if (!receiverUsernameSet.isEmpty()) {
					inboxMessageTextService.save(title, content.toString(), creatorUsername, messageType, sendEmail, receiverUsernameSet.toArray(new String[receiverUsernameSet.size()]));
				}
				if(!receiverEmailSet.isEmpty()) {
					inboxMessageTextService.saveForEmail(title, content.toString(), creatorUsername, messageType, receiverEmailSet.toArray(new String[receiverEmailSet.size()]));
				}
				UpdateWrapper<GrievanceMgrEntity> updateWrapper = new UpdateWrapper<>();
				updateWrapper.eq("mgm_no", mgmNo);
				updateWrapper.setSql("last_remind_date3=now()");
				grievanceMgrService.update(updateWrapper);
			}
			dataSourceTransactionManager.commit(transactionStatus);
		} catch(Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}
	
	/**
	 * 每隔 10 分钟检测 “受付件数管理” 中 “お客様完了日” 未填, 并且距离 “要求回答日” 不到 3 天则每天报警一次。
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task2() {
		boolean trySuccess = task2Lock.tryLock();
		if (trySuccess) {
			log.info("task2()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					long lastExecuteTime = System.currentTimeMillis();
					// 标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					boolean handleData = false;
					String[] factoryTypeArray = {"F","M"};
					String url = "";
					for (int h=0,len_h = factoryTypeArray.length;h<len_h;h++) {
						String factoryType = factoryTypeArray[h];
						url = getUrl(factoryType);
						for (;;) {
							if (System.currentTimeMillis() - lastExecuteTime > 5 * 60 * 1000) {
								log.info("运行时长超过5分钟，马上退出...");
								break;
							}
							QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.eq("factory_type", factoryType);
							queryWrapper.apply("(last_remind_date2 is null or datediff(now(),last_remind_date2)>=1)");
							queryWrapper.isNull("cust_complet_date");
							queryWrapper
							.apply("(datediff(demand_reply_date,now())<=3 or datediff(now(),demand_reply_date)>0)");
							queryWrapper.last(" limit 500");
							queryWrapper.select("id,mgm_no,demand_reply_date,creator_username,editor_username");
							List<GrievanceMgrEntity> grievanceMgrList = grievanceMgrService.list(queryWrapper);
							if (grievanceMgrList.isEmpty()) {
								break;
							} else {
								handleData = true;
							}
							//整理受付件数信息列表
							Map<String,GrievanceMgrEntity> grievanceMgrMap = new HashMap<>();
							{
								for (int i=0,len_i = grievanceMgrList.size();i<len_i;i++) {
									GrievanceMgrEntity grievanceMgr = grievanceMgrList.get(i);
									grievanceMgrMap.put(grievanceMgr.getMgmNo(), grievanceMgr);
								}
							}
							Map<String,List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap = getAllMgrMsgAccept(grievanceMgrList);
							sendMsg2(url, grievanceMgrMap, grievanceMgrMsgAcceptExtendMap);
						}
					}
					// 检测标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					if (handleData) {
						inboxMessageElasticsearch.dataSync();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task2()：任务结束...");
			task2Lock.unlock();
		}
	}

	/**
	 * 把发送消息和设置发送日期使用同一个事务，使用手动事务
	 * @param title
	 * @param content
	 * @param creatorUsername
	 * @param messageType
	 * @param sendEmail
	 * @param receiverUsername
	 * @param mgmNoList
	 */
	public void sendMsg2(String url, Map<String,GrievanceMgrEntity> grievanceMgrMap, Map<String,List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		try {
			long lastExecuteTime = System.currentTimeMillis();
			String content = "<a href=\"" + url+ "\">点击这里回答</a>";
			String creatorUsername = "admin";
			Integer messageType = MessageType.html.getV();
			Integer sendEmail = YesOrNo.是.getV();
			for(Entry<String,List<GrievanceMgrMsgAcceptExtend>> entry:grievanceMgrMsgAcceptExtendMap.entrySet()) {
				GrievanceMgrEntity grievanceMgr = grievanceMgrMap.get(entry.getKey());
				List<GrievanceMgrMsgAcceptExtend> grievanceMgrMsgAcceptExtendList = entry.getValue();
				long demandReplyTime = grievanceMgr.getDemandReplyDate().getTime();
				String title;
				if (lastExecuteTime < demandReplyTime) {
					title = "发现有单号“" + grievanceMgr.getMgmNo() + "”的受付件快到要求回答日";
				} else {
					title = "发现有单号“" + grievanceMgr.getMgmNo() + "”的受付件过了要求回答日";
				}
				HashSet<String> receiverUsernameSet = new HashSet<>();
				HashSet<String> receiverEmailSet = new HashSet<>();
				for (int i = 0,len_i = grievanceMgrMsgAcceptExtendList.size();i < len_i;i++) {
					GrievanceMgrMsgAcceptExtend grievanceMgrMsgAcceptExtend = grievanceMgrMsgAcceptExtendList.get(i);
					String username = grievanceMgrMsgAcceptExtend.getUsername();
					String email = grievanceMgrMsgAcceptExtend.getEmail();
					if(StringUtils.isEmpty(username) && !StringUtils.isEmpty(email)) {
						receiverEmailSet.add(email);
					} else {
						receiverUsernameSet.add(username);
					}
				}
				if (!receiverUsernameSet.isEmpty()) {
					inboxMessageTextService.save(title, content, creatorUsername, messageType, sendEmail, receiverUsernameSet.toArray(new String[receiverUsernameSet.size()]));
				}
				if (!receiverEmailSet.isEmpty()) {
					inboxMessageTextService.saveForEmail(title, content, creatorUsername, messageType, receiverEmailSet.toArray(new String[receiverEmailSet.size()]));
				}
				UpdateWrapper<GrievanceMgrEntity> updateWrapper = new UpdateWrapper<>();
				updateWrapper.eq("mgm_no", grievanceMgr.getMgmNo());
				updateWrapper.setSql("last_remind_date2=now()");
				grievanceMgrService.update(updateWrapper);
			}
			dataSourceTransactionManager.commit(transactionStatus);
		} catch(Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}

	/**
	 * 每隔 10 分钟检查 "受付件数管理" 中自受付日算起一周后仍然没有回答内容的，发出消息提醒
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					long lastExecuteTime = System.currentTimeMillis();
					// 标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					boolean handleData = false;
					String[] factoryTypeArray = {"F","M"};
					String url = "";
					for (int h=0,len_h = factoryTypeArray.length;h<len_h;h++) {
						String factoryType = factoryTypeArray[h];
						url = getUrl(factoryType);
						for (;;) {
							if (System.currentTimeMillis() - lastExecuteTime > 5 * 60 * 1000) {
								log.info("运行时长超过5分钟，马上退出...");
								break;
							}
							QueryWrapper<GrievanceMgrEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.eq("factory_type", factoryType);
							queryWrapper.apply("(last_remind_date1 is null or datediff(now(),last_remind_date1)>=1)");
							queryWrapper.apply("datediff(now(),reception_date)>=7");
							queryWrapper.isNull("one_ans_day");
							queryWrapper.last(" limit 500");
							queryWrapper.select("id,mgm_no,creator_username,editor_username");
							List<GrievanceMgrEntity> grievanceMgrList = grievanceMgrService.list(queryWrapper);
							if (grievanceMgrList.isEmpty()) {
								break;
							} else {
								handleData = true;
							}
							Map<String, List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap = getAllMgrMsgAccept(grievanceMgrList);
							sendMsg1(url, grievanceMgrMsgAcceptExtendMap);
						}
					}
					// 检测标记是否有处理过数据，有则触发站内信同步到 Elasticsearch
					if (handleData) {
						inboxMessageElasticsearch.dataSync();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}

	/**
	 * 把发送消息和设置发送日期使用同一个事务，使用手动事务
	 * @param title
	 * @param content
	 * @param creatorUsername
	 * @param messageType
	 * @param sendEmail
	 * @param receiverUsername
	 * @param mgmNoList
	 */
	public void sendMsg1(String url,Map<String, List<GrievanceMgrMsgAcceptExtend>> grievanceMgrMsgAcceptExtendMap) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		try {
			String content = "<a style=\"font-size:30px;\" href=\"" + url + "\">点击这里回答</a>";
			String creatorUsername = "admin";
			Integer messageType = MessageType.html.getV();
			Integer sendEmail = YesOrNo.是.getV();
			for(Entry<String, List<GrievanceMgrMsgAcceptExtend>> entry:grievanceMgrMsgAcceptExtendMap.entrySet()) {
				String mgmNo = entry.getKey();
				List<GrievanceMgrMsgAcceptExtend> grievanceMgrMsgAcceptExtendList = entry.getValue();
				String title = "发现有单号“" + mgmNo + "”的受付件超过1周时间未回答，请及时回答。";
				HashSet<String> receiverUsernameSet = new HashSet<>();
				HashSet<String> receiverEmailSet = new HashSet<>();
				for (int i = 0,len_i = grievanceMgrMsgAcceptExtendList.size();i < len_i;i++) {
					GrievanceMgrMsgAcceptExtend grievanceMgrMsgAcceptExtend = grievanceMgrMsgAcceptExtendList.get(i);
					String username = grievanceMgrMsgAcceptExtend.getUsername();
					String email = grievanceMgrMsgAcceptExtend.getEmail();
					if(StringUtils.isEmpty(username) && !StringUtils.isEmpty(email)) {
						receiverEmailSet.add(email);
					} else {
						receiverUsernameSet.add(username);
					}
				}
				if (!receiverUsernameSet.isEmpty()) {
					inboxMessageTextService.save(title, content, creatorUsername, messageType, sendEmail, receiverUsernameSet.toArray(new String[receiverUsernameSet.size()]));
				}
				if (!receiverEmailSet.isEmpty()) {
					inboxMessageTextService.saveForEmail(title, content, creatorUsername, messageType, receiverEmailSet.toArray(new String[receiverEmailSet.size()]));
				}
				UpdateWrapper<GrievanceMgrEntity> updateWrapper = new UpdateWrapper<>();
				updateWrapper.eq("mgm_no", mgmNo);
				updateWrapper.setSql("last_remind_date1=now()");
				grievanceMgrService.update(updateWrapper);
			}
			dataSourceTransactionManager.commit(transactionStatus);
		} catch(Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}

	@Autowired
	private GrievanceMgrMsgAcceptMapper grievanceMgrMsgAcceptMapper;
	
	/**
	 * 获取所有的接收者
	 * @param grievanceMgrList
	 * @return
	 */
	private Map<String,List<GrievanceMgrMsgAcceptExtend>> getAllMgrMsgAccept(List<GrievanceMgrEntity> grievanceMgrList) {
		List<GrievanceMgrMsgAcceptExtend> grievanceMgrMsgAcceptExtendList;
		{ //获取消息接收者列表
			//整理出客户投诉信息的创建用户，以及消息的id
			List<Integer> idList = new ArrayList<>();
			HashSet<String> usernameSet = new HashSet<>();
			for (int i=0,len_i=grievanceMgrList.size();i<len_i;i++) {
				GrievanceMgrEntity grievanceMgr = grievanceMgrList.get(i);
				idList.add(grievanceMgr.getId());
				usernameSet.add(grievanceMgr.getCreatorUsername());
				usernameSet.add(grievanceMgr.getEditorUsername());
			}
			//获取追加的接收的邮箱
			QueryWrapper<GrievanceMgrMsgAcceptExtend> queryWrapper = new QueryWrapper<>();
			queryWrapper.in("grievance_mgr_id", idList);
			queryWrapper.select("gmma.grievance_mgr_id,gmma.email,gm.mgm_no");
			grievanceMgrMsgAcceptExtendList = grievanceMgrMsgAcceptMapper.listExtend1(queryWrapper);
			
			//获取投诉信息创建者的用户名和邮箱
			QueryWrapper<UserEntity> queryWrapper2 = new QueryWrapper<>();
			queryWrapper2.in("username", usernameSet);
			queryWrapper2.apply("email is not null and email!=''");
			queryWrapper2.select("username,email");
			List<UserEntity> userList = userService.list(queryWrapper2);
			
			Map<String,String> usernameEmailMap = new HashMap<>();
			for (int i=0,len_i = userList.size();i<len_i;i++) {
				UserEntity user = userList.get(i);
				usernameEmailMap.put(user.getUsername(), user.getEmail());
			}
			//整合创建者、追加接收者的邮箱和用户名
			for (int i=0,len_i=grievanceMgrList.size();i<len_i;i++) {
				GrievanceMgrEntity grievanceMgr = grievanceMgrList.get(i);
				GrievanceMgrMsgAcceptExtend grievanceMgrMsgAccept = new GrievanceMgrMsgAcceptExtend();
				grievanceMgrMsgAccept.setGrievanceMgrId(grievanceMgr.getId());
				grievanceMgrMsgAccept.setMgmNo(grievanceMgr.getMgmNo());
				grievanceMgrMsgAccept.setUsername(grievanceMgr.getCreatorUsername());
				grievanceMgrMsgAccept.setEmail(usernameEmailMap.get(grievanceMgr.getCreatorUsername()));
				grievanceMgrMsgAcceptExtendList.add(grievanceMgrMsgAccept);
			}
		}
		Map<String,List<GrievanceMgrMsgAcceptExtend>> retMap = new HashMap<>();
		{ //让消息接收者的邮箱与用户名关联，并整理成 hashMap 的形式
			//整理未关联用户的邮箱。
			List<String> emailList = new ArrayList<>();
			for (int i=0,len_i = grievanceMgrMsgAcceptExtendList.size();i<len_i;i++) {
				GrievanceMgrMsgAcceptExtend grievanceMgrMsgAcceptExtend = grievanceMgrMsgAcceptExtendList.get(i);
				if(StringUtils.isEmpty(grievanceMgrMsgAcceptExtend.getUsername())) {
					emailList.add(grievanceMgrMsgAcceptExtend.getEmail());
				}
				String mgmNo = grievanceMgrMsgAcceptExtend.getMgmNo();
				List<GrievanceMgrMsgAcceptExtend> tempList = retMap.get(mgmNo);
				if (tempList == null) {
					retMap.put(mgmNo, tempList = new ArrayList<>());
				}
				tempList.add(grievanceMgrMsgAcceptExtend);
			}
			//通过邮箱反查用户名
			if (!emailList.isEmpty()) {
				QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.in("email", emailList);
				queryWrapper.select("email,username");
				List<UserEntity> userList = userService.list(queryWrapper);
				Map<String,String> emailUsernameMap = new HashMap<>();
				for (int i=0,len_i = userList.size();i<len_i;i++) {
					UserEntity user = userList.get(i);
					emailUsernameMap.put(user.getEmail(), user.getUsername());
				}
				//通过邮箱关联用户名
				for (int i=0,len_i = grievanceMgrMsgAcceptExtendList.size();i<len_i;i++) {
					GrievanceMgrMsgAcceptExtend grievanceMgrMsgAcceptExtend = grievanceMgrMsgAcceptExtendList.get(i);
					if (StringUtils.isEmpty(grievanceMgrMsgAcceptExtend.getUsername())) {
						grievanceMgrMsgAcceptExtend.setUsername(emailUsernameMap.get(grievanceMgrMsgAcceptExtend.getEmail()));
					}
				}
			}
		}
		return retMap;
	}

	/**
	 * 获取导航 url
	 * @param factoryType
	 * @return
	 */
	private String getUrl(String factoryType) {
		String url = "";
		if("F".equals(factoryType)) {
			if ("dev".equals(springProfilesActive)) {
				url = "http://localhost:8080/index.html#/pstfsGrievanceMgr";
			} else if ("pro".equals(springProfilesActive)) {
				url = "http://10.199.32.12:38849/index.html#/pstfsGrievanceMgr";
			}
		} else if("M".equals(factoryType)) {
			if ("dev".equals(springProfilesActive)) {
				url = "http://localhost:8080/index.html#/pstmsGrievanceMgr";
			} else if ("pro".equals(springProfilesActive)) {
				url = "http://10.199.32.12:38849/index.html#/pstmsGrievanceMgr";
			}
		}
		return url;
	}
}