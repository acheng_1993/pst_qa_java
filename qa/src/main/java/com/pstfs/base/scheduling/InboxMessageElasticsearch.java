package com.pstfs.base.scheduling;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.PostConstruct;

import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.MaxAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.Main;
import com.pstfs.base.entity.InboxMessageEntity;
import com.pstfs.base.envm.MessageType;
import com.pstfs.base.esUtils.ElasticsearchConnPool;
import com.pstfs.base.esUtils.ElasticsearchIndex;
import com.pstfs.base.extend.InboxMessageExtend;
import com.pstfs.base.service.InboxMessageService;

import cn.hutool.http.HtmlUtil;
import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class InboxMessageElasticsearch {

	/**
	 * task1,2,3的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	private Lock task2Lock;
	private Lock task3Lock;
	
	public InboxMessageElasticsearch() {
		task1Lock = new ReentrantLock();
		task2Lock = new ReentrantLock();
		task3Lock = new ReentrantLock();
		Main.LOCK_LIST.add(task1Lock);
		Main.LOCK_LIST.add(task2Lock);
		Main.LOCK_LIST.add(task3Lock);
	}
	

	@PostConstruct
	private void init() throws Exception {
		RestHighLevelClient esClient = null;
		try {
			GetIndexRequest request = new GetIndexRequest(ElasticsearchIndex.INBOX_MESSAGE_INDEX);
			//从连接池获取连接
			esClient = elasticsearchConnPool.getEsClient();
			// 索引不存在时会创建索引
			if (!esClient.indices().exists(request, RequestOptions.DEFAULT)) {
				//归还连接到连接池
				elasticsearchConnPool.returnEsClient(esClient);
				CreateIndexRequest request2 = new CreateIndexRequest(ElasticsearchIndex.INBOX_MESSAGE_INDEX);
				// DSL
				JSONObject jsonObject = new JSONObject();
				JSONObject mappings = new JSONObject();
				JSONObject properties = new JSONObject();

				JSONObject id = new JSONObject();
				id.set("type", "integer");
				properties.set("id", id); // id 不切分词，整个词做倒排索引
				
				JSONObject title = new JSONObject();
				title.set("type", "text");
				properties.set("title", title); // title 使用切分词倒排索引
				
				JSONObject content = new JSONObject();
				content.set("type", "text");
				properties.set("content", content); // content 使用切分词倒排索引

				JSONObject receiverUsername = new JSONObject();
				receiverUsername.set("type", "keyword");
				properties.set("receiverUsername", receiverUsername);// receiverUsername 不切分词，整个词做倒排索引

				JSONObject creatorUsername = new JSONObject();
				creatorUsername.set("type", "keyword");
				properties.set("creatorUsername", creatorUsername);// receiverUsername 不切分词，整个词做倒排索引

				JSONObject deleteTime = new JSONObject();
				deleteTime.set("type", "date");
				properties.set("deleteTime", deleteTime); // deleteTime 不切分词，整个词做倒排索引
				
				JSONObject readedTime = new JSONObject();
				readedTime.set("type", "date");
				properties.set("readedTime", readedTime); // readedTime 不切分词，整个词做倒排索引
				
				JSONObject inboxMessageTextId = new JSONObject(); // inboxMessageTextId 不切分词
				inboxMessageTextId.set("type", "integer");
				inboxMessageTextId.set("index",false); //不做索引，仅保存
				properties.set("inboxMessageTextId", inboxMessageTextId);
				
				JSONObject createTime = new JSONObject();
				createTime.set("type", "date");
				properties.set("createTime", createTime); // createTime 不切分词，整个词做倒排索引

				JSONObject simpleContent = new JSONObject();
				simpleContent.set("type", "keyword");
				simpleContent.set("index", false);
				properties.set("simpleContent", simpleContent); // simpleContent 不做索引，直接存储
				
				mappings.set("properties", properties);
				jsonObject.set("mappings", mappings);
				String json = jsonObject.toString(); // 各个字段的索引规则
				request2.source(json, XContentType.JSON);
				esClient = elasticsearchConnPool.getEsClient();
				esClient.indices().create(request2, RequestOptions.DEFAULT);
				//归还连接到连接池
				elasticsearchConnPool.returnEsClient(esClient);
			}
		} catch (RuntimeException | IOException e1) {
			elasticsearchConnPool.dropEsClient(esClient);
			log.error(e1.getMessage(), e1);
		}
		
		threadWaitObj = new Object();
		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				for(;;) {
					synchronized(threadWaitObj) {
						try {
							threadWaitObj.wait();
							task1();
							task2();
							task3();
						} catch (InterruptedException e) {
							log.error(e.getMessage(), e);
						}
					}
				}
			}
		});
		thread.start();
	}
	
	/**
	 * 线程等待用到的对象
	 */
	private Object threadWaitObj;
	private Thread thread;
	
	/**
	 * 让定时把数据同步到 Elasticsearch 的任务马上执行。
	 */
	public void dataSync() {
		synchronized(threadWaitObj) {
			threadWaitObj.notifyAll();
		}
	}

	@Autowired
	private ElasticsearchConnPool elasticsearchConnPool;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;

	@Lazy
	@Autowired
	private InboxMessageService inboxMessageService;

	/**
	 * 每隔 30 分钟检测一次是否存在未同步到 Elasticsearch 的删除时间
	 */
	@Scheduled(cron = "0 0/30 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task3() {
		boolean trySuccess = task3Lock.tryLock();
		if (trySuccess) {
			log.info("task3()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				RestHighLevelClient esClient = null;
				try {
					long lastExecuteTime = System.currentTimeMillis();
					for(;;) {
						if (System.currentTimeMillis() - lastExecuteTime > 5 * 60 * 1000) {
							log.info("自动同步站内信到 Elasticsearch 的功能，运行时长超过5分钟，马上退出...");
							break;
						}
						//从连接池获取连接
						esClient = elasticsearchConnPool.getEsClient();
						if(esClient == null) {
							log.info("系统繁忙，稍候再同步数据...");
							break;
						}
						Date lastDeleteTime = getDeleteTimeFromElasticsearch(esClient);
						log.info("上次同步删除时间进度：" + lastDeleteTime);
						//归还连接到连接池
						elasticsearchConnPool.returnEsClient(esClient);
						List<InboxMessageEntity> inboxMessageList = null;
						{
							QueryWrapper<InboxMessageEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.select("id,delete_time");
							queryWrapper.isNotNull("delete_time");
							//保证同步到 Elasticsearch 的删除时间小于当前时间
							String nowDeleteTimeStr = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
							queryWrapper.lt("delete_time", nowDeleteTimeStr);
							if (lastDeleteTime != null) {
								String deleteTimeStr = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.ofInstant(lastDeleteTime.toInstant(), ZoneId.systemDefault()));
								queryWrapper.gt("delete_time", deleteTimeStr);
							}
							inboxMessageList = inboxMessageService.list(queryWrapper);
						}
						if (inboxMessageList.isEmpty()) {
							log.info("未发现需要同步的站内信...");
							break;
						} else {
							BulkRequest request = new BulkRequest();
							for (int i = 0, len_i = inboxMessageList.size(); i < len_i; i++) {
								InboxMessageEntity inboxMessage = inboxMessageList.get(i);
							    UpdateRequest updateRequest = new UpdateRequest();
							    updateRequest.index(ElasticsearchIndex.INBOX_MESSAGE_INDEX);
							    updateRequest.id(String.valueOf(inboxMessage.getId()));
							    updateRequest.doc(XContentType.JSON, "deleteTime", inboxMessage.getDeleteTime());
							    request.add(updateRequest);
							}
							esClient = elasticsearchConnPool.getEsClient();
							if(esClient == null) {
								log.info("系统繁忙，稍候再同步数据...");
								break;
							}
							esClient.bulk(request, RequestOptions.DEFAULT);
							elasticsearchConnPool.returnEsClient(esClient);
						}
					}
				} catch (IOException e) {
					log.error(e.getMessage(), e);
					//疑似连接失败，把连接作废
					elasticsearchConnPool.dropEsClient(esClient);
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					//归还连接
					elasticsearchConnPool.returnEsClient(esClient);
				}
			}
			log.info("task3()：任务结束...");
			task3Lock.unlock();
		}
	}
	
	/**
	 * 每隔 30 分钟检测一次是否存在未同步到 Elasticsearch 的阅读时间
	 */
	@Scheduled(cron = "0 0/30 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task2() {
		boolean trySuccess = task2Lock.tryLock();
		if (trySuccess) {
			log.info("task2()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				RestHighLevelClient esClient = null;
				try {
					long lastExecuteTime = System.currentTimeMillis();
					for(;;) {
						if (System.currentTimeMillis() - lastExecuteTime > 5 * 60 * 1000) {
							log.info("自动同步站内信到 Elasticsearch 的功能，运行时长超过5分钟，马上退出...");
							break;
						}
						//从连接池获取连接
						esClient = elasticsearchConnPool.getEsClient();
						if(esClient == null) {
							log.info("系统繁忙，稍候再同步数据...");
							break;
						}
						Date lastReadedTime = getReadedTimeFromElasticsearch(esClient);
						log.info("上次同步阅读时间进度：" + lastReadedTime);
						//归还连接到连接池
						elasticsearchConnPool.returnEsClient(esClient);
						List<InboxMessageEntity> inboxMessageList = null;
						{
							QueryWrapper<InboxMessageEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.select("id,readed_time");
							queryWrapper.isNotNull("readed_time");
							//保证同步到 Elasticsearch 的阅读时间小于当前时间
							String nowReadedTimeStr = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
							queryWrapper.lt("readed_time", nowReadedTimeStr);
							if (lastReadedTime != null) {
								String readedTimeStr = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.ofInstant(lastReadedTime.toInstant(), ZoneId.systemDefault()));
								queryWrapper.gt("readed_time", readedTimeStr);
							}
							inboxMessageList = inboxMessageService.list(queryWrapper);
						}
						if (inboxMessageList.isEmpty()) {
							log.info("未发现需要同步的站内信...");
							break;
						} else {
							BulkRequest request = new BulkRequest();
							for (int i = 0, len_i = inboxMessageList.size(); i < len_i; i++) {
								InboxMessageEntity inboxMessage = inboxMessageList.get(i);
							    UpdateRequest updateRequest = new UpdateRequest();
							    updateRequest.index(ElasticsearchIndex.INBOX_MESSAGE_INDEX);
							    updateRequest.id(String.valueOf(inboxMessage.getId()));
							    updateRequest.doc(XContentType.JSON, "readedTime", inboxMessage.getReadedTime());
							    request.add(updateRequest);
							}
							esClient = elasticsearchConnPool.getEsClient();
							if(esClient == null) {
								log.info("系统繁忙，稍候再同步数据...");
								break;
							}
							esClient.bulk(request, RequestOptions.DEFAULT);
							elasticsearchConnPool.returnEsClient(esClient);
						}
					}
				} catch (IOException e) {
					log.error(e.getMessage(), e);
					//疑似连接失败，把连接作废
					elasticsearchConnPool.dropEsClient(esClient);
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					//归还连接
					elasticsearchConnPool.returnEsClient(esClient);
				}
			}
			log.info("task2()：任务结束...");
			task2Lock.unlock();
		}
	}
	
	/**
	 * 每隔 10 分钟检测一次是否存在未同步到 Elasticsearch 的站内信
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				RestHighLevelClient esClient = null;
				try {
					long lastExecuteTime = System.currentTimeMillis();
					for(;;) {
						if (System.currentTimeMillis() - lastExecuteTime > 5 * 60 * 1000) {
							log.info("自动同步站内信到 Elasticsearch 的功能，运行时长超过5分钟，马上退出...");
							break;
						}
						//从连接池获取连接
						esClient = elasticsearchConnPool.getEsClient();
						if(esClient == null) {
							log.info("系统繁忙，稍候再同步数据...");
							break;
						}
						Integer lastProgressId = getProgressIdFromElasticsearch(esClient);
						log.info("上次同步站内信进度：" + lastProgressId);
						//归还连接到连接池
						elasticsearchConnPool.returnEsClient(esClient);
						Map<String, Object> params = new HashMap<>();
						params.put("firstId", lastProgressId);
						params.put("delete", "NULL");
						params.put("limit", 100);
						params.put("idOrder", "ASC");
						List<InboxMessageExtend> inboxMessageList = inboxMessageService.scrollLoad(params,
								"im.inbox_message_text_id,im.readed_time,im.delete_time,imt.message_type,imt.title,imt.content,imt.creator_username,imt.simple_content,im.receiver_username,im.id,imt.create_time");
						if (inboxMessageList.isEmpty()) {
							log.info("未发现需要同步的站内信...");
							break;
						} else {
							BulkRequest request = new BulkRequest();
							for (int i = 0, len_i = inboxMessageList.size(); i < len_i; i++) {
								InboxMessageExtend inboxMessageExtend = inboxMessageList.get(i);
								IndexRequest indexRequest = new IndexRequest();
								indexRequest.index(ElasticsearchIndex.INBOX_MESSAGE_INDEX);
								indexRequest.id(String.valueOf(inboxMessageExtend.getId()));
								
								String content = inboxMessageExtend.getContent();
								//如果内容是 html 且不为空，则过滤掉所有 html 标签
								if(inboxMessageExtend.getMessageType() == MessageType.html.getV() && StringUtils.isNotEmpty(content)) {
									content = HtmlUtil.cleanHtmlTag(content);
								}
								indexRequest.source(XContentType.JSON, 
										"id", inboxMessageExtend.getId(),
										"title", inboxMessageExtend.getTitle(),
										"content", content, 
										"receiverUsername", inboxMessageExtend.getReceiverUsername(), 
										"createTime", inboxMessageExtend.getCreateTime(),
										"simpleContent", inboxMessageExtend.getSimpleContent(),
										"creatorUsername", inboxMessageExtend.getCreatorUsername(),
										"readedTime", inboxMessageExtend.getReadedTime(),
										"deleteTime", inboxMessageExtend.getDeleteTime(),
										"inboxMessageTextId", inboxMessageExtend.getInboxMessageTextId());
								request.add(indexRequest);
							}
							esClient = elasticsearchConnPool.getEsClient();
							if(esClient == null) {
								log.info("系统繁忙，稍候再同步数据...");
								break;
							}
							esClient.bulk(request, RequestOptions.DEFAULT);
							elasticsearchConnPool.returnEsClient(esClient);
						}
					}
				} catch (IOException e) {
					log.error(e.getMessage(), e);
					//疑似连接失败，把连接作废
					elasticsearchConnPool.dropEsClient(esClient);
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					//归还连接
					elasticsearchConnPool.returnEsClient(esClient);
				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}

	/**
	 * 从 Elasticsearch 现有的数据中获取最大的删除时间
	 * @param esClient
	 * @return
	 * @throws IOException
	 */
	private Date getDeleteTimeFromElasticsearch(RestHighLevelClient esClient) throws IOException {
		SearchSourceBuilder builder = new SearchSourceBuilder();
		MaxAggregationBuilder aggregationBuilder = AggregationBuilders.max("maxDeleteTime").field("deleteTime");
		builder.size(0);
		builder.aggregation(aggregationBuilder);
		SearchRequest request = new SearchRequest();
		request.indices(ElasticsearchIndex.INBOX_MESSAGE_INDEX).source(builder);
		SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);
		JSONObject ret = new JSONObject(response.toString());
		Date maxDeleteTime = ret.getJSONObject("aggregations").getJSONObject("max#maxDeleteTime").getDate("value");
		return maxDeleteTime;
	}
	
	/**
	 * 从 Elasticsearch 现有的数据中获取最大的阅读时间
	 * @param esClient
	 * @return
	 * @throws IOException
	 */
	private Date getReadedTimeFromElasticsearch(RestHighLevelClient esClient) throws IOException {
		SearchSourceBuilder builder = new SearchSourceBuilder();
		MaxAggregationBuilder aggregationBuilder = AggregationBuilders.max("maxReadedTime").field("readedTime");
		builder.size(0);
		builder.aggregation(aggregationBuilder);
		SearchRequest request = new SearchRequest();
		request.indices(ElasticsearchIndex.INBOX_MESSAGE_INDEX).source(builder);
		SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);
		JSONObject ret = new JSONObject(response.toString());
		Date maxReadedTime = ret.getJSONObject("aggregations").getJSONObject("max#maxReadedTime").getDate("value");
		return maxReadedTime;
	}
	
	/**
	 * 从 Elasticsearch 现有的数据中获取最大的 id.
	 * @param schedulingProgressEntity
	 * @throws IOException
	 */
	private Integer getProgressIdFromElasticsearch(RestHighLevelClient esClient) throws IOException {
		SearchSourceBuilder builder = new SearchSourceBuilder();
		MaxAggregationBuilder aggregationBuilder = AggregationBuilders.max("maxId").field("id");
		builder.size(0);
		builder.aggregation(aggregationBuilder);
		SearchRequest request = new SearchRequest();
		request.indices(ElasticsearchIndex.INBOX_MESSAGE_INDEX).source(builder);
		SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);
		JSONObject ret = new JSONObject(response.toString());
		Integer maxId = ret.getJSONObject("aggregations").getJSONObject("max#maxId").getInt("value");
		if (maxId == null) {
			return -1;
		} else {
			return maxId;
		}
	}
}