package com.pstfs.base.scheduling;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import com.pstfs.base.Main;
import com.pstfs.base.entity.BreakPointTransEntity;
import com.pstfs.base.entity.SchedulingProgressEntity;
import com.pstfs.base.entity.UploadFileEntity;
import com.pstfs.base.envm.BreakPointTransStatus;
import com.pstfs.base.envm.ProgressType;
import com.pstfs.base.service.BreakPointTransService;
import com.pstfs.base.service.SchedulingProgressService;
import com.pstfs.base.service.UploadFileService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UploadFileAutoDelete {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	/**
	 * task2的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task2Lock;

	public UploadFileAutoDelete(@Value("${breakPointTransPath}") String breakPointTransPath,@Value("${uploadFilePath}") String uploadFilePath) { 
		task1Lock = new ReentrantLock();
		task2Lock = new ReentrantLock();
		Main.LOCK_LIST.add(task1Lock);
		Main.LOCK_LIST.add(task2Lock);
		File file = new File(breakPointTransPath);
		file.mkdirs();
		this.breakPointTransPath = breakPointTransPath;
		file = new File(uploadFilePath);
		file.mkdirs();
		this.uploadFilePath = uploadFilePath;
	}
	
	@Autowired
	private UploadFileService uploadFileService;
	
	@Autowired
	private BreakPointTransService breakPointTransService;
	
	@Autowired
	private SchedulingProgressService schedulingProgressService;

	private String uploadFilePath;
	
	private String breakPointTransPath;

	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 每天凌晨准时自动删除未被引用的文件
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	/**
	 * 每隔10秒钟，自动删除未被引用的文件
	 */
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				List<UploadFileEntity> uploadFileEntityList;
				try {
					long lastExecuteTime = System.currentTimeMillis();
					Integer newLastProgressId = null;
					do {
						if (System.currentTimeMillis() - lastExecuteTime > 5*60*1000) {
							log.info("自动删除文件功能，运行时长超过5分钟，马上退出...");
							break;
						}
						SchedulingProgressEntity schedulingProgressEntity = schedulingProgressService.getByProgressType(ProgressType.自动删除未引用文件.getV());
						Integer lastProgressId = schedulingProgressEntity.getProgressId();
						newLastProgressId = uploadFileService.newLastProgressId(lastProgressId, 100);
						if (newLastProgressId == null) {
							log.info("未发现需要删除的文件...");
							schedulingProgressEntity.setProgressId(-1);
							schedulingProgressService.updateById(schedulingProgressEntity);
						} else {
							log.info("上次文件删除进度："+lastProgressId);
							uploadFileEntityList = uploadFileService.notUseUploadFileList(lastProgressId, newLastProgressId);
							schedulingProgressEntity.setProgressId(newLastProgressId);
							if(uploadFileEntityList.isEmpty()) {
								schedulingProgressService.updateById(schedulingProgressEntity);
							} else {
								log.info("发现 "+uploadFileEntityList.size()+" 个文件需要删除...");
								deleteNotUseUploadFile(uploadFileEntityList, schedulingProgressEntity);
								log.info("已成功删除 "+uploadFileEntityList.size()+" 个文件。");
							}
						}
					} while(newLastProgressId!=null);
				} catch (Exception e) {
					log.error("自动删除文件时出错，错误原因：", e);
				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}

	/**
	 * 把删除文件和更新进度放在同一个事务范围内，保证同时成功或同时失败,
	 * 使用手动事务
	 */
	private void deleteNotUseUploadFile(List<UploadFileEntity> uploadFileEntityList,SchedulingProgressEntity schedulingProgressEntity) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		try {
			List<Integer> ids = new ArrayList<>(uploadFileEntityList.size());
			for (UploadFileEntity uploadFileEntity:uploadFileEntityList) {
				ids.add(uploadFileEntity.getId());
			}
			uploadFileService.removeByIds(ids);
			schedulingProgressService.updateById(schedulingProgressEntity);
			for (UploadFileEntity uploadFileEntity:uploadFileEntityList) {
				String targetPath = uploadFilePath + File.separator + uploadFileEntity.getPath();
				File targetPathObj = new File(targetPath);
				targetPathObj.delete();
			}
			dataSourceTransactionManager.commit(transactionStatus);
		} catch(Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}

	/**
	 * 每天凌晨准时自动删除断点续传的文件
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	/**
	 * 每隔10秒钟，自动删除断点续传的文件
	 */
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task2() {
		boolean trySuccess = task2Lock.tryLock();
		if (trySuccess) {
			log.info("task2()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				long lastExecuteTime = System.currentTimeMillis();
				Integer newLastProgressId = null;
				do {
					if (System.currentTimeMillis() - lastExecuteTime > 5*60*1000) {
						log.info("自动删除断点续传文件功能，运行时长超过5分钟，马上退出...");
						break;
					}
					breakPointTransService.changeTransStatus();
					SchedulingProgressEntity schedulingProgressEntity = schedulingProgressService.getByProgressType(ProgressType.自动删除断点续传文件.getV());
					Integer lastProgressId = schedulingProgressEntity.getProgressId();
					newLastProgressId = breakPointTransService.newLastProgressId(lastProgressId, 100);
					if (newLastProgressId == null) {
						log.info("未发现需要删除的文件...");
						schedulingProgressEntity.setProgressId(-1);
						schedulingProgressService.updateById(schedulingProgressEntity);
					} else {
						log.info("上次文件删除进度："+lastProgressId);
						List<BreakPointTransEntity> breakPointTransList = null;
						{
							Map<String,Object> params = new HashMap<>();
							params.put("idRangeStart", lastProgressId);
							params.put("idRangeEnd", newLastProgressId);
							List<Integer> transStatusList = new ArrayList<>();
							transStatusList.add(BreakPointTransStatus.中断.getV());
							transStatusList.add(BreakPointTransStatus.处理完成.getV());
							params.put("transStatusList",transStatusList); 
							breakPointTransList = breakPointTransService.list(params, "id,sha1,update_time,trans_status", 100);
						}
						schedulingProgressEntity.setProgressId(newLastProgressId);
						if (breakPointTransList.isEmpty()) {
							schedulingProgressService.updateById(schedulingProgressEntity);
						} else {
							log.info("发现 "+breakPointTransList.size()+" 个文件需要删除...");
							deleteBreakPointTrans(breakPointTransList, schedulingProgressEntity);
							log.info("已成功删除 "+breakPointTransList.size()+" 个文件。");
						}
					}
				} while(newLastProgressId!=null);
			}
			log.info("task2()：任务结束...");
			task2Lock.unlock();
		}
	}
	
	@Autowired
	private DataSourceTransactionManager dataSourceTransactionManager;
	@Autowired
	private TransactionDefinition transactionDefinition;
	
	/**
	 * 把删除文件和更新进度放在同一个事务范围内，保证同时成功或同时失败,
	 * 使用手动事务
	 */
	private void deleteBreakPointTrans(List<BreakPointTransEntity> breakPointTransList,SchedulingProgressEntity schedulingProgressEntity) {
		TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
		try {
			List<Integer> ids = new ArrayList<>(breakPointTransList.size());
			long now = System.currentTimeMillis();
			for (BreakPointTransEntity breakPointTransEntity:breakPointTransList) {
				//为了保险，处理完成的不到 24 小时不能删除
				if(breakPointTransEntity.getTransStatus() == BreakPointTransStatus.处理完成.getV()
						&& now < breakPointTransEntity.getUpdateTime().getTime() + 24*3600*1000) {
					continue;
				}
				ids.add(breakPointTransEntity.getId());
			}
			breakPointTransService.removeByIds(ids);
			schedulingProgressService.saveOrUpdate(schedulingProgressEntity);
			for (BreakPointTransEntity breakPointTransEntity:breakPointTransList) {
				//为了保险，处理完成的不到 24 小时不能删除
				if(breakPointTransEntity.getTransStatus() == BreakPointTransStatus.处理完成.getV()
						&& now < breakPointTransEntity.getUpdateTime().getTime() + 24*3600*1000) {
					continue;
				}
				File targetPathObj = new File(breakPointTransPath+File.separator+breakPointTransEntity.getSha1().substring(0,2)+File.separator+breakPointTransEntity.getId());
				targetPathObj.delete();
			}
			dataSourceTransactionManager.commit(transactionStatus);
		} catch(Throwable e) {
			dataSourceTransactionManager.rollback(transactionStatus);
			throw e;
		}
	}
}