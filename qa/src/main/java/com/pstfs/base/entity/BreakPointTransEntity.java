package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 断点续传表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-07 10:20:57
 */
@Data
@TableName("break_point_trans")
public class BreakPointTransEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 文件的sha1
	 */
	private String sha1;
	/**
	 * 文件大小(字节)
	 */
	private Integer fileSize;
	/**
	 * 上传进度
	 */
	private Integer process;
	/**
	 * 传输状态，0:处理中，1:处理完成，2:中断
	 */
	private Integer transStatus;
	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;
	/**
	 * 该续传任务的创建用户
	 */
	private String creatorUsername;
}
