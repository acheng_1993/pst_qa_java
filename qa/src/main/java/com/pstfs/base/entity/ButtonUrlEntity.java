package com.pstfs.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 按钮对应的url
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-30 15:15:42
 */
@Data
@TableName("button_url")
public class ButtonUrlEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 关联 button.id
	 */
	private Integer buttonId;
	/**
	 * 关联 request_url.id
	 */
	private Integer urlId;

}
