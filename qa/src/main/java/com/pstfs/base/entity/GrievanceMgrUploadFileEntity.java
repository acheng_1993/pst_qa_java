package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 客户投诉管理的上传附件表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-01-29 10:36:10
 */
@Data
@TableName("grievance_mgr_upload_file")
public class GrievanceMgrUploadFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 上传的文件id
	 */
	private Integer fileId;
	/**
	 * 上传的文件名
	 */
	private String srcName;
	/**
	 * 对应的客户投诉项目, 关联:grievance_mgr.id
	 */
	private Integer grievanceMgrId;
	/**
	 * 上传文件类型:解析依頼書文件名(0), 部品解析結果文件名(1), 不具合写真文件名(2), 品質異常書文件名(3), 最終回答書文件名(4);
	 */
	private Integer type;

}
