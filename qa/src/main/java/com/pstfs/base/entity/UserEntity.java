package com.pstfs.base.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Data
@TableName("user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * sha1密码
	 */
	private String password;
	/**
	 * 盐
	 */
	private String salt;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;
	/**
	 * 上次登录失败时间
	 */
	private Date lastLoginFailTime;
	/**
	 * 登录失败次数
	 */
	private Integer loginFailCount;
	/**
	 * 用户具有的角色开关位的base64
	 */
	private String userHasRole;
	/**
	 * 是否可删除，0:不可删除，1:可删除
	 */
	private Integer deletable;
	/**
	 * 用户名称
	 */
	private String name;
	/**
	 * 用户邮箱，如果没有设置将无法使用邮箱接收系统信息。
	 */
	private String email;
}
