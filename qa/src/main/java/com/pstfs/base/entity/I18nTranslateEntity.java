package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 国际化翻译表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-01-03 15:30:32
 */
@Data
@TableName("i18n_translate")
public class I18nTranslateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 表名
	 */
	private String tableName;
	/**
	 * 列名
	 */
	private String col;
	/**
	 * 行主键id
	 */
	private Integer rowId;
	/**
	 * 语言标识码
	 */
	private String langCode;
	/**
	 * 翻译值
	 */
	private String val;

}
