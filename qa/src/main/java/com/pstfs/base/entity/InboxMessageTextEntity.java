package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 站内信主表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-12-06 15:19:30
 */
@Data
@TableName("inbox_message_text")
public class InboxMessageTextEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 部分内容，通常取前20个文字
	 */
	private String simpleContent;
	/**
	 * 创建日期
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 创建用户
	 */
	private String creatorUsername;
	/**
	 * 消息类型
	 */
	private Integer messageType;
	/**
	 * 发送到邮箱  0: 不发送,1:发送
	 */
	private Integer sendEmail;
}
