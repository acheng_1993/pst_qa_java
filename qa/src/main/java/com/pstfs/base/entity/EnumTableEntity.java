package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 枚举表，用于让用户自行维护某种业务的枚举数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-23 13:26:25
 */
@Data
@TableName("enum_table")
public class EnumTableEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 枚举组
	 */
	private String enumGroup;
	/**
	 * 枚举描述
	 */
	private String enumDesc;
	/**
	 * 启用状态: 0:启用,1:禁用
	 */
	@TableField(fill = FieldFill.INSERT)
	private Integer status;

}
