package com.pstfs.base.entity;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 后台服务进度表，用于统一维护所有的后台服务进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-22 16:20:19
 */
@Data
@TableName("scheduling_progress")
public class SchedulingProgressEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 进度类型
	 */
	private Integer progressType;
	/**
	 * 进度id，一般是对应的表的主键id
	 */
	private Integer progressId;
	/**
	 * 冗余字段，如果后台服务是针对第三方的后台，而第三方后台不用自增id作为主键的情况下使用。
	 */
	private String otherCol1;
	/**
	 * 冗余字段，如果后台服务是针对第三方的后台，而第三方后台不用自增id作为主键的情况下使用。
	 */
	private String otherCol2;
	/**
	 * 冗余字段，如果后台服务是针对第三方的后台，而第三方后台不用自增id作为主键的情况下使用。
	 */
	private String otherCol3;

}
