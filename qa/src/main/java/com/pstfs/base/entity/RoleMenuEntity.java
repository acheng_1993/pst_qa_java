package com.pstfs.base.entity;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 角色菜单表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Data
@TableName("role_menu")
public class RoleMenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 角色id
	 */
	private Integer roleId;
	/**
	 * 菜单id
	 */
	private Integer menuId;

}
