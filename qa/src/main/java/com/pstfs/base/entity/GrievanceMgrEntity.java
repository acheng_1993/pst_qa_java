package com.pstfs.base.entity;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * 客户投诉管理
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-18 10:25:06
 */
@Data
@TableName("grievance_mgr")
public class GrievanceMgrEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	
	/**
	 * 工厂类型，F：PSTFS、M：PSTMS
	 */
	private String factoryType;
	
	/**
	 * 苦情タイプ(投诉类型)
	 */
	private Integer complaintType;
	/**
	 * 管理No.
	 */
	private String mgmNo;
	/**
	 * 受付日付
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date receptionDate;
	/**
	 * 製品品番
	 */
	private String prodNum;
	/**
	 * お客様品番
	 */
	private String customerService;
	/**
	 * お客様社名
	 */
	private String companyName;
	/**
	 * お客様工場
	 */
	private String customerFactory;
	/**
	 * 発生場所
	 */
	private String location;
	/**
	 * 不良発生日
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date failureDay;
	/**
	 * 不良内容
	 */
	private String badContent;
	/**
	 * 不良内容详情
	 */
	private String badContentsDetails;
	/**
	 * お客様管理No.
	 */
	private String customerControlNo;
	/**
	 * 指定回答日
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date designatedAnswerDay;
	/**
	 * 生産場所
	 */
	private String prodPlace;
	/**
	 * 製品区分
	 */
	private String prodDivision;
	/**
	 * 製造ロットNo.
	 */
	private String lotLotNo;
	/**
	 * 製品DMC
	 */
	private String lotLotNo2;
	/**
	 * 生産日
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date prodDay;
	/**
	 * 重要度ランク
	 */
	private String importanceRank;
	/**
	 * 要因
	 */
	private String cause;
	/**
	 * 责任方
	 */
	private String respParty;
	/**
	 * 解析結果／原因
	 */
	private String analysisRet;
	/**
	 * 要因詳細
	 */
	private String factorsDetails;
	/**
	 * １次回答日
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date oneAnsDay;
	/**
	 * １次回答内容
	 */
	private String oneRespContent;
	/**
	 * 中間回答日
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date intermediateAnsDay;
	/**
	 * 中間回答内容
	 */
	private String intermediateAnsAttr;
	/**
	 * 最終回答日
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date finalRespDate;
	/**
	 * 最終回答内容
	 */
	private String finalAnsAttr;
	/**
	 * お客様完了日
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date custCompletDate;
	/**
	 * 担当者
	 */
	private String personInCharge;
	/**
	 * 版本号
	 */
	private Integer version;
	/**
	 * 解析依頼書文件名
	 */
//    private String anAnalysisReqBookSrcName;
    /**
	 * 解析依頼書文件id
	 */
//    private Integer anAnalysisReqBookFileId;
    /**
     * 部品解析結果文件名
     */
//    private String componentAnalysisRetSrcName;
    /**
     * 部品解析結果文件id
     */
//    private Integer componentAnalysisRetFileId;
    /**
     * 不具合写真文件名
     */
//    private String glamourPhotoSrcName;
    /**
     * 不具合写真文件id
     */
//    private Integer glamourPhotoFileId;
    /**
     * 品質異常書文件名
     */
//    private String aQualityAbnormalityBookSrcName;
    /**
     * 品質異常書文件id
     */
//    private Integer aQualityAbnormalityBookFileId;
    /**
     * 最終回答書文件名
     */
//    private String aFinalAnsBookSrcName;
    /**
     * 最終回答書文件id
     */
//    private Integer aFinalAnsBookFileId;
    /**
     * 创建者的用户名
     */
    private String creatorUsername;
    /**
     * 最近一次编辑者的用户名
     */
    private String editorUsername;
    /**
     * 状态，1:启用，0:禁用
     */
    private Integer status;
	/**
	 * 要求回答日
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd")
	private Date demandReplyDate;
	/**
	 * 最近的一次提醒日期，当该受付件7天内没有处理时，会触发一次提醒。如果提醒后一天仍然没有处理，则继续提醒。
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm")
	private Date lastRemindDate1;
	/**
	 * 最近的一次提醒日期，当该受付件“お客様完了日” 未填, 并且距离 “要求回答日” 不到 3 天则每天报警一次。
	 */
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm")
	private Date lastRemindDate2;
	/**
	 * 创建日期
	 */
	@TableField(fill = FieldFill.INSERT)
	@JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm")
	private Date createTime;
}