package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 站内信接收用户表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-12-06 15:19:29
 */
@Data
@TableName("inbox_message")
public class InboxMessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 关联的站内信主表的 id
	 */
	private Integer inboxMessageTextId;
	/**
	 * 接收用户名
	 */
	private String receiverUsername;
	/**
	 * 阅读时间
	 */
	private Date readedTime;
	/**
	 * 删除时间
	 */
	private Date deleteTime;
	/**
	 * 是否已经发送到邮箱, 0:否,1:是,2:不需要发送,3:暂无用户邮箱信息,无法发送,4:邮箱地址有误发送失败
	 */
	private Integer sendedEmail;
}
