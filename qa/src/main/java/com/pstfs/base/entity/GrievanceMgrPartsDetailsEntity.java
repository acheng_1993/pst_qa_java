package com.pstfs.base.entity;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 客户投诉管理部品详情
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-12-19 09:13:59
 */
@Data
@TableName("grievance_mgr_parts_details")
public class GrievanceMgrPartsDetailsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 客户投诉管理的主键id, 关联 grievance_mgr.id
	 */
	private Integer grievanceMgrId;
	/**
	 * 部品位置
	 */
	private String componentPos;
	/**
	 * 部品品番
	 */
	private String partNum;
	/**
	 * メーカ名
	 */
	private String makerName;
}