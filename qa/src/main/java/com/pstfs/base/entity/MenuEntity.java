package com.pstfs.base.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pstfs.base.entity.intf.ISortable;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 菜单表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Data
@TableName("menu")
public class MenuEntity implements Serializable,ISortable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 菜单名称
	 */
	private String menuName;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;
	/**
	 * 菜单下标
	 */
	private Integer menuIndex;
	/**
	 * 菜单类型，0:目录、1:菜单、2:表单
	 */
	private Integer type;
	/**
	 * 上级菜单
	 */
	private Integer parentId;
	/**
	 * 菜单顺序
	 */
	private Integer sortIndex;
	/**
	 * 菜单功能描述
	 */
	private String menuDesc;
}