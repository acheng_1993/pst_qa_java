package com.pstfs.base.entity;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 管理No.分配表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-18 13:04:29
 */
@Data
@TableName("mgm_no_allocation")
public class MgmNoAllocationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 分配的管理No.
	 */
	private String mgmNo;
	/**
	 * 管理No.类型
	 */
	private String type;
	/**
	 * 是否被使用，0:否，1:是
	 */
	private Integer used;
	/**
	 * 年月，格式: YYMM
	 */
	private String yyMm;
	/**
	 * 创建者用户名
	 */
	private String creatorUsername;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
}
