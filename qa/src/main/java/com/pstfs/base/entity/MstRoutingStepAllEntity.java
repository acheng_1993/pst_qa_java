package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 制品工艺节点追踪路线
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-08-22 15:01:31
 */
@Data
@TableName("mst_routing_step_all")
public class MstRoutingStepAllEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private String id;
	/**
	 * 工厂ID
	 */
	private String plantId;
	/**
	 * 工厂名称
	 */
	private String plantName;
	/**
	 * 物品代码
	 */
	private String itmCode;
	/**
	 * 物品名称
	 */
	private String itmName;
	/**
	 * 工序ID
	 */
	private String operationCode;
	/**
	 * 工序名称
	 */
	private String operationName;
	/**
	 * 工序序号
	 */
	private Integer operationSeq;
	/**
	 * 工程步骤代码
	 */
	private String operationStep;
	/**
	 * 工程步骤名称
	 */
	private String operationStepName;
	/**
	 * 扫描方式（固定/PC）
	 */
	private String scanMode;
	/**
	 * 检查方式（自动/人工）
	 */
	private String detectionMode;
	/**
	 * 是否实绩登录工序（Y/N）
	 */
	private String isDetResult;
	/**
	 * 
	 */
	private String isQcTest;
	/**
	 * MO工单打印节点（Y/N）
	 */
	private String isPrintMo;
	/**
	 * 连番盒子使用
	 */
	private String renban;
	/**
	 * 有效无效
	 */
	private String isDel;
	/**
	 * 创建用户
	 */
	private String createdByName;
	/**
	 * 创建日期
	 */
	private Date createdWhen;
	/**
	 * 更新用户
	 */
	private String lastModifiedByName;
	/**
	 * 更新日期
	 */
	private Date lastModifiedWhen;

}
