package com.pstfs.base.entity.intf;

/**
 * 可以排序的实体类接口
 * @author jiajian
 */
public interface ISortable {

	/**
	 * 获取主键id
	 * @return
	 */
	Integer getId();
	
	/**
	 * 设置主键id
	 * @param id
	 */
	void setId(Integer id);
	
	/**
	 * 获取排序字段
	 * @return
	 */
	Integer getSortIndex();
	
	/**
	 * 设置排序字段
	 * @param sortIndex
	 */
	void setSortIndex(Integer sortIndex);
	
	/**
	 * 获取上级id
	 * @return
	 */
	Integer getParentId();
	
	/**
	 * 设置上级id
	 * @param parentId
	 */
	void setParentId(Integer parentId);
}