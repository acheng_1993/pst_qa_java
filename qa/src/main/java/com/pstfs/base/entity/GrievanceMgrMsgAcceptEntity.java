package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 客户投诉管理消息接收者
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-01-29 16:25:45
 */
@Data
@TableName("grievance_mgr_msg_accept")
public class GrievanceMgrMsgAcceptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 客户投诉管理的主键id, 关联 grievance_mgr.id
	 */
	private Integer grievanceMgrId;
	/**
	 * 追加接收用户的邮箱
	 */
	private String email;

}
