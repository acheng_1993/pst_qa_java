package com.pstfs.base.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 按钮表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Data
@TableName("button")
public class ButtonEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 按钮名称
	 */
	private String btnName;
	/**
	 * 按钮所属菜单（表单）
	 */
	private Integer menuId;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;
	/**
	 * 按钮下标
	 */
	private Integer btnIndex;
	/**
	 * 按钮功能描述
	 */
	private String btnDesc;
}
