package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 涉及到文件上传的表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-19 10:31:31
 */
@Data
@TableName("upload_file_table")
public class UploadFileTableEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 涉及文件上传的表名
	 */
	private String tableName;
	/**
	 * 涉及文件上传的表字段
	 */
	private String fileIdField;

}
