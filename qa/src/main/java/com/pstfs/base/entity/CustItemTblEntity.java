package com.pstfs.base.entity;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 客户品目查询
 * @author jiajian
 */
@Data
@TableName("TKR.CUST_ITEM_TBL")
public class CustItemTblEntity {

	/**
	 * 工厂id
	 */
	private String fcId;
	/**
	 * 客户CD
	 */
	private String custCd;
	/**
	 * 客户品目编号
	 */
	private String custItemCd;
	private Date efctDt;
	/**
	 * 销售公司品目编号
	 */
	private String salItmCd;
	/**
	 * 品目编号
	 */
	private String itemCd;
	/**
	 * 更新日
	 */
	private Date updDt;
	private Date entDt;
	/**
	 * 登录者ID
	 */
	private String userId;
}