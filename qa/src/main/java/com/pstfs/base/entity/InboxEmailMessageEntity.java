package com.pstfs.base.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 站内信接收邮箱表，针对不发送到 inbox_message 表, 而是直接发送到邮箱的消息
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-02-04 11:28:31
 */
@Data
@TableName("inbox_email_message")
public class InboxEmailMessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 关联的站内信主表的 id
	 */
	private Integer inboxMessageTextId;
	/**
	 * 接收邮箱
	 */
	private String receiverEmail;
	/**
	 * 是否已经发送到邮箱, 0:否,1:是, 4:邮箱地址有误发送失败
	 */
	private Integer sendedEmail;

}
