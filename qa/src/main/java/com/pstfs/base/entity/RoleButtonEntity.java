package com.pstfs.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 角色按钮表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-17 13:57:22
 */
@Data
@TableName("role_button")
public class RoleButtonEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 角色id
	 */
	private Integer roleId;
	/**
	 * 按钮id
	 */
	private Integer buttonId;

}
