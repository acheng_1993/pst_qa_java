package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.GrievanceMgrUploadFileEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 客户投诉管理的上传附件表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-01-29 10:36:10
 */
@Mapper
public interface GrievanceMgrUploadFileMapper extends BaseMapper<GrievanceMgrUploadFileEntity> {
	
}
