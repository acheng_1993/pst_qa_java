package com.pstfs.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.base.entity.RequestUrlEntity;
import com.pstfs.base.extend.RequestUrlExtend;

/**
 * 请求url表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Mapper
public interface RequestUrlMapper extends BaseMapper<RequestUrlEntity> {

	/**
	 * 关联 menu_url 查询(inner join)
	 * @param queryWrapper
	 * @return
	 */
	@Select("select ${" + Constants.WRAPPER + ".sqlSelect} from request_url ru inner join menu_url mu "+
	"on ru.id = mu.url_id where ${"+ Constants.WRAPPER + ".sqlSegment}")
	public List<RequestUrlExtend> listExtend1(@Param(Constants.WRAPPER) Wrapper<RequestUrlExtend> queryWrapper);
	
	/**
	 * 关联 button_url 查询(inner join)
	 * @param queryWrapper
	 * @return
	 */
	@Select("select ${" + Constants.WRAPPER + ".sqlSelect} from request_url ru inner join button_url bu "+
	"on ru.id = bu.url_id where ${"+ Constants.WRAPPER + ".sqlSegment}")
	public List<RequestUrlExtend> listExtend2(@Param(Constants.WRAPPER) Wrapper<RequestUrlExtend> queryWrapper);
}