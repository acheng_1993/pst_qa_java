package com.pstfs.base.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.ButtonUrlEntity;

/**
 * 按钮对应的url
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-30 15:15:42
 */
@Mapper
public interface ButtonUrlMapper extends BaseMapper<ButtonUrlEntity> {
	
}
