package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.MgmNoAllocationEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 管理No.分配表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-18 13:04:29
 */
@Mapper
public interface MgmNoAllocationMapper extends BaseMapper<MgmNoAllocationEntity> {
	
}
