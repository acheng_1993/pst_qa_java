package com.pstfs.base.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.CustItemTblEntity;

/**
 * 客户品目查询
 * @author jiajian
 */
@Mapper
public interface CustItemTblMapper extends BaseMapper<CustItemTblEntity> {

}
