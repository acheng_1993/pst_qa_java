package com.pstfs.base.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.SchedulingProgressEntity;

/**
 * 后台服务进度表，用于统一维护所有的后台服务进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-22 16:20:19
 */
@Mapper
public interface SchedulingProgressMapper extends BaseMapper<SchedulingProgressEntity> {

	
}