package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.GrievanceMgrPartsDetailsEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 客户投诉管理部品详情
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-12-19 09:13:59
 */
@Mapper
public interface GrievanceMgrPartsDetailsMapper extends BaseMapper<GrievanceMgrPartsDetailsEntity> {
	
}
