package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.EnumTableEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 枚举表，用于让用户自行维护某种业务的枚举数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-23 13:26:25
 */
@Mapper
public interface EnumTableMapper extends BaseMapper<EnumTableEntity> {
	
}
