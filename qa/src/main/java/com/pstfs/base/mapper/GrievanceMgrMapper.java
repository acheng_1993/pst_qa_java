package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.GrievanceMgrEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 客户投诉管理
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-18 10:25:06
 */
@Mapper
public interface GrievanceMgrMapper extends BaseMapper<GrievanceMgrEntity> {
	
}
