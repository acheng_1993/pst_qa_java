package com.pstfs.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.base.entity.ButtonEntity;
import com.pstfs.base.extend.ButtonExtend;

/**
 * 按钮表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Mapper
public interface ButtonMapper extends BaseMapper<ButtonEntity> {
	
	@Select("select ${" + Constants.WRAPPER + ".sqlSelect} from button b left join menu m "+
	"on b.menu_id = m.id where ${"+ Constants.WRAPPER + ".sqlSegment}")
	public List<ButtonExtend> listExtend(@Param(Constants.WRAPPER) Wrapper<ButtonExtend> queryWrapper);
}
