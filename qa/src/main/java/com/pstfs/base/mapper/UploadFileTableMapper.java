package com.pstfs.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.UploadFileTableEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 涉及到文件上传的表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-19 10:31:31
 */
@Mapper
public interface UploadFileTableMapper extends BaseMapper<UploadFileTableEntity> {
	
}
