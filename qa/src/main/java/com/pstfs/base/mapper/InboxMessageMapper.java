package com.pstfs.base.mapper;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.base.entity.InboxMessageEntity;
import com.pstfs.base.extend.InboxMessageExtend;

/**
 * 站内信接收用户表
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-12-06 15:19:29
 */
@Mapper
public interface InboxMessageMapper extends BaseMapper<InboxMessageEntity> {

	@Select("select ${" + Constants.WRAPPER + ".sqlSelect} from inbox_message im inner join inbox_message_text imt "+
	"on im.inbox_message_text_id = imt.id where 1=1 and ${"+ Constants.WRAPPER + ".sqlSegment}")
	public List<InboxMessageExtend> listExtend(@Param(Constants.WRAPPER) Wrapper<InboxMessageExtend> queryWrapper);
	
	@Select("select count(*) from ("+
	"select id from inbox_message where 1=1 and ${"+ Constants.WRAPPER + ".sqlSegment} limit 100) t")
	public int topCount(@Param(Constants.WRAPPER) Wrapper<InboxMessageEntity> queryWrapper);
}