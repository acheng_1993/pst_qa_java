package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.InboxEmailMessageEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 站内信接收邮箱表，针对不发送到 inbox_message 表, 而是直接发送到邮箱的消息
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-02-04 11:28:31
 */
@Mapper
public interface InboxEmailMessageMapper extends BaseMapper<InboxEmailMessageEntity> {
	
}