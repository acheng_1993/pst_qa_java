package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.UploadFileEntity;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 后台保存的文件表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-19 10:31:31
 */
@Mapper
public interface UploadFileMapper extends BaseMapper<UploadFileEntity> {

	/**
	 * 根据上次删除的进度 lastProgressId，然后以及本次删除的数据数 limit 获取本次需要
	 * 检阅的 upload_file 表的数据最后一条的 id 即 newLastProgressId，并返回。
	 * 实际检阅的文件是 [lastProgressId,newLastProgressId] 区间内的数据
	 * @param lastProgressId
	 * @param limit
	 * @return
	 */
	@Select("select max(t.id) id from (select id from upload_file where id > #{lastProgressId} order by id asc limit #{limit}) t")
	public Integer newLastProgressId(@Param("lastProgressId")int lastProgressId,@Param("limit")int limit);
}