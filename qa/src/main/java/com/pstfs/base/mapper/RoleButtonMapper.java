package com.pstfs.base.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.RoleButtonEntity;

/**
 * 角色按钮表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-17 13:57:22
 */
@Mapper
public interface RoleButtonMapper extends BaseMapper<RoleButtonEntity> {
	
}
