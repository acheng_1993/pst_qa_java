package com.pstfs.base.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.MenuUrlEntity;

/**
 * 菜单的url
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Mapper
public interface MenuUrlMapper extends BaseMapper<MenuUrlEntity> {
	
}
