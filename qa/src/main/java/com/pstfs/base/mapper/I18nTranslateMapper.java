package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.I18nTranslateEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 国际化翻译表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-01-03 15:30:32
 */
@Mapper
public interface I18nTranslateMapper extends BaseMapper<I18nTranslateEntity> {
	
}
