package com.pstfs.base.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.base.entity.RoleEntity;
import com.pstfs.base.extend.RoleExtend;

/**
 * 角色表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Mapper
public interface RoleMapper extends BaseMapper<RoleEntity> {
	
	/**
	 * 与 user_role 关联的扩展
	 * @param queryWrapper
	 * @return
	 */
	@Select("select ${" + Constants.WRAPPER + ".sqlSelect} from role r left join user_role ur "+
	"on r.id = ur.role_id where ${"+ Constants.WRAPPER + ".sqlSegment}")
	public List<RoleExtend> listExtend1(@Param(Constants.WRAPPER) Wrapper<RoleExtend> queryWrapper);
}
