package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.base.entity.GrievanceMgrMsgAcceptEntity;
import com.pstfs.base.extend.GrievanceMgrMsgAcceptExtend;
import com.pstfs.base.extend.RoleExtend;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 客户投诉管理消息接收者
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-01-29 16:25:45
 */
@Mapper
public interface GrievanceMgrMsgAcceptMapper extends BaseMapper<GrievanceMgrMsgAcceptEntity> {
	
	/**
	 * 与 user_role 关联的扩展
	 * @param queryWrapper
	 * @return
	 */
	
	@Select("select ${" + Constants.WRAPPER + ".sqlSelect} from grievance_mgr_msg_accept gmma left join grievance_mgr gm "+
	"on gmma.grievance_mgr_id = gm.id where ${"+ Constants.WRAPPER + ".sqlSegment}")
	public List<GrievanceMgrMsgAcceptExtend> listExtend1(@Param(Constants.WRAPPER) Wrapper<GrievanceMgrMsgAcceptExtend> queryWrapper);
}