package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.InboxMessageTextEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 站内信主表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-12-06 15:19:30
 */
@Mapper
public interface InboxMessageTextMapper extends BaseMapper<InboxMessageTextEntity> {
	
}
