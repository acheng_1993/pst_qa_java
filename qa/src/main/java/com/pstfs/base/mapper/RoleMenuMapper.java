package com.pstfs.base.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.RoleMenuEntity;

/**
 * 角色菜单表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenuEntity> {
	
}
