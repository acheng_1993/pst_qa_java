package com.pstfs.base.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.MstItmAllEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 物品基础信息主表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-10 11:38:27
 */
@Mapper
public interface MstItmAllMapper extends BaseMapper<MstItmAllEntity> {
	
}
