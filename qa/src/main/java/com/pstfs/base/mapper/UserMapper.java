package com.pstfs.base.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.base.entity.UserEntity;

/**
 * 用户表
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-24 14:46:28
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
	
	/**
	 * 增加登录失败次数
	 * @param username
	 */
	@Select("update user set login_fail_count+=1,last_login_fail_time=now() where username=#{username}")
	public void addFailCount(@Param("username") String username);
}
