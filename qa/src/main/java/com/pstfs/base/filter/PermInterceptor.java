package com.pstfs.base.filter;
import java.time.LocalDate;
import java.time.ZoneOffset;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.oauth2.SecretKeyTable;
import com.pstfs.base.oauth2.SecretKeyTable.SecretKeyTableMgr;
import com.pstfs.base.oauth2.UrlMap;
import com.pstfs.base.utils.AuthorizationUtils;
import com.pstfs.base.utils.AuthorizationUtils.AuthorizationObj;
import com.pstfs.base.utils.R;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import cn.hutool.json.JSONObject;

/**
 * 权限拦截器
 * @author jiajian
 */
@Component
public class PermInterceptor implements HandlerInterceptor {
	
	@Autowired
	private UrlMap urlMap;
	
	/**
	 * 密码表管理类
	 */
	@Autowired
	private SecretKeyTableMgr secretKeyTableMgr;
	
	@Autowired
	private AuthorizationUtils authorizationUtils;
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//在报文头传入 Authorization
		String authorization = authorizationUtils.getAuthorization(request);
		if (StringUtils.isEmpty(authorization)) {
			JSONObject jsonObj = new JSONObject(R.error(HttpStatus.SC_UNAUTHORIZED));
			response.getWriter().print(jsonObj.toString());
			return false;
		} else {
			/**
			 * 把认证信息切分成两段：用户名长度+用户名+有效期+角色、加密串、
			 */
			AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(authorization);
			long validity = authorizationObj.getValidity();
			long now = System.currentTimeMillis();
			//登录超时
			if (validity<now) {
				JSONObject jsonObj = new JSONObject(R.error(HttpStatus.SC_UNAUTHORIZED));
				response.getWriter().print(jsonObj.toString());
				return false;
			} else {
				byte[] secretKey;
				long todayTime = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.of("+8")).toEpochMilli();
				//如果登录有效期在今天、登录时间却在昨天，则使用昨天的密钥表获取密钥（因为每隔24小时密钥表会发生变化）
				if(validity - 2*3600*1000 < todayTime) {
					//获取不了昨天的密钥表
					SecretKeyTable yestodaySecretKeyTable = secretKeyTableMgr.getYestodaySecretKeyTable();
					if(yestodaySecretKeyTable == null) {
						JSONObject jsonObj = new JSONObject(R.error(HttpStatus.SC_UNAUTHORIZED));
						response.getWriter().print(jsonObj.toString());
						return false;
					} else {
						char[] username = authorizationObj.getUsername().toCharArray();
						secretKey = yestodaySecretKeyTable.getSecretKey(username);
					}
				} else {
					//其他情况获取当天的密钥表
					char[] username = authorizationObj.getUsername().toCharArray();
					secretKey = secretKeyTableMgr.getSecretKeyTable().getSecretKey(username);
				}
				SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, secretKey);
				String[] authorizations = authorization.split("\\.");
				//把令牌进行加密，和加密后的令牌进行比较，如果不一样则认为令牌是伪造的
				if (!authorizations[1].equals(aes.encryptBase64(authorizationObj.getData()))) {
					JSONObject jsonObj = new JSONObject(R.error(HttpStatus.SC_UNAUTHORIZED));
					response.getWriter().print(jsonObj.toString());
					return false;
				}
				Integer index = urlMap.get(request.getRequestURI());
				if (index == null) {
					return true;
				} else {
					boolean ret = authorizationObj.checkIndex(index);
					if (ret == Boolean.FALSE) {
						JSONObject jsonObj = new JSONObject(R.error(HttpStatus.SC_FORBIDDEN));
						response.getWriter().print(jsonObj.toString());
					}
					return ret;
				}
			}
		}
	}
}