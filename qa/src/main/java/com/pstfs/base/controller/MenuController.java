package com.pstfs.base.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pstfs.base.entity.I18nTranslateEntity;
import com.pstfs.base.entity.MenuEntity;
import com.pstfs.base.entity.RequestUrlEntity;
import com.pstfs.base.service.I18nTranslateService;
import com.pstfs.base.service.MenuService;
import com.pstfs.base.service.RequestUrlService;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.utils.R;
import cn.hutool.json.JSONObject;

@RestController
@RequestMapping("/menu")
public class MenuController {

	@Autowired
	private MenuService menuService;

	@Autowired
	private RequestUrlService requestUrlService;

	/**
	 * 加载菜单管理首页
	 * @param params
	 * @return
	 */
	@RequestMapping("/index")
	public R index(@RequestBody(required = false) Map<String,Object> params) {
		List<MenuEntity> list = menuService.list(params,"id,menu_name,menu_desc,menu_index,type,parent_id,sort_index");
		return R.ok().put("list", list);
	}

	/**
	 * 根据条件加载菜单列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public R list(@RequestBody Map<String,Object> params) {
		List<MenuEntity> list = menuService.list(params,"id,menu_desc",20);
		return R.ok().put("list", list);
	}
	
	/**
	 * 删除菜单
	 * @param id
	 * @return
	 */
	@RequestMapping("/del")
	public R del(Integer id) {
		menuService.removeById(id);
		return R.ok();
	}

	/**
	 * 菜单移动
	 * @param id
	 * @param targetId
	 * @param moveType
	 * @return
	 */
	@RequestMapping("/move")
	public R move(int id, int targetId, int moveType) {
		menuService.move(id, targetId, moveType);
		return R.ok();
	}
	
	@Autowired
	private I18nTranslateService i18nTranslateService;
	
	@RequestMapping("/edit")
	public R edit(Integer id) {
		MenuEntity entity = menuService.getById(id);
		MenuEntity parentMenu = null;
		if (entity != null) {
			parentMenu = menuService.getById(entity.getParentId());
		}
		List<RequestUrlEntity> requestUrlList = requestUrlService.list(MapUtils.hmo("menuId",id), "url");
		List<String> urlList = new ArrayList<>();
		for(int i=0,len_i=requestUrlList.size();i<len_i;i++) {
			urlList.add(requestUrlList.get(i).getUrl());
		}
		List<I18nTranslateEntity> i18nTranslateList = i18nTranslateService.list("menu", "menu_desc", id);
		return R.ok().put("entity", entity).put("urlList", urlList).put("parentMenu", parentMenu).put("i18nTranslateList", i18nTranslateList);
	}
	
	@RequestMapping("/save")
	public R save(@RequestBody JSONObject jsonObj) {
		String menuName = jsonObj.getStr("menuName");
		Integer type = jsonObj.getInt("type");
		Integer parentId = jsonObj.getInt("parentId");
		String menuDesc = jsonObj.getStr("menuDesc");
		List<String> urlList = jsonObj.getBeanList("urlList", String.class);
		List<I18nTranslateEntity> i18nTranslateList = jsonObj.getBeanList("i18nTranslateList",I18nTranslateEntity.class);
		menuService.save(menuName, type, parentId, menuDesc, urlList, i18nTranslateList);
		return R.ok();
	}
	
	@RequestMapping("/update")
	public R update(@RequestBody JSONObject jsonObj) {
		Integer id = jsonObj.getInt("id");
		String menuName = jsonObj.getStr("menuName");
		String menuDesc = jsonObj.getStr("menuDesc");
		List<String> urlList = jsonObj.getBeanList("urlList", String.class);
		List<I18nTranslateEntity> i18nTranslateList = jsonObj.getBeanList("i18nTranslateList",I18nTranslateEntity.class);
		menuService.update(id, menuName, menuDesc, urlList, i18nTranslateList);
		return R.ok();
	}
}