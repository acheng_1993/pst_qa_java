package com.pstfs.base.controller;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pstfs.base.service.MgmNoAllocationService;
import com.pstfs.base.utils.AuthorizationUtils;
import com.pstfs.base.utils.AuthorizationUtils.AuthorizationObj;
import com.pstfs.base.utils.R;

/**
 * 管理No.分配表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-18 13:04:29
 */
@RestController
@RequestMapping("/mgmnoallocation")
public class MgmNoAllocationController {
    @Autowired
    private MgmNoAllocationService mgmNoAllocationService;
    
    @Autowired
	private AuthorizationUtils authorizationUtils;

    @RequestMapping("/createNewMgmNo")
    public R createNewMgmNo(String type,HttpServletRequest request) {
    	AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
    	String mgmNo = mgmNoAllocationService.createNewMgmNo(authorizationObj.getUsername(), type);
    	return R.ok().put("mgmNo", mgmNo);
    }
}