package com.pstfs.base.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pstfs.base.entity.ButtonEntity;
import com.pstfs.base.entity.MenuEntity;
import com.pstfs.base.entity.RoleEntity;
import com.pstfs.base.service.ButtonService;
import com.pstfs.base.service.MenuService;
import com.pstfs.base.service.RoleService;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.utils.R;

import cn.hutool.json.JSONObject;

@RestController
@RequestMapping("/role")
public class RoleController {

	@Autowired
	private I18nUtils i;
	
	@Autowired
	private RoleService roleService;

	@Autowired
	private MenuService menuService;

	@Autowired
	private ButtonService buttonService;
	
	@RequestMapping("/list")
	public R list(@RequestBody(required = false) Map<String,Object> params) {
		List<RoleEntity> list = roleService.list(params,"id,role_name,role_desc");
		return R.ok().put("list", list);
	}

	@RequestMapping("/copy")
	public R copy(Integer id) {
		roleService.copy(id);
		return R.ok();
	}

	@RequestMapping("/index")
	public R index(@RequestBody Map<String,Object> params) {
		List<RoleEntity> list = roleService.scrollLoad(params);
		return R.ok().put("list", list);
	}
	
	@RequestMapping("/edit")
	public R edit(Integer id) {
		Map<String,Object> params = MapUtils.hmo("roleId",id);
		List<MenuEntity> menuList = menuService.list(params,"id");
		List<Integer> menuIdList = new ArrayList<>(); 
		for (int i=0,len_i=menuList.size();i<len_i;i++) {
			menuIdList.add(menuList.get(i).getId());
		}
		List<ButtonEntity> buttonList = buttonService.list(params,"id");
		List<Integer> buttonIdList = new ArrayList<>();
		for (int i=0,len_i=buttonList.size();i<len_i;i++) {
			buttonIdList.add(buttonList.get(i).getId());
		}
		RoleEntity entity = roleService.getById(id);
		return R.ok().put("entity", entity).put("menuIdList", menuIdList).put("buttonIdList", buttonIdList);
	}
	
	@RequestMapping("/save")
	public R save(@RequestBody JSONObject json) {
		String roleName = json.getStr("roleName");
		String roleDesc = json.getStr("roleDesc");
		List<Integer> menuIdList = json.getBeanList("menuIdList", Integer.class);
		List<Integer> buttonIdList = json.getBeanList("buttonIdList", Integer.class);
		roleService.save(roleName, roleDesc, menuIdList, buttonIdList);
		return R.ok();
	}
	
	@RequestMapping("/update")
	public R update(@RequestBody JSONObject json) {
		Integer id = json.getInt("id");
		String roleName = json.getStr("roleName");
		String roleDesc = json.getStr("roleDesc");
		List<Integer> menuIdList = json.getBeanList("menuIdList", Integer.class);
		List<Integer> buttonIdList = json.getBeanList("buttonIdList", Integer.class);
		roleService.update(id, roleName, roleDesc, menuIdList, buttonIdList);
		return R.ok();
	}
	
	@RequestMapping("/del")
	public R del(Integer id) {
		if(roleService.removeById(id)) {
			return R.ok();
		} else {
			return R.error(i.t("RoleController.del.delFail"));//该角色正在使用中，无法删除！
		}
	}
}