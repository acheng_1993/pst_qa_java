package com.pstfs.base.controller;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pstfs.base.entity.CustItemTblEntity;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.service.CustItemTblService;
import com.pstfs.base.utils.R;

@RestController
@RequestMapping("/custItemTbl")
public class CustItemTblController {
	
	@Autowired
	private CustItemTblService custItemTblService;
	
	@RequestMapping("/list")
	public R list(@RequestBody(required = false) Map<String,Object> params) {
		List<CustItemTblEntity> list = null;
		String field = (String)params.get("field");
		if ("distinct item_cd".equals(field)) {
			list = custItemTblService.list(params,"item_cd",20);
		} else if("distinct cust_item_cd".equals(field)) {
			list = custItemTblService.list(params,"cust_item_cd",20);
		} else {
			throw new RRException("field字段异常！");
		}
		return R.ok().put("list", list);
	}
}