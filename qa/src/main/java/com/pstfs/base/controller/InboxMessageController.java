package com.pstfs.base.controller;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.InboxMessageTextEntity;
import com.pstfs.base.extend.InboxMessageExtend;
import com.pstfs.base.service.InboxMessageService;
import com.pstfs.base.service.InboxMessageTextService;
import com.pstfs.base.utils.AuthorizationUtils;
import com.pstfs.base.utils.AuthorizationUtils.AuthorizationObj;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.R;

@RestController
@RequestMapping("/inboxMessage")
public class InboxMessageController {

	@Autowired
	private I18nUtils i;
	
	@Autowired
	private InboxMessageService inboxMessageService;
	
	@Autowired
	private InboxMessageTextService inboxMessageTextService;

	@Autowired
	private AuthorizationUtils authorizationUtils;

	@RequestMapping("/del")
	public R del(Integer id,HttpServletRequest request) {
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		if(inboxMessageService.remove(id,authorizationObj.getUsername())) {
			return R.ok();
		} else {
			return R.error(i.t("InboxMessageController.del.delFail"));//删除失败！
		}
	}

	/**
	 * 阅读站内信
	 * @return
	 */
	@RequestMapping("/edit")
	public R edit(int id, int inboxMessageTextId,String readedTime,HttpServletRequest request) {
		InboxMessageTextEntity entity = inboxMessageTextService.getById(inboxMessageTextId);
		if (readedTime == null) {
			AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
			inboxMessageService.updateReaded(id, authorizationObj.getUsername());
		}
		return R.ok().put("entity", entity);
	}
	
	/**
	 * 获取最近一周内未读的所有消息
	 * @param params
	 * @param request
	 * @return
	 */
	@RequestMapping("/lastWeekUnread")
	public R lastWeekUnread(@RequestBody(required = false) Map<String,Object> params,HttpServletRequest request) {
		if (params == null) {
			params = new HashMap<>();
		}
		LocalDate localDate = LocalDate.now().minusDays(7);
		params.put("createTimeRangeStart", localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		params.put("receiverUsername", authorizationObj.getUsername());
		params.put("delete", "NULL");
		params.put("readed", "NULL");
		List<InboxMessageExtend> list = inboxMessageService.scrollLoad(params, "imt.title,imt.simple_content");
		return R.ok().put("list", list);
	}
	
	/**
	 * 滚动式加载站内信
	 * @param params
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public R index(@RequestBody(required = false) Map<String,Object> params,HttpServletRequest request) {
		if (params == null) {
			params = new HashMap<>();
		}
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		params.put("receiverUsername", authorizationObj.getUsername());
		params.put("delete", "NULL");
		
		String titleLike = (String)params.get("titleLike");
		boolean titleLikeExists = StringUtils.isNotEmpty(titleLike);
		String contentLike = (String)params.get("contentLike");
		boolean contentLikeExists = StringUtils.isNotEmpty(contentLike);
		if (titleLikeExists || contentLikeExists) {
			List<Map<String, Object>> list = inboxMessageService.scrollLoadFromEs(params);
			return R.ok().put("list", list);
		} else {
			List<InboxMessageExtend> list = inboxMessageService.scrollLoad(params, "im.id,im.inbox_message_text_id,imt.title,imt.simple_content,im.readed_time,imt.creator_username,imt.create_time");
			return R.ok().put("list", list);
		}
	}

	/**
	 * 计算未读消息数
	 * @param params
	 * @param request
	 * @return
	 */
	@RequestMapping("/unreadCount")
	public R unreadCount(@RequestBody(required = false) Map<String,Object> params,HttpServletRequest request) {
		if(params == null) {
			params = new HashMap<>();
		}
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		params.put("receiverUsername", authorizationObj.getUsername());
		params.put("delete", "NULL");
		params.put("readed", "NULL");
		int count = inboxMessageService.count(params);
		return R.ok().put("count", count);
	}
}