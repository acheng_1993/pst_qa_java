package com.pstfs.base.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.base.entity.RoleEntity;
import com.pstfs.base.entity.UserEntity;
import com.pstfs.base.extend.RoleExtend;
import com.pstfs.base.service.RoleService;
import com.pstfs.base.service.UserService;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.utils.R;
import com.pstfs.base.utils.SqlParserHelper;
import cn.hutool.json.JSONObject;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private I18nUtils i;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;

	@RequestMapping("/index")
	public R index(@RequestBody Map<String,Object> params) {
		List<UserEntity> list = userService.scrollLoad(params,"id,create_time,deletable,name,username");
		List<Integer> userIdList = new ArrayList<>();
		for(int i=0,len_i=list.size();i<len_i;i++) {
			userIdList.add(list.get(i).getId());
		}
		List<RoleExtend> roleList = new ArrayList<RoleExtend>();
		if (!userIdList.isEmpty()) {
			roleList = roleService.listExtend(MapUtils.hmo("userIdList",userIdList), "r.role_name,ur.user_id");
		}
		return R.ok().put("list", list).put("roleList", roleList);
	}

	@RequestMapping("/list")
	public R list(@RequestBody Map<String,Object> params) {
		String selectColumns = (String)params.get("selectColumns");
		if (StringUtils.isEmpty(selectColumns)) {
			selectColumns = "email";
		}
		selectColumns = SqlParserHelper.selectColumnsFilter(selectColumns, 
				"password","salt","create_time","update_time","last_login_fail_time","login_fail_count","user_has_role","deletable");
		List<UserEntity> list = userService.scrollLoad(params,selectColumns);
		return R.ok().put("list", list);
	}
	
	@RequestMapping("/edit")
	public R edit(Integer id) {
		UserEntity entity = userService.getById(id);
		List<RoleEntity> roleList = roleService.list(MapUtils.hmo("userId",id), "id,role_name");
		return R.ok().put("entity", entity).put("roleList", roleList);
	}
	
	@RequestMapping("/update")
	public R update(@RequestBody JSONObject jsonObject) {
		Integer id = jsonObject.getInt("id");
		String password = jsonObject.getStr("password");
		String name = jsonObject.getStr("name");
		String email = jsonObject.getStr("email");
		List<Integer> roleIdList = jsonObject.getBeanList("roleIdList", Integer.class);
		userService.update(id, password, name, email, roleIdList);
		return R.ok();
	}
	
	@RequestMapping("/save")
	public R save(@RequestBody JSONObject jsonObject) {
		String username = jsonObject.getStr("username");
		String password = jsonObject.getStr("password");
		List<Integer> roleIdList = jsonObject.getBeanList("roleIdList", Integer.class);
		String name = jsonObject.getStr("name");
		String email = jsonObject.getStr("email");
		userService.save(username, password, name, email, roleIdList);
		return R.ok();
	}
	
	@RequestMapping("/del")
	public R del(Integer id) {
		if(userService.remove(id)) {
			return R.ok();
		} else {
			return R.error(i.t("UserController.del.delFail"));//删除失败，用户不可删除！
		}
	}
}