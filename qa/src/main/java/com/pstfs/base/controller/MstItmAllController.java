package com.pstfs.base.controller;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pstfs.base.entity.MstItmAllEntity;
import com.pstfs.base.service.MstItmAllService;
import com.pstfs.base.utils.R;

/**
 * 物品基本信息控制器
 * @author jiajian
 */
@RestController
@RequestMapping("/mstItmAll")
public class MstItmAllController {

	@Autowired
    private MstItmAllService mstItmAllService;
	
	@RequestMapping("/list")
	public R list(@RequestBody(required = false) Map<String,Object> params) {
		List<MstItmAllEntity> list = mstItmAllService.list(params,"distinct itm_code",20);
		return R.ok().put("list", list);
	}
}