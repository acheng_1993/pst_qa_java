package com.pstfs.base.controller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pstfs.base.entity.GrievanceMgrEntity;
import com.pstfs.base.entity.GrievanceMgrMsgAcceptEntity;
import com.pstfs.base.entity.GrievanceMgrPartsDetailsEntity;
import com.pstfs.base.entity.GrievanceMgrUploadFileEntity;
import com.pstfs.base.entity.UserEntity;
import com.pstfs.base.envm.Status;
import com.pstfs.base.service.GrievanceMgrMsgAcceptService;
import com.pstfs.base.service.GrievanceMgrPartsDetailsService;
import com.pstfs.base.service.GrievanceMgrService;
import com.pstfs.base.service.GrievanceMgrUploadFileService;
import com.pstfs.base.service.UserService;
import com.pstfs.base.utils.AuthorizationUtils;
import com.pstfs.base.utils.AuthorizationUtils.AuthorizationObj;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.R;

import cn.hutool.json.JSONObject;

/**
 * 客户投诉管理
 * @author jiajian
 */
@RestController
@RequestMapping("/grievancemgr")
public class GrievanceMgrController {

	@Autowired
	private GrievanceMgrService grievanceMgrService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AuthorizationUtils authorizationUtils;

	@Autowired
	private I18nUtils i;
	
	/**
	 * 用 excel 批量导入数据
	 * @param json
	 * @param request
	 * @return
	 */
	@RequestMapping("/saveBatch")
	public R saveBatch(@RequestBody List<GrievanceMgrEntity> grievanceMgrList, HttpServletRequest request) {
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		grievanceMgrService.saveBatch(authorizationObj.getUsername(), grievanceMgrList);
		return R.ok();
	}
	
	/**
	 * 复制一份出来
	 * @param id
	 * @return
	 */
	@RequestMapping("/copy")
	public R copy(@RequestBody JSONObject json, HttpServletRequest request) {
		Integer id = json.getInt("id");
		List<String> copyFieldList = json.getBeanList("copyFieldList", String.class);
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		if(copyFieldList == null) {			
			grievanceMgrService.copy(authorizationObj.getUsername(), id);
		} else {
			grievanceMgrService.copy(authorizationObj.getUsername(), id, copyFieldList);
		}
		return R.ok();
	}

	@RequestMapping("/index")
	public R index(@RequestBody Map<String,Object> params) {
		if(params == null) {
			params = new HashMap<>();
		}
		params.put("status", Status.启用.getV());
		List<GrievanceMgrEntity> list = grievanceMgrService.scrollLoad(params);
		List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList;
		if (list.isEmpty()) {
			grievanceMgrPartsDetailsList = new ArrayList<>();
		} else {
			List<Integer> grievanceMgrIdList = new ArrayList<Integer>(list.size());
			for (int i=0,len_i = list.size();i<len_i;i++) {
				grievanceMgrIdList.add(list.get(i).getId());
			}
			grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(grievanceMgrIdList);
		}
		return R.ok().put("list", list).put("grievanceMgrPartsDetailsList", grievanceMgrPartsDetailsList);
	}
	
	@RequestMapping("/getMinIdMaxId")
	public R getMinIdMaxId(@RequestBody Map<String,Object> params) {
		Integer[] minIdMaxId = grievanceMgrService.getMinIdMaxId(params);
		return R.ok().put("minId", minIdMaxId[0]).put("maxId", minIdMaxId[1]);
	}
	
	@RequestMapping("/save")
	public R save(@RequestBody JSONObject json, HttpServletRequest request) {
		GrievanceMgrEntity entity = json.getBean("entity", GrievanceMgrEntity.class);
		List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList = json.getBeanList("grievanceMgrPartsDetailsList", GrievanceMgrPartsDetailsEntity.class);
		List<GrievanceMgrUploadFileEntity> grievanceMgrUploadFileList = json.getBeanList("grievanceMgrUploadFileList", GrievanceMgrUploadFileEntity.class);
		List<String> acceptEmailList = json.getBeanList("acceptEmailList", String.class);
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		grievanceMgrService.save(authorizationObj.getUsername(), entity, grievanceMgrPartsDetailsList, grievanceMgrUploadFileList, acceptEmailList);
		return R.ok();
	}
	
	@Autowired
	private GrievanceMgrPartsDetailsService grievanceMgrPartsDetailsService;
	
	@Autowired
	private GrievanceMgrUploadFileService grievanceMgrUploadFileService;
	
	@Autowired
	private GrievanceMgrMsgAcceptService grievanceMgrMsgAcceptService;
	
	@RequestMapping("/edit")
	public R edit(Integer id) {
		GrievanceMgrEntity entity = grievanceMgrService.getById(id);
		List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList;
		{
			QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("grievance_mgr_id", id);
			queryWrapper.select("component_pos,part_num,maker_name");
			grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(queryWrapper);
		}
		List<GrievanceMgrUploadFileEntity> grievanceMgrUploadFileList;
		{
			QueryWrapper<GrievanceMgrUploadFileEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("grievance_mgr_id", id);
			queryWrapper.select("file_id,src_name,type");
			grievanceMgrUploadFileList = grievanceMgrUploadFileService.list(queryWrapper);
		}
		List<GrievanceMgrMsgAcceptEntity> grievanceMgrMsgAcceptList;
		{
			QueryWrapper<GrievanceMgrMsgAcceptEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("grievance_mgr_id", id);
			queryWrapper.select("email");
			grievanceMgrMsgAcceptList = grievanceMgrMsgAcceptService.list(queryWrapper);
		}
		return R.ok().put("entity", entity).put("grievanceMgrPartsDetailsList", grievanceMgrPartsDetailsList)
				.put("grievanceMgrUploadFileList", grievanceMgrUploadFileList)
				.put("grievanceMgrMsgAcceptList", grievanceMgrMsgAcceptList);
	}

	@RequestMapping("/update")
	public R update(@RequestBody JSONObject json, HttpServletRequest request) {
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		GrievanceMgrEntity entity = json.getBean("entity", GrievanceMgrEntity.class);
		List<GrievanceMgrPartsDetailsEntity> grievanceMgrPartsDetailsList = json.getBeanList("grievanceMgrPartsDetailsList", GrievanceMgrPartsDetailsEntity.class);
		List<GrievanceMgrUploadFileEntity> grievanceMgrUploadFileList = json.getBeanList("grievanceMgrUploadFileList", GrievanceMgrUploadFileEntity.class);
		List<String> acceptEmailList = json.getBeanList("acceptEmailList", String.class);
		if (grievanceMgrService.update(authorizationObj.getUsername(), entity, grievanceMgrPartsDetailsList, grievanceMgrUploadFileList, acceptEmailList)) {
			return R.ok();
		} else {
			GrievanceMgrEntity grievanceMgrEntity = grievanceMgrService.getById(entity.getId());
			UserEntity userEntity = userService.getByUsername(grievanceMgrEntity.getEditorUsername(), "name,username");
			Integer updateId = grievanceMgrEntity.getId();
			{
				QueryWrapper<GrievanceMgrPartsDetailsEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("grievance_mgr_id", updateId);
				queryWrapper.select("component_pos,part_num,maker_name");
				grievanceMgrPartsDetailsList = grievanceMgrPartsDetailsService.list(queryWrapper);
			}
			List<GrievanceMgrMsgAcceptEntity> grievanceMgrMsgAcceptList;
			{
				QueryWrapper<GrievanceMgrMsgAcceptEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("grievance_mgr_id", updateId);
				queryWrapper.select("email");
				grievanceMgrMsgAcceptList = grievanceMgrMsgAcceptService.list(queryWrapper);
			}
			{
				QueryWrapper<GrievanceMgrUploadFileEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.eq("grievance_mgr_id", updateId);
				queryWrapper.select("file_id,src_name,type");
				grievanceMgrUploadFileList = grievanceMgrUploadFileService.list(queryWrapper);
			}
//			存在冲突的更新内容，更新失败！
			return R.error(i.t("GrievanceMgrController.update.dataClash")).put("entity", grievanceMgrEntity).put("user", userEntity)
					.put("grievanceMgrPartsDetailsList", grievanceMgrPartsDetailsList)
					.put("grievanceMgrMsgAcceptList", grievanceMgrMsgAcceptList)
					.put("grievanceMgrUploadFileList", grievanceMgrUploadFileList);
		}
	}
	
	@RequestMapping("/changeStatus")
	public R changeStatus(Integer id) {
		grievanceMgrService.changeStatus(id);
		return R.ok();
	}
}