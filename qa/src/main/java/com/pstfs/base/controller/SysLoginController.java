package com.pstfs.base.controller;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.druid.util.StringUtils;
import com.pstfs.base.entity.ButtonEntity;
import com.pstfs.base.entity.MenuEntity;
import com.pstfs.base.entity.UserEntity;
import com.pstfs.base.service.ButtonService;
import com.pstfs.base.service.I18nTranslateService;
import com.pstfs.base.service.MenuService;
import com.pstfs.base.service.UserService;
import com.pstfs.base.utils.AuthorizationUtils;
import com.pstfs.base.utils.AuthorizationUtils.AuthorizationObj;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.R;

/**
 * 登录相关
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("/sys")
public class SysLoginController {

	@Autowired
	private I18nUtils i;
	
	@Autowired
	private UserService userService;

	@Autowired
	private MenuService menuService;

	@Autowired
	private ButtonService buttonService;
	
	@Autowired
	private AuthorizationUtils authorizationUtils;

	@Autowired
	private I18nTranslateService i18nTranslateService;
	
	@RequestMapping("/logout")
	public R logout(HttpServletResponse response) {
		Cookie cookie = new Cookie("Authorization",""); 
	    cookie.setMaxAge(0);
	    cookie.setPath("/");
	    cookie.setHttpOnly(true);
	    response.addCookie(cookie);
		return R.ok();
	}

	@RequestMapping("/login")
	public R login(String username,String password,HttpServletResponse response) {
		UserEntity userEntity = userService.login(username, password);
		if(userEntity == null) {
			//用户名或密码错误
			return R.error(HttpStatus.SC_INTERNAL_SERVER_ERROR, i.t("SysLoginController.login.usernameOrPasswordError"));
		} else {
			String userHasRole = userEntity.getUserHasRole();
			List<Integer> roleIndexList = authorizationUtils.getIndexList(userHasRole);
			List<MenuEntity> menuList = menuService.getMenuListByRoleIndex(roleIndexList);
			List<ButtonEntity> buttonList = buttonService.getButtonListByRoleIndex(roleIndexList);
			Locale locale = LocaleContextHolder.getLocale();
			String langCode = locale.toLanguageTag().substring(0,2);
			for(int i=0,len_i = menuList.size();i<len_i;i++) {
				MenuEntity menu = menuList.get(i);
				String val = i18nTranslateService.getValFromCache("menu", "menu_desc", menu.getId(), langCode);
				if(!StringUtils.isEmpty(val)) {
					menu.setMenuDesc(val);
				}
			}
			String Authorization = authorizationUtils.createAuthorization(username, userHasRole);
			{
				Cookie authCookie = new Cookie("Authorization", Authorization);
				authCookie.setHttpOnly(true);
				authCookie.setMaxAge(7200);
				authCookie.setPath("/");
				response.addCookie(authCookie);
			}
			return R.ok().put("Authorization", Authorization).put("menuList", menuList).put("buttonList", buttonList);
		}
	}
	
	/**
	 * 校验登录的令牌是否有效，如果有效返回角色、菜单、按钮信息
	 * @return
	 */
	@RequestMapping("/checkLogin")
	public R checkLogin(HttpServletRequest request) {
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		List<Integer> roleIndexList = authorizationObj.getRoleIndexList();
		List<MenuEntity> menuList = menuService.getMenuListByRoleIndex(roleIndexList);
		List<ButtonEntity> buttonList = buttonService.getButtonListByRoleIndex(roleIndexList);
		Locale locale = LocaleContextHolder.getLocale();
		String langCode = locale.toLanguageTag().substring(0,2);
		for (int i=0,len_i = menuList.size();i<len_i;i++) {
			MenuEntity menu = menuList.get(i);
			String val = i18nTranslateService.getValFromCache("menu", "menu_desc", menu.getId(), langCode);
			if(!StringUtils.isEmpty(val)) {
				menu.setMenuDesc(val);
			}
		}
		return R.ok().put("Authorization", authorizationObj.getAuthorization()).put("menuList", menuList).put("buttonList", buttonList);
	}

	/**
	 * 加载个人信息
	 * @param request
	 * @return
	 */
	@RequestMapping("/loadSelf")
	public R loadSelf(HttpServletRequest request) {
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		UserEntity entity = userService.getByUsername(authorizationObj.getUsername(), "username,name,email");
		return R.ok().put("entity", entity);
	}
	
	/**
	 * 更新个人信息
	 * @param username 用户名
	 * @param oldPassword 旧密码
	 * @param newPassword 新密码
	 * @return
	 */
	@RequestMapping("/updateSelf")
	public R updateSelf(HttpServletRequest request, String name, String email,String oldPassword, String newPassword) {
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		if (userService.updateSelf(authorizationObj.getUsername(), name, email, oldPassword, newPassword)) {
			return R.ok();
		} else {
			return R.error();
		}
	}	
}