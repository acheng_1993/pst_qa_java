package com.pstfs.base.controller;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pstfs.base.entity.EnumTableEntity;
import com.pstfs.base.service.EnumTableService;
import com.pstfs.base.utils.R;
import cn.hutool.json.JSONObject;

/**
 * 枚举表控制器
 * @author jiajian
 */
@RestController
@RequestMapping("/enumTable")
public class EnumTableController {

	@Autowired
	private EnumTableService enumTableService;
	
	/**
	 * 加载枚举表概览数据
	 * @return
	 */
	@RequestMapping("/index")
	public R index() {
		return R.ok().put("list", enumTableService.getEnumGroupList());
	}

	/**
	 * 根据条件加载枚举列表
	 * @param params
	 * @return
	 */
	@RequestMapping("/list")
	public R list(@RequestBody Map<String,Object> params) {
		List<EnumTableEntity> list = enumTableService.list(params);
		return R.ok().put("list", list);
	}
	
	@RequestMapping("/save")
	public R save(@RequestBody JSONObject jsonObj) {
		String enumGroup = jsonObj.getStr("enumGroup");
		List<String> enumDescList = jsonObj.getBeanList("enumDescList", String.class);
		List<Integer> statusList = jsonObj.getBeanList("statusList", Integer.class);
		enumTableService.save(enumGroup, enumDescList, statusList);
		return R.ok();
	}
}