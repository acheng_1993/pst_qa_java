package com.pstfs.base.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pstfs.base.entity.ButtonEntity;
import com.pstfs.base.entity.RequestUrlEntity;
import com.pstfs.base.extend.ButtonExtend;
import com.pstfs.base.service.ButtonService;
import com.pstfs.base.service.RequestUrlService;
import com.pstfs.base.utils.MapUtils;
import com.pstfs.base.utils.R;

import cn.hutool.json.JSONObject;

@RestController
@RequestMapping("/button")
public class ButtonController {

	@Autowired
	private ButtonService buttonService;

	@Autowired
	private RequestUrlService requestUrlService;
	
	@RequestMapping("/index")
	public R index(@RequestBody(required = false) Map<String,Object> params) {
		List<ButtonExtend> list = buttonService.listExtend(params,"b.id,b.btn_name,b.btn_desc,b.menu_id,m.menu_desc");
		return R.ok().put("list", list);
	}
	
	
	@RequestMapping("/edit")
	public R edit(Integer id) {
		ButtonEntity entity = buttonService.getById(id);
		List<RequestUrlEntity> requestUrlList = requestUrlService.list(MapUtils.hmo("buttonId",id), "url");
		List<String> urlList = new ArrayList<>();
		for(int i=0,len_i=requestUrlList.size();i<len_i;i++) {
			urlList.add(requestUrlList.get(i).getUrl());
		}
		return R.ok().put("entity", entity).put("urlList", urlList);
	}
	
	/**
	 * 删除按钮
	 * @param id
	 * @return
	 */
	@RequestMapping("/del")
	public R del(Integer id) {
		buttonService.removeById(id);
		return R.ok();
	}
	
	@RequestMapping("/save")
	public R save(@RequestBody JSONObject json) {
		String btnName = json.getStr("btnName");
		String btnDesc = json.getStr("btnDesc");
		Integer menuId = json.getInt("menuId");
		List<String> urlList = json.getBeanList("urlList", String.class);
		buttonService.save(btnName,btnDesc,menuId,urlList);
		return R.ok();
	}
	
	@RequestMapping("/update")
	public R update(@RequestBody JSONObject json) {
		Integer id = json.getInt("id");
		String btnName = json.getStr("btnName");
		String btnDesc = json.getStr("btnDesc");
		Integer menuId = json.getInt("menuId");
		List<String> urlList = json.getBeanList("urlList", String.class);
		buttonService.update(id,btnName,btnDesc,menuId,urlList);
		return R.ok();
	}
}