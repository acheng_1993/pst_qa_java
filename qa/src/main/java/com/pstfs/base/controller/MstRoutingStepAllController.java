package com.pstfs.base.controller;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pstfs.base.entity.MstRoutingStepAllEntity;
import com.pstfs.base.service.MstRoutingStepAllService;
import com.pstfs.base.utils.R;

/**
 * 机种品番查询控制器
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-18 13:04:29
 */
@RestController
@RequestMapping("/mstRoutingStepAll")
public class MstRoutingStepAllController {

	@Autowired
    private MstRoutingStepAllService mstRoutingStepAllService;
	
	@RequestMapping("/list")
	public R list(@RequestBody(required = false) Map<String,Object> params) {
		List<MstRoutingStepAllEntity> list = mstRoutingStepAllService.list(params,"distinct itm_code",20);
		return R.ok().put("list", list);
	}
}