package com.pstfs.base.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pstfs.base.entity.BreakPointTransEntity;
import com.pstfs.base.entity.UploadFileEntity;
import com.pstfs.base.envm.BreakPointTransRetCode;
import com.pstfs.base.exception.RRException;
import com.pstfs.base.service.BreakPointTransService;
import com.pstfs.base.service.UploadFileService;
import com.pstfs.base.utils.AuthorizationUtils;
import com.pstfs.base.utils.I18nUtils;
import com.pstfs.base.utils.AuthorizationUtils.AuthorizationObj;
import com.pstfs.base.utils.R;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONObject;

/**
 * 后台保存的文件表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-19 10:31:31
 */
@RestController
@RequestMapping("/uploadfile")
public class UploadFileController {

	@Autowired
	private UploadFileService uploadFileService;
	
	@Autowired
	private I18nUtils i;

	private String uploadFilePath;

	public UploadFileController(@Value("${uploadFilePath}") String uploadFilePath) {
		File uploadFilePathObj = new File(uploadFilePath);
		if (!uploadFilePathObj.exists()) {
			uploadFilePathObj.mkdirs();
		}
		this.uploadFilePath = uploadFilePath;
	}
	
	/**
	 * 获取文件大小
	 * @param id
	 * @return
	 */
	@RequestMapping("/getFileSize")
	public R getFileSize(Integer id) {
		UploadFileEntity uploadFile = uploadFileService.getById(id, "path");
		if (uploadFile == null) {
			//文件不存在
			return R.error(HttpStatus.SC_NOT_FOUND, i.t("UploadFileController.getFileSize.fileNotExists"));
		} else {
			String fullPath = uploadFilePath + File.separator + uploadFile.getPath();
			File file = new File(fullPath);
			if (file.exists()) {
				return R.ok().put("fileSize", file.length());
			} else {
				//文件不存在
				return R.error(HttpStatus.SC_NOT_FOUND, i.t("UploadFileController.getFileSize.fileNotExists"));
			}
		}
	}
	
	/**
	 * 分片下载（超过 10MB 一概认为是大文件，需要分片下载）
	 * @param id
	 * @param request
	 * @param response
	 */
	@RequestMapping("/splitDownload")
	public R splitDownload(Integer id,int prevProcess,int length) {
		if (length> 1024*1024) {
			return R.error(i.t("UploadFileController.splitDownload.filesExceeding1M"));//下载长度不能超过1MB！
		}
		UploadFileEntity uploadFile = uploadFileService.getById(id, "path");
		if (uploadFile == null) {
			//文件不存在
			return R.error(HttpStatus.SC_NOT_FOUND, i.t("UploadFileController.getFileSize.fileNotExists"));
		} else {
			String fullPath = uploadFilePath + File.separator + uploadFile.getPath();
			File file = new File(fullPath);
			if (file.exists()) {
				long fileLen;
				if ((fileLen = file.length()) > 1024*1024*1024) {
					//文件过大，请使用客户端下载！
					return R.error(i.t("UploadFileController.splitDownload.filesExceeding1G"));
				}
				if (prevProcess+length>fileLen) {
//					提交的数据异常，保存失败！
					return R.error(i.t("common.commitException"));
				}
				try(RandomAccessFile raf = new RandomAccessFile(file, "rw")){
					raf.seek(prevProcess);
					byte[] data = new byte[length];
					raf.read(data);
					return R.ok().put("d", Base64.encode(data));
				} catch (Exception e) {
					return R.error(e.getMessage());
				}
			} else {
				//文件不存在
				return R.error(HttpStatus.SC_NOT_FOUND, i.t("UploadFileController.getFileSize.fileNotExists"));
			}
		}
	}

	/**
	 * 下载文件
	 * @param id
	 * @param fileName
	 * @param request
	 * @param response
	 */
	@RequestMapping("/download")
	public void download(Integer id,String fileName,HttpServletRequest request, HttpServletResponse response) {
		if(request.getHeader("IF_MODIFIED_SINCE")!=null) {
			response.setStatus(HttpStatus.SC_NOT_MODIFIED);
			return;
		}
		UploadFileEntity uploadFile = uploadFileService.getById(id, "path");
		FileInputStream fis = null;
		try {
			if (uploadFile == null) {
				response.setHeader("Content-Type", "text/json; charset=utf-8");
				PrintWriter pw = response.getWriter();
				//文件不存在
				pw.write(new JSONObject(R.error(HttpStatus.SC_NOT_FOUND, i.t("UploadFileController.getFileSize.fileNotExists"))).toString());
			} else {
				String fullPath = uploadFilePath + File.separator + uploadFile.getPath();
				File file = new File(fullPath);
				if (file.exists()) {
					if(file.length()> 10 * 1024 * 1024) {
			            response.setHeader("Content-Type", "text/json; charset=utf-8");
						PrintWriter pw = response.getWriter();
//						文件较大，暂不支持使用该方式下载
						pw.write(new JSONObject(R.error(HttpStatus.SC_INTERNAL_SERVER_ERROR, i.t("UploadFileController.download.filesExceeding10M"))).toString());
					} else {
						response.setHeader("Pragma", "primary");
						response.setHeader("Cache-Control","max-age=86400");
						String nowFormat = LocalDateTime.now().format(DateTimeFormatter.ofPattern("D, d M y H:m:s"))+" GMT";
						response.setHeader("Last-Modified", nowFormat);
						response.setHeader("Date", nowFormat);
						response.setHeader("Expires", LocalDateTime.MAX.format(DateTimeFormatter.ofPattern("D, d M y H:m:s"))+" GMT");
						response.setHeader("ETag", uploadFile.getPath());
						response.setHeader("Content-Description", "File Transfer");
						response.setHeader("Content-Type",FileUtil.getMimeType(fullPath));
						response.setHeader("Content-Transfer-Encoding","binary");
						response.setHeader("Content-Length", String.valueOf(file.length()));
						response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ContentDisposition
								.attachment()
								.filename(fileName, StandardCharsets.UTF_8)
								.build()
								.toString());
						fis = new FileInputStream(file);
						IoUtil.copy(fis, response.getOutputStream());
					}
				} else {
					response.setHeader("Content-Type", "text/json; charset=utf-8");
					PrintWriter pw = response.getWriter();
					//文件不存在
					pw.write(new JSONObject(R.error(HttpStatus.SC_NOT_FOUND, i.t("UploadFileController.getFileSize.fileNotExists"))).toString());
				}
			}
		} catch (IOException e) {
			throw new RRException(e.getMessage(), e);
		} finally {
			if(fis!=null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 展示图片
	 * @return
	 */
	@RequestMapping("/showImg")
	public void showImg(Integer id,HttpServletRequest request, HttpServletResponse response) {
		if(request.getHeader("IF_MODIFIED_SINCE")!=null) {
			response.setStatus(HttpStatus.SC_NOT_MODIFIED);
			return;
		}
		UploadFileEntity uploadFile = uploadFileService.getById(id, "path");
		FileInputStream fis = null;
		try {
			if (uploadFile == null) {
				PrintWriter pw = response.getWriter();
				//文件不存在
				pw.write(new JSONObject(R.error(HttpStatus.SC_NOT_FOUND, i.t("UploadFileController.getFileSize.fileNotExists"))).toString());
			} else {
				String fullPath = uploadFilePath + File.separator + uploadFile.getPath();
				File file = new File(fullPath);
				if (file.exists()) {
					response.setHeader("Pragma", "primary");
					response.setHeader("Cache-Control","max-age=86400");
					String nowFormat = LocalDateTime.now().format(DateTimeFormatter.ofPattern("D, d M y H:m:s"))+" GMT";
					response.setHeader("Last-Modified", nowFormat);
					response.setHeader("Date", nowFormat);
					response.setHeader("Expires", LocalDateTime.MAX.format(DateTimeFormatter.ofPattern("D, d M y H:m:s"))+" GMT");
					response.setHeader("ETag", uploadFile.getPath());
					response.setHeader("Content-Description", "File Transfer");
					response.setHeader("Content-Type",FileUtil.getMimeType(fullPath));
					response.setHeader("Content-Transfer-Encoding","binary");
					response.setHeader("Content-Length", String.valueOf(file.length()));
					fis = new FileInputStream(file);
					IoUtil.copy(fis, response.getOutputStream());
				} else {
					PrintWriter pw = response.getWriter();
					//文件不存在
					pw.write(new JSONObject(R.error(HttpStatus.SC_NOT_FOUND, i.t("UploadFileController.getFileSize.fileNotExists"))).toString());
				}
			}
		} catch (IOException e) {
			throw new RRException(e.getMessage(), e);
		} finally {
			if(fis!=null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Autowired
	private BreakPointTransService breakPointTransService;
	
	@Autowired
	private AuthorizationUtils authorizationUtils;
	
	/**
	 * 创建断点续传任务
	 * @return
	 */
	@RequestMapping("/breakPointTransStart")
	public R breakPointTransStart(HttpServletRequest request,String sha1,int fileSize) {
		UploadFileEntity uploadFileEntity = uploadFileService.getBySha1(sha1, "id");
		if (uploadFileEntity == null) {
			AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
			//创建断点续传任务，并返回(如果已存在，则直接返回已存在的任务)
			BreakPointTransEntity breakPointTransEntity = breakPointTransService.save(authorizationObj.getUsername(), sha1, fileSize);
			return R.ok().put("breakPointTransId", breakPointTransEntity.getId()).put("breakPointTransProcess", breakPointTransEntity.getProcess());
		} else {
			//如果文件存在，则无需上传直接使用该文件
			return R.ok().put("uploadFileId", uploadFileEntity.getId());
		}
	}
	
	/**
	 * 断点续传
	 * @param breakPointTransId
	 * @param prevProcess
	 * @param base64Data
	 * @return
	 */
	@RequestMapping("/breakPointTrans")
	public R breakPointTrans(HttpServletRequest request, int breakPointTransId, int prevProcess, String base64Data) {
		boolean[] finish = new boolean[1];
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		int process = breakPointTransService.update(authorizationObj.getUsername(), breakPointTransId, prevProcess, base64Data,finish);
		if(process >-1) {
			if(finish[0]) {
				int uploadFileId = breakPointTransService.moveUploadFinishFile(authorizationObj.getUsername(), breakPointTransId);
				return R.ok().put("uploadFileId", uploadFileId);
			} else {
				return R.ok().put("process", process);
			}
		} else if(process == BreakPointTransRetCode.文件上传进度异常.getV()){
//			文件上传进度异常，上传失败！
			return R.error(i.t("UploadFileController.breakPointTrans.processException")).put("retCode", process);
		} else if(process == BreakPointTransRetCode.禁止同时执行两个上传相同文件的上传任务.getV()) {
//			禁止同时执行两个上传相同文件的上传任务！
			return R.error(i.t("UploadFileController.breakPointTrans.sameUploadTask")).put("retCode", process);
		} else {
//			未知异常，请联系管理员
			return R.error(i.t("common.otherException"));
		}
	}
	
	/**
	 * 中断断点续传
	 * @param breakPointTransId
	 * @return
	 */
	public R cancelUpload(HttpServletRequest request,int breakPointTransId) {
		AuthorizationObj authorizationObj = authorizationUtils.getAuthorizationObj(request);
		if(breakPointTransService.cancelUpload(authorizationObj.getUsername(), breakPointTransId)) {
			return R.ok();
		} else {
			return R.error();
		}
	}
	
	/**
	 * 上传文件并返回文件id
	 * 
	 * @param file
	 * @return
	 */
	@RequestMapping("/upload")
	public R upload(@RequestParam("file") MultipartFile file) {
		String sha1 = null;
		try {
			sha1 = SecureUtil.sha1(file.getResource().getInputStream());
		} catch (IOException e) {
			throw new RRException(e.getMessage(), e);
		}
		UploadFileEntity uploadFile = uploadFileService.getBySha1(sha1, "id");
		Integer uploadFileId = uploadFile == null ? null : uploadFile.getId();
		if (uploadFileId == null) {
			String targetPath = uploadFilePath + File.separator + sha1.substring(0, 2) + File.separator
					+ sha1.substring(2, 4);
			File targetPathObj = new File(targetPath);
			if (!targetPathObj.exists()) {
				targetPathObj.mkdirs();
			}
			try {
				file.transferTo(new File(targetPath + File.separator + sha1.substring(4)));
			} catch (Exception e) {
				throw new RRException(e.getMessage(), e);
			}
			uploadFileId = uploadFileService.save(sha1);
		}
		return R.ok().put("uploadFileId", uploadFileId);
	}

	/**
	 * 获取上传文件id
	 * 
	 * @param sha1
	 * @return
	 */
	@RequestMapping("/getUploadFileId")
	public R getUploadFileId(String sha1) {
		UploadFileEntity uploadFile = uploadFileService.getBySha1(sha1, "id");
		Integer uploadFileId = uploadFile == null ? null : uploadFile.getId();
		return R.ok().put("uploadFileId", uploadFileId);
	}
}