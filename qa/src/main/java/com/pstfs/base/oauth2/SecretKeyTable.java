package com.pstfs.base.oauth2;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicBoolean;

import org.springframework.stereotype.Component;

/**
 * 密钥表，根据用户名生成 jwt 密钥 
 * @author jiajian
 */
@Component
public class SecretKeyTable {

	/**
	 * 密钥表管理
	 * @author jiajian
	 */
	@Component
	public static class SecretKeyTableMgr {
		/**
		 * 今天的密钥
		 */
		public SecretKeyTable secretKeyTable;
		/**
		 * 昨天的密钥
		 */
		public SecretKeyTable yestodaySecretKeyTable;
		/**
		 * 最近使用时间
		 */
		private LocalDateTime lastUsedDate;
		
		public SecretKeyTableMgr() {
			secretKeyTable = new SecretKeyTable();
			lastUsedDate = LocalDateTime.now();
			atomicBoolean = new AtomicBoolean(false);
		}
		
		/**
		 * 原子类
		 */
		private AtomicBoolean atomicBoolean;
		
		public SecretKeyTable getSecretKeyTable() {
			LocalDateTime now = LocalDateTime.now();
			if (now.getDayOfYear() > lastUsedDate.getDayOfYear() || now.getYear()>lastUsedDate.getYear()) {
				while(atomicBoolean.compareAndSet(false, true) == false);
				if (now.getDayOfYear() > lastUsedDate.getDayOfYear() || now.getYear()>lastUsedDate.getYear()) {
					yestodaySecretKeyTable = secretKeyTable;
					secretKeyTable = new SecretKeyTable();
				}
				atomicBoolean.set(false);
			}
			lastUsedDate = now;			
			return secretKeyTable;
		}

		public SecretKeyTable getYestodaySecretKeyTable() {
			return yestodaySecretKeyTable;
		}
	}
	
	/**
	 * 随机字节串
	 */
	private byte[] randomBytes;
	/**
	 * 偏移大小
	 */
	private byte[] strShifting;

	public SecretKeyTable() {
		//设置随机字节，这些字节构成密钥
		randomBytes = new byte[255];
		for(int i=0,len_i= randomBytes.length;i<len_i;i++) {
			randomBytes[i] = (byte)(Math.random() * 256 - 128);
		}
		//设置映射偏移，读取用户名后将得到一个偏移值，通过这个偏移值加到总偏移量，然后总偏移量取余获取随机字节
		int RANDOM_STRING_LEN = 64; //26个大小写字母，下划线，10个数字对应的偏移值，以及初始偏移值，共64位
		strShifting = new byte[RANDOM_STRING_LEN];
		for (int i = 0; i < RANDOM_STRING_LEN ; i++) {
			strShifting[i] = (byte)(Math.random()*randomBytes.length);//偏移量应该保证能触及 randomBytes 的每一个元素
		}
	}

	/**
	 * 密钥长度
	 */
	private static final int SECRET_KEY_LEN = 16;
	/**
	 * 根据用户名生成密钥，根据用户名的字符获取一个偏移量，然后使用该偏移量获取 randomBytes 中
	 * 对应的字节值，获取 SECRET_KEY_LEN 个字节值作为密钥。
	 * @param username
	 * @return
	 */
	public byte[] getSecretKey(char[] username) {
		final int usernameLen = username.length;
		byte initShifting = strShifting[0];
		byte[] ret = new byte[SECRET_KEY_LEN];
		//生产长度为 SECRET_KEY_LEN 字节的密钥
		for(int i=0;i<SECRET_KEY_LEN;i++) {
			//根据用户名的字符获取偏移值，把偏移值加到总偏移量中
			char c = username[i % usernameLen];
			if(c>='A' && c<='Z') {
				initShifting += strShifting[c + 1 - 'A'];
			} else if(c>='a' && c<='z') {
				initShifting += strShifting[c + 1 + ('Z'-'A' + 1) - 'a'];
			} else if(c>='0' && c<='9') {
				initShifting += strShifting[c + 1 + ('Z'-'A' + 1)+('z'-'a'+1) - '0'];
			} else if(c == '_') {
				initShifting += strShifting[63];
			}
			//转化为无符号的数字
			short tempShifting = (short) (initShifting & 0xff);
			//偏移量取余得到随机字节的下标，利用该下标获取随机字节
			ret[i] = randomBytes[tempShifting % randomBytes.length];
		}
		return ret;
	}
}