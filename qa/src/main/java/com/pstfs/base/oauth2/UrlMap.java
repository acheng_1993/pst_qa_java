package com.pstfs.base.oauth2;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

import org.springframework.stereotype.Component;

import com.pstfs.base.entity.RequestUrlEntity;

/**
 * url映射
 * @author jiajian
 */
@Component
public class UrlMap {

	/**
	 * url和下标的映射
	 */
	private static Map<String,RequestUrlEntity> URL_MAP = new HashMap<>();
	
	/**
	 * 获取下标
	 * @param url
	 * @return
	 */
	public Integer get(String url) {
		RequestUrlEntity requestUrlEntity = URL_MAP.get(url);
		return requestUrlEntity == null?null: requestUrlEntity.getUrlIndex();
	}

	/**
	 * 自旋锁
	 */
	private final static AtomicBoolean LOCK = new AtomicBoolean(Boolean.FALSE);
	
	public void put(Integer id,String url, Integer index) {
		RequestUrlEntity entity = new RequestUrlEntity();
		entity.setId(id);
		entity.setUrl(url);
		entity.setUrlIndex(index);
		for(;;) {
			if(LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				Map<String,RequestUrlEntity> temp = new HashMap<>();
				temp.put(url, entity);
				temp.putAll(URL_MAP);
				URL_MAP = temp;
				LOCK.set(Boolean.FALSE);
			}
		}
	}
	
	/**
	 * 设置url
	 * @param requestUrlEntityList
	 * @return
	 */
	public Integer putAll(List<RequestUrlEntity> requestUrlEntityList) {
		int ret = -1;
		for(;;) {
			if(LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				Map<String,RequestUrlEntity> temp = new HashMap<>();
				for (int i=0,len_i=requestUrlEntityList.size();i<len_i;i++) {
					RequestUrlEntity requestUrlEntity = requestUrlEntityList.get(i);
					temp.put(requestUrlEntity.getUrl(), requestUrlEntity);
				}
				temp.putAll(URL_MAP);
				URL_MAP = temp;
				LOCK.set(Boolean.FALSE);
				return ret;
			}
		}
	}
	
	/**
	 * 获取url列表
	 * @return
	 */
	public List<RequestUrlEntity> getAll() {
		for(;;) {
			if(LOCK.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {
				List<RequestUrlEntity> ret = new ArrayList<>();
				for(Entry<String, RequestUrlEntity> entry:URL_MAP.entrySet()) {
					RequestUrlEntity val = entry.getValue();
					RequestUrlEntity requestUrlEntity = new RequestUrlEntity();
					requestUrlEntity.setId(val.getId());
					requestUrlEntity.setUrl(entry.getKey());
					requestUrlEntity.setUrlIndex(val.getUrlIndex());
					ret.add(requestUrlEntity);
				}
				LOCK.set(Boolean.FALSE);
				return ret;
			}
		}
	}	
}