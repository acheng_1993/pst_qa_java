package com.pstfs.base.extend;
import java.util.Date;

import com.pstfs.base.entity.InboxMessageEntity;

import lombok.Data;

/**
 * 站内信接收用户表扩展
 * @author jiajian
 *
 */
@Data
public class InboxMessageExtend extends InboxMessageEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 部分内容，通常取前20个文字
	 */
	private String simpleContent;
	/**
	 * 创建日期
	 */
	private Date createTime;
	/**
	 * 创建用户
	 */
	private String creatorUsername;
	/**
	 * 消息类型
	 */
	private Integer messageType;
}
