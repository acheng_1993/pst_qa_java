package com.pstfs.base.extend;
import com.pstfs.base.entity.MenuEntity;
import lombok.Data;

@Data
public class MenuExtend extends MenuEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 角色id
	 */
	private Integer roleId;
}