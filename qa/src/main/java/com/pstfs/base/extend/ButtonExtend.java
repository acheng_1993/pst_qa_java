package com.pstfs.base.extend;
import com.pstfs.base.entity.ButtonEntity;
import lombok.Data;

@Data
public class ButtonExtend extends ButtonEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 菜单描述
	 */
	private String menuDesc;
	
	/**
	 * 角色id
	 */
	private Integer roleId;
}