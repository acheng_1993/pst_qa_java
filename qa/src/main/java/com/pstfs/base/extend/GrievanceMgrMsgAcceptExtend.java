package com.pstfs.base.extend;
import com.pstfs.base.entity.GrievanceMgrMsgAcceptEntity;
import lombok.Data;

/**
 * 客户投诉管理消息接收者扩展类
 * @author jiajian
 */
@Data
public class GrievanceMgrMsgAcceptExtend extends GrievanceMgrMsgAcceptEntity{

	/**
	 * 管理No.
	 */
	private String mgmNo;
	
	/**
	 * 邮箱对应的用户名
	 */
	private String username;
}
