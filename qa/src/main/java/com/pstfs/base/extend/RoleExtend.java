package com.pstfs.base.extend;
import com.pstfs.base.entity.RoleEntity;
import lombok.Data;

@Data
public class RoleExtend extends RoleEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 用户id
	 */
	private Integer userId;
}
