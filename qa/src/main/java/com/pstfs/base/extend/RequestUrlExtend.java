package com.pstfs.base.extend;
import com.pstfs.base.entity.RequestUrlEntity;
import lombok.Data;

@Data
public class RequestUrlExtend extends RequestUrlEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 按钮id
	 */
	private Integer buttonId;
	/**
	 * 菜单id
	 */
	private Integer menuId;
}