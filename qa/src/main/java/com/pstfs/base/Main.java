package com.pstfs.base;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;

@EnableScheduling
@SpringBootApplication
@MapperScan("com.pstfs.base.mapper")
@Slf4j
public class Main {
	public static final List<Lock> LOCK_LIST = new Vector<Lock>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(Main.class, args);
		Runtime run = Runtime.getRuntime();
        run.addShutdownHook(new Thread() {
            @Override
            public void run() {
            	for (Lock lock : LOCK_LIST) {
            		lock.lock();
            	}
            	log.info("程序结束调用...");
            }
        });
	}
}