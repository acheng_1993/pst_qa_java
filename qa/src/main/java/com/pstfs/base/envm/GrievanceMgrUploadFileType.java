package com.pstfs.base.envm;

/**
 * 客户投诉管理的上传附件类型
 * @author jiajian
 */
public enum GrievanceMgrUploadFileType {
	解析依頼書(0),
	部品解析結果(1),
	不具合写真(2),
	品質異常書(3),
	最終回答書(4);

	private int v;
	private GrievanceMgrUploadFileType(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}