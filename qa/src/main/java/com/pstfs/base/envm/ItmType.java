package com.pstfs.base.envm;

/**
 * mes 系统的物品类型枚举
 * @return
 */
public enum ItmType {
	原料(0),辅料(1),中间部品(2),成品(3);
	private int v;
	private ItmType(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}
