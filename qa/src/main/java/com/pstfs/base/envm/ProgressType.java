package com.pstfs.base.envm;
/**
 * 统一维护所有的后台服务进度的枚举
 * @author jiajian
 */
public enum ProgressType {
	自动删除未引用文件(0),自动删除断点续传文件(1);
	private int v;
	private ProgressType(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}
