package com.pstfs.base.envm;

/**
 * 移动方式枚举
 * @author jiajian
 */
public enum MoveType {
	前面(-1),里面(0),后面(1);
	private int v;
	private MoveType(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}