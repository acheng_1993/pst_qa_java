package com.pstfs.base.envm;

/**
 * 站内信是否已经发送到邮箱
 * @return
 */
public enum SendedEmail {
	否(0),是(1),不需要发送(2),暂无用户邮箱信息无法发送(3),邮箱地址有误发送失败(4);
	private int v;
	private SendedEmail(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}