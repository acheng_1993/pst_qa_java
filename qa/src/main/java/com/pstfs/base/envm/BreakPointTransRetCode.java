package com.pstfs.base.envm;

/**
 * 断点续传的返回结果编码
 * @author jiajian
 */
public enum BreakPointTransRetCode {
	文件上传进度异常(-1),
	禁止同时执行两个上传相同文件的上传任务(-2);

	private int v;
	private BreakPointTransRetCode(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}
