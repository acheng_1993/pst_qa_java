package com.pstfs.base.envm;
/**
 * 菜单类型枚举
 * @author jiajian
 */
public enum MenuType {
	目录(0),菜单(1),表单(2);
	private int v;
	private MenuType(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}
