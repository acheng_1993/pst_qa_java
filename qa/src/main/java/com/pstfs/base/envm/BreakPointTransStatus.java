package com.pstfs.base.envm;
/**
 * 断点续传状态枚举
 * @author jiajian
 */
public enum BreakPointTransStatus {
	处理中(0),处理完成(1),中断(2);
	private int v;
	private BreakPointTransStatus(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}
