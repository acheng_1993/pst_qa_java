package com.pstfs.base.envm;

/**
 * 站内信的消息已读状态
 * @author jiajian
 */
public enum MessageReaded {
	未读(0),已读(1);
	private int v;
	private MessageReaded(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}
