package com.pstfs.base.envm;

/**
 * 启用、禁用状态
 * @author jiajian
 */
public enum Status {
	启用(1),禁用(0);
	private int v;
	private Status(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}