package com.pstfs.base.envm;

/**
 * 通用的是否枚举，多数情况下是用数字1表示，否用数字0表示。
 * @author jiajian
 */
public enum YesOrNo {
	是(1),否(0);
	private int v;
	private YesOrNo(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}
