package com.pstfs.base.envm;

/**
 * 站内信的消息类型
 * @author jiajian
 */
public enum MessageType {
	文本(0),html(1);
	private int v;
	private MessageType(int v) {
		this.v = v;
	}
	public int getV() {
		return v;
	}
}
