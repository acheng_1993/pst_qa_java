package com.pstfs.scheduling;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.ApproveflowProcessinstanceEntity;
import com.pstfs.entity.FormForminstanceEntity;
import com.pstfs.entity.FormForminstanceProcessEntity;
import com.pstfs.entity.PaymentSummonsDataEntity;
import com.pstfs.entity.SysUserEntity;
import com.pstfs.service.ApproveflowProcessinstanceService;
import com.pstfs.service.FormForminstanceProcessService;
import com.pstfs.service.FormForminstanceService;
import com.pstfs.service.PaymentSummonsDataService;
import com.pstfs.service.SysUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * 自动爬取支付传票信息
 * @author chenlong
 */
@Component
@Slf4j
public class AutoCrawlingPaymentSummons extends MessageListenerAdapter{

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	public AutoCrawlingPaymentSummons() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Autowired
	private FormForminstanceService formForminstanceService;
	
	@Autowired
	private ApproveflowProcessinstanceService approveflowProcessinstanceService; 
	
	@Autowired
	private FormForminstanceProcessService formForminstanceProcessService;
	
	@Autowired
	private PaymentSummonsDataService paymentSummonsDataService;
	
	@Autowired
	private SysUserService sysUserService;
	
	/**
	 * 日期格式1
	 */
	private static final DateTimeFormatter FORMATTER1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	
	/**
	 * 每隔5分钟同步一次
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if(trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					long beforeMillis = System.currentTimeMillis();
					for(;;) {
						FormForminstanceProcessEntity formForminstanceProcessEntity = formForminstanceProcessService.getOne(null);
						List<FormForminstanceEntity> formForminstanceEntityList = null;
						if (formForminstanceProcessEntity == null) {
							formForminstanceEntityList = formForminstanceService.getNextList();
						} else {
							formForminstanceEntityList = formForminstanceService.getNextList(formForminstanceProcessEntity.getFormForminstanceId());
						}
						if(System.currentTimeMillis()-beforeMillis>60000 || formForminstanceEntityList.isEmpty()) {
							break;
						}
						List<Long> formForminstanceIdList = new ArrayList<>();
						List<Long> userIdList = new ArrayList<>();
						for (int i=0,len_i=formForminstanceEntityList.size();i<len_i;i++) {
							FormForminstanceEntity formForminstanceEntity = formForminstanceEntityList.get(i);
							formForminstanceIdList.add(formForminstanceEntity.getId());
							userIdList.add(formForminstanceEntity.getCreaterid());
						}
						Map<Long,String> idUsernameMap = sysUserService.getUsernameByIdList(userIdList);
						List<ApproveflowProcessinstanceEntity> approveflowProcessinstanceEntityList = approveflowProcessinstanceService
								.getListByFormIdList(formForminstanceIdList,"formId,status");
						Map<Long,Integer> approveflowProcessinstanceEntityMap = new HashMap<>();
						for(int i=0,len_i = approveflowProcessinstanceEntityList.size();i<len_i;i++) {
							ApproveflowProcessinstanceEntity tempEntity = approveflowProcessinstanceEntityList.get(i);
							approveflowProcessinstanceEntityMap.put(tempEntity.getFormid(), tempEntity.getStatus());
						}
						List<PaymentSummonsDataEntity> paymentSummonsDataEntityList = new ArrayList<>();
						for (int i=0,len_i=formForminstanceEntityList.size();i<len_i;i++) {
							FormForminstanceEntity formForminstanceEntity = formForminstanceEntityList.get(i);
							JSONObject jsonObject = JSONObject.parseObject(formForminstanceEntity.getFormdatajson());
							JSONArray jsonArray = jsonObject.getJSONArray("table");
							Date applyDate = null;
							if(StringUtils.isNotEmpty(jsonObject.getString("applyDate"))) {
								applyDate = Date.from(LocalDate.parse(jsonObject.getString("applyDate"), FORMATTER1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
							}
							Date docDate = Date.from(LocalDate.parse(jsonObject.getString("docDate"), FORMATTER1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
							for(int j=0,len_j=jsonArray.size();j<len_j;j++) {
								JSONObject tableRow = jsonArray.getJSONObject(j);
								String accountSubject = tableRow.getString("accountSubject");
								if(StringUtils.isNotEmpty(accountSubject)) {
									PaymentSummonsDataEntity paymentSummonsDataEntity = new PaymentSummonsDataEntity();
									paymentSummonsDataEntity.setAccountSubject(accountSubject);
									paymentSummonsDataEntity.setApplyDate(applyDate);
									paymentSummonsDataEntity.setDocDate(docDate);
									paymentSummonsDataEntity.setCostCenter(tableRow.getString("costCenter"));
									paymentSummonsDataEntity.setCreaterName(formForminstanceEntity.getCreatername());
									paymentSummonsDataEntity.setDocNum(jsonObject.getString("docNum"));
									paymentSummonsDataEntity.setFormForminstanceId(formForminstanceEntity.getId());
									paymentSummonsDataEntity.setMoney(new BigDecimal(tableRow.getDouble("money")));
									paymentSummonsDataEntity.setDetailIndex(tableRow.getInteger("index"));
									paymentSummonsDataEntity.setStatus(approveflowProcessinstanceEntityMap.get(formForminstanceEntity.getId()));
									paymentSummonsDataEntity.setMemo(tableRow.getString("memo"));
									paymentSummonsDataEntity.setCreaterId(idUsernameMap.get(formForminstanceEntity.getCreaterid()));
									paymentSummonsDataEntityList.add(paymentSummonsDataEntity);
								}
							}
						}
						paymentSummonsDataService.saveBatch(paymentSummonsDataEntityList);
					}
					LocalDateTime now = LocalDateTime.now();
					int hour = now.getHour();
					//查看未审批完的支付传票，检查是否审批完，在上线版只能在 19~20 点检查
					if("dev".equals(springProfilesActive) || hour>= 19 && hour<=20) {
						QueryWrapper<PaymentSummonsDataEntity> queryWrapper = new QueryWrapper<>();
						queryWrapper.select("id,detail_index,form_forminstance_id");
						queryWrapper.last(" where form_forminstance_id in(select form_forminstance_id from payment_summons_data where status=1 or status is null)");
						List<PaymentSummonsDataEntity> paymentSummonsDataEntityList = paymentSummonsDataService.list(queryWrapper);
						Map<Long,Map<Integer,PaymentSummonsDataEntity>> paymentSummonsDataEntityMap = new HashMap<>();
						List<Long> formForminstanceIdList = new ArrayList<>();
						for (int i=0,len_i=paymentSummonsDataEntityList.size();i<len_i;i++) {
							PaymentSummonsDataEntity paymentSummonsDataEntity = paymentSummonsDataEntityList.get(i);
							formForminstanceIdList.add(paymentSummonsDataEntity.getFormForminstanceId());
							if (paymentSummonsDataEntityMap.containsKey(paymentSummonsDataEntity.getFormForminstanceId())) {
								Map<Integer,PaymentSummonsDataEntity> paymentSummonsDataEntityMap2 = paymentSummonsDataEntityMap.get(paymentSummonsDataEntity.getFormForminstanceId());
								paymentSummonsDataEntityMap2.put(paymentSummonsDataEntity.getDetailIndex(), paymentSummonsDataEntity);
							} else {
								Map<Integer,PaymentSummonsDataEntity> tempMap = new HashMap<>();
								tempMap.put(paymentSummonsDataEntity.getDetailIndex(), paymentSummonsDataEntity);
								paymentSummonsDataEntityMap.put(paymentSummonsDataEntity.getFormForminstanceId(), tempMap);
							}
						}
						//把OA中已经删除的数据删除
						List<Long> isDeletedIdList = formForminstanceService.getIsDeleted(formForminstanceIdList);
						if(!isDeletedIdList.isEmpty()) {
							UpdateWrapper<PaymentSummonsDataEntity> updateWrapper = new UpdateWrapper<>();
							updateWrapper.in("form_forminstance_id", isDeletedIdList);
							paymentSummonsDataService.remove(updateWrapper);
							formForminstanceIdList.removeAll(isDeletedIdList);
						}
						List<ApproveflowProcessinstanceEntity> approveflowProcessinstanceEntityList = approveflowProcessinstanceService.getListByFormIdList(formForminstanceIdList,"createrId,createrName,formId,status,formJson");
						List<PaymentSummonsDataEntity> updateList = new ArrayList<>();
						List<PaymentSummonsDataEntity> saveList = new ArrayList<>();
						for(int i=0,len_i = approveflowProcessinstanceEntityList.size();i<len_i;i++) {
							ApproveflowProcessinstanceEntity tempEntity = approveflowProcessinstanceEntityList.get(i);
							if (tempEntity.getStatus()!=1) {
								JSONObject jsonObject = JSONObject.parseObject(tempEntity.getFormjson());
								JSONArray table = jsonObject.getJSONArray("table");
								Date applyDate = Date.from(LocalDate.parse(jsonObject.getString("applyDate"), FORMATTER1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
								Date docDate = Date.from(LocalDate.parse(jsonObject.getString("docDate"), FORMATTER1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
								String docNum = jsonObject.getString("docNum");
								for(int j=0,len_j=table.size();j<len_j;j++) {
									JSONObject tableRow = table.getJSONObject(j);
									String accountSubject = tableRow.getString("accountSubject");
									if (StringUtils.isNotEmpty(accountSubject)) {
										Map<Integer,PaymentSummonsDataEntity> paymentSummonsDataEntityMap2 = paymentSummonsDataEntityMap.get(tempEntity.getFormid());
										Integer index = tableRow.getInteger("index");
										PaymentSummonsDataEntity paymentSummonsDataEntity = paymentSummonsDataEntityMap2.get(index);
										if(paymentSummonsDataEntity == null) {
											SysUserEntity sysUserEntity = sysUserService.getById(tempEntity.getCreaterid());
											paymentSummonsDataEntity = new PaymentSummonsDataEntity();
											paymentSummonsDataEntity.setAccountSubject(accountSubject);
											paymentSummonsDataEntity.setApplyDate(applyDate);
											paymentSummonsDataEntity.setDocDate(docDate);
											paymentSummonsDataEntity.setCostCenter(tableRow.getString("costCenter"));
											paymentSummonsDataEntity.setCreaterName(tempEntity.getCreatername());//
											paymentSummonsDataEntity.setDocNum(jsonObject.getString("docNum"));
											paymentSummonsDataEntity.setFormForminstanceId(tempEntity.getFormid());//
											paymentSummonsDataEntity.setMoney(new BigDecimal(tableRow.getDouble("money")));
											paymentSummonsDataEntity.setDetailIndex(tableRow.getInteger("index"));
											paymentSummonsDataEntity.setStatus(tempEntity.getStatus());//
											paymentSummonsDataEntity.setMemo(tableRow.getString("memo"));
											paymentSummonsDataEntity.setCreaterId(sysUserEntity.getUsername());//
											saveList.add(paymentSummonsDataEntity);
										} else {
											paymentSummonsDataEntity.setAccountSubject(accountSubject);
											paymentSummonsDataEntity.setApplyDate(applyDate);
											paymentSummonsDataEntity.setDocDate(docDate);
											paymentSummonsDataEntity.setCostCenter(tableRow.getString("costCenter"));
											paymentSummonsDataEntity.setDocNum(docNum);
											paymentSummonsDataEntity.setStatus(tempEntity.getStatus());
											paymentSummonsDataEntity.setMoney(new BigDecimal(tableRow.getDouble("money")));
											paymentSummonsDataEntity.setMemo(tableRow.getString("memo"));
											updateList.add(paymentSummonsDataEntity);
										}
									}
								}
							}
						}
						if(!updateList.isEmpty()) {
							paymentSummonsDataService.updateBatch(updateList);
						}
						if(!saveList.isEmpty()) {
							paymentSummonsDataService.saveBatch(saveList);
						}
					}
				} catch (Exception e) {
					log.error("迁移OA数据出错，错误原因：", e);
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
}
