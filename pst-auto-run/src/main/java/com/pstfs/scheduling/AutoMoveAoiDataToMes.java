package com.pstfs.scheduling;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.MfgMoInstructionPrEntity;
import com.pstfs.entity.ProdLog;
import com.pstfs.service.MfgMoInstructionPrService;
import com.pstfs.service.ProdLogService;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 自动地把AOI的数据搬运到MES系统
 * 
 * @author jiajian
 */
@Component
@Slf4j
public class AutoMoveAoiDataToMes extends MessageListenerAdapter {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public AutoMoveAoiDataToMes() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}

	private String plRegex = "^M\\d{11}$";

	/**
	 * 用户名和Mo单号的集合操作时的锁
	 */
	private AtomicBoolean usernameAndPlAndStartTimeLock = new AtomicBoolean(Boolean.FALSE);
	
	/**
	 * 用户名和Mo单号、开启时间的集合
	 */
	private Set<String> usernameAndPlAndStartTimeHashSet = new HashSet<>();

	/**
	 * 使用该方法监听，当key失效时执行该方法
	 * 
	 * @param message
	 * @param pattern
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		String key = new String(message.getChannel());
		if ("AutoScanMoQrcode".equals(key)) {
			String usernameAndPlAndStartTime = redisTemplate.opsForValue().get("AutoScanMoQrcodeData");
			for(;;) {
				if(Boolean.TRUE == usernameAndPlAndStartTimeLock.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {					
					usernameAndPlAndStartTimeHashSet.add(usernameAndPlAndStartTime);
					usernameAndPlAndStartTimeLock.set(Boolean.FALSE);
					break;
				}
			}
//			task1();
			task2();
		}
	}

	@Value("${spring.profiles.active}")
	private String springProfilesActive;

//	@Autowired
//	private Mentrydb1Service mentrydb1Service;

//	@Autowired
//	private Mentrydb2111Service mentrydb2111Service;

	@Autowired
	private MfgMoInstructionPrService mfgMoInstructionPrService;
	
	@Autowired
	private ProdLogService prodLogService;

	private SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	/**
	 * 每隔5分钟，把aoi的数据搬运到新mes系统
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
//	@Scheduled(cron = "*/59 * * * * ?")
	public void task2() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task2()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				String usernameAndPlAndStartTime;
				try {
					usernameAndPlAndStartTime = redisTemplate.opsForValue().get("AutoScanMoQrcodeData");
				} catch (Exception e) {
					usernameAndPlAndStartTime = null;
					log.error(e.getMessage(),e);
				}
				for(;;) {
					if(Boolean.TRUE == usernameAndPlAndStartTimeLock.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {					
						usernameAndPlAndStartTimeHashSet.add(usernameAndPlAndStartTime);
						usernameAndPlAndStartTimeLock.set(Boolean.FALSE);
						break;
					}
				}
				long lastExecuteTime = System.currentTimeMillis();
				for (Iterator<String> it = usernameAndPlAndStartTimeHashSet.iterator(); it.hasNext();) {
					usernameAndPlAndStartTime = it.next();
					// 当前被处理的MO单号，如果为空则不处理
					String pl = "";
					//要处理的单的起始日期
					long startDateTimestamp = System.currentTimeMillis() - 15 * 86400 * 1000;
					// 当前被处理的MO单号绑定的用户
					if (usernameAndPlAndStartTime != null) {
						String[] usernameAndPlAndStartTimeArray = usernameAndPlAndStartTime.split("&");
						if (usernameAndPlAndStartTimeArray.length>2) {
							pl = usernameAndPlAndStartTimeArray[1];
							try {
								startDateTimestamp = Long.parseLong(usernameAndPlAndStartTimeArray[2]);
							} catch (Exception e) {
								log.error(e.getMessage(), e);
							}
						} else if(usernameAndPlAndStartTimeArray.length>1) {
							pl = usernameAndPlAndStartTimeArray[1];
						} else {
							pl = "";
						}
					} else {
						pl = "";
					}
					if (pl != null && !"".equals(pl) && pl.matches(plRegex)) {
						log.info("使用MO单号：" + pl + "，从MES系统查找相应的数据..");
						QueryWrapper<MfgMoInstructionPrEntity> queryWrapper1 = new QueryWrapper<>();
						queryWrapper1 = queryWrapper1.eq("OPERATION_WO_NO", pl);
						queryWrapper1 = queryWrapper1.last("limit 1");
						queryWrapper1 = queryWrapper1.select("MODEL_NO");
						
						QueryWrapper<ProdLog> queryWrapper2 = new QueryWrapper<>();
						queryWrapper2 = queryWrapper2.eq("into_new_mes", 0);
						queryWrapper2 = queryWrapper2.eq("test_status", "P");
						String date = sdf1.format(new Date(startDateTimestamp));
						queryWrapper2 = queryWrapper2.ge("start_date", date);
						Page<ProdLog> page = new Page<>(1, 100);
						try {
							MfgMoInstructionPrEntity mfgMoInstructionPrEntity = mfgMoInstructionPrService.getOne(queryWrapper1);
							if (mfgMoInstructionPrEntity != null && mfgMoInstructionPrEntity.getModelNo() != null && !"".equals(mfgMoInstructionPrEntity.getModelNo())) {
								while(true) {
									if (System.currentTimeMillis() - lastExecuteTime > 60000) {
										log.info("运行时长超过一分钟，马上退出...");
										break;
									}
									queryWrapper2 = queryWrapper2.like("srff_file", mfgMoInstructionPrEntity.getModelNo() + "%");
									IPage<ProdLog> prodLogPage = prodLogService.page(page, queryWrapper2);
									List<ProdLog> prodLogList = prodLogPage.getRecords();
									if (prodLogList.size() > 0) {
										log.info("发现未迁移的数据有" + prodLogList.size() + "条...");
										int successCount = 0;
										for (ProdLog prodLog : prodLogList) {
											JSONObject json = new JSONObject();
											json.set("operationWoNo", pl);
											json.set("lotSno",prodLog.getPanelId());
											json.set("operationCode", "M-AOI");
											json.set("userAcc", "AOIAuto");
											json.set("mcCode", "3D-AOI-1");
											log.info("准备把数据更新到新MES系统："+json.toString());
											String ret = null;
											if ("dev".equals(springProfilesActive)) {
												ret = HttpUtil.post("http://10.199.32.97:8080/api/mfgMoLotSnAll/dmcUpdate", json.toString());
											} else if("pro".equals(springProfilesActive)) {
												ret = HttpUtil.post("http://10.198.43.249:8080/api/mfgMoLotSnAll/dmcUpdate", json.toString());
											}
											JSONObject jsonObject = JSONUtil.parseObj(ret);
											int status = jsonObject.getInt("status");
											if(status == 200 || status == 0) {
												jsonObject = jsonObject.getJSONObject("data");
												status = jsonObject.getInt("status");
												if(status == 200 || status == 0 || status == 1000) {
													log.info("把数据更新到新MES系统成功，返回结果："+ret);
													ProdLog prodLogUpdate = new ProdLog();
													prodLogUpdate.setId(prodLog.getId());
													prodLogUpdate.setIntoNewMes(1);
													prodLogUpdate.setMo(pl);
													prodLogService.updateById(prodLogUpdate);
													successCount++;
												} else if(status == 996 || status == 997) {
													log.info("把数据更新到新MES系统失败，返回结果："+ret);
													ProdLog prodLogUpdate = new ProdLog();
													prodLogUpdate.setId(prodLog.getId());
													prodLogUpdate.setIntoNewMes(2);
													prodLogUpdate.setNotIntoNewMesReason(jsonObject.getStr("message"));
													prodLogUpdate.setMo(pl);
													prodLogService.updateById(prodLogUpdate);
												} else {
													log.info("把数据更新到新MES系统失败，返回结果："+ret);
												}
											} else {
												log.info("把数据更新到新MES系统失败，返回结果："+ret);
											}
										}
										log.info("成功地把" + successCount + "条数据从AOI迁移到MES系统...");
										log.info("把已经迁移的AOI数据标记为“已迁移”状态...");
									} else {
										log.info("未能找到MO单号匹配的数据...");
										break;
									}
								}
							}
						} catch (Exception e) {
							log.error("aoi数据搬运过程中出错，错误原因：", e);
						}
					}
					for(;;) {
						if(Boolean.TRUE == usernameAndPlAndStartTimeLock.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {					
							usernameAndPlAndStartTimeHashSet.remove(usernameAndPlAndStartTime);
							usernameAndPlAndStartTimeLock.set(Boolean.FALSE);
							break;
						}
					}
				}
			}
			log.info("task2()：任务结束...");
			task1Lock.unlock();
		}
	}
	
//	/**
//	 * 每隔5分钟，把aoi的数据搬运到mes系统
//	 */
//	@Scheduled(cron = "0 0/5 * * * ?")
////	@Scheduled(cron = "*/59 * * * * ?")
//	public void task1() {
//		boolean trySuccess = task1Lock.tryLock();
//		if (trySuccess) {
//			log.info("task1()：开始执行任务...");
//			if("pro".equals(springProfilesActive)) {
//				String usernameAndPl;
//				try {
//					usernameAndPl = redisTemplate.opsForValue().get("AutoScanMoQrcodeData");
//				} catch (Exception e) {
//					usernameAndPl = null;
//					log.error(e.getMessage(),e);
//				}
//				for(;;) {
//					if(Boolean.TRUE == usernameAndPlLock.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {					
//						usernameAndPlHashSet.add(usernameAndPl);
//						usernameAndPlLock.set(Boolean.FALSE);
//						break;
//					}
//				}
//				for (Iterator<String> it = usernameAndPlHashSet.iterator(); it.hasNext();) {
//					usernameAndPl = it.next();
//					log.info("task1()：开始执行任务...");
//					// 当前被处理的MO单号，如果为空则不处理
//					String pl = "";
//					// 当前被处理的MO单号绑定的用户
//					String username = "";
//					if (usernameAndPl != null) {
//						String[] usernameAndPlArray = usernameAndPl.split("&");
//						if (usernameAndPlArray.length == 2) {
//							username = usernameAndPlArray[0];
//							pl = usernameAndPlArray[1];
//						} else {
//							username = "";
//							pl = "";
//						}
//					} else {
//						username = "";
//						pl = "";
//					}
//					if (pl != null && !"".equals(pl) && pl.matches(plRegex)) {
//						log.info("使用MO单号：" + pl + "，从MES系统查找相应的数据..");
//						QueryWrapper<Mentrydb1> queryWrapper1 = new QueryWrapper<>();
//						queryWrapper1 = queryWrapper1.eq("PL", pl);
//						queryWrapper1 = queryWrapper1.select("top 1 Product");
//						QueryWrapper<ProdLog> queryWrapper2 = new QueryWrapper<>();
//						queryWrapper2 = queryWrapper2.eq("into_mes", 0);
//						long startDateTimestamp = System.currentTimeMillis() - 15 * 86400 * 1000;
//						String date = sdf1.format(new Date(startDateTimestamp));
//						queryWrapper2 = queryWrapper2.ge("start_date", date);
//						Page<ProdLog> page = new Page<>(1, 100);
//						try {
//							Mentrydb1 product = mentrydb1Service.getOne(queryWrapper1);
//							if (product != null && product.getProduct() != null && !"".equals(product.getProduct())) {
//								queryWrapper2 = queryWrapper2.like("srff_file", product.getProduct() + "%");
//								log.info("通过MO单号：" + pl + " 找到型号：" + product.getProduct() + " ...");
//								while(true) {
//									IPage<ProdLog> prodLogPage = prodLogService.page(page, queryWrapper2);
//									List<ProdLog> prodLogList = prodLogPage.getRecords();
//									if (prodLogList.size() > 0) {
//										log.info("发现aoi设备的数据型号：" + product.getProduct() + " 匹配的数据有" + prodLogList.size()
//										+ "条...");
//										QueryWrapper<ProdLog> queryWrapper3 = new QueryWrapper<>();
//										List<Integer> prodLogIdList = new ArrayList<>();
//										
//										if ("dev".equals(springProfilesActive)) {
//											List<Mentrydb2111> mentrydb2111List = new ArrayList<>();
//											for (ProdLog prodLog : prodLogList) {
//												Mentrydb2111 mentrydb2111 = new Mentrydb2111();
//												mentrydb2111.setProduct(product.getProduct());
//												mentrydb2111.setSNo(prodLog.getPanelId());
//												mentrydb2111.setProcess("AOI");
//												mentrydb2111.setMachine(prodLog.getIpAddress());
//												mentrydb2111.setMember("MES_TEST_" + username);
//												mentrydb2111.setProcesspattern(0);
//												mentrydb2111.setEntrytime(new Date());
//												mentrydb2111
//												.setJudgment("P".equals(prodLog.getTestStatus()) ? "GOOD" : "NOGOOD");
//												mentrydb2111.setPl(pl);
//												mentrydb2111List.add(mentrydb2111);
//												prodLogIdList.add(prodLog.getId());
//											}
//											mentrydb2111Service.saveBatch(mentrydb2111List);
//										} else if ("pro".equals(springProfilesActive)) {
//											List<Mentrydb1> mentrydb1List = new ArrayList<>();
//											for (ProdLog prodLog : prodLogList) {
//												Mentrydb1 mentrydb1 = new Mentrydb1();
//												mentrydb1.setProduct(product.getProduct());
//												mentrydb1.setSNo(prodLog.getPanelId());
//												mentrydb1.setProcess("AOI");
//												mentrydb1.setMachine(prodLog.getIpAddress());
//												mentrydb1.setMember("MES_TEST_" + username);
//												mentrydb1.setProcesspattern(0);
//												mentrydb1.setEntrytime(new Date());
//												mentrydb1.setJudgment("P".equals(prodLog.getTestStatus()) ? "GOOD" : "NOGOOD");
//												mentrydb1.setPl(pl);
//												mentrydb1List.add(mentrydb1);
//												prodLogIdList.add(prodLog.getId());
//											}
//											mentrydb1Service.saveBatch(mentrydb1List);
//										}
//										log.info("成功地把" + prodLogList.size() + "条数据从AOI迁移到MES系统...");
//										queryWrapper3.in("id", prodLogIdList);
//										ProdLog prodLogUpdate = new ProdLog();
//										prodLogUpdate.setIntoMes(1);
//										prodLogUpdate.setMo(pl);
//										prodLogService.update(prodLogUpdate, queryWrapper3);
//										log.info("把已经迁移的AOI数据标记为“已迁移”状态...");
//									} else {
//										log.info("未能找到MO单号匹配的数据...");
//										break;
//									}
//								}
//							} else {
//								log.info("在MES系统中，该MO单号: " + pl + " 不存在！");
//							}
//						} catch (Exception e) {
//							log.error("aoi数据搬运过程中出错，错误原因：", e);
//						}
//					}
//					for(;;) {
//						if(Boolean.TRUE == usernameAndPlAndStartTimeLock.compareAndSet(Boolean.FALSE, Boolean.TRUE)) {					
//							usernameAndPlHashSet.remove(usernameAndPl);
//							usernameAndPlAndStartTimeLock.set(Boolean.FALSE);
//							break;
//						}
//					}
//				}
//			}
//			log.info("task1()：任务结束...");
//			task1Lock.unlock();
//		}
//	}
}