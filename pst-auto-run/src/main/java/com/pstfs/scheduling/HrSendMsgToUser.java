package com.pstfs.scheduling;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.A01Entity;
import com.pstfs.entity.KDayEntity;
import com.pstfs.service.A01Service;
import com.pstfs.service.KDayService;
import com.pstfs.service.QyWxApi;

import lombok.extern.slf4j.Slf4j;

/**
 * 人事系统消息提醒
 * 
 * @author jiajian
 */
//@Component
@Slf4j
public class HrSendMsgToUser {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public HrSendMsgToUser() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}

	@Autowired
	private A01Service a01Service;

	@Autowired
	private KDayService kDayService;

	@Autowired
	private QyWxApi qyWxApi;

	/**
	 * 日期格式化对象1
	 */
	private DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
	

	/**
	 * 日期格式化对象2
	 */
	private DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy年MM月dd日HH时mm分");

	/**
	 * 重试间隔毫秒数
	 */
	public static long[] distances = { 10 * 60, 20 * 60, 30 * 60, 30 * 60, 30 * 60, 60 * 60, 3 * 60 * 60, 3 * 60 * 60,
			3 * 60 * 60, 6 * 60 * 60, 6 * 60 * 60 };

	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 每天早上10:30分发送考勤异常消息到用户微信，然后每隔10m/20m/30m/30m/30m/60m/3h/3h/3h/6h/6h 提醒一次
	 */
//	@Scheduled(cron = "0 30 10 * * ?")
//	@Scheduled(cron = "*/59 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				LocalDateTime yesterday = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0)
						.minusDays(1);
				QueryWrapper<KDayEntity> kDayEntityWrapper = new QueryWrapper<KDayEntity>();
				kDayEntityWrapper.gt("K_ACTIONTIME", yesterday);
				kDayEntityWrapper.in("K_DAYSTATE", new String[] { "迟到", "迟到/早退", "旷工", "早退" });
				kDayEntityWrapper.select("a0188", "K_DAYSTATE", "card_begin", "card_end", "shift_no", "late_min",
						"early_min","duty_date");
				QueryWrapper<A01Entity> a01Wrapper = new QueryWrapper<A01Entity>();
				a01Wrapper.select("A0188", "A0190");
				
				List<KDayEntity> kDayEntityList;
				List<A01Entity> a01EntityList;
				// 不断重试获取迟到、早退、旷工用户列表
				while (true) {
					try {
						kDayEntityList = kDayService.list(kDayEntityWrapper);
						List<Integer> a0188List = kDayEntityList.stream().map(KDayEntity::getA0188)
								.collect(Collectors.toList());
						a01Wrapper.in("A0188", a0188List);
						a01EntityList = a01Service.list(a01Wrapper);
						break;
					} catch (Exception ex) {
						ex.printStackTrace();
						try {
							TimeUnit.SECONDS.sleep(10);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				
				Map<Integer, String> userNoMap = new HashMap<Integer, String>();
				for (A01Entity a01Entity : a01EntityList) {
					userNoMap.put(a01Entity.getA0188(), a01Entity.getA0190());
				}
				// 获取昨天时间
				for (int i = 0; i < distances.length; i++) {
					for (Iterator<KDayEntity> it = kDayEntityList.iterator(); it.hasNext();) {
						KDayEntity kDayEntity = it.next();
						try {
							String a0190 = userNoMap.get(kDayEntity.getA0188());
							StringBuilder content = new StringBuilder();
							LocalDateTime dutyDate = LocalDateTime.ofInstant(kDayEntity.getDutyDate().toInstant(),ZoneId.systemDefault());
							content.append("员工工号“").append(a0190).append("”在").append(dutyDate.format(dtf1));
							if (kDayEntity.getShiftNo().equals(1)) {
								if ("迟到".equals(kDayEntity.getKDaystate())) {
									LocalDateTime cardBegin = LocalDateTime.ofInstant(kDayEntity.getCardBegin().toInstant(),
											ZoneId.systemDefault());
									content.append("上班迟到，当天上班打卡时间是“").append(cardBegin.format(dtf2)).append("”，迟到了")
									.append(kDayEntity.getLateMin()).append("分钟，");
								} else if ("旷工".equals(kDayEntity.getKDaystate())) {
									content.append("上午旷工，");
								}
							} else if (kDayEntity.getShiftNo().equals(2)) {
								if ("早退".equals(kDayEntity.getKDaystate())) {
									LocalDateTime cardEnd = LocalDateTime.ofInstant(kDayEntity.getCardEnd().toInstant(),
											ZoneId.systemDefault());
									content.append("下班早退，当天下班打卡时间时“").append(cardEnd.format(dtf2)).append("”，早退了")
									.append(kDayEntity.getEarlyMin()).append("分钟，");
								} else if ("旷工".equals(kDayEntity.getKDaystate())) {
									content.append("下午旷工，");
								}
							}
							content.append(
									"<a href=\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wwcaf6bede6d102606&redirect_uri=http%3A%2F%2Fzb.zhuyuns.com%3A8001%2Fpst-qywx-app%2Fpublic%2Findex.php%2Findex%2FIndex%2Flogin&response_type=code&scope=snsapi_base&state=%2FMyAttendance#wechat_redirect\">点击查看详情</a>。");
							String json = qyWxApi.messageSendText(content.toString(), a0190);
							JSONObject jsonObject = JSONObject.parseObject(json);
							Integer errcode = jsonObject.getInteger("errcode");
							String errmsg = jsonObject.getString("errmsg");
							if (errcode.equals(0)) {
								log.info("task1()：消息已推送，推送结果：" + errmsg);
							} else {
								log.error("task1()：消息推送失败，失败原因：" + errmsg);
							}
							it.remove();
						} catch (Exception e) {
							log.error("task1()：执行任务异常，" + distances[i] + "秒后重试", e);
						}
					}
					if (kDayEntityList.size() > 0) {
						try {
							TimeUnit.SECONDS.sleep(distances[i]);
						} catch (InterruptedException e2) {
							e2.printStackTrace();
						}
					} else {
						break;
					}
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
}