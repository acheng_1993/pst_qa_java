package com.pstfs.scheduling;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.QywxUserListEntity;
import com.pstfs.service.QyWxApi;
import com.pstfs.service.QywxUserListService;

import lombok.extern.slf4j.Slf4j;

/**
 * 每天从企业微信把员工信息同步到数据库
 * 
 * @author jiajian
 */
@Component
@Slf4j
public class CopyQywxUserList {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public CopyQywxUserList() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}

	@Autowired
	private QyWxApi qyWxApi;

	@Autowired
	private QywxUserListService qywxUserListService;

	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 每天凌晨准时同步数据
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				String userListJson;
				while (true) {
					try {
						userListJson = qyWxApi.userList(1, 1);
						break;
					} catch (Exception e) {
						e.printStackTrace();
						try {
							TimeUnit.SECONDS.sleep(10);
						} catch (InterruptedException e2) {
							e2.printStackTrace();
						}
					}
				}
				JSONObject jsonObject = JSONObject.parseObject(userListJson);
				JSONArray userlist = jsonObject.getJSONArray("userlist");
				QueryWrapper<QywxUserListEntity> queryWrapper = new QueryWrapper<>();
				queryWrapper.select("id", "userid");
				List<QywxUserListEntity> qywxUserListEntityList;
				while (true) {
					try {
						qywxUserListEntityList = qywxUserListService.list(queryWrapper);
						break;
					} catch (Exception e) {
						e.printStackTrace();
						try {
							TimeUnit.SECONDS.sleep(10);
						} catch (InterruptedException e2) {
							e2.printStackTrace();
						}
					}
				}
				Map<String, Integer> userIdMapToId = new HashMap<>();
				for (QywxUserListEntity entity : qywxUserListEntityList) {
					userIdMapToId.put(entity.getUserid(), entity.getId());
				}
				List<QywxUserListEntity> qywxUserListEntityUpdateList = new ArrayList<QywxUserListEntity>();
				List<QywxUserListEntity> qywxUserListEntitySaveList = new ArrayList<QywxUserListEntity>();
				QywxUserListEntity qywxUserListEntity;
				for (int j = 0, len_j = userlist.size(); j < len_j; j++) {
					jsonObject = userlist.getJSONObject(j);
					String userid = jsonObject.getString("userid");
					String name = jsonObject.getString("name");
					String avatar = jsonObject.getString("avatar");
					String thumb_avatar = jsonObject.getString("thumb_avatar");
					String alias = jsonObject.getString("alias");
					qywxUserListEntity = new QywxUserListEntity();
					qywxUserListEntity.setUserid(userid);
					qywxUserListEntity.setName(name);
					qywxUserListEntity.setAvatar(avatar);
					qywxUserListEntity.setThumbAvatar(thumb_avatar);
					qywxUserListEntity.setAlias(alias);
					qywxUserListEntity.setCreateTime(new Date());
					qywxUserListEntity.setUpdateTime(qywxUserListEntity.getCreateTime());
					Integer id = userIdMapToId.get(userid);
					if (id == null) {
						qywxUserListEntitySaveList.add(qywxUserListEntity);
					} else {
						qywxUserListEntity.setId(id);
						qywxUserListEntityUpdateList.add(qywxUserListEntity);
					}
				}
				while (qywxUserListEntityUpdateList.size() > 0) {
					try {
						qywxUserListService.updateBatchById(qywxUserListEntityUpdateList);
						break;
					} catch (Exception e) {
						e.printStackTrace();
						try {
							TimeUnit.SECONDS.sleep(10);
						} catch (InterruptedException e2) {
							e2.printStackTrace();
						}
					}
				}
				while (qywxUserListEntitySaveList.size() > 0) {
					try {
						qywxUserListService.saveBatch(qywxUserListEntitySaveList);
						break;
					} catch (Exception e) {
						e.printStackTrace();
						try {
							TimeUnit.SECONDS.sleep(10);
						} catch (InterruptedException e2) {
							e2.printStackTrace();
						}
					}
				}
			}
		}
		log.info("task1()：结束执行任务...");
		task1Lock.unlock();
	}
}