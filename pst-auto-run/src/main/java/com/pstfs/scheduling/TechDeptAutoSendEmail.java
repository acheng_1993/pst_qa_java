package com.pstfs.scheduling;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.MaxSendTechissIdEntity;
import com.pstfs.entity.TechissEntity;
import com.pstfs.entity.UsersEntity;
import com.pstfs.service.MaxSendTechissIdService;
import com.pstfs.service.TechissService;
import com.pstfs.service.UsersService;
import lombok.extern.slf4j.Slf4j;

/**
 * 技术部技联网自动发送邮件功能
 * @author jiajian
 */
@Component
@Slf4j
public class TechDeptAutoSendEmail {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	public TechDeptAutoSendEmail() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Autowired
	private MaxSendTechissIdService maxSendTechissIdService;
	
	@Autowired
	private TechissService techissService;
	
	@Autowired
	private UsersService usersService;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	@Scheduled(cron = "0/60 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				MaxSendTechissIdEntity maxSendTechissId;
				try {
					maxSendTechissId = maxSendTechissIdService.getOne(new QueryWrapper<MaxSendTechissIdEntity>());
				} catch (Exception e) {
					maxSendTechissId = null;
					log.error(e.getMessage(),e);
				}
				if(maxSendTechissId == null || maxSendTechissId.getTechissId() == null) {
					QueryWrapper<TechissEntity> queryWrapper = new QueryWrapper<>();
					queryWrapper.select("max(id) id");
					try {
						TechissEntity techissEntity = techissService.getOne(queryWrapper);
						maxSendTechissId = new MaxSendTechissIdEntity();
						maxSendTechissId.setTechissId(techissEntity.getId());
						maxSendTechissIdService.save(maxSendTechissId);
					} catch (Exception e) {
						log.error(e.getMessage(),e);
					}
				} else {
					QueryWrapper<TechissEntity> queryWrapper = new QueryWrapper<>();
					queryWrapper.gt("id", maxSendTechissId.getTechissId());
					queryWrapper.orderByAsc("id");
					queryWrapper.select("id,category,pnpa,iss_ty,issto,issno,status,ctrlno,isdate,doctype");
					List<TechissEntity> techissEntityList = techissService.list(queryWrapper);

					Properties pro = new Properties();
					pro.setProperty("mail.host", "smtp.mxhichina.com");
					pro.setProperty("mail.from", "pstfs_system02@pst-hk.com");
					pro.setProperty("mail.smtp.host", "smtp.mxhichina.com");
					pro.setProperty("mail.transport.protocol","smtp");
					pro.setProperty("mail.smtp.starttls.enable", "true");
					pro.setProperty("mail.smtp.auth","true");
					pro.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // 使用JSSE的SSL
					pro.setProperty("mail.smtp.socketFactory.fallback", "false");
					pro.setProperty("mail.smtp.port", "465");
					pro.setProperty("mail.smtp.socketFactory.port","465");

					for(int i=0,len_i=techissEntityList.size();i<len_i;i++) {
						TechissEntity techissEntity = techissEntityList.get(i);
						techissEntity.getIssto().split(",");
						QueryWrapper<UsersEntity> usersEntityQueryWrapper = new QueryWrapper<>();
						usersEntityQueryWrapper.in("DEPTID", techissEntity.getIssto().split(","));
						usersEntityQueryWrapper.isNotNull("email");
						usersEntityQueryWrapper.ne("email", "");
						usersEntityQueryWrapper.select("distinct email");
						List<UsersEntity> usersEntityList = usersService.list(usersEntityQueryWrapper);
						
						List<Address> tos = new ArrayList<>();
						for(int j=0,len_j=usersEntityList.size();j<len_j;j++) {
							UsersEntity usersEntity = usersEntityList.get(j);
							try {
								tos.add(new InternetAddress(usersEntity.getEmail()));
							} catch (AddressException e2) {
								e2.printStackTrace();
							}
						}
						
						Session session = Session.getDefaultInstance(pro);
//						session.setDebug(true);
						try {
							MimeMessage message = new MimeMessage(session);
							message.setFrom(new InternetAddress("pstfs_system02@pst-hk.com"));
							message.addRecipients(Message.RecipientType.TO,tos.toArray(new Address[tos.size()]));
							message.setSubject(techissEntity.getPnpa()+"-"+techissEntity.getDoctype()+"["+techissEntity.getStatus()+"-"+techissEntity.getIssno()+"]");
							message.setSentDate(new Date());
							Multipart multipart = new MimeMultipart();
							BodyPart html = new MimeBodyPart();
							html.setContent("<div>Record status changed: New Technical Document !</div>"+
									"<div>key value : "+techissEntity.getId()+"</div>"+
									"<div>Doc No. : <!--ctrlno--></div>"+
									"<div>Doc Type : "+techissEntity.getCategory()+"</div>"+
									"<div>Model : "+techissEntity.getPnpa()+"</div>"+
									"<div>Issue Type : "+techissEntity.getIssTy()+"</div>"+
									"<div>Issue : "+techissEntity.getIssno()+"</div>"+
									"<div>Status : "+techissEntity.getStatus()+"</div>"+
									"<div>Part No. : "+techissEntity.getCtrlno()+"</div>"+
									"<div>Issue Date : "+techissEntity.getIsdate()+"</div>","text/html;charset=UTF-8");
							multipart.addBodyPart(html);
							message.setContent(multipart);
							message.saveChanges();
							Transport transport = session.getTransport("smtp");
							transport.connect("smtp.mxhichina.com", "pstfs_system02@pst-hk.com", "System010");
							transport.sendMessage(message,message.getAllRecipients());
							transport.close();
							QueryWrapper<MaxSendTechissIdEntity> update=new QueryWrapper<>();
							update.eq("id", 0);
							maxSendTechissId.setTechissId(techissEntity.getId());
							maxSendTechissIdService.update(maxSendTechissId, update);
						} catch (NoSuchProviderException e1) {
							e1.printStackTrace();
						} catch (MessagingException e) {
							e.printStackTrace();
						}
					}
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
}