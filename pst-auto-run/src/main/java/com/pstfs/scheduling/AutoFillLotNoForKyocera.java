package com.pstfs.scheduling;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.imageio.ImageIO;
import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.contentstream.operator.DrawObject;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.contentstream.operator.state.Concatenate;
import org.apache.pdfbox.contentstream.operator.state.Restore;
import org.apache.pdfbox.contentstream.operator.state.Save;
import org.apache.pdfbox.contentstream.operator.state.SetGraphicsStateParameters;
import org.apache.pdfbox.contentstream.operator.state.SetMatrix;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.KyoceraPdfHandleEntity;
import com.pstfs.entity.UploadFileEntity;
import com.pstfs.service.KyoceraPdfHandleService;
import com.pstfs.service.UploadFileService;
import cn.hutool.core.io.FileUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import lombok.extern.slf4j.Slf4j;

/**
 * 自动地把 lot no 填入京瓷的二维码标签
 * 
 * @author jiajian
 */
@Component
@Slf4j
public class AutoFillLotNoForKyocera extends MessageListenerAdapter {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	/**
	 * 上传文件路径
	 */
	private String uploadFilePath;
	
	/**
	 * pdf文件的保存路径
	 */
	private String pdfPath;
	
	public AutoFillLotNoForKyocera(@Value("${uploadFilePath}") String uploadFilePath,@Value("${pdfPath}") String pdfPath) {
		this.uploadFilePath = uploadFilePath;
		new File(uploadFilePath).mkdirs();
		this.pdfPath = pdfPath;
		new File(pdfPath).mkdirs();
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}

	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 使用该方法监听，当key失效时执行该方法
	 * 
	 * @param message
	 * @param pattern
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		if ("dev".equals(springProfilesActive)) {
			String key = new String(message.getChannel());
			if ("AutoFillLotNoForKyocera".equals(key)) {
				task1Lock.lock();
				try {
					task1();
				} catch (Exception e) {
					log.error("AutoFillLotNoForKyocera 键值执行任务异常", e);
				} finally {
					task1Lock.unlock();
				}
			}
		}
	}
	
	@Autowired
	private KyoceraPdfHandleService kyoceraPdfHandleService;
	
	@Autowired
	private UploadFileService uploadFileService;
	
	/**
	 * 每隔10分钟，把京瓷的交货指示的 pdf 内容进行追加 lot no，数量；以及扫码的二维码也追加 lot no，数量。
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("dev".equals(springProfilesActive)) {
//				try {
//					List<KyoceraPdfHandleEntity> kyoceraPdfHandleList;
//					List<Integer> uploadFileIdList = new ArrayList<>();
//					{
//						QueryWrapper<KyoceraPdfHandleEntity> queryWrapper = new QueryWrapper<>();
//						queryWrapper.eq("handle_status", 0);
//						queryWrapper.select("id,upload_file_id");
//						kyoceraPdfHandleList = kyoceraPdfHandleService.list(queryWrapper);
//						for (int i=0,len_i = kyoceraPdfHandleList.size();i<len_i;i++) {
//							KyoceraPdfHandleEntity kyoceraPdfHandle = kyoceraPdfHandleList.get(i);
//							uploadFileIdList.add(kyoceraPdfHandle.getUploadFileId());
//						}
//					}
//					Map<Integer,UploadFileEntity> uploadFileMap = new HashMap<>(); 
//					{
//						QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
//						queryWrapper.select("id,sha1,path");
//						queryWrapper.in("id", uploadFileIdList);
//						List<UploadFileEntity> uploadFileList = uploadFileService.list(queryWrapper);
//						for(int i=0,len_i = uploadFileList.size();i<len_i;i++) {
//							UploadFileEntity uploadFile = uploadFileList.get(i);
//							uploadFileMap.put(uploadFile.getId(), uploadFile);
//						}
//					}
//					for (int h=0,len_h = kyoceraPdfHandleList.size();h<len_h;h++) {
//						KyoceraPdfHandleEntity kyoceraPdfHandle = kyoceraPdfHandleList.get(h);
//						UploadFileEntity uploadFile = uploadFileMap.get(kyoceraPdfHandle.getUploadFileId());
//						File file = new File(uploadFilePath+File.separator+uploadFile.getPath());
//						try (PDDocument doc = PDDocument.load(file)) {
//							GetTextLocations stripper = new GetTextLocations();
//							stripper.setSortByPosition(true);
//							int numOfPages = doc.getNumberOfPages();
//							GetImageLocations printer = new GetImageLocations();
//							for (int i = 1; i <= numOfPages; i++) {
//								stripper.setStartPage(i);
//								stripper.setEndPage(i);
//								stripper.getText(doc);
//								ArrayList<TextPosition> textPositionList = stripper.getTextPositionList();
//								stripper.clearTextPositionList();
//								
//								PDPage page = doc.getPage(i - 1);
//								printer.setTextPositionList(textPositionList);
//								printer.processPage(page);
//								
//								Matrix[] textPositionImgArray = printer.getTextPositionImgArray();
//								COSName[] objectNameArray = printer.getObjectNameArray();
//								
//								PDResources resources = page.getResources();
//								
//								try (PDPageContentStream contentStream = new PDPageContentStream(doc, page, AppendMode.APPEND, true,false)) {
//									for (int j=0,len_j = textPositionList.size();j<len_j;j++) {
//										TextPosition textPosition = textPositionList.get(j);
//										Matrix textPositionImg = textPositionImgArray[j];
//										if (textPositionImg == null) {
//											break;
//										}
//										COSName objectName = objectNameArray[j];
//										
//										contentStream.beginText();
//										contentStream.setFont(PDType1Font.HELVETICA_BOLD, 8);
//										contentStream.newLineAtOffset(textPosition.getEndX(), textPosition.getEndY()-9);
//										contentStream.showText("4117:150");
//										contentStream.endText();
//										
//										PDImageXObject image = (PDImageXObject) resources.getXObject(objectName);
//										String qrcodeStr = QrCodeUtil.decode(image.getImage());
//										//删除旧的二维码
//										page.getCOSObject().removeItem(objectName);
//										QrConfig config = new QrConfig((int)Math.abs(textPositionImg.getScalingFactorX()*2), (int)Math.abs(textPositionImg.getScalingFactorY()*2));
//										config.setMargin(1);
//										BufferedImage qrcode = QrCodeUtil.generate(qrcodeStr + " 4117:150", config);
//										try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
//											ImageIO.write(qrcode, "png", baos);
//											PDImageXObject qrcodeXObj = PDImageXObject.createFromByteArray(doc, baos.toByteArray(), "replace"+j);
//											contentStream.drawImage(qrcodeXObj, textPositionImg);
//										}
//									}
//								}
//							}
//							doc.setAllSecurityToBeRemoved(true);
//							String pdfFullPath = pdfPath+File.separator+UUID.randomUUID().toString()+".kyoceraPdf";
//							File pdfFile = new File(pdfFullPath);
//							doc.save(pdfFile);
//							saveNewPdf(pdfFile, kyoceraPdfHandle.getId());
//						} catch (IOException e) {
//							throw e;
//						} catch (Exception e) {
//							throw e;
//						}
//					}
//					File pdfFile = new File(pdfPath);
//					File[] listFiles = pdfFile.listFiles();
//					for (int i=0, len_i = listFiles.length;i < len_i; i++) {
//						File tempFile = listFiles[i];
//						if (tempFile.isFile()) {
//							if (tempFile.getName().endsWith(".kyoceraPdf")) {
//								tempFile.delete();
//							}
//						}
//					}
//				} catch(Exception e) {
//					log.error(e.getMessage(), e);
//				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}
	
	@Transactional
	public void saveNewPdf(File pdfFile,Integer id) {
		String sha1 = SecureUtil.sha1(pdfFile);
		String targetFilePath = sha1.substring(0, 2) + File.separator + sha1.substring(2) + ".sha1";
		Integer uploadFileId = uploadFileService.save(sha1, targetFilePath);
		UpdateWrapper<KyoceraPdfHandleEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("id", id);
		updateWrapper.set("handle_upload_file_id", uploadFileId);
		kyoceraPdfHandleService.update(updateWrapper);
		FileUtil.move(pdfFile, new File(pdfPath + File.separator + targetFilePath), false);
	}
	
	/**
	 * 获取 pdf 文件的文字位置
	 * @author jiajian
	 */
	public static class GetTextLocations extends PDFTextStripper {

		public GetTextLocations() throws IOException {
			super();
			textPositionList = new ArrayList<TextPosition>();
		}
	 
		private ArrayList<TextPosition> textPositionList;
		
		protected void writeString(String text, List<TextPosition> textPositions) throws IOException {
			int fromIdx = -1;
			String PRODUCTION = "PRODUCTION";
			String LOT = "LOT";
			String NUMBER = "NUMBER";
			if (text.indexOf("(") == -1 && (fromIdx = text.indexOf(PRODUCTION)) > -1
					&& (fromIdx = text.indexOf(LOT, fromIdx + PRODUCTION.length())) > -1
					&& text.indexOf(NUMBER, fromIdx + LOT.length()) > -1) {
				TextPosition textPosition = textPositions.get(0);
				textPositionList.add(textPosition);
			}
		}
		
		public void clearTextPositionList() {
			textPositionList.clear();
		}
		
		public ArrayList<TextPosition> getTextPositionList() {
			ArrayList<TextPosition> ret = new ArrayList<>(textPositionList.size());
			ret.addAll(textPositionList);
			return ret;
		}
	}

	/**
	 * 获取 pdf 文件的图片位置，代码出自官方文档:
	 * https://www.tutorialkart.com/pdfbox/how-to-get-location-and-size-of-images-in-pdf/
	 * @author jiajian
	 */
	public static class GetImageLocations extends PDFStreamEngine {
		public GetImageLocations() throws IOException {
	        // preparing PDFStreamEngine
	        addOperator(new Concatenate());
	        addOperator(new DrawObject());
	        addOperator(new SetGraphicsStateParameters());
	        addOperator(new Save());
	        addOperator(new Restore());
	        addOperator(new SetMatrix());
	    }
		
		private ArrayList<TextPosition> textPositionList;
		private Matrix[] textPositionImgArray;
		private COSName[] objectNameArray;
		
		public void setTextPositionList(ArrayList<TextPosition> textPositionList) {
			this.textPositionList = new ArrayList<>();
			this.textPositionList.addAll(textPositionList);
			textPositionImgArray = new Matrix[textPositionList.size()];
			objectNameArray = new COSName[textPositionList.size()];
		}

		@Override
		protected void processOperator(Operator operator, List<COSBase> operands) throws IOException {
			String operation = operator.getName();
			if ("Do".equals(operation)) {
				COSName objectName = (COSName) operands.get(0);
				// get the PDF object
				PDXObject xobject = getResources().getXObject(objectName);
				// check if the object is an image object
				if (xobject instanceof PDImageXObject) {
					Matrix ctmNew = getGraphicsState().getCurrentTransformationMatrix();
					for (int i=0,len_i = textPositionList.size();i<len_i;i++) {
						TextPosition textPosition = textPositionList.get(i);
						if (ctmNew.getTranslateX() > textPosition.getEndX() && ctmNew.getTranslateY() > textPosition.getEndY()
								&& (ctmNew.getTranslateY() - Math.abs(ctmNew.getScalingFactorY())) < textPosition.getEndY()) {
							if(textPositionImgArray[i] == null) {
								textPositionImgArray[i] = ctmNew;
								objectNameArray[i] = objectName;
							} else {
								Matrix ctm = textPositionImgArray[i];
								if (ctmNew.getTranslateX() < ctm.getTranslateX() && ctmNew.getTranslateY() < ctm.getTranslateY()) {
									textPositionImgArray[i] = ctmNew;
									objectNameArray[i] = objectName;
								}
							}
						}
					}
				} else if (xobject instanceof PDFormXObject) {
					PDFormXObject form = (PDFormXObject) xobject;
					showForm(form);
				}
			} else {
				super.processOperator(operator, operands);
			}
		}

		public Matrix[] getTextPositionImgArray() {
			return Arrays.copyOf(textPositionImgArray, textPositionImgArray.length);
		}

		public COSName[] getObjectNameArray() {
			return Arrays.copyOf(objectNameArray, objectNameArray.length);
		}
	}
}