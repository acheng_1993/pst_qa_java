package com.pstfs.scheduling;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.pstfs.PstAutoRun;
import com.pstfs.entity.UploadFileDeleteProgressEntity;
import com.pstfs.entity.UploadFileEntity;
import com.pstfs.service.UploadFileDeleteProgressService;
import com.pstfs.service.UploadFileService;

import lombok.extern.slf4j.Slf4j;

/**
 * 自动删除未被引用的文件
 * @author jiajian
 */
@Component
@Slf4j
public class UploadFileAutoDelete {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public UploadFileAutoDelete() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Autowired
	private UploadFileService uploadFileService;
	
	@Autowired
	private UploadFileDeleteProgressService uploadFileDeleteProgressService;
	
	@Value("${uploadFilePath}")
	private String uploadFilePath;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 每天凌晨准时自动删除未被引用的文件
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	/**
	 * 每隔1分钟，自动删除未被引用的文件
	 */
//	@Scheduled(cron = "0 */1 * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				List<UploadFileEntity> uploadFileEntityList;
				try {
					long lastExecuteTime = System.currentTimeMillis();
					Integer newLastProgressId;
					do {
						if(System.currentTimeMillis() - lastExecuteTime > 3600000) {
							log.info("自动删除文件功能，运行时长超过一个小时，马上退出...");
							break;
						}
						UploadFileDeleteProgressEntity uploadFileDeleteProgressEntity = uploadFileDeleteProgressService.getOne(null);
						Integer lastProgressId = uploadFileDeleteProgressEntity.getProgressId();
						newLastProgressId = uploadFileService.newLastProgressId(lastProgressId, 100);
						if(newLastProgressId == null) {
							log.info("未发现需要删除的文件...");
							uploadFileDeleteProgressService.resetProgress();
						} else {
							log.info("上次文件删除进度："+lastProgressId);
							uploadFileEntityList = uploadFileService.notUseUploadFileList(lastProgressId, newLastProgressId);
							if(uploadFileEntityList.isEmpty()) {
								uploadFileDeleteProgressService.updateProgress(newLastProgressId);
							} else {
								log.info("发现 "+uploadFileEntityList.size()+" 个文件需要删除...");
								List<Integer> ids = new ArrayList<>(uploadFileEntityList.size());
								for(UploadFileEntity uploadFileEntity:uploadFileEntityList) {
									ids.add(uploadFileEntity.getId());
								}
								uploadFileService.deleteNotUseUploadFile(ids,newLastProgressId);
								for(UploadFileEntity uploadFileEntity:uploadFileEntityList) {
									File file = new File(uploadFilePath+File.separator+uploadFileEntity.getPath());
									file.delete();
								}
								log.info("已成功删除 "+uploadFileEntityList.size()+" 个文件。");
							}
						}
					} while(newLastProgressId != null);
				} catch (Exception e) {
					log.error("自动删除文件时出错，错误原因：", e);
				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}
}