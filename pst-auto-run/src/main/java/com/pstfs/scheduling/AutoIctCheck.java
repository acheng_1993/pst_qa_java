package com.pstfs.scheduling;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.pstfs.PstAutoRun;
import com.pstfs.entity.UploadFileEntity;
import com.pstfs.entity.UploadIctFileListEntity;
import com.pstfs.service.UploadFileService;
import com.pstfs.service.UploadIctFileListService;
import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ZipUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 检查待解析的ict对比的文件，自动将解析后的数据写入数据库
 * @author chenlong
 *
 */
@Component
@Slf4j
public class AutoIctCheck extends MessageListenerAdapter {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	
	//记录字母与数字的对应关系
	private static HashMap<String,Integer> number = new HashMap();
	
	@Value("${ICTPath}")
	private String ICTPath;
	
	@Autowired
	private UploadIctFileListService uploadIctFileListService;
	
	@Autowired
	private UploadFileService uploadFileService;

	public AutoIctCheck() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 每隔5分钟，检查待解析的ict对比的文件
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
//	@Scheduled(cron = "*/5 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			//excel(91);
			if ("pro".equals(springProfilesActive)) {
				//上面为调试使用，正式要把下面的注释取消
				try {
					boolean flag = true;
					while(flag) {
						ArrayList<UploadIctFileListEntity> list = uploadIctFileListService.getType();
						if(list.size() != 0) {
							for(UploadIctFileListEntity file : list) {
								if("pdf".equals(file.getSuffix())) {
									pdf(file.getFileId());
								}else if("xlsm".equals(file.getSuffix()) || "xlsx".equals(file.getSuffix())) {
									excel(file.getFileId());
								}
							}	
						} else {
							flag = false;
						}
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
	
	/**
	 * 使用该方法监听，当key失效时执行该方法
	 * @param message
	 * @param pattern
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		if("pro".equals(springProfilesActive)) {
			String key = new String(message.getChannel());
			if("AutoCheckICTFileAndAnalysis".equals(key)) {
				task1Lock.lock();
				try {
					task1();
				} catch (Exception e) {
					log.error("AutoCheckICTFileAndAnalysis 键值执行任务异常", e);
				} finally {
					task1Lock.unlock();
				}
			} 
		}
	}

	private String findPrintForm1Sheet(String path) {
		SAXReader reader = new SAXReader();
		try {
			Document document = reader.read(new File(path));
            Element rootElement  = document.getRootElement();
            List<Element> workbook = rootElement.elements();
            String rId = null;
            for (Element userElement : workbook) {
            	if("sheets".equals(userElement.getName())) {
            		List<Element> sheets = userElement.elements();
            		for (Element sheet : sheets) {
            			if("PrintForm1".equals(sheet.attributeValue("name"))){
            				rId = sheet.attributeValue("id");
            				break;
            			}else if("Sheet1".equals(sheet.attributeValue("name"))) {
            				rId = sheet.attributeValue("id");
            				break;
            			}else if(sheet.attributeValue("state") == null) {
            				rId = sheet.attributeValue("id");
            				break;
            			}
            		}
            	}
            }
            if(rId == null) {
            	return null;
            }else {
            	rId = rId.substring(3);
            	return "sheet" + rId + ".xml";
            }
            
		}catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}
	
	private void excel(int fileId) {
		FileUtil.mkdir(new File(ICTPath + "temp"));
		UploadFileEntity uploadFile = uploadFileService.getPathById(fileId);

		try {
			ZipUtil.unzip(ICTPath + uploadFile.getPath(), ICTPath + "temp");
			//ZipUtil.unzip(ICTPath + "abc.xlsm", ICTPath + "temp");
		}catch(IORuntimeException e) {
			e.printStackTrace();
			return;
		}finally {
			uploadIctFileListService.updateType(0, fileId);
		}
		
		if(findPrintForm1Sheet(ICTPath + "temp\\xl\\workbook.xml") == null) {
			uploadIctFileListService.updateType(0, fileId);
			return;
		}
		
		SAXReader reader = new SAXReader();
		JSONConfig config2 = JSONConfig.create();
		config2.setIgnoreNullValue(false);
		JSONArray results = JSONUtil.createArray(config2);
		try {
			/*
			 * TODO 通过观察， shareStrings只用到了前1000多个，实际shareStrings有4000多个，
			 * 所以在遍历时应该只考虑前1000多个，其他可以忽略。
			 */
			Document document = reader.read(new File(ICTPath + "temp\\xl\\sharedStrings.xml"));
            Element rootElement  = document.getRootElement();
            List<Element> usersSubElementList = rootElement.elements();
            for (Element userElement : usersSubElementList) { 
                List<Element> userSubElementList = userElement.elements();
                results.add(userSubElementList.get(0).getText());
            }
		}catch (Exception e) {
            e.printStackTrace();
        }
		String resultStr = results.toString();	
		byte[] zipStr = ZipUtil.zlib(resultStr,CharsetUtil.UTF_8,9);
		String temp1;	
		temp1 = Base64.encode(zipStr);
		//后续将temp1加到数据库里面
		if(!uploadIctFileListService.updateShareString(fileId,temp1)) {
			log.info("fileId:" + fileId +  ",ShareString 插入失败");
		}
		
		//-------------------------下面处理另一个xml--------------------------
		//TODO 如果返回null，应该判定为不明类型
		String desSheet = findPrintForm1Sheet(ICTPath + "temp\\xl\\workbook.xml");
		SAXReader saxReader = new SAXReader();
		initNumber();
		JSONArray results2 = JSONUtil.createArray();
        try {
        	Document document = saxReader.read(new File(ICTPath + "temp\\xl\\worksheets\\" + desSheet));
        	Element rootElement = document.getRootElement();
            List<Element> rootElements = rootElement.elements();
            for (Element userElement : rootElements) {
            	//TODO 读完 sheetData 就应该马上跳出循环，能节省一半的时间
            	if("sheetData".equals(userElement.getName())) {
            		List<Element> rows = userElement.elements();
            		for(Element row : rows) {		            			
            			List<Element> cs = row.elements();
            			String rowNum = row.attributeValue("r");//获取当前行数
            			JSONConfig config = JSONConfig.create();
        				config.setIgnoreNullValue(false);
            			JSONArray result = JSONUtil.createArray(config);
            			for(Element c : cs) {
            				List<Element> vs = c.elements();
            				String index = c.attributeValue("r");
            				int indexNum = countNum(index,rowNum);
            				if(vs.size() >= 1) {
            					String t = c.attributeValue("t");
            					if(t == null) {
            						if (vs.size() == 1) {
            							result.set(indexNum,c.element("v").getText());
            						} else {
            							if (c.element("f") != null) {
            								result.set(indexNum,"e" + c.element("f").getText());
            							}
            						}
            					}else if("s".equals(t)) {
            						result.set(indexNum,"s" + c.element("v").getText());
            					}else if("e".equals(t)) {
            						result.set(indexNum,"e" + c.element("f").getText());
            					}else if("str".equals(t)) {
            						result.set(indexNum,"e" + c.element("f").getText());
            					}
            				}else {
            					result.set(indexNum,null);
            				}
            				
            			}
            			results2.add(result);
            		}
            	}
            }          
        } catch (DocumentException e) {
            e.printStackTrace();
        }	
        String result2 = results2.toString();
		byte[] zipStr2 = ZipUtil.zlib(result2,CharsetUtil.UTF_8,9);
		String temp2;	
		temp2 = Base64.encode(zipStr2);
		//后续将temp2加到数据库里面
		if(!uploadIctFileListService.updateSheetData(fileId,temp2)) {
			log.info("fileId:" + fileId + ",sheetData 插入失败");
		}
		uploadIctFileListService.updateType(1, fileId);
		FileUtil.del(new File(ICTPath + "temp"));
	}
	
	private void pdf(int fileId) {
		UploadFileEntity uploadFile = uploadFileService.getPathById(fileId);
		String pdfPath = ICTPath + uploadFile.getPath();
		File file = new File(pdfPath);
		if (file.exists()) {
			try (PDDocument doc = PDDocument.load(file)) {
				PDFTextStripper textStripper = new PDFTextStripper();
				int pageCount = doc.getNumberOfPages();
				StringBuilder sb = new StringBuilder();
				String temp;
				for (int i = 1; i <= pageCount; i++) {
					textStripper.setStartPage(i);
					textStripper.setEndPage(i);
					temp = textStripper.getText(doc);
					sb.append(temp);
				}
				//TODO 要检查pdf的内容，如果不符合要求的，设置为类型不明。
				//log.info("pdf Content 内容：" + sb.toString());
				byte[] zipStr2 = ZipUtil.zlib(sb.toString(),CharsetUtil.UTF_8,9);
				String temp2 = Base64.encode(zipStr2);	
				if(!uploadIctFileListService.updateSheetData(fileId,temp2)) {
					log.info("fileId:" + fileId + ",sheetData 插入失败");
				}else {
					uploadIctFileListService.updateType(2, fileId);
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private int countNum(String r, String row) {
		int rowLength = row.length();
		int rLength = r.length();
		String r1 = r.substring(0, (rLength - rowLength));
		int r1Length = r1.length();
		if(r1Length == 1) {
			return number.get(r1) - 1;
		}else if(r1Length == 2) {
			return number.get(r1.substring(0, 1)) * 26 + number.get(r1.substring(1)) - 1;
		}
		return -1;
	}
	
	private static void initNumber() {
		number.put("A",1);
		number.put("B",2);
		number.put("C",3);
		number.put("D",4);
		number.put("E",5);
		number.put("F",6);
		number.put("G",7);
		number.put("H",8);
		number.put("I",9);
		number.put("J",10);
		number.put("K",11);
		number.put("L",12);
		number.put("M",13);
		number.put("N",14);
		number.put("O",15);
		number.put("P",16);
		number.put("Q",17);
		number.put("R",18);
		number.put("S",19);
		number.put("T",20);
		number.put("U",21);
		number.put("V",22);
		number.put("W",23);
		number.put("X",24);
		number.put("Y",25);
		number.put("Z",26);
	}
}