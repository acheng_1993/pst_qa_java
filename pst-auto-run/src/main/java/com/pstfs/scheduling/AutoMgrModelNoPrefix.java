package com.pstfs.scheduling;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.MfgMoItemAllEntity;
import com.pstfs.entity.ModelNoPrefixMgrProcessEntity;
import com.pstfs.entity.MstRoutingStepAllEntity;
import com.pstfs.service.ModelNoPrefixMgrProcessService;
import com.pstfs.service.ModelNoPrefixMgrService;
import com.pstfs.service.MstRoutingStepAllService;
import lombok.extern.slf4j.Slf4j;

/**
 * 自动管理制品品番前缀
 * @author jiajian
 */
//@Component
@Slf4j
public class AutoMgrModelNoPrefix {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	
	public AutoMgrModelNoPrefix() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	@Autowired
	private ModelNoPrefixMgrProcessService modelNoPrefixMgrProcessService;
	
	@Autowired
	private MstRoutingStepAllService mstRoutingStepAllService;
	
	@Autowired
	private ModelNoPrefixMgrService modelNoPrefixMgrService;
	
	
	@Value("${spring.datasource.dynamic.datasource.new_mes.driver-class-name}")
	private String driverClassName;
	@Value("${spring.datasource.dynamic.datasource.new_mes.url2}")
	private String url;
	@Value("${spring.datasource.dynamic.datasource.new_mes.username}")
	private String username;
	@Value("${spring.datasource.dynamic.datasource.new_mes.password}")
	private String password;
	
	
//	@Scheduled(cron = "0 0/5 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if(trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				List<MstRoutingStepAllEntity> mstRoutingStepAllEntityList1 = null;
				List<MstRoutingStepAllEntity> mstRoutingStepAllEntityList2 = null;
				String idProcess = null;
				Date createdWhenProcess = null;
				String idProcess2 = null;
				Date createdWhenProcess2 = null;
				ModelNoPrefixMgrProcessEntity modelNoPrefixMgrProcessEntity = null;
				try {
					modelNoPrefixMgrProcessEntity = modelNoPrefixMgrProcessService.getById(0);
					if (modelNoPrefixMgrProcessEntity != null) {
						idProcess = modelNoPrefixMgrProcessEntity.getIdProcess();
						createdWhenProcess = modelNoPrefixMgrProcessEntity.getCreatedWhenProcess();
						idProcess2 = modelNoPrefixMgrProcessEntity.getIdProcess2();
						createdWhenProcess2 = modelNoPrefixMgrProcessEntity.getCreatedWhenProcess2();
					}
					mstRoutingStepAllEntityList1 = mstRoutingStepAllService.getNextDataForId(idProcess);
					if (!mstRoutingStepAllEntityList1.isEmpty()) {
						Set<String> modelNoSet = new HashSet<>();
						for(int i=0,len_i=mstRoutingStepAllEntityList1.size();i<len_i;i++) {
							modelNoSet.add(mstRoutingStepAllEntityList1.get(i).getItmCode());
						}
						if(modelNoPrefixMgrService.distriPrefix(modelNoSet)) {
							String newIdProcess = mstRoutingStepAllEntityList1.get(mstRoutingStepAllEntityList1.size()-1).getId();
							if (modelNoPrefixMgrProcessEntity == null) {
								modelNoPrefixMgrProcessEntity = new ModelNoPrefixMgrProcessEntity();
								modelNoPrefixMgrProcessEntity.setId(0);
								modelNoPrefixMgrProcessEntity.setIdProcess(newIdProcess);
								modelNoPrefixMgrProcessService.save(modelNoPrefixMgrProcessEntity);
							} else {
								modelNoPrefixMgrProcessEntity.setIdProcess(newIdProcess);
								modelNoPrefixMgrProcessService.updateById(modelNoPrefixMgrProcessEntity);
							}
						}
					}
					mstRoutingStepAllEntityList2 = mstRoutingStepAllService.getNextDataForCreatedWhen(createdWhenProcess);
					if (!mstRoutingStepAllEntityList2.isEmpty()) {
						Set<String> modelNoSet = new HashSet<>();
						for(int i=0,len_i=mstRoutingStepAllEntityList2.size();i<len_i;i++) {
							modelNoSet.add(mstRoutingStepAllEntityList2.get(i).getItmCode());
						}
						if (modelNoPrefixMgrService.distriPrefix(modelNoSet)) {
							Date newCreatedWhenProcess = mstRoutingStepAllEntityList2.get(mstRoutingStepAllEntityList2.size()-1).getCreatedWhen();
							if (modelNoPrefixMgrProcessEntity == null) {
								modelNoPrefixMgrProcessEntity = new ModelNoPrefixMgrProcessEntity();
								modelNoPrefixMgrProcessEntity.setId(0);
								modelNoPrefixMgrProcessEntity.setCreatedWhenProcess(newCreatedWhenProcess);
								modelNoPrefixMgrProcessService.save(modelNoPrefixMgrProcessEntity);
							} else {
								modelNoPrefixMgrProcessEntity.setCreatedWhenProcess(newCreatedWhenProcess);
								modelNoPrefixMgrProcessService.updateById(modelNoPrefixMgrProcessEntity);
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (mstRoutingStepAllEntityList1!=null && mstRoutingStepAllEntityList2!=null && mstRoutingStepAllEntityList1.isEmpty() && mstRoutingStepAllEntityList2.isEmpty()) {
					Connection conn = null;
					PreparedStatement ps = null;
					ResultSet rs = null;
					try {
						Class.forName(driverClassName);
						if (createdWhenProcess2 == null) {
							conn = DriverManager.getConnection(url.replace("{{db}}", "bsh_2022"), username, password);
							ps = conn.prepareStatement("select id,created_when,model_no from mfg_mo_item_all_202201 order by created_when asc,id desc limit 2000");
							rs = ps.executeQuery();
							List<MfgMoItemAllEntity> mfgMoItemAllList = new ArrayList<>();
							while(rs.next()) {
								MfgMoItemAllEntity mfgMoItemAll = new MfgMoItemAllEntity();
								mfgMoItemAll.setId(rs.getString("id"));
								mfgMoItemAll.setModelNo(rs.getString("model_no"));
								mfgMoItemAll.setCreatedWhen(rs.getDate("created_when"));
								mfgMoItemAllList.add(mfgMoItemAll);
							}
							rs.close();
							ps.close();
							if (mfgMoItemAllList.isEmpty()) {
								LocalDateTime localDateTime = LocalDateTime.of(2022, Month.of(2), 1, 0, 0);
								createdWhenProcess2 = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
								if (modelNoPrefixMgrProcessEntity == null) {
									modelNoPrefixMgrProcessEntity = new ModelNoPrefixMgrProcessEntity();
									modelNoPrefixMgrProcessEntity.setId(0);
									modelNoPrefixMgrProcessEntity.setCreatedWhenProcess2(createdWhenProcess2);
									modelNoPrefixMgrProcessEntity.setIdProcess2("0");
									modelNoPrefixMgrProcessService.save(modelNoPrefixMgrProcessEntity);
								} else {
									modelNoPrefixMgrProcessEntity.setCreatedWhenProcess2(createdWhenProcess2);
									modelNoPrefixMgrProcessEntity.setIdProcess2("0");
									modelNoPrefixMgrProcessService.updateById(modelNoPrefixMgrProcessEntity);
								}
							} else {
								Map<String,String> modelNoPrefixMap = modelNoPrefixMgrService.loadModelNoPrefixMap(mfgMoItemAllList);
								ps.close();
								ps = conn.prepareStatement("update mfg_mo_item_all_202201 set model_no_prefix=? where id=?");
								conn.setAutoCommit(false);
								for(int i=0,len_i=mfgMoItemAllList.size();i<len_i;i++) {
									MfgMoItemAllEntity currentRow = mfgMoItemAllList.get(i);
									ps.setString(1, modelNoPrefixMap.get(currentRow.getModelNo()));
									ps.setString(2, currentRow.getId());
									ps.addBatch();
									if(i%200 == 0) {
										ps.executeBatch();
										ps.clearBatch();
									}
								}
								ps.executeBatch();
								conn.commit();
								conn.setAutoCommit(true);
								MfgMoItemAllEntity lastRow = mfgMoItemAllList.get(mfgMoItemAllList.size()-1);
								if (modelNoPrefixMgrProcessEntity == null) {
									modelNoPrefixMgrProcessEntity = new ModelNoPrefixMgrProcessEntity();
									modelNoPrefixMgrProcessEntity.setId(0);
									modelNoPrefixMgrProcessEntity.setCreatedWhenProcess2(lastRow.getCreatedWhen());
									modelNoPrefixMgrProcessEntity.setIdProcess2(lastRow.getId());
									modelNoPrefixMgrProcessService.save(modelNoPrefixMgrProcessEntity);
								} else {
									modelNoPrefixMgrProcessEntity.setCreatedWhenProcess2(lastRow.getCreatedWhen());
									modelNoPrefixMgrProcessEntity.setIdProcess2(lastRow.getId());
									modelNoPrefixMgrProcessService.updateById(modelNoPrefixMgrProcessEntity);
								}
							}
						} else {
							LocalDateTime createdWhenProcess2Locale = LocalDateTime.ofInstant(createdWhenProcess2.toInstant(), ZoneId.systemDefault());
							Class.forName(driverClassName);
							int year = createdWhenProcess2Locale.getYear();
							String monthValue = String.format("%02d",createdWhenProcess2Locale.getMonthValue());
							conn = DriverManager.getConnection(url.replace("{{db}}", "bsh_"+year), username, password);
							ps = conn.prepareStatement("select id,created_when,model_no from mfg_mo_item_all_"+year+monthValue+" where "
									+"(created_when=? and id<?) or (created_when>?) order by created_when asc,id desc limit 2000");
							Timestamp createdWhenProcess2SqlTimestamp = new Timestamp(createdWhenProcess2.getTime());
							ps.setTimestamp(1, createdWhenProcess2SqlTimestamp);
							ps.setString(2, idProcess2);
							ps.setTimestamp(3, createdWhenProcess2SqlTimestamp);
							List<MfgMoItemAllEntity> mfgMoItemAllList = new ArrayList<>();
							rs = ps.executeQuery();
							while(rs.next()) {
								MfgMoItemAllEntity mfgMoItemAll = new MfgMoItemAllEntity();
								mfgMoItemAll.setId(rs.getString("id"));
								mfgMoItemAll.setModelNo(rs.getString("model_no"));
								mfgMoItemAll.setCreatedWhen(rs.getTimestamp("created_when"));
								mfgMoItemAllList.add(mfgMoItemAll);
							}
							if (mfgMoItemAllList.isEmpty()) {
								LocalDate now = LocalDate.now();
								if (createdWhenProcess2Locale.getYear() < now.getYear() ||
										(createdWhenProcess2Locale.getYear() == now.getYear() && createdWhenProcess2Locale.getMonthValue() < now.getMonthValue())) {
									createdWhenProcess2Locale = createdWhenProcess2Locale.plusMonths(1);
									createdWhenProcess2Locale = LocalDateTime.of(createdWhenProcess2Locale.getYear(), createdWhenProcess2Locale.getMonthValue(), 1, 0, 0);
									if (modelNoPrefixMgrProcessEntity == null) {
										modelNoPrefixMgrProcessEntity = new ModelNoPrefixMgrProcessEntity();
										modelNoPrefixMgrProcessEntity.setId(0);
										modelNoPrefixMgrProcessEntity.setCreatedWhenProcess2(Date.from(createdWhenProcess2Locale.atZone(ZoneId.systemDefault()).toInstant()));
										modelNoPrefixMgrProcessEntity.setIdProcess2("0");
										modelNoPrefixMgrProcessService.save(modelNoPrefixMgrProcessEntity);
									} else {
										modelNoPrefixMgrProcessEntity.setCreatedWhenProcess2(Date.from(createdWhenProcess2Locale.atZone(ZoneId.systemDefault()).toInstant()));
										modelNoPrefixMgrProcessEntity.setIdProcess2("0");
										modelNoPrefixMgrProcessService.updateById(modelNoPrefixMgrProcessEntity);
									}
								}
							} else {
								Map<String,String> modelNoPrefixMap = modelNoPrefixMgrService.loadModelNoPrefixMap(mfgMoItemAllList);
								ps.close();
								ps = conn.prepareStatement("update mfg_mo_item_all_"+year+monthValue+" set model_no_prefix=? where id=?");
								conn.setAutoCommit(false);
								for(int i=0,len_i=mfgMoItemAllList.size();i<len_i;i++) {
									MfgMoItemAllEntity currentRow = mfgMoItemAllList.get(i);
									ps.setString(1, modelNoPrefixMap.get(currentRow.getModelNo()));
									ps.setString(2, currentRow.getId());
									ps.addBatch();
									if(i%200 == 0) {
										ps.executeBatch();
										ps.clearBatch();
									}
								}
								ps.executeBatch();
								conn.commit();
								conn.setAutoCommit(true);
								MfgMoItemAllEntity lastRow = mfgMoItemAllList.get(mfgMoItemAllList.size()-1);
								if (modelNoPrefixMgrProcessEntity == null) {
									modelNoPrefixMgrProcessEntity = new ModelNoPrefixMgrProcessEntity();
									modelNoPrefixMgrProcessEntity.setId(0);
									modelNoPrefixMgrProcessEntity.setCreatedWhenProcess2(lastRow.getCreatedWhen());
									modelNoPrefixMgrProcessEntity.setIdProcess2(lastRow.getId());
									modelNoPrefixMgrProcessService.save(modelNoPrefixMgrProcessEntity);
								} else {
									modelNoPrefixMgrProcessEntity.setCreatedWhenProcess2(lastRow.getCreatedWhen());
									modelNoPrefixMgrProcessEntity.setIdProcess2(lastRow.getId());
									modelNoPrefixMgrProcessService.updateById(modelNoPrefixMgrProcessEntity);
								}
							}
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					} finally {
						if (rs != null) {
							try {
								rs.close();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
						if(ps!=null) {
							try {
								ps.close();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
						if(conn!=null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
}