package com.pstfs.scheduling;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.BeforeDeptUserSet;
import com.pstfs.entity.DeptUserSet;
import com.pstfs.service.BeforeDeptUserSetService;
import com.pstfs.service.DeptUserSetService;

import lombok.extern.slf4j.Slf4j;

/**
 * 把BeforeDeptUserSet（员工入职前预先录入的基础信息）的数据迁移到DeptUserSet（新OA系统的员工信息表）
 * 
 * @author jiajian
 */
//@Component
@Slf4j
public class BeforeDeptUserSetToDeptUserSet {
	/**
	 * 重试间隔时间：5秒/30秒/1分钟/3分钟/5分钟/10分钟/30分钟
	 */
	private long[] timeSpans;

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放，
	 * 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	
	public BeforeDeptUserSetToDeptUserSet() {
		timeSpans = new long[] { 5, 30, 60, 180, 300, 600, 1800 };
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}

	@Autowired
	private BeforeDeptUserSetService beforeDeptUserSetService;

	@Autowired
	private DeptUserSetService deptUserSetService;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;

	/**
	 * 每天凌晨0点钟，准时执行任务，任务内容：把BeforeDeptUserSet（员工入职前预先录入的基础信息）
	 * 的数据迁移到DeptUserSet（新OA系统的员工信息表），按姓名匹配，仅迁移当天入职的员工。
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if(trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				final LocalDateTime hireDateEnd = LocalDateTime.now(); // 前一天凌晨
				final LocalDateTime hireDateStart = hireDateEnd.minusDays(1); // 今天凌晨
				QueryWrapper<BeforeDeptUserSet> beforeDeptUserSetWrapper = new QueryWrapper<BeforeDeptUserSet>();
				beforeDeptUserSetWrapper.isNull(true, "delete_time");
				beforeDeptUserSetWrapper.between(true, "create_time", hireDateStart, hireDateEnd);
				QueryWrapper<DeptUserSet> deptUserSetWrapper = new QueryWrapper<DeptUserSet>();
				deptUserSetWrapper.or(true, (QueryWrapper<DeptUserSet> t) -> {
					t.between(true, "d1", hireDateStart, hireDateEnd); // 初始入职日期是前一天
				});
				deptUserSetWrapper.or(true, (QueryWrapper<DeptUserSet> t) -> {
					t.between(true, "i2", hireDateStart, hireDateEnd); // 返聘日期是前一天
				});
				for (int i = 0; i < timeSpans.length; i++) {
					try {
						List<DeptUserSet> deptUserSetList = deptUserSetService.list(deptUserSetWrapper);
						if (!deptUserSetList.isEmpty()) {
							List<BeforeDeptUserSet> beforeDeptUserSetList = beforeDeptUserSetService
									.list(beforeDeptUserSetWrapper);
							for (DeptUserSet deptUserSet : deptUserSetList) {
								for (BeforeDeptUserSet beforeDeptUserSet : beforeDeptUserSetList) {
									if (beforeDeptUserSet.getUserName().equals(deptUserSet.getUserName())) {
										deptUserSet.setA22(beforeDeptUserSet.getA22());
										deptUserSet.setA24(beforeDeptUserSet.getA24());
										deptUserSet.setA26(beforeDeptUserSet.getA26());
										deptUserSet.setA28(beforeDeptUserSet.getA28());
										deptUserSet.setA29(beforeDeptUserSet.getA29());
										deptUserSet.setA33(beforeDeptUserSet.getA33());
									}
								}
							}
							deptUserSetService.updateBatchById(deptUserSetList);
						}
						log.info("task1()：已同步数据到新OA系统的dept_user_set（人员信息表），本次同步的数据量是：" + deptUserSetList.size() + "条...");
						break;
					} catch (Exception e) {
						log.error("task1()：执行任务异常，" + timeSpans[i] + "秒后重试", e);
						try {
							TimeUnit.SECONDS.sleep(timeSpans[i]);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
}