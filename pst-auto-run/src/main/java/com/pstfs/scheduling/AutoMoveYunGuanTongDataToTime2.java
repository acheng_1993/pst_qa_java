package com.pstfs.scheduling;
import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.ApplyBillItemCurrentIdEntity;
import com.pstfs.entity.BgImpSpecListViewEntity;
import com.pstfs.entity.ImpreturnEntity;
import com.pstfs.service.ApplyBillItemCurrentIdService;
import com.pstfs.service.ApplybillheadoriginService;
import com.pstfs.service.BgImpSpecListViewService;
import com.pstfs.service.ImpreturnService;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.http.HttpUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 自动地把云关通系统的报关数据返填到time2中
 * 
 * @author jiajian
 */
@Component
@Slf4j
public class AutoMoveYunGuanTongDataToTime2 extends MessageListenerAdapter {

	@Autowired
	private ApplyBillItemCurrentIdService applyBillItemCurrentIdService;

	@Autowired
	private BgImpSpecListViewService bgImpSpecListViewService;

	@Autowired
	private ImpreturnService impreturnService;

	@Autowired
	private ApplybillheadoriginService applybillheadoriginService;

	@Value("${time2BgImpSpecCreateAutoSaveUrl}")
	private String time2BgImpSpecCreateAutoSaveUrl;

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public AutoMoveYunGuanTongDataToTime2() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}

	@Value("${spring.profiles.active}")
	private String springProfilesActive;

	@Value("${time2LockImpInvNo}")
	private String time2LockImpInvNo;

	@Value("${time2LockImpInvNoSid}")
	private String time2LockImpInvNoSid;

	
	
	/**
	 * 每隔 5 分钟检测一次云关通，是否存在报关待返填数据
	 */
	@Scheduled(cron = "0 0/1 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {//springProfilesActive
				try {
					ApplyBillItemCurrentIdEntity applyBillItemCurrentIdEntity = applyBillItemCurrentIdService.getOne(null);
					String currentId = null;
					String currentHandleDate = null;
					if (applyBillItemCurrentIdEntity == null) {
						log.info("ApplyBillItemCurrentIdEntity 为空，初始化");
						currentHandleDate = "19970101";
						currentId = "0";
					} else {
						currentHandleDate = applyBillItemCurrentIdEntity.getHandleDate();
						currentId = applyBillItemCurrentIdEntity.getCurrentId();
						log.info("上次进度：" + currentId);
					}
					log.info("1_");
					String tmp = applyBillItemCurrentIdEntity.getCurrentInvoiceno();
					log.info("1_"+tmp);
					TreeSet<String> allNextInvoiceno = applybillheadoriginService.getAllNextInvoiceno(tmp);
					log.info("2_"+allNextInvoiceno.size());
					TreeSet<String> allNextInvoiceno2 = impreturnService.getAllNextInvoiceno(currentId,currentHandleDate);
					log.info("3_"+allNextInvoiceno2.size()+"_"+currentId+"_"+currentHandleDate);
					int over = allNextInvoiceno.size() - allNextInvoiceno2.size();
					log.info("4_"+over);
					// for (;over>0;over--) {
					// 	allNextInvoiceno.remove(allNextInvoiceno.first());
					// }
					// 记录是否所有元素都匹配
					boolean allElementsMatch = false;
					for (String element : allNextInvoiceno2) {
						if (allNextInvoiceno.contains(element)) {
							allElementsMatch = true;
						}else{
						allElementsMatch = false;
						log.info("5_"+allElementsMatch+"_"+element);
						break;
						}
					}
					log.info("5_"+allElementsMatch);
					
					//allNextInvoiceno2.containsAll(allNextInvoiceno) && allNextInvoiceno.containsAll(allNextInvoiceno2)
					if (allElementsMatch) {
						List<ImpreturnEntity> impreturnList = null;
						do {
							impreturnList = impreturnService.list(currentId,currentHandleDate, 20);
							log.info("6_"+impreturnList.size());
							List<ImpreturnEntity> groupImpreturnList = new ArrayList<>();
							if (!impreturnList.isEmpty()) {
								String invoiceno = impreturnList.get(0).getInvoiceno();

								boolean stop = Boolean.FALSE;
								do {
									int i = 0, len_i = impreturnList.size();
									for (; i < len_i; i++) {
										if (!invoiceno.equals(impreturnList.get(i).getInvoiceno())) {
											stop = Boolean.TRUE;
											if (i > 0) {
												ImpreturnEntity tempImpreturn = impreturnList.get(i - 1);
												currentId = tempImpreturn.getId();
												LocalDateTime declaredate2 = LocalDateTime.ofInstant(tempImpreturn.getDeclaredate().toInstant(), ZoneId.systemDefault());
												currentHandleDate = DateTimeFormatter.ofPattern("yyyyMMdd").format(declaredate2);
												groupImpreturnList.addAll(impreturnList.subList(0, i));
											}
											break;
										}
									}
									if (stop == Boolean.FALSE) {
										groupImpreturnList.addAll(impreturnList);
										ImpreturnEntity tempImpreturn = impreturnList.get(len_i - 1);
										currentId = tempImpreturn.getId();
										LocalDateTime declaredate2 = LocalDateTime.ofInstant(tempImpreturn.getDeclaredate().toInstant(), ZoneId.systemDefault());
										currentHandleDate = DateTimeFormatter.ofPattern("yyyyMMdd").format(declaredate2);
										impreturnList = impreturnService.list(currentId, currentHandleDate, 20);
									}
								} while (stop == Boolean.FALSE && impreturnList.isEmpty() == Boolean.FALSE);
								
								List<BgImpSpecListViewEntity> bgImpSpecListViewEntityList = bgImpSpecListViewService
										.getBgImpSpecListViewEntityList(invoiceno);
								
								if (groupImpreturnList.size() != bgImpSpecListViewEntityList.size()) {
									log.info("云关通可返填数据是：" + groupImpreturnList.size() + " 条，实际需要返填的数据是："
											+ bgImpSpecListViewEntityList.size() + "条，数量不一致，稍后再返填。。。");
									break;
								}
								Map<ItemCdCstmQtyKey, List<BgImpSpecListViewEntity>> bgImpSpecListViewEntityMap = new HashMap<>();
								for (BgImpSpecListViewEntity bgImpSpecListViewEntity : bgImpSpecListViewEntityList) {
									ItemCdCstmQtyKey key = new ItemCdCstmQtyKey(bgImpSpecListViewEntity.getItemCd(),
											bgImpSpecListViewEntity.getCstmQty(),bgImpSpecListViewEntity.getNw());
									if (bgImpSpecListViewEntityMap.containsKey(key)) {
										bgImpSpecListViewEntityMap.get(key).add(bgImpSpecListViewEntity);
									} else {
										List<BgImpSpecListViewEntity> tempList = new ArrayList<>();
										tempList.add(bgImpSpecListViewEntity);
										bgImpSpecListViewEntityMap.put(key, tempList);
									}
								}
								JSONObject postBody = new JSONObject();
								JSONObject formData = new JSONObject();
								postBody.put("formData", formData);
								formData.put("imp_Inv_No", invoiceno);
								JSONArray subList3 = new JSONArray();
								formData.put("subList3", subList3);
								ItemCdCstmQtyKey itemCdCstmQtyKey = new ItemCdCstmQtyKey();
								DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
								for (ImpreturnEntity impreturnEntity : groupImpreturnList) {
									itemCdCstmQtyKey.setItemCd(impreturnEntity.getLjptno());
									BigDecimal qty = impreturnEntity.getQty();
									BigDecimal newQty = new BigDecimal(qty.intValue());
									if (newQty.compareTo(qty) == 0) {
										qty = newQty;
									}
									itemCdCstmQtyKey.setCstmQty(qty);
									
									BigDecimal netweight = impreturnEntity.getNetweight();
									BigDecimal newNetweight = new BigDecimal(netweight.intValue());
									if(newNetweight.compareTo(netweight) == 0) {
										netweight = newNetweight;
									}
									itemCdCstmQtyKey.setNetweight(netweight);
									List<BgImpSpecListViewEntity> bgImpSpecListViewEntityTempList = bgImpSpecListViewEntityMap
											.get(itemCdCstmQtyKey);
									for (BgImpSpecListViewEntity bgImpSpecListViewEntity : bgImpSpecListViewEntityTempList) {
										JSONObject sub = new JSONObject(false);
										sub.put("g2_rcv_Ksq", bgImpSpecListViewEntity.getRcvKsq());
										sub.put("g2_cstm_Ksq", bgImpSpecListViewEntity.getCstmKsq());
										if (bgImpSpecListViewEntity.getCrtnQty() == null) {
											sub.put("g2_crtn_Qty", "");
										} else {
											sub.put("g2_crtn_Qty", bgImpSpecListViewEntity.getCrtnQty());
										}
										sub.put("g2_hb_no", impreturnEntity.getCopinvtno());
										sub.put("g2_hb_seq", impreturnEntity.getItemno2());
										sub.put("g2_htno", impreturnEntity.getContractno());
										sub.put("g2_cstm_dcl_no", impreturnEntity.getCorrespondentryno());
										sub.put("g2_chk_no", impreturnEntity.getBillno());
										sub.put("g2_cstm_dt", impreturnEntity.getDeclaredate().toInstant()
												.atZone(ZoneId.systemDefault()).toLocalDate().format(dtf));
										sub.put("g2_remark", "");
										subList3.add(sub);
									}
								}
								log.info("推送" + groupImpreturnList.size() + "条数据到 time2 的返填接口...");
								if ("dev".equals(springProfilesActive)) {
									FileWriter.create(new File("E:\\json.json")).write(postBody.toString());
								}
								String json = HttpUtil.post(time2BgImpSpecCreateAutoSaveUrl, postBody.toString());
								JSONObject ret = JSONObject.parseObject(json);
								int errorCode = ret.getInteger("errorCode");
								if (errorCode == 1) {
									int errorCode2 = ret.getInteger("errorCode2");
									if (errorCode2 == 1) {
										log.info("发现支付传票 " + invoiceno + " 已经被锁定，现在尝试解锁...");
										Map<String, Object> paramMap = new HashMap<>();
										paramMap.put("sid", time2LockImpInvNoSid);
										paramMap.put("inv_no", invoiceno);
										paramMap.put("lock_flg", 0);
										json = HttpUtil.post(time2LockImpInvNo, paramMap);
										ret = JSONObject.parseObject(json);
										errorCode = ret.getInteger("errorCode");
										if (errorCode == 1) {
											throw new RuntimeException(ret.getString("errorText"));
										} else {
											log.info("支付传票 " + invoiceno + " 已经解锁，现在推送" + groupImpreturnList.size()
											+ "条数据到 time2 的返填接口...");
											json = HttpUtil.post(time2BgImpSpecCreateAutoSaveUrl, postBody.toString());
											ret = JSONObject.parseObject(json);
											errorCode = ret.getInteger("errorCode");
											if (errorCode == 1) {
												throw new RuntimeException(ret.getString("errorText"));
											} else {
												log.info("数据推送成功！");
											}
											log.info("现在重新锁定支付传票 " + invoiceno + " ...");
											paramMap.put("lock_flg", 1);
											json = HttpUtil.post(time2LockImpInvNo, paramMap);
											ret = JSONObject.parseObject(json);
											errorCode = ret.getInteger("errorCode");
											if (errorCode == 1) {
												throw new RuntimeException(ret.getString("errorText"));
											} else {
												log.info("支付传票 " + invoiceno + " 已经重新锁定！");
												applyBillItemCurrentIdService.update(applyBillItemCurrentIdEntity.getId(),
														currentId, invoiceno, currentHandleDate);
											}
										}
									} else {
										throw new RuntimeException(ret.getString("errorText"));
									}
								} else {
									log.info("数据推送成功！");
									applyBillItemCurrentIdService.update(applyBillItemCurrentIdEntity.getId(), currentId,
											invoiceno, currentHandleDate);
								}
							}
						} while (!impreturnList.isEmpty());
					}
				} catch (Exception e) {
					log.info("task1()任务异常...",e);
					log.error("把云关通系统的报关数据返填到time2过程中出错，错误原因：", e);
				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	@EqualsAndHashCode
	public static class ItemCdCstmQtyKey {
		/**
		 * 品番
		 */
		private String itemCd;

		/**
		 * 报关数
		 */
		private BigDecimal cstmQty;
		
		/**
		 * 净重
		 */
		private BigDecimal Netweight;
	}
}