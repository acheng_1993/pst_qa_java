package com.pstfs.scheduling;
import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.pstfs.PstAutoRun;
import com.pstfs.entity.IqcCheckListEntity;
import com.pstfs.entity.ZProdExemFromInspEntity;
import com.pstfs.entity.ZprodExemFromInspCheckRepeatProcessEntity;
import com.pstfs.service.IqcCheckListService;
import com.pstfs.service.ZProdExemFromInspService;
import com.pstfs.service.ZprodExemFromInspCheckRepeatProcessService;

import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 免检部品清单失效日期
 * @author jiajian
 */
@Component
@Slf4j
public class ProdExemFromInspLoseEfficacy {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public ProdExemFromInspLoseEfficacy() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	/**
	 * 发送邮件的用户
	 */
	private final String MAIL_FROM = "pstfs_system03@pst-hk.com";

	/**
	 * 发送邮件的用户密码
	 */
	private final String MAIL_FROM_PASSWORD = "System010";
	
	/**
	 * 操作系统默认的地区id
	 */
	private static final ZoneId SYSTEM_DEFAULT_ZONE_ID = ZoneId.systemDefault();
	
	@Autowired
	private ZProdExemFromInspService zprodExemFromInspService;
	
	@Autowired
	private ZprodExemFromInspCheckRepeatProcessService zprodExemFromInspCheckRepeatProcessService;
	
	@Autowired
	private IqcCheckListService iqcCheckListService;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 日期格式1
	 */
	private static final DateTimeFormatter FORMATTER1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	
	@Scheduled(cron = "0 0/5 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if(trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				try {
					LocalDateTime now = LocalDateTime.now();
					int hour = now.getHour();
					if(hour>= 19 && hour<=20) {
						ZprodExemFromInspCheckRepeatProcessEntity zprodExemFromInspCheckRepeatProcess = zprodExemFromInspCheckRepeatProcessService.getOne(null);
						LocalDateTime remindDate = null;
						if (zprodExemFromInspCheckRepeatProcess != null) {
							Date tempRemindDate = zprodExemFromInspCheckRepeatProcess.getRemindDate();
							remindDate = LocalDateTime.ofInstant(tempRemindDate.toInstant(), ZoneId.systemDefault());
						}
						if (remindDate == null || (now.getDayOfYear() > remindDate.getDayOfYear() || now.getYear() > remindDate.getYear())) {
							StringBuilder sb = new StringBuilder(
								"<!DOCTYPE html>"+
								"<html><head></head>"+
								"<body>"+
									"<h1 style=\"color:red;\">上传进料通知检验单提醒，<a href=\"http://10.199.32.12:8001/pst-app/public/static/index.html\">点击此处进入系统</a>。</h1>"+
								"</body>"+
								"</html>"
							);
							Session session = null;
							{
								Properties pro = new Properties();
								pro.setProperty("mail.host", "smtp.mxhichina.com");
								pro.setProperty("mail.from", MAIL_FROM);
								pro.setProperty("mail.smtp.host", "smtp.mxhichina.com");
								pro.setProperty("mail.transport.protocol","smtp");
								pro.setProperty("mail.smtp.starttls.enable", "true");
								pro.setProperty("mail.smtp.auth","true");
								pro.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // 使用JSSE的SSL
								pro.setProperty("mail.smtp.socketFactory.fallback", "false");
								pro.setProperty("mail.smtp.port", "465");
								pro.setProperty("mail.smtp.socketFactory.port","465");
								session = Session.getDefaultInstance(pro);
							}
							MimeMessage message = new MimeMessage(session);
							message.setFrom(new InternetAddress(MAIL_FROM));
							message.addRecipients(Message.RecipientType.TO,new Address[] {
								new InternetAddress("iqc@pst-hk.com"),
								new InternetAddress("jiajian.lin@pst-hk.com"),
								new InternetAddress("tao.guo@pst-hk.com"),
								new InternetAddress("qaeqc@pst-hk.com")
							});
							message.setSubject("上传进料通知检验单提醒");
							message.setSentDate(new Date());
							Multipart multipart = new MimeMultipart();
							{
								BodyPart html = new MimeBodyPart();
								html.setContent(sb.toString(),"text/html;charset=UTF-8");
								multipart.addBodyPart(html);
							}
							message.setContent(multipart);
							message.saveChanges();
							Transport transport = session.getTransport("smtp");
							try {
								transport.connect("smtp.mxhichina.com", MAIL_FROM, MAIL_FROM_PASSWORD);
								transport.sendMessage(message,message.getAllRecipients());
								if(zprodExemFromInspCheckRepeatProcess == null) {
									zprodExemFromInspCheckRepeatProcess = new ZprodExemFromInspCheckRepeatProcessEntity();
									zprodExemFromInspCheckRepeatProcess.setId(0);
									zprodExemFromInspCheckRepeatProcess.setRemindDate(new Date());
									zprodExemFromInspCheckRepeatProcessService.save(zprodExemFromInspCheckRepeatProcess);
								} else {
									zprodExemFromInspCheckRepeatProcess.setRemindDate(new Date());
									zprodExemFromInspCheckRepeatProcessService.updateById(zprodExemFromInspCheckRepeatProcess);
								}
							} catch (Exception e) {
								log.error(e.getMessage(), e);
							} finally {
								try {
									transport.close();
								} catch (Exception e2) {
								}
							}
						}
					}					
					//检测是否存在重复数据
					boolean repeatDataExists = zprodExemFromInspService.repeatDataExists();
					if (repeatDataExists) {
						long startTime = System.currentTimeMillis();
						//如果存在重复数据，则删除
						for(;;) {
							boolean ret = zprodExemFromInspService.delRepeatData();
							//当没有重复数据被删除、或操作总时长超过30秒，则跳过
							if (ret == false || System.currentTimeMillis() - startTime> 30000) {
								break;
							}
						}
					}
					
					//只有不存在重复数据，才能往下走
					if(repeatDataExists == false || (repeatDataExists && zprodExemFromInspService.repeatDataExists() == false)) {
						//检测是否发现新版本数据
						if (zprodExemFromInspService.discoverNewVersion()) {
							zprodExemFromInspService.handleDataByVersion();
						}
						
						//检测是否存在表 mst_itm_all 和 zprod_exem_from_insp 表的免检信息不一致的数据
						if (zprodExemFromInspService.diffIsIqc()) {
							//保证 mst_itm_all 和 zprod_exem_from_insp 表的数据的一致性
							zprodExemFromInspService.updateDiffIsIqc();
						}
						
						/*
						 * 检测免检清单重是否有三个月内没有签收的品番，有则邮件提示iqc进行清单变更。
						 * 该功能只在每天早上 7:00~8:00 检测
						 */
						now = LocalDateTime.now();
						hour = now.getHour();
						if ("dev".equals(springProfilesActive) || (hour >= 7 && hour <= 8)) {
							List<ZProdExemFromInspEntity> zprodExemFromInspEntityList = zprodExemFromInspService.getLastThreeMonthNotReceipt();
							if (!zprodExemFromInspEntityList.isEmpty()) {
								StringBuilder sb = new StringBuilder(
									"<!DOCTYPE html>"+
									"<html><head>"+
									"<style>"+
										"table td,table th{"+
											"border-right:1px solid;"+
											"border-bottom:1px solid;"+
										"}"+
									"</style></head>"+
									"<body>"+
									"<table style=\"border-collapse: collapse;border-top:1px solid;border-left:1px solid;\">"+
										"<thead>"+
										"<tr><th>品番</th><th>最近签收日期</th><th>品番</th><th>最近签收日期</th><th>品番</th><th>最近签收日期</th><th>品番</th><th>最近签收日期</th></tr>"+
										"</thead>"+
										"<tbody>"
								);
								List<Integer> idList = new ArrayList<>(zprodExemFromInspEntityList.size());
								int i = 0;
								for (int len_i=zprodExemFromInspEntityList.size();i<len_i;i++) {
									if (i%4 == 0) {
										sb.append("<tr>");
									}
									ZProdExemFromInspEntity zprodExemFromInspEntity = zprodExemFromInspEntityList.get(i);
									idList.add(zprodExemFromInspEntity.getId());
									sb.append("<td>").append(zprodExemFromInspEntity.getProductCode()).append("</td>");
									sb.append("<td>").append(zprodExemFromInspEntity.getLastCargReceiptDate().toInstant().atZone(SYSTEM_DEFAULT_ZONE_ID).toLocalDateTime().format(FORMATTER1)).append("</td>");
									if ((i+1)%4 == 0) {
										sb.append("</tr>");
									}
								}
								for(;;i++) {
									if ((i+1)%4 == 0) {
										sb.append("<td></td><td></td></tr></tbody></table>");
										break;
									} else {
										sb.append("<td></td><td></td>");
									}
								}
								sb.append("</body></html>");
								if("dev".equals(springProfilesActive)) {
									FileUtil.writeUtf8String(sb.toString(),new File("E:\\工作文档\\test.html"));
								}
								Session session = null;
								{
									Properties pro = new Properties();
									pro.setProperty("mail.host", "smtp.mxhichina.com");
									pro.setProperty("mail.from", MAIL_FROM);
									pro.setProperty("mail.smtp.host", "smtp.mxhichina.com");
									pro.setProperty("mail.transport.protocol","smtp");
									pro.setProperty("mail.smtp.starttls.enable", "true");
									pro.setProperty("mail.smtp.auth","true");
									pro.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // 使用JSSE的SSL
									pro.setProperty("mail.smtp.socketFactory.fallback", "false");
									pro.setProperty("mail.smtp.port", "465");
									pro.setProperty("mail.smtp.socketFactory.port","465");
									session = Session.getDefaultInstance(pro);
								}
								MimeMessage message = new MimeMessage(session);
								message.setFrom(new InternetAddress(MAIL_FROM));
								message.addRecipients(Message.RecipientType.TO,new Address[] {
									new InternetAddress("iqc@pst-hk.com"),
									new InternetAddress("jiajian.lin@pst-hk.com"),
									new InternetAddress("tao.guo@pst-hk.com"),
									new InternetAddress("qaeqc@pst-hk.com")
								});
								message.setSubject("发现有"+zprodExemFromInspEntityList.size()+"个免检部品在3个月内没有签收信息！");
								message.setSentDate(new Date());
								Multipart multipart = new MimeMultipart();
								{
									BodyPart html = new MimeBodyPart();
									html.setContent(sb.toString(),"text/html;charset=UTF-8");
									multipart.addBodyPart(html);
								}
								message.setContent(multipart);
								message.saveChanges();
								Transport transport = session.getTransport("smtp");
								try {
									transport.connect("smtp.mxhichina.com", MAIL_FROM, MAIL_FROM_PASSWORD);
									transport.sendMessage(message,message.getAllRecipients());
									zprodExemFromInspService.updateLastSendMailDate(idList);
								} catch (Exception e) {
									log.error(e.getMessage(), e);
								} finally {
									try {
										transport.close();
									} catch (Exception e2) {
									}
								}
							}
						}
						//检测是否有新的签收单，如果有则更新免检清单的最近签收日期
						long startTime = System.currentTimeMillis(); 
						for (;;) {
							List<IqcCheckListEntity> iqcCheckListEntityList = iqcCheckListService.getToBeHandleList();
							if (iqcCheckListEntityList.isEmpty()) {
								break;
							}
							zprodExemFromInspService.updateLastCargReceiptDate(iqcCheckListEntityList);
							if(System.currentTimeMillis() - startTime> 30000) {
								break;
							}
						}
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
}