package com.pstfs.scheduling;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.pstfs.PstAutoRun;
import com.pstfs.entity.TechComparePdf;
import com.pstfs.entity.extend.TechComparePdfExtend;
import com.pstfs.service.TechComparePdfService;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 把技术部上传的pdf文档内容录入到数据库
 * 
 * @author jiajian
 */
@Component
@Slf4j
public class ReadPdfContentToDatabase extends MessageListenerAdapter {

	@Autowired
	private TechComparePdfService techComparePdfService;

	@Value("${pdfPath}")
	private String pdfPath;

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public ReadPdfContentToDatabase() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}

	/**
	 * 使用该方法监听，当key失效时执行该方法
	 * 
	 * @param message
	 * @param pattern
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		if("pro".equals(springProfilesActive)) {
			String key = new String(message.getChannel());
			if ("techComparePdf:ReadContent".equals(key)) {
				this.task1();
			}
		}
	}

	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 每隔5分钟，把pdf的内容读取到数据库
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				try {
					boolean exist = techComparePdfService.getWithoutPdfContentExist();
					if (exist) {
						List<TechComparePdfExtend> techComparePdfList;
						try {
							techComparePdfList = techComparePdfService.getWithoutPdfContentList();
						} catch (Exception e) {
							techComparePdfList = new ArrayList<>();
							log.error(e.getMessage(), e);
						}
						List<TechComparePdf> updateList = new ArrayList<>();
						for (TechComparePdfExtend techComparePdf : techComparePdfList) {
							String pdfFullPath = pdfPath + File.separator + techComparePdf.getPath();
							File file = new File(pdfFullPath);
							if (file.exists()) {
								try (PDDocument doc = PDDocument.load(file)) {
									PDFTextStripper textStripper = new PDFTextStripper();
									int pageCount = doc.getNumberOfPages();
									StringBuilder sb = new StringBuilder();
									String temp;
									JSONArray pageArray = JSONUtil.createArray();
									for (int i = 1, len = 0; i <= pageCount; i++) {
										textStripper.setStartPage(i);
										textStripper.setEndPage(i);
										temp = textStripper.getText(doc);
										sb.append(temp);
										len += temp.length();
										pageArray.add(len - 1);
									}
									log.info("pdf Content 内容：" + sb.toString());
									techComparePdf.setPdfContent(sb.toString());
									
									String pdfContent = techComparePdf.getPdfContent();
									Pattern pattern = Pattern.compile("[A-Z\\d][vA-Z\\d-]{7,11}\\s+");
									Matcher matcher = pattern.matcher(pdfContent);
									if (matcher.find(0)) {
										String name = matcher.group().trim();
										String standardDiffDesc = "";
										String equipNumDesc = "";
										int type = 1;
										if (name.matches("^[A-Z][A-Z\\d-]{5}(F|Q)(Z[1-3])[A-Z\\d][A-Z\\d][\\d]$")) {
											char standardDiff = name.charAt(6);
											if (standardDiff == 'F') {
												standardDiffDesc = "制造";
											} else if (standardDiff == 'Q') {
												standardDiffDesc = "QA";
											}
											String equipNum = name.substring(7, 9);
											if ("Z1".equals(equipNum)) {
												equipNumDesc = "C机标准程序";
											} else if ("Z2".equals(equipNum)) {
												equipNumDesc = "新机标准程序";
											} else if ("Z3".equals(equipNum)) {
												equipNumDesc = "PST新机标准程序";
											}
										} else if (name.matches("^[A-Z][A-Z\\d-]{5}(X|Y|Z)[A-Z\\d]$")) {
											char equipNum = name.charAt(6);
											if (equipNum == 'X') {
												equipNumDesc = "TAKAYA旧机标准程序";
											} else if (equipNum == 'Y') {
												equipNumDesc = "TAKAYA新机标准程序";
											} else if (equipNum == 'Z') {
												equipNumDesc = "国产振华机标准程序";
											}
										} else if (name.matches("^[A-Z][A-Z\\d-]{6}(Z[12])[A-Z\\d][A-Z\\d][\\d]$")) {
											String equipNum = name.substring(7, 9);
											if ("Z1".equals(equipNum)) {
												equipNumDesc = "新机标准程序";
											} else if ("Z2".equals(equipNum)) {
												equipNumDesc = "旧机标准程序";
											}
										} else if (name.matches("^[A-Z][A-Z\\d-]{6}[Vv](Z[12])[A-Z\\d][A-Z\\d]$")) {
											standardDiffDesc = "工检程序";
											String equipNum = name.substring(8, 10);
											if ("Z1".equals(equipNum)) {
												equipNumDesc = "新机标准程序";
											} else if ("Z2".equals(equipNum)) {
												equipNumDesc = "旧机标准程序";
											}
										} else if (name.matches("^[\\d][A-Z\\d-]{5}(X|Y|Z)[A-Z\\d]$")) {
											char equipNum = name.charAt(6);
											if (equipNum == 'X') {
												equipNumDesc = "TAKAYA旧机标准程序";
											} else if (equipNum == 'Y') {
												equipNumDesc = "TAKAYA新机标准程序";
											} else if (equipNum == 'Z') {
												equipNumDesc = "国产振华机标准程序";
											}
										} else if (name.matches("^[\\d][A-Z\\d-]{6}(Z[1-5])[A-Z\\d][A-Z\\d]$")) {
											String equipNum = name.substring(7, 9);
											if ("Z1".equals(equipNum)) {
												equipNumDesc = "SAKI机标准程序";
											} else if ("Z2".equals(equipNum)) {
												equipNumDesc = "名古屋机标准程序";
											} else if ("Z3".equals(equipNum)) {
												equipNumDesc = "3D机标准程序";
											} else if ("Z4".equals(equipNum)) {
												equipNumDesc = "镭晨机标准程序";
											} else if ("Z5".equals(equipNum)) {
												equipNumDesc = "钜子机标准程序";
											}
										} else {
											type = 0;
										}
										techComparePdf.setType(type);
										techComparePdf.setTypeDesc(standardDiffDesc + equipNumDesc);
									} else {
										techComparePdf.setType(0);
									}
									techComparePdf.setPdfPageInfo(pageArray.toString());
									updateList.add(techComparePdf);
								} catch (IOException ex) {
									log.error("task1()：执行任务异常，" + pdfFullPath + "文件不存在", ex);
								}
							}
						}
						if(updateList.size()>0) {						
							techComparePdfService.updateBatchById(updateList);
						}
					}
				} catch (Exception e) {
					log.error("task1()：执行任务异常", e);
				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}
}