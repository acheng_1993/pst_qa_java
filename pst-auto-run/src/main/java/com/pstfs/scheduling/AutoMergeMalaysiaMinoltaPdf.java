package com.pstfs.scheduling;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.imageio.ImageIO;
import org.apache.pdfbox.multipdf.LayerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.MalaysiaMinoltaPdfDetailsDataEntity;
import com.pstfs.entity.MalaysiaMinoltaPdfEntity;
import com.pstfs.entity.UploadFileEntity;
import com.pstfs.entity.extend.InqSyukoViewExtend;
import com.pstfs.service.InqSyukoViewService;
import com.pstfs.service.MalaysiaMinoltaPdfDetailsDataService;
import com.pstfs.service.MalaysiaMinoltaPdfService;
import com.pstfs.service.UploadFileService;

import cn.hutool.core.io.FileUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AutoMergeMalaysiaMinoltaPdf extends MessageListenerAdapter {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public AutoMergeMalaysiaMinoltaPdf() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}

	@Value("${spring.profiles.active}")
	private String springProfilesActive;

	@Autowired
	private MalaysiaMinoltaPdfService malaysiaMinoltaPdfService;
	
	@Autowired
	private MalaysiaMinoltaPdfDetailsDataService malaysiaMinoltaPdfDetailsDataService;

	@Autowired
	private UploadFileService uploadFileService;
	
	@Autowired
	private InqSyukoViewService inqSyukoViewService;

	@Value("${pdfPath}")
	private String pdfPath;

	/**
	 * 使用该方法监听，当key失效时执行该方法
	 * 
	 * @param message
	 * @param pattern
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		if ("pro".equals(springProfilesActive)) {
			String key = new String(message.getChannel());
			if ("AutoMergeMalaysiaMinoltaPdf".equals(key)) {
				task1Lock.lock();
				try {
					task1();
				} catch (Exception e) {
					log.error("AutoMergeMalaysiaMinoltaPdf 键值执行任务异常", e);
				} finally {
					task1Lock.unlock();
				}
			} else if("AutoMergeMalaysiaMinoltaSubPdf".equals(key)) {
				task1Lock.lock();
				try {
					task2();
				} catch (Exception e) {
					log.error("AutoMergeMalaysiaMinoltaSubPdf 键值执行任务异常", e);
				} finally {
					task1Lock.unlock();
				}
			}
		}
	}

	/**
	 * 每隔10分钟，自动地把 pdf 的指定页码的内容截取出来, 并单独生成一份新的 pdf。
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
	public void task2() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task2()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					for(;;) {
						List<MalaysiaMinoltaPdfEntity> malaysiaMinoltaPdfList;
						{
							QueryWrapper<MalaysiaMinoltaPdfEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.eq("status", 1);
							queryWrapper.eq("invno_status",0);
							queryWrapper.isNotNull("invno");
							queryWrapper.isNull("invno_upload_file_id");
							queryWrapper.select("id,invno,upload_file_id,merge_file_id");
							malaysiaMinoltaPdfList = malaysiaMinoltaPdfService.list(queryWrapper);
						}
						if (malaysiaMinoltaPdfList.isEmpty()) {
							break;
						}
						for (int i=0,len_i = malaysiaMinoltaPdfList.size();i<len_i;i++) {
							MalaysiaMinoltaPdfEntity malaysiaMinoltaPdfEntity = malaysiaMinoltaPdfList.get(i);
							QueryWrapper<MalaysiaMinoltaPdfDetailsDataEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.eq("upload_file_id", malaysiaMinoltaPdfEntity.getUploadFileId());
							queryWrapper.select("po_no,parts_no,order_qty,page_num,item_no");
							List<MalaysiaMinoltaPdfDetailsDataEntity> malaysiaMinoltaPdfDetailsDataList = malaysiaMinoltaPdfDetailsDataService.list(queryWrapper);
							Map<MalaysiaMinoltaPdfDetailsDataKey,Integer> malaysiaMinoltaPdfDetailsDataMap = new HashMap<>();
							for(int j=0,len_j = malaysiaMinoltaPdfDetailsDataList.size();j<len_j;j++) {
								MalaysiaMinoltaPdfDetailsDataEntity malaysiaMinoltaPdfDetailsDataEntity = malaysiaMinoltaPdfDetailsDataList.get(j);
								malaysiaMinoltaPdfDetailsDataMap.put(new MalaysiaMinoltaPdfDetailsDataKey(malaysiaMinoltaPdfDetailsDataEntity), malaysiaMinoltaPdfDetailsDataEntity.getPageNum());
							}
							List<InqSyukoViewExtend> inqSyukoViewExtendList = inqSyukoViewService.getInqSyukoViewListByInvno(malaysiaMinoltaPdfEntity.getInvno());
							boolean handleRet = false;
							HashSet<Integer> pageNumSet = null;
							if(!inqSyukoViewExtendList.isEmpty()) {
								pageNumSet = new HashSet<>();
								for (int j=0,len_j = inqSyukoViewExtendList.size();j<len_j;j++) {
									InqSyukoViewExtend inqSyukoViewExtend = inqSyukoViewExtendList.get(j);
									MalaysiaMinoltaPdfDetailsDataKey key = new MalaysiaMinoltaPdfDetailsDataKey(inqSyukoViewExtend);
									Integer pageNum = malaysiaMinoltaPdfDetailsDataMap.get(key);
									if (pageNum != null) {
										pageNumSet.add(pageNum);
									}
								}
								handleRet = !pageNumSet.isEmpty();
							}
							UpdateWrapper<MalaysiaMinoltaPdfEntity> updateWrapper = new UpdateWrapper<>();
							if (handleRet) {
								UploadFileEntity uploadFileEntity = uploadFileService.getPathById(malaysiaMinoltaPdfEntity.getMergeFileId());
								Integer invnoUploadFileId = copyToNewPdf(uploadFileEntity.getPath(), pageNumSet);
								updateWrapper.set("invno_status", 1);
								updateWrapper.set("invno_upload_file_id", invnoUploadFileId);
								updateWrapper.eq("id", malaysiaMinoltaPdfEntity.getId());
							} else {
								updateWrapper.set("invno_status", 2);
								updateWrapper.eq("id", malaysiaMinoltaPdfEntity.getId());
							}
							malaysiaMinoltaPdfService.update(updateWrapper);
						}
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task2()：任务结束...");
			task1Lock.unlock();
		}
	}
	
	@Data
	@NoArgsConstructor
	@EqualsAndHashCode
	public static class MalaysiaMinoltaPdfDetailsDataKey {
		private String poNo;
		private String partsNo;
		private String orderQty;
		private String itemNo;
		
		public MalaysiaMinoltaPdfDetailsDataKey(MalaysiaMinoltaPdfDetailsDataEntity entity) {
			poNo = entity.getPoNo();
			partsNo = entity.getPartsNo();
			orderQty = entity.getOrderQty();
			itemNo = entity.getItemNo();
		}
		
		public MalaysiaMinoltaPdfDetailsDataKey(InqSyukoViewExtend entity) {
			poNo = entity.getCustOdrNo();
			int poSplitIdx = poNo.indexOf("-");
			if(poSplitIdx>-1) {
				itemNo = poNo.substring(poSplitIdx + 1);
				poNo = poNo.substring(0,poSplitIdx);
			} else {
				itemNo = "00010";
			}
			partsNo = entity.getEuCustItemCd();
			orderQty = String.valueOf(entity.getShpQty());
		}
	}
	
	/**
	 * 每隔10分钟，解析pdf的内容, 把用户上传的 pdf 内容复制到模板上，然后产生新的 pdf。
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					for (;;) {
						List<MalaysiaMinoltaPdfEntity> malaysiaMinoltaPdfList;
						{
							QueryWrapper<MalaysiaMinoltaPdfEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.eq("status", 0);
							queryWrapper.select("id,upload_file_id");
							malaysiaMinoltaPdfList = malaysiaMinoltaPdfService.list(queryWrapper);
						}
						if (malaysiaMinoltaPdfList.isEmpty()) {
							break;
						}
						List<Integer> uploadFileIdList = new ArrayList<>();
						for (int i = 0, len_i = malaysiaMinoltaPdfList.size(); i < len_i; i++) {
							MalaysiaMinoltaPdfEntity malaysiaMinoltaPdfEntity = malaysiaMinoltaPdfList.get(i);
							uploadFileIdList.add(malaysiaMinoltaPdfEntity.getUploadFileId());
						}
						List<UploadFileEntity> uploadFileList;
						{
							QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
							queryWrapper.in("id", uploadFileIdList);
							queryWrapper.select("id,path");
							uploadFileList = uploadFileService.list(queryWrapper);
						}
						for (int i = 0, len_i = uploadFileList.size(); i < len_i; i++) {
							UploadFileEntity uploadFileEntity = uploadFileList.get(i);
							File file = new File(pdfPath + File.separator + uploadFileEntity.getPath());
							if (file.exists()) {
								try (PDDocument doc = PDDocument.load(file); PDDocument targetDoc = new PDDocument();) {
									int numOfPages = doc.getNumberOfPages();
									boolean[] textValidate = new boolean[1];
									List<MalaysiaMinoltaPdfDetailsDataEntity> qrCodeContentList = loadText(doc, textValidate);
									if (textValidate[0]) {
										copyToTargetDoc(targetDoc, numOfPages); // 把原始模板拷贝到新的 pdf 上
										fillData(targetDoc, qrCodeContentList);
										int pathSeparatorIdx = malaysiaMinoltaPdfTemplatePath.lastIndexOf(File.separator);
										// 新 pdf 的路径，在模板旁边
										String newPdfFullPath = malaysiaMinoltaPdfTemplatePath.substring(0,pathSeparatorIdx) + File.separator + UUID.randomUUID().toString()+ ".tempPdf";
										targetDoc.save(newPdfFullPath);
										File newPdfFile = new File(newPdfFullPath);
										String sha1 = SecureUtil.sha1(newPdfFile);
										UploadFileEntity uploadFileExists = uploadFileService.getBySha1(sha1, "id");
										if (uploadFileExists == null) {
											String targetFilePath = sha1.substring(0, 2) + File.separator + sha1.substring(2) + ".sha1";
											FileUtil.move(newPdfFile, new File(pdfPath + File.separator + targetFilePath), false);
											Integer uploadFileId = uploadFileService.save(sha1, targetFilePath);
											malaysiaMinoltaPdfService.update(uploadFileEntity.getId(), uploadFileId, qrCodeContentList);
										}
									} else {
										UpdateWrapper<MalaysiaMinoltaPdfEntity> updateWrapper = new UpdateWrapper<>();
										updateWrapper.eq("upload_file_id", uploadFileEntity.getId());
										updateWrapper.eq("status", 0);
										updateWrapper.set("status", 2);
										malaysiaMinoltaPdfService.update(updateWrapper);
									}
								} catch (Exception e) {
									throw e;
								}
							}
						}
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}

	@Value("${malaysiaMinoltaPdfTemplatePath}")
	private String malaysiaMinoltaPdfTemplatePath;

	/**
	 * 把数据填充到目标文档中
	 * 
	 * @param targetDoc
	 * @param qrCodeContentList
	 * @throws IOException
	 */
	private void fillData(PDDocument targetDoc, List<MalaysiaMinoltaPdfDetailsDataEntity> qrCodeContentList) throws IOException {
		List<MalaysiaMinoltaPdfDetailsDataEntity> qrCodeContentList2 = new ArrayList<>(qrCodeContentList); 
		int numOfPages = targetDoc.getNumberOfPages();
		for (int i = 1; i <= numOfPages; i++) {
			List<TextAndPos> textAndPosForTextList = new ArrayList<>();
			List<TextAndPos> textAndPosForImgXList = new ArrayList<>();
			List<TextAndPos> textAndPosForImgYList = new ArrayList<>();
			PDFTextStripper stripper = new PDFTextStripper() {
				protected void writeString(String text, List<TextPosition> textPositions) throws IOException {
					if ("No.".equals(text) || "Vendor:".equals(text) || "PO No.".equals(text)
							|| "Storage Location".equals(text) || "Parts No.".equals(text)
							|| "Parts Description".equals(text) || "Item No.".equals(text)
							|| text.matches("^Order\\s+Qty\\.\\s+Unit$") || "Unit".equals(text)
							|| "Quality Inspection".equals(text) || "Delivery Date".equals(text)) {
						TextAndPos textAndPos = new TextAndPos();
						textAndPos.setText(text);
						textAndPos.setTextPositions(textPositions);
						textAndPosForTextList.add(textAndPos);
					} else if ("Total Delivery".equals(text)) {
						TextAndPos textAndPos = new TextAndPos();
						textAndPos.setText(text);
						textAndPos.setTextPositions(textPositions);
						textAndPosForImgXList.add(textAndPos);
					} else if ("Remarks".equals(text)) {
						TextAndPos textAndPos = new TextAndPos();
						textAndPos.setText(text);
						textAndPos.setTextPositions(textPositions);
						textAndPosForImgYList.add(textAndPos);
					}
				}
			};
			stripper.setSortByPosition(true);
			stripper.setStartPage(i);
			stripper.setEndPage(i);
			stripper.getText(targetDoc);

			PDPage page = targetDoc.getPage(i - 1);
			MalaysiaMinoltaPdfDetailsDataEntity currentData = null;
			try (PDPageContentStream contentStream = new PDPageContentStream(targetDoc, page, AppendMode.APPEND, true,
					false)) {
				for (int j = 0, len_j = textAndPosForTextList.size(); j < len_j; j++) {
					TextAndPos textAndPos = textAndPosForTextList.get(j);
					String text = textAndPos.getText();
					List<TextPosition> textPositions = textAndPos.getTextPositions();
					TextPosition firstTextPos = textPositions.get(0);
					TextPosition lastTextPos = textPositions.get(textPositions.size() - 1);
					contentStream.beginText();
					contentStream.setFont(PDType1Font.HELVETICA_BOLD, 8);
					if ("No.".equals(text)) {
						if (qrCodeContentList2.isEmpty()) {
							break;
						}
						currentData = qrCodeContentList2.remove(0);
						contentStream.newLineAtOffset(lastTextPos.getX() + 5, lastTextPos.getEndY());
						contentStream.showText(currentData.getPoNo() + "-" + currentData.getItemNo());
					} else if ("Vendor:".equals(text)) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getVendor());
						contentStream.endText();
						contentStream.beginText();
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - 2 * (firstTextPos.getHeight() + 5));
						contentStream.showText("POWER SUPPLY TECHNOLOGY (HONG KONG)");
					} else if ("PO No.".equals(text)) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getPoNo());
					} else if ("Storage Location".equals(text)) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getStorageLocation());
					} else if ("Parts No.".equals(text)) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getPartsNo());
					} else if ("Parts Description".equals(text)) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getPartsDescription());
					} else if ("Item No.".equals(text)) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getItemNo());
					} else if (text.matches("^Order\\s+Qty\\.\\s+Unit$")) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getOrderQty());
						contentStream.endText();
						contentStream.beginText();
						contentStream.newLineAtOffset(textPositions.get(textPositions.size() - 4).getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getUnit());
					} else if ("Quality Inspection".equals(text)) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getQualityInspection());
					} else if ("Delivery Date".equals(text)) {
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - firstTextPos.getHeight() - 5);
						contentStream.showText(currentData.getDeliveryDate());
						contentStream.endText();
						contentStream.beginText();
						contentStream.newLineAtOffset(firstTextPos.getX(),
								firstTextPos.getEndY() - 8 * firstTextPos.getHeight());
						contentStream.showText(currentData.getBottomText());
					}
					contentStream.endText();
				}
				for (int j = 0, len_j = textAndPosForImgXList.size(); j < len_j; j++) {
					TextAndPos textAndPosX = textAndPosForImgXList.get(j);
					String text = textAndPosX.getText();
					List<TextPosition> textPositionsX = textAndPosX.getTextPositions();
					TextPosition firstTextPosX = textPositionsX.get(0);

					TextAndPos textAndPosY = textAndPosForImgYList.get(j);
					List<TextPosition> textPositionsY = textAndPosY.getTextPositions();
					TextPosition firstTextPosY = textPositionsY.get(0);

					StringBuilder sb = new StringBuilder(currentData.getPartsNo());
					sb.append("\t\t").append(currentData.getVendor());
					sb.append("\t\t\t\t").append(currentData.getStorageLocation());
					sb.append("\t\t").append(currentData.getPoNo()).append(currentData.getItemNo());
					sb.append("\t").append(currentData.getOrderQty());
					sb.append("\t1");
					BufferedImage qrcode = QrCodeUtil.generate(sb.toString(), 75, 75);
					try (ByteArrayOutputStream baos = new ByteArrayOutputStream()){
						ImageIO.write(qrcode, "png", baos);
						PDImageXObject qrcodeXObj = PDImageXObject.createFromByteArray(targetDoc, baos.toByteArray(), text);
						contentStream.drawImage(qrcodeXObj, firstTextPosX.getX() + 2, firstTextPosY.getEndY() - qrcode.getHeight());
					}
				}
			}
		}
	}

	/**
	 * 加载二维码内容
	 * 
	 * @param qrCodeDoc
	 * @return
	 * @throws IOException
	 */
	private List<MalaysiaMinoltaPdfDetailsDataEntity> loadText(PDDocument qrCodeDoc, boolean[] textValidate) throws IOException {
		int numOfPages = qrCodeDoc.getNumberOfPages();
		final int PO_NO_ITEM_NO_LINE_STATUS = 0;
		final int STORAGE_LOCATION_LINE_STATUS = 1;

		final int PO_NO_SUB_LINE_STATUS = 2;
		final int ITEM_NO_SUB_LINE_STATUS = 3;

		final int VENDOR_PO_NO_LINE_STATUS = 4;
		final int VENDOR_PO_NO_STORAGE_LOCATION_LINE_STATUS = 5;
		final int PO_NO_LINE_STATUS = 6;

		final int PARTS_NO_PARTS_DESCRIPTION_ITEM_NO_ORDER_QTY_UNIT_LINE_STATUS = 7;

		final int PARTS_NO_SUB_LINE_STATUS = 8;
		final int PARTS_DESCRIPTION_SUB_LINE_STATUS = 9;
//		final int ITEM_NO_SUB_LINE_STATUS = 2;
		final int ORDER_QTY_SUB_LINE_STATUS = 10;
		final int UNIT_SUB_LINE_STATUS = 11;

		final int QUALITY_INSPECTION_LINE_STATUS = 12;
		final int DELIVERY_DATE_LINE_STATUS = 13;
		final int BOTTOM_TEXT_LINE_STATUS = 14;
		int currentLineStatus = PO_NO_ITEM_NO_LINE_STATUS;
		PDFTextStripper stripper = new PDFTextStripper();
		stripper.setSortByPosition(true);
		stripper.setStartPage(1);
		stripper.setEndPage(numOfPages);
		String text = stripper.getText(qrCodeDoc);
		if(text.trim().isEmpty()) {
			textValidate[0] = false;
			return null;
		}
		String[] lineArray = text.split("\r\n");
		List<MalaysiaMinoltaPdfDetailsDataEntity> malaysiaMinoltaPdfDetailsDataList = new ArrayList<>();
		MalaysiaMinoltaPdfDetailsDataEntity malaysiaMinoltaPdfDetailsData = null;
		for (int i = 0, len_i = lineArray.length; i < len_i; i++) {
			String line = lineArray[i];
			switch (currentLineStatus) {
			case PO_NO_ITEM_NO_LINE_STATUS:
				malaysiaMinoltaPdfDetailsData = new MalaysiaMinoltaPdfDetailsDataEntity();
				int tempRest = malaysiaMinoltaPdfDetailsDataList.size()%3;
				if(tempRest == 0) {
					malaysiaMinoltaPdfDetailsData.setPageNum(malaysiaMinoltaPdfDetailsDataList.size()/3);
				} else {
					malaysiaMinoltaPdfDetailsData.setPageNum((malaysiaMinoltaPdfDetailsDataList.size()-tempRest)/3);
				}
				int subLineStatus = PO_NO_SUB_LINE_STATUS;
				char[] lineCharArray = line.toCharArray();
				char[] tempString = new char[lineCharArray.length];
				int currentIdx = 0;
				for (int j = 0, len_j = lineCharArray.length; j < len_j; j++) {
					char c = lineCharArray[j];
					switch (subLineStatus) {
					case PO_NO_SUB_LINE_STATUS:
						if (c >= '0' && c <= '9') {
							tempString[currentIdx++] = c;
						} else if (c == '-') {
							malaysiaMinoltaPdfDetailsData.setPoNo(new String(tempString, 0, currentIdx));
							currentIdx = 0;
							subLineStatus = ITEM_NO_SUB_LINE_STATUS;
						} else if (c == ' ') {
							continue;
						} else {
							textValidate[0] = false;
							return null;
						}
						break;
					case ITEM_NO_SUB_LINE_STATUS:
						if (c >= '0' && c <= '9') {
							tempString[currentIdx++] = c;
						} else if (c == ' ') {
							continue;
						} else {
							textValidate[0] = false;
							return null;
						}
						break;
					}
				}
				malaysiaMinoltaPdfDetailsData.setItemNo(new String(tempString, 0, currentIdx));
				currentLineStatus = STORAGE_LOCATION_LINE_STATUS;
				break;
			case STORAGE_LOCATION_LINE_STATUS:
				if (line.matches("\\w+")) {
					malaysiaMinoltaPdfDetailsData.setStorageLocation(line);
					currentLineStatus = VENDOR_PO_NO_LINE_STATUS;
					/*
					 * PO No. 混入到括号中的情况 PM90831 POWER SUPPLY TECHNOLOGY (HONG KOPN/GO) No.
					 * 6500152099 KZ01
					 */
				} else if (line.matches("^\\w+\\s+(\\w+\\s+)+(\\(.+\\))\\s+[\\w+\\.]+\\s+\\d+\\s+\\w+$")) {
					malaysiaMinoltaPdfDetailsData.setVendor(line.substring(0, line.indexOf(" ")));
					malaysiaMinoltaPdfDetailsData.setStorageLocation(line.substring(line.lastIndexOf(" ") + 1));
					currentLineStatus = PARTS_NO_PARTS_DESCRIPTION_ITEM_NO_ORDER_QTY_UNIT_LINE_STATUS;
					/*
					 * PO No. 完全没有混入到括号中的情况 PM90831 POWER SUPPLY TECHNOLOGY (HONG KONG) KZ01
					 */
				} else if (line.matches("^\\w+\\s+(\\w+\\s+)+(\\(.+\\))\\s+\\w+$")) {
					malaysiaMinoltaPdfDetailsData.setVendor(line.substring(0, line.indexOf(" ")));
					malaysiaMinoltaPdfDetailsData.setStorageLocation(line.substring(line.lastIndexOf(" ") + 1));
					currentLineStatus = PO_NO_LINE_STATUS;
				} else {
					textValidate[0] = false;
					return null;
				}
				break;
			case VENDOR_PO_NO_LINE_STATUS:
				/*
				 * PO No. 混入到括号中的情况，并且不含 Storage Location PM90831 POWER SUPPLY TECHNOLOGY (HONG
				 * KOPN/GO) No. 6500152099
				 */
				if (line.matches("^\\w+\\s+(\\w+\\s+)+(\\(.+\\))\\s+[\\w+\\.]+\\s+\\d+$")) {
					malaysiaMinoltaPdfDetailsData.setVendor(line.substring(0, line.indexOf(" ")));
					currentLineStatus = PARTS_NO_PARTS_DESCRIPTION_ITEM_NO_ORDER_QTY_UNIT_LINE_STATUS;
					/*
					 * PO No. 完全没有混入到括号中的情况，并且不含 Storage Location PM90831 POWER SUPPLY TECHNOLOGY
					 * (HONG KONG)
					 */
				} else if (line.matches("^\\w+\\s+(\\w+\\s+)+(\\(.+\\))$")) {
					malaysiaMinoltaPdfDetailsData.setVendor(line.substring(0, line.indexOf(" ")));
					currentLineStatus = PO_NO_LINE_STATUS;
				} else {
					textValidate[0] = false;
					return null;
				}
				break;
			case VENDOR_PO_NO_STORAGE_LOCATION_LINE_STATUS:
				/*
				 * PO No. 混入到括号中的情况 PM90831 POWER SUPPLY TECHNOLOGY (HONG KOPN/GO) No.
				 * 6500152099 KZ01
				 */
				if (line.matches("^\\w+\\s+(\\w+\\s+)+(\\(.+\\))\\s+[\\w+\\.]+\\s+\\d+\\s+\\w+$")) {
					malaysiaMinoltaPdfDetailsData.setVendor(line.substring(0, line.indexOf(" ")));
					malaysiaMinoltaPdfDetailsData.setStorageLocation(line.substring(line.lastIndexOf(" ") + 1));
					currentLineStatus = PARTS_NO_PARTS_DESCRIPTION_ITEM_NO_ORDER_QTY_UNIT_LINE_STATUS;
					/*
					 * PO No. 完全没有混入到括号中的情况 PM90831 POWER SUPPLY TECHNOLOGY (HONG KONG) KZ01
					 */
				} else if (line.matches("^\\w+\\s+(\\w+\\s+)+(\\(.+\\))\\s+\\w+$")) {
					malaysiaMinoltaPdfDetailsData.setVendor(line.substring(0, line.indexOf(" ")));
					malaysiaMinoltaPdfDetailsData.setStorageLocation(line.substring(line.lastIndexOf(" ") + 1));
					currentLineStatus = PO_NO_LINE_STATUS;
				} else {
					textValidate[0] = false;
					return null;
				}
				break;
			case PO_NO_LINE_STATUS:
				if (line.matches("^P/O\\s+No\\.\\s+\\d+$")) {
					currentLineStatus = PARTS_NO_PARTS_DESCRIPTION_ITEM_NO_ORDER_QTY_UNIT_LINE_STATUS;
				} else {
					textValidate[0] = false;
					return null;
				}
				break;
			case PARTS_NO_PARTS_DESCRIPTION_ITEM_NO_ORDER_QTY_UNIT_LINE_STATUS:
				subLineStatus = PARTS_NO_SUB_LINE_STATUS;
				lineCharArray = line.toCharArray();
				tempString = new char[lineCharArray.length];
				currentIdx = 0;
				for (int j = 0, len_j = lineCharArray.length; j < len_j; j++) {
					char c = lineCharArray[j];
					switch (subLineStatus) {
					case PARTS_NO_SUB_LINE_STATUS:
						if ((c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z')) {
							tempString[currentIdx++] = c;
						} else if (c == ' ') {
							malaysiaMinoltaPdfDetailsData.setPartsNo(new String(tempString, 0, currentIdx));
							currentIdx = 0;
							subLineStatus = PARTS_DESCRIPTION_SUB_LINE_STATUS;
						} else {
							textValidate[0] = false;
							return null;
						}
						break;
					case PARTS_DESCRIPTION_SUB_LINE_STATUS:
						if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == ' ') {
							tempString[currentIdx++] = c;
						} else if (c >= '0' && c <= '9') {
							malaysiaMinoltaPdfDetailsData.setPartsDescription(new String(tempString, 0, currentIdx));
							currentIdx = 0;
							subLineStatus = ITEM_NO_SUB_LINE_STATUS;
							tempString[currentIdx++] = c;
						} else {
							textValidate[0] = false;
							return null;
						}
						break;
					case ITEM_NO_SUB_LINE_STATUS:
						if (c >= '0' && c <= '9') {
							tempString[currentIdx++] = c;
						} else if (c == ' ') {
							malaysiaMinoltaPdfDetailsData.setItemNo(new String(tempString, 0, currentIdx));
							currentIdx = 0;
							subLineStatus = ORDER_QTY_SUB_LINE_STATUS;
						} else {
							textValidate[0] = false;
							return null;
						}
						break;
					case ORDER_QTY_SUB_LINE_STATUS:
						if (c >= '0' && c <= '9') {
							tempString[currentIdx++] = c;
						} else if (c == ' ') {
							malaysiaMinoltaPdfDetailsData.setOrderQty(new String(tempString, 0, currentIdx));
							currentIdx = 0;
							subLineStatus = UNIT_SUB_LINE_STATUS;
						} else {
							textValidate[0] = false;
							return null;
						}
						break;
					case UNIT_SUB_LINE_STATUS:
						if (c >= 'A' && c <= 'Z') {
							tempString[currentIdx++] = c;
						} else {
							textValidate[0] = false;
							return null;
						}
						break;
					}
				}
				malaysiaMinoltaPdfDetailsData.setUnit(new String(tempString, 0, currentIdx));
				currentLineStatus = QUALITY_INSPECTION_LINE_STATUS;
				break;
			case QUALITY_INSPECTION_LINE_STATUS:
				if (line.matches("^[A-Za-z]+$")) {
					malaysiaMinoltaPdfDetailsData.setQualityInspection(line);
				} else {
					textValidate[0] = false;
					return null;
				}
				currentLineStatus = DELIVERY_DATE_LINE_STATUS;
				break;
			case DELIVERY_DATE_LINE_STATUS:
				if (line.matches("^\\d{4}/\\d{1,2}/\\d{1,2}$")) {
					malaysiaMinoltaPdfDetailsData.setDeliveryDate(line);
				} else {
					textValidate[0] = false;
					return null;
				}
				currentLineStatus = BOTTOM_TEXT_LINE_STATUS;
				break;
			case BOTTOM_TEXT_LINE_STATUS:
				if (line.matches("^.*Konica.+Minolta.+Malaysia.+$")) {
					malaysiaMinoltaPdfDetailsData.setBottomText(line);
					malaysiaMinoltaPdfDetailsDataList.add(malaysiaMinoltaPdfDetailsData);
				} else {
					textValidate[0] = false;
					return null;
				}
				currentLineStatus = PO_NO_ITEM_NO_LINE_STATUS;
				break;
			}
		}

		textValidate[0] = true;
		return malaysiaMinoltaPdfDetailsDataList;
	}

	/**
	 * pdf的文本和位置集合
	 * 
	 * @author jiajian
	 */
	@Data
	public static class TextAndPos {
		String text;
		List<TextPosition> textPositions;
	}

	/**
	 * 把指定的pdf文档的指定页拷贝到新的pdf上
	 * @param path
	 * @param pageNumSet
	 * @throws IOException 
	 */
	private Integer copyToNewPdf(String path, HashSet<Integer> pageNumSet) throws IOException {
		try (
			PDDocument srcDoc = PDDocument.load(new File(pdfPath+File.separator+path));
			PDDocument targetDoc = new PDDocument();
		){
			// 先创建 pageSize 个空白页
			for (int i = 0,len_i=pageNumSet.size(); i < len_i; i++) {
				PDPage clonePage = new PDPage();
				targetDoc.addPage(clonePage);
				PDPageContentStream contentStream = new PDPageContentStream(targetDoc, clonePage, AppendMode.APPEND,
						true);
				contentStream.beginText();
				contentStream.endText();
				contentStream.close();
			}
			// 获取 pageSize 个空白页
			PDPageTree targetPages = targetDoc.getDocumentCatalog().getPages();
			// pdf图层操作工具
			LayerUtility layerUtility = new LayerUtility(targetDoc);
			int i = 0;
			for(Integer pageNum:pageNumSet) {
				// 使用图层工具把原始模板的内容拿出来
				PDFormXObject firstForm = layerUtility.importPageAsForm(srcDoc, pageNum);
				PDPage clonePage = targetPages.get(i);
				layerUtility.wrapInSaveRestore(clonePage);
				// 把原始模板的内容写入到每一页中
				AffineTransform affineTransform = new AffineTransform();
//				affineTransform.scale(1, .95);
				layerUtility.appendFormAsLayer(clonePage, firstForm, affineTransform, i+"t");
				i++;
			}
			int pathSeparatorIdx = malaysiaMinoltaPdfTemplatePath.lastIndexOf(File.separator);
			// 新 pdf 的路径，在模板旁边
			String newPdfFullPath = malaysiaMinoltaPdfTemplatePath.substring(0,pathSeparatorIdx) + File.separator + UUID.randomUUID().toString()+ ".tempPdf";
			File newPdfFile = new File(newPdfFullPath);
			targetDoc.save(newPdfFile);
			String sha1 = SecureUtil.sha1(newPdfFile);
			UploadFileEntity uploadFileExists = uploadFileService.getBySha1(sha1, "id");
			if (uploadFileExists == null) {
				String targetFilePath = sha1.substring(0, 2) + File.separator + sha1.substring(2) + ".sha1";
				FileUtil.move(newPdfFile, new File(pdfPath + File.separator + targetFilePath), false);
				return uploadFileService.save(sha1, targetFilePath);
			} else {
				return uploadFileExists.getId();
			}
		} catch (IOException e) {
			throw e;
		}
	}
	
	/**
	 * 根据原始模板，把模板的内容拷贝到 targetDoc，如果 targetDoc 有 pageSize 页，则 拷贝 pageSize 次。
	 * @param pageSize
	 * @return
	 * @throws IOException
	 */
	private void copyToTargetDoc(PDDocument targetDoc, int pageSize) throws IOException {
		File file = new File(malaysiaMinoltaPdfTemplatePath);
		try (PDDocument srcDoc = PDDocument.load(file)) {
			// 先创建 pageSize 个空白页
			for (int i = 0; i < pageSize; i++) {
				PDPage clonePage = new PDPage();
				targetDoc.addPage(clonePage);
				PDPageContentStream contentStream = new PDPageContentStream(targetDoc, clonePage, AppendMode.APPEND,
						true);
				contentStream.beginText();
				contentStream.endText();
				contentStream.close();
			}
			// 获取 pageSize 个空白页
			PDPageTree targetPages = targetDoc.getDocumentCatalog().getPages();
			// pdf图层操作工具
			LayerUtility layerUtility = new LayerUtility(targetDoc);
			// 使用图层工具把原始模板第1页的内容拿出来
			PDFormXObject firstForm = layerUtility.importPageAsForm(srcDoc, 0);
			for (int i = 0; i < pageSize; i++) {
				PDPage clonePage = targetPages.get(i);
				layerUtility.wrapInSaveRestore(clonePage);
				// 把原始模板的内容写入到每一页中
				AffineTransform affineTransform = new AffineTransform();
				affineTransform.scale(1, .95);
				layerUtility.appendFormAsLayer(clonePage, firstForm, affineTransform, String.valueOf(i));
			}
		} catch (IOException e) {
			throw e;
		}
	}
}
