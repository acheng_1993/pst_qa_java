package com.pstfs.scheduling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.springframework.data.redis.connection.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.pstfs.PstAutoRun;
import com.pstfs.entity.extend.IctCompareRetExtend;
import com.pstfs.service.IctCompareRetService;
import com.pstfs.service.IctCompareUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * 自动发送ict对比结果进度的邮件，当进度发生变化时发送邮件提醒
 * @author jiajian
 */
@Component
@Slf4j
public class IctCompareProgressMail extends MessageListenerAdapter{

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	public IctCompareProgressMail() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	/**
	 * 发送邮件的用户
	 */
	private final String MAIL_FROM = "pstfs_system03@pst-hk.com";

	/**
	 * 发送邮件的用户密码
	 */
	private final String MAIL_FROM_PASSWORD = "System010";
	
	@Autowired
	private IctCompareRetService ictCompareRetService;
	
	@Autowired
	private IctCompareUserService ictCompareUserService;
	
	/**
	 * 使用该方法监听，当key失效时执行该方法
	 * 
	 * @param message
	 * @param pattern
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		if("pro".equals(springProfilesActive)) {
			String key = new String(message.getChannel());
			if ("IctCompareProgressMail".equals(key)) {
				this.task1();
			}
		}
	}
	
	/**
	 * 每隔 10 分钟检测一次，是否存在需要发送的邮件
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "*/10 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {
				try {
					for(;;) {
						List<IctCompareRetExtend> ictCompareRetEntityList = ictCompareRetService.toBeSendEmailList();
						if(ictCompareRetEntityList.isEmpty()) {
							break;
						} else {
							Integer confirmProgress = ictCompareRetEntityList.get(0).getConfirmProgress();
							List<IctCompareRetExtend> tempIctCompareRetEntityList = new ArrayList<>();
							for (int i=0,len_i = ictCompareRetEntityList.size();i<len_i;i++) {
								IctCompareRetExtend ictCompareRetEntity = ictCompareRetEntityList.get(i);
								if (ictCompareRetEntity.getConfirmProgress().equals(confirmProgress)) {
									tempIctCompareRetEntityList.add(ictCompareRetEntity);
								} else {
									sendMail(tempIctCompareRetEntityList);
									tempIctCompareRetEntityList.clear();
									confirmProgress = ictCompareRetEntity.getConfirmProgress();
									tempIctCompareRetEntityList.add(ictCompareRetEntity);
								}
							}
							if (!tempIctCompareRetEntityList.isEmpty()) {
								sendMail(tempIctCompareRetEntityList);
								tempIctCompareRetEntityList.clear();
							}
						}
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
	
	/**
	 * 发送邮件
	 * @param tempIctCompareRetEntityList
	 * @throws MessagingException 
	 * @throws AddressException 
	 */
	private void sendMail(List<IctCompareRetExtend> ictCompareRetEntityList) throws AddressException, MessagingException {
		Integer confirmProgress = ictCompareRetEntityList.get(0).getConfirmProgress();
		List<String> mailList = ictCompareUserService.getMailByRoleType(confirmProgress-1);
		Session session = null;
		{
			Properties pro = new Properties();
			pro.setProperty("mail.host", "smtp.mxhichina.com");
			pro.setProperty("mail.from", MAIL_FROM);
			pro.setProperty("mail.smtp.host", "smtp.mxhichina.com");
			pro.setProperty("mail.transport.protocol","smtp");
			pro.setProperty("mail.smtp.starttls.enable", "true");
			pro.setProperty("mail.smtp.auth","true");
			pro.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // 使用JSSE的SSL
			pro.setProperty("mail.smtp.socketFactory.fallback", "false");
			pro.setProperty("mail.smtp.port", "465");
			pro.setProperty("mail.smtp.socketFactory.port","465");
			session = Session.getDefaultInstance(pro);
//			session.setDebug(true);
		}
		MimeMessage message = new MimeMessage(session);
		
		StringBuilder sb = new StringBuilder(
			"<!DOCTYPE html>"+
			"<html><head>"+
			"<style>"+
				"table td,table th{"+
					"border-right:1px solid;"+
					"border-bottom:1px solid;"+
				"}"+
			"</style></head>"+
			"<body>"+
			"<table style=\"border-collapse: collapse;border-top:1px solid;border-left:1px solid;\">"+
				"<thead>"+
					"<tr><th>对比的原始文件名1</th><th>对比的原始文件名2</th><th>对比类型</th></tr>"+
				"</thead>"+
				"<tbody>"
		);
		List<Integer> idList = new ArrayList<>();
		for(int i=0,len_i=ictCompareRetEntityList.size();i<len_i;i++) {
			IctCompareRetExtend ictCompareRetEntity = ictCompareRetEntityList.get(i);
			idList.add(ictCompareRetEntity.getId());
			int compareType = ictCompareRetEntity.getCompareType();
			String compareDesc = null;
			if (compareType == 0) {
				compareDesc = "partList和ict对比";
			} else if(compareType == 1) {
				compareDesc = "ict和ict对比";
			} else if(compareType == 2) {
				compareDesc = "partList和partList对比";
			}
			sb.append("<tr>")
			.append("<td>").append(ictCompareRetEntity.getCompareSrcName1()).append("</td>")
			.append("<td>").append(ictCompareRetEntity.getCompareSrcName2()).append("</td>")
			.append("<td>").append(compareDesc).append("</td>")
			.append("</tr>");
		}
		sb.append("</tbody></table>");
		
		String confirmProgressDesc = null;
		if(confirmProgress == 1) {
			confirmProgressDesc = "革新课";
		} else if(confirmProgress == 2) {
			confirmProgressDesc = "生技";
		} else if(confirmProgress == 3) {
			confirmProgressDesc = "QA";
		}
		message.setFrom(new InternetAddress(MAIL_FROM));
		Address[] addressArray = new Address[mailList.size()];
		for(int i=0,len_i=addressArray.length;i<len_i;i++) {
			addressArray[i] = new InternetAddress(mailList.get(i));
		}
		message.addRecipients(javax.mail.Message.RecipientType.TO, addressArray);
		message.setSubject("发现有"+ictCompareRetEntityList.size()+"个ICT对比结果需要由“"+confirmProgressDesc+"”确认，请及时处理！");
		message.setSentDate(new Date());
		Multipart multipart = new MimeMultipart();
		{
			BodyPart html = new MimeBodyPart();
			html.setContent(sb.toString(),"text/html;charset=UTF-8");
			multipart.addBodyPart(html);
		}
		message.setContent(multipart);
		message.saveChanges();
		Transport transport = session.getTransport("smtp");
		try {
			transport.connect("smtp.mxhichina.com", MAIL_FROM, MAIL_FROM_PASSWORD);
			transport.sendMessage(message,message.getAllRecipients());
			ictCompareRetService.updateLastSendMailDate(idList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			try {
				transport.close();
			} catch (Exception e2) {
			}
		}
	}
}