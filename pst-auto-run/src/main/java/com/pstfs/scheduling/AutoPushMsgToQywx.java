package com.pstfs.scheduling;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.ScheduledSendMsgEntity;
import com.pstfs.entity.ScheduledSentMsgEntity;
import com.pstfs.service.QyWxApi;
import com.pstfs.service.ScheduledSendMsgService;
import com.pstfs.service.ScheduledSentMsgService;
import lombok.extern.slf4j.Slf4j;

/**
 * 自动推送消息到企业微信
 * @author jiajian
 */
@Component
@Slf4j
public class AutoPushMsgToQywx {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public AutoPushMsgToQywx() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	@Autowired
	private ScheduledSendMsgService scheduledSendMsgService;
	
	@Autowired
	private ScheduledSentMsgService scheduledSentMsgService;
	
	@Autowired
	private QyWxApi qyWxApi;
	
	/**
	 * 每隔 5 分钟检测一次，是否存在到了推送时间，但是未推送的任务
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
//	@Scheduled(cron = "*/59 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if("pro".equals(springProfilesActive)) {				
				List<ScheduledSendMsgEntity> scheduledSendMsgEntityList;
				try {
					do {
						log.info("开始检测是否存在待发送消息...");
						scheduledSendMsgEntityList = scheduledSendMsgService.findToBePushScheduledSendMsgList(10);
						log.info("发现有"+scheduledSendMsgEntityList.size()+"条待发送消息...");
						for(ScheduledSendMsgEntity scheduledSendMsgEntity:scheduledSendMsgEntityList) {
							ScheduledSentMsgEntity scheduledSentMsgEntity = new ScheduledSentMsgEntity();
							scheduledSentMsgEntity.setTitle(scheduledSendMsgEntity.getTitle());
							scheduledSentMsgEntity.setContentText(scheduledSendMsgEntity.getContentText());
							scheduledSentMsgEntity.setContentHtml(scheduledSendMsgEntity.getContentHtml());
							scheduledSentMsgEntity.setScheduledSendMsgId(scheduledSendMsgEntity.getId());
							scheduledSentMsgEntity.setRedirectUrl(scheduledSendMsgEntity.getRedirectUrl());
							int id = scheduledSentMsgService.add(scheduledSentMsgEntity);
							//测试时只发给部分测试人员，或者注释掉不发送
							if("dev".equals(springProfilesActive)) {
//								qyWxApi.messageSendText(scheduledSendMsgEntity.getTitle()+
//										"...<br/><a href=\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wwcaf6bede6d102606&redirect_uri=http%3A%2F%2Fzb.zhuyuns.com%3A8001%2Fpst-qywx-app%2Fpublic%2Findex.php%2Findex%2FIndex%2Flogin&response_type=code&scope=snsapi_base&state=%2FScheduledSentMsg%2F"+id+"#wechat_redirect\">详细请点击这里</a>",
//								"14515","14535","13138");
							//正式版会发给全公司所有员工
							} else if("pro".equals(springProfilesActive)) {
								qyWxApi.messageSendText(scheduledSendMsgEntity.getTitle()+
										"...<br/><a href=\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wwcaf6bede6d102606&redirect_uri=http%3A%2F%2Fzb.zhuyuns.com%3A8001%2Fpst-qywx-app%2Fpublic%2Findex.php%2Findex%2FIndex%2Flogin&response_type=code&scope=snsapi_base&state=%2FScheduledSentMsg%2F"+id+"#wechat_redirect\">详细请点击这里</a>");
							}
							scheduledSentMsgService.updateSendWechat(id);
						}
					} while(!scheduledSendMsgEntityList.isEmpty());
					QueryWrapper<ScheduledSentMsgEntity> queryWrapper = new QueryWrapper<>();
					queryWrapper.eq("send_wechat", 0);
					queryWrapper.select("id,title,redirect_url");
					List<ScheduledSentMsgEntity> scheduledSentMsgEntityList = scheduledSentMsgService.list(queryWrapper);
					for(ScheduledSentMsgEntity scheduledSentMsgEntity:scheduledSentMsgEntityList) {
						//测试时只发给部分测试人员，或者注释掉不发送
						if ("dev".equals(springProfilesActive)) {
//							qyWxApi.messageSendText(scheduledSentMsgEntity.getTitle()+
//									"...<br/><a href=\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wwcaf6bede6d102606&redirect_uri=http%3A%2F%2Fzb.zhuyuns.com%3A8001%2Fpst-qywx-app%2Fpublic%2Findex.php%2Findex%2FIndex%2Flogin&response_type=code&scope=snsapi_base&state=%2FScheduledSentMsg%2F"+scheduledSentMsgEntity.getId()+"#wechat_redirect\">详细请点击这里</a>",
//							"14515","14535","13138");
						//正式版会发给全公司所有员工
						} else if("pro".equals(springProfilesActive)) {
							qyWxApi.messageSendText(scheduledSentMsgEntity.getTitle()+
									"...<br/><a href=\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wwcaf6bede6d102606&redirect_uri=http%3A%2F%2Fzb.zhuyuns.com%3A8001%2Fpst-qywx-app%2Fpublic%2Findex.php%2Findex%2FIndex%2Flogin&response_type=code&scope=snsapi_base&state=%2FScheduledSentMsg%2F"+scheduledSentMsgEntity.getId()+"#wechat_redirect\">详细请点击这里</a>");
						}
						scheduledSentMsgService.updateSendWechat(scheduledSentMsgEntity.getId());
					}
				} catch (Exception e) {
					log.error("自动推送消息功能发送异常，异常原因：",e);
				}
			}
			log.info("task1()：任务结束...");
			task1Lock.unlock();
		}
	}
}