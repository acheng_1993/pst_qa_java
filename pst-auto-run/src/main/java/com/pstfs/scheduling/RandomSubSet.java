package com.pstfs.scheduling;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.pstfs.PstAutoRun;
import com.pstfs.service.RandomSectionNoService;
import com.pstfs.service.RandomSubSetService;

//@Component
//@Slf4j
public class RandomSubSet {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	
	public RandomSubSet() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Autowired
	private RandomSectionNoService randomSectionNoService;
	
	@Autowired
	private RandomSubSetService randomSubSetService;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
//	@Scheduled(cron = "*/59 * * * * ?")
//	public void task1() {
//		boolean trySuccess = task1Lock.tryLock();
//		if (trySuccess) {
//			log.info("task1()：开始执行任务...");
//			if ("pro".equals(springProfilesActive)) {
//				try {
//					RandomSectionNoEntity randomSectionNoEntity = randomSectionNoService.getMaxStartDateData();
//					if (randomSectionNoEntity == null) {
//						randomSectionNoEntity = randomSectionNoService.genRandomSectionNoDCB(0);
//					}
//					HashMap<String, Integer> randomSubSetDCBMap = randomSubSetService.genRandomSubSetDCBMap(1000);
//					randomSubSetService.saveBatch(randomSectionNoEntity.getId(), randomSubSetDCBMap);
//				} catch (Exception e) {
//					log.error(e.getMessage(), e);
//				}
//			}
//			log.info("task1()：结束执行任务...");
//			task1Lock.unlock();
//		}
//	}
}