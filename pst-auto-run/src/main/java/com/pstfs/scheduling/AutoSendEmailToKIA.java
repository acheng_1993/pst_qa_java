package com.pstfs.scheduling;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.activation.DataHandler;
import javax.imageio.ImageIO;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.AutoSendKiaEmailEntity;
import com.pstfs.entity.AutoSendKiaTechissCfgEntity;
import com.pstfs.entity.TechissEntity;
import com.pstfs.entity.extend.AutoSendKiaEmailExtend;
import com.pstfs.entity.extend.TechissEntityExtend;
import com.pstfs.service.AutoSendKiaEmailDetailsService;
import com.pstfs.service.AutoSendKiaEmailService;
import com.pstfs.service.AutoSendKiaTechissCfgService;
import com.pstfs.service.TechissService;
import com.pstfs.service.UsersService;

import cn.hutool.core.lang.UUID;
import cn.hutool.extra.qrcode.QrCodeUtil;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AutoSendEmailToKIA {

	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;

	public AutoSendEmailToKIA() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Autowired
	private TechissService techissService;
	
	@Autowired
	private AutoSendKiaEmailService autoSendKiaEmailService;
	
	@Autowired
	private AutoSendKiaEmailDetailsService autoSendKiaEmailDetailsService;
	
	@Autowired
	private AutoSendKiaTechissCfgService autoSendKiaTechissCfgService;
	
	@Autowired
	private UsersService usersService;
	
	//日期格式
	private final static DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	/**
	 * 发送邮件的用户
	 */
	private final String MAIL_FROM = "pstfs_system03@pst-hk.com";
	
	/**
	 * 发送邮件的用户密码
	 */
	private final String MAIL_FROM_PASSWORD = "System010";
	
	/**
	 * 每隔 10 分钟检测一次，是否存在到了推送时间，但是未推送的任务
	 */
	@Scheduled(cron = "0 0/10 * * * ?")
//	@Scheduled(cron = "0/30 * * * * ?")
	public void task1() {
		boolean trySuccess = task1Lock.tryLock();
		if (trySuccess) {
			log.info("task1()：开始执行任务...");
			if ("pro".equals(springProfilesActive)) {
				try {
					Date now = new Date();
					boolean allowCreateEmail;
					if ("dev".equals(springProfilesActive)) {
						allowCreateEmail = true;
					} else {
						//当天凌晨3点
						Date today3Clock = new Date(LocalDate.now().atTime(3, 0, 0).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
						//当天凌晨6点
						Date today6Clock = new Date(LocalDate.now().atTime(6, 0, 0).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
						allowCreateEmail = now.compareTo(today3Clock)>=0 && now.compareTo(today6Clock)<=0;
//						allowCreateEmail = true;
					}
					//只能在当天凌晨3~6点之间生成邮件，测试版无限制
					if (allowCreateEmail) {
						log.info("到了抓取时间，开始抓取技联网的数据....");
						for(;;) {
							AutoSendKiaTechissCfgEntity autoSendKiaTechissCfgEntity = autoSendKiaTechissCfgService.getOne(null);
							if(autoSendKiaTechissCfgEntity == null) {
								autoSendKiaTechissCfgEntity = new AutoSendKiaTechissCfgEntity();
								LocalDateTime ldt1 = LocalDateTime.now();
								autoSendKiaTechissCfgEntity.setIsdate(ldt1.format(DATE_FORMAT));
								autoSendKiaTechissCfgEntity.setTechissId("0");
							}
							List<TechissEntity> techissList = techissService.getTechissListByMakercd(autoSendKiaTechissCfgEntity.getIsdate(), autoSendKiaTechissCfgEntity.getTechissId());
							log.info("在技联网发现："+techissList.size()+"条待发送数据。");
							if (techissList.isEmpty()) {
								//设置更新时间
								autoSendKiaTechissCfgEntity.setUpdateTime(now);
								if(autoSendKiaTechissCfgEntity.getId() == null) {
									autoSendKiaTechissCfgService.save(autoSendKiaTechissCfgEntity);
								} else {
									autoSendKiaTechissCfgService.updateById(autoSendKiaTechissCfgEntity);
								}
								break;
							}
							autoSendKiaEmailService.saveBatch(techissList);
						}
					}

					Properties pro = new Properties();
					pro.setProperty("mail.host", "smtp.mxhichina.com");
					pro.setProperty("mail.from", MAIL_FROM);
					pro.setProperty("mail.smtp.host", "smtp.mxhichina.com");
					pro.setProperty("mail.transport.protocol","smtp");
					pro.setProperty("mail.smtp.starttls.enable", "true");
					pro.setProperty("mail.smtp.auth","true");
					pro.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // 使用JSSE的SSL
					pro.setProperty("mail.smtp.socketFactory.fallback", "false");
					pro.setProperty("mail.smtp.port", "465");
					pro.setProperty("mail.smtp.socketFactory.port","465");
					
					List<String> operEmailList = usersService.getOperEmail();
					if(!operEmailList.isEmpty()) {
						for(;;) {
							List<AutoSendKiaEmailExtend> autoSendKiaEmailExtendList = autoSendKiaEmailService.getToBeSendEmailRemindList();
							log.info("发现"+autoSendKiaEmailExtendList.size()+"条数据需要在发送前需要由PSTFS确认。。");
							if (autoSendKiaEmailExtendList.isEmpty()) {
								break;
							}
							List<Address> tos = new ArrayList<>();
							for(int i=0,len_i = operEmailList.size();i<len_i;i++) {
								tos.add(new InternetAddress(operEmailList.get(i).trim()));
							}
							Session session = Session.getDefaultInstance(pro);
							MimeMessage message = new MimeMessage(session);
							message.setFrom(new InternetAddress(MAIL_FROM));
							message.addRecipients(Message.RecipientType.TO,tos.toArray(new Address[tos.size()]));
							message.setSubject("发现有"+autoSendKiaEmailExtendList.size()+"个邮件准备发送给加工商，请及时确认！");
							message.setSentDate(now);
							Multipart multipart = new MimeMultipart();
							BodyPart html = new MimeBodyPart();
							StringBuilder sb = new StringBuilder("<table><thead><td>邮件标题</td><td>品番</td><td>加工商编码</td><td>加工商名称</td></thead><tbody>");
							List<Integer> idList = new ArrayList<Integer>(autoSendKiaEmailExtendList.size());
							for (int i=0,len_i=autoSendKiaEmailExtendList.size();i<len_i;i++) {
								sb.append("<tr>");
								AutoSendKiaEmailExtend autoSendKiaEmailExtend = autoSendKiaEmailExtendList.get(i);
								sb.append("<td>").append(autoSendKiaEmailExtend.getTitle()).append("</td>");
								sb.append("<td>").append(autoSendKiaEmailExtend.getPnpa()).append("</td>");
								sb.append("<td>").append(autoSendKiaEmailExtend.getMakerCd()).append("</td>");
								sb.append("<td>").append(autoSendKiaEmailExtend.getMakerName()).append("</td>");
								sb.append("</tr>");
								idList.add(autoSendKiaEmailExtend.getId());
							}
							sb.append("</tbody></table><br/>");
							if ("dev".equals(springProfilesActive)) {
								sb.append("<a href=\"http://10.198.43.172:8080/\">点击这里提交确认</a>");
							} else if("pro".equals(springProfilesActive)) {
								sb.append("<a href=\"http://10.199.32.12:8001/pst-app/public/static/index.html\">点击这里提交确认</a>");
							}
							html.setContent(sb.toString(),"text/html;charset=UTF-8");
							multipart.addBodyPart(html);
							message.setContent(multipart);
							message.saveChanges();
							Transport transport = session.getTransport("smtp");
							transport.connect("smtp.mxhichina.com", MAIL_FROM, MAIL_FROM_PASSWORD);
							transport.sendMessage(message,message.getAllRecipients());
							transport.close();
							autoSendKiaEmailService.updateSendEmailRemind(idList);
						}
					} else {
						log.error("暂无设置操作员，或暂无设置操作员邮箱！");
					}
					for(;;) {
						List<AutoSendKiaEmailEntity> autoSendKiaEmailEntityList = autoSendKiaEmailService.getToBeSendEmailList();
						log.info("发现"+autoSendKiaEmailEntityList.size()+"条需要发送给加工商确认的数据..");
						if (autoSendKiaEmailEntityList.isEmpty()) {
							break;
						}
						List<Address> tos = new ArrayList<>();
						List<Address> ccs = new ArrayList<>();
						int len_i = autoSendKiaEmailEntityList.size();
						List<Integer> autoSendKiaEmailIdList = new ArrayList<>();
						for (int i=0;i<len_i;i++) {
							autoSendKiaEmailIdList.add(autoSendKiaEmailEntityList.get(i).getId());
						}
						Map<Integer, List<String>> uploadMap = autoSendKiaEmailDetailsService.getMapListByAutoSendKiaEmailIds(autoSendKiaEmailIdList);
						Map<Integer,List<TechissEntityExtend>> techissMap = techissService.getTechissMapListByAutoSendKiaEmailIds(autoSendKiaEmailIdList);
						for (int i=0;i<len_i;i++) {
							tos.clear();
							ccs.clear();
							AutoSendKiaEmailEntity emailEntity = autoSendKiaEmailEntityList.get(i);
							List<TechissEntityExtend> techissEntityExtendList = techissMap.get(emailEntity.getId());
							
							//先发送一条信息到 redis，该消息记录确认状态，确认的信息，确认的id
							String uuid = UUID.randomUUID().toString();
							{
								JSONArray jsonArray = new JSONArray();
								jsonArray.add(0); //确认状态：0
								jsonArray.add(emailEntity.getId()); //对应的邮件确认id
								JSONArray jsonArray2 = new JSONArray();
								jsonArray.add(jsonArray2); //需要确认的内容
								for (int j=0,len_j = techissEntityExtendList.size();j<len_j;j++) {
									TechissEntityExtend techissEntityExtend = techissEntityExtendList.get(j);
									JSONObject jsonObject = new JSONObject();
									jsonObject.put("ctrlno", techissEntityExtend.getCtrlno());
									jsonObject.put("doctype", techissEntityExtend.getDoctype());
									jsonObject.put("drawingno", techissEntityExtend.getDrawingno());
									jsonObject.put("pnpa", techissEntityExtend.getPnpa());
									jsonArray2.add(jsonObject);
								}
								redisTemplate.opsForValue().set("AutoSendEmailToMaker:"+uuid, jsonArray.toString(), 24, TimeUnit.HOURS);
							}
							String recipient = emailEntity.getRecipient();
							String[] recipientArray = recipient.split(";");
							for (int j=0,len_j=recipientArray.length;j<len_j;j++) {
								tos.add(new InternetAddress(recipientArray[j].trim()));
							}
							String cc = emailEntity.getCc();
							if (StringUtils.isNotEmpty(cc)) {
								String[] ccArray = cc.split(";");
								for (int j=0,len_j=ccArray.length;j<len_j;j++) {
									ccs.add(new InternetAddress(ccArray[j].trim()));
								}
							}
							Session session = Session.getDefaultInstance(pro);
							MimeMessage message = new MimeMessage(session);
							message.setFrom(new InternetAddress(MAIL_FROM));
							message.addRecipients(Message.RecipientType.TO,tos.toArray(new Address[tos.size()]));
							message.addRecipients(Message.RecipientType.CC,ccs.toArray(new Address[ccs.size()]));
							if(emailEntity.getLastSendDate() == null) {
								message.setSubject(emailEntity.getTitle());
							} else {
								message.setSubject("【重发】"+emailEntity.getTitle());
							}
							message.setSentDate(now);
							Multipart multipart = new MimeMultipart("related");
							{//设置发送邮件的附件内容									
								List<String> uploads = uploadMap.get(emailEntity.getId());
								if(uploads !=null) {
									for(int j=0,len_j=uploads.size();j<len_j;j++) {
										File uploadFile = techissService.getUploadFile(uploads.get(j));
										if (uploadFile!=null) {
											MimeBodyPart mimeBodyPart = new MimeBodyPart();								
											mimeBodyPart.attachFile(uploadFile);
											multipart.addBodyPart(mimeBodyPart);
										}
									}
								}
							}
							
							{//设置发送邮件的主体内容
								BodyPart html = new MimeBodyPart();
								StringBuilder sb = new StringBuilder(emailEntity.getContent()); //邮件原始内容
								sb.append("<br/>"); //附加内容，用于引导客户去确认技术资料
								String url = null; //引导的url
								if ("dev".equals(springProfilesActive)) {
									sb.append("<a href=\"http://10.198.43.172/Techeno/ENO_Latest_Listlist.php\" style=\"color:red;\">这是调试连接，仅在测试版出现！上线版不会出现！</a><br/>");
									url = "http://10.198.43.172:8081#"+uuid;
								} else if("pro".equals(springProfilesActive)) {
									url = "http://218.13.58.56:8001/pst-app/public/static3/index.html#"+uuid;
								}
								sb.append("<a href=\"").append(url).append("\">点击这里提交确认</a>，或扫描下方二维码提交确认。<br/>");

								BufferedImage bufferedImage = QrCodeUtil.generate(url, 300, 300);
								ByteArrayOutputStream os = new ByteArrayOutputStream();
								ImageIO.write(bufferedImage, "jpg", os);
								MimeBodyPart qrcode = new MimeBodyPart();
								qrcode.setDataHandler(new DataHandler(os.toByteArray(),"application/octet-stream"));
								qrcode.setContentID("qrcode"); 
							    multipart.addBodyPart(qrcode); 
								sb.append("<img src=\"cid:qrcode\"/>");
								html.setContent(sb.toString(),"text/html;charset=UTF-8");
								multipart.addBodyPart(html);
							}								
							message.setContent(multipart);
							message.saveChanges();
							Transport transport = session.getTransport("smtp");
							transport.connect("smtp.mxhichina.com", MAIL_FROM, MAIL_FROM_PASSWORD);
							transport.sendMessage(message,message.getAllRecipients());
							transport.close();
							autoSendKiaEmailService.recordSendKiaEmailHistory(emailEntity.getId());
						}
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.info("task1()：结束执行任务...");
			task1Lock.unlock();
		}
	}
}