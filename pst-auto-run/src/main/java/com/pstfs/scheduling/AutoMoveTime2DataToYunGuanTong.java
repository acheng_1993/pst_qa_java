package com.pstfs.scheduling;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pstfs.PstAutoRun;
import com.pstfs.entity.BgImpSpecListViewEntity;
import com.pstfs.entity.ExpapplybilldbtempEntity;
import com.pstfs.entity.ExportTimeTblEntity;
import com.pstfs.entity.ImpapplybillorigindbtempEntity;
import com.pstfs.service.BgImpSpecListViewService;
import com.pstfs.service.ExpapplybilldbtempService;
import com.pstfs.service.ExportTimeTblService;
import com.pstfs.service.ImpapplybillorigindbtempService;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 自动地把time2的数据移动到云关通系统中
 * @author jiajian
 */
@Component
@Slf4j
public class AutoMoveTime2DataToYunGuanTong extends MessageListenerAdapter{

	@Autowired
	private ExportTimeTblService exportTimeTblService;
	
	@Autowired
	private BgImpSpecListViewService bgImpSpecListViewService;
	
	@Autowired
	private ExpapplybilldbtempService expapplybillorigindbtempService;
	
	@Autowired
	private ImpapplybillorigindbtempService impapplybillorigindbtempService;
	
	/**
	 * 中宝公司编码
	 */
	private static final String COMPANY_CODE = "4419940090";
	
	/**
	 * 操作系统默认的地区id
	 */
	private static final ZoneId SYSTEM_DEFAULT_ZONE_ID = ZoneId.systemDefault();
	
	/**
	 * 日期格式1
	 */
	private static final DateTimeFormatter FORMATTER1 = DateTimeFormatter.ofPattern("yyyy/M/d");
	
	/**
	 * task1的锁，当任务执行时会锁定，任务执行完毕时会释放， 保证锁定时就无法结束进程
	 */
	private Lock task1Lock;

	public AutoMoveTime2DataToYunGuanTong() {
		task1Lock = new ReentrantLock();
		PstAutoRun.LOCK_LIST.add(task1Lock);
	}
	
	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	@Value("${time2LockImpInvNo}")
	private String time2LockImpInvNo;
	
	@Value("${time2LockImpInvNoSid}")
	private String time2LockImpInvNoSid;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 使用该方法监听，当key失效时执行该方法
	 * @param message
	 * @param pattern
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		if("pro".equals(springProfilesActive)) {
			String key = new String(message.getChannel());
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("sid", time2LockImpInvNoSid);
			String inv_no = new String(message.getBody());
			paramMap.put("inv_no", inv_no);
			if("AutoMoveTime2DataToYunGuanTongLock".equals(key)) {
				paramMap.put("lock_flg", 1);
				task1Lock.lock();
				try {
					String json = HttpUtil.get(time2LockImpInvNo,paramMap);
					log.info("time2系统锁定传票号返回的json:::"+json);
					JSONObject jsonObject = JSONUtil.parseObj(json);
					int errorCode = jsonObject.getInt("errorCode");
					if(errorCode == 0) {
						pullLockData(inv_no);
						redisTemplate.opsForValue().set("AutoMoveTime2DataToYunGuanTongLockFinish", inv_no, 10, TimeUnit.SECONDS);
					} else if(errorCode == 1) {
						log.error("AutoMoveTime2DataToYunGuanTongLock 调用time2接口出错，错误原因："+jsonObject.getStr("errorText"));	
					}
				} catch (Exception e) {
					log.error("AutoMoveTime2DataToYunGuanTongLock 键值执行任务异常", e);
				} finally {
					task1Lock.unlock();
				}
			} else if("AutoMoveTime2DataToYunGuanTongUnLock".equals(key)) {
				paramMap.put("lock_flg", 0);
				try {
					String json = HttpUtil.get(time2LockImpInvNo,paramMap);
					log.info("time2系统解锁传票号返回的json:::"+json);
					JSONObject jsonObject = JSONUtil.parseObj(json);
					int errorCode = jsonObject.getInt("errorCode");
					if(errorCode == 0) {
						redisTemplate.opsForValue().set("AutoMoveTime2DataToYunGuanTongUnLockFinish", inv_no, 10, TimeUnit.SECONDS);
					} else if(errorCode == 1) {
						log.error("AutoMoveTime2DataToYunGuanTongUnLock 调用time2接口出错，错误原因："+jsonObject.getStr("errorText"));
					}
				} catch (Exception e) {
					log.error("AutoMoveTime2DataToYunGuanTongUnLock 键值执行任务异常", e);
				}
			}
		}
	}
	
	private BigDecimal readFrvc3(String frvc3) {
		char[] frvc3Chars = frvc3.toCharArray();
		final int NORMAL = 0;
		final int NUM = 1;
		final int SPLIT = 2;
		int status = NORMAL;
		List<Character> charArray = new ArrayList<>();
		String max = null;
		for(int i=0,len_i=frvc3Chars.length;i<len_i;i++) {
			char c = frvc3Chars[i];
			switch (status) {
			case NORMAL:
				if (c>='0' && c<='9') {
					status = NUM;
					charArray.add(c);
				}
				break;
			case NUM:
				if(c>='0' && c<='9') {
					charArray.add(c);
				} else if(c == '-' || c == '~' || c == '.' || c == '、') {
					String newMax;
					if(charArray.size() == 0) {
						newMax = "0";
					} else {
						char[] maxChar = new char[charArray.size()];
						for(int j=0,len_j=maxChar.length;j<len_j;j++) {
							maxChar[j] = charArray.get(j);
						}
						newMax = new String(maxChar);
					}
					if(max == null) {
						max = newMax;
					} else {
						if(newMax.compareTo(max)>0) {
							max = newMax;
						}
					}
					charArray.clear();
					status = SPLIT;
				}
				break;
			case SPLIT:
				if(c>='0' && c<='9') {
					status = NUM;
					charArray.add(c);
				}
				break;
			default:
				break;
			}
		}
		String newMax;
		if(charArray.size() == 0) {
			newMax = "0";
		} else {
			char[] maxChar = new char[charArray.size()];
			for(int j=0,len_j=maxChar.length;j<len_j;j++) {
				maxChar[j] = charArray.get(j);
			}
			newMax = new String(maxChar);			
		}
		if(max == null) {
			max = newMax;
		} else {
			if(newMax.compareTo(max)>0) {
				max = newMax;
			}
		}
		charArray.clear();
		return new BigDecimal(max);
	}
	
	/**
	 * 拉取被锁定的传票号码相关的货物累总信息
	 * @param inv_no
	 */
	public void pullLockData(String inv_no) {
		Date now = new Date();
		QueryWrapper<BgImpSpecListViewEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("imp_inv_no", inv_no);
		queryWrapper.orderByAsc("item_cd");
		List<BgImpSpecListViewEntity> bgImpSpecListViewEntityList = bgImpSpecListViewService.list(queryWrapper);
		List<ImpapplybillorigindbtempEntity> impapplybillorigindbtempEntityList = new ArrayList<>();
		BigDecimal maxFrvc3 = null;
		int maxFrvc3Index = -1;
		for(int i=0,len_i=bgImpSpecListViewEntityList.size();i<len_i;i++) {
			BgImpSpecListViewEntity bgImpSpecListViewEntity = bgImpSpecListViewEntityList.get(i);
			String frvc3 = bgImpSpecListViewEntity.getFrvc3();
			if(frvc3 != null && !"#".equals(frvc3)) {
				BigDecimal bigDecimalFrvc3 = readFrvc3(frvc3);
				if(maxFrvc3 == null) {
					maxFrvc3 = bigDecimalFrvc3; 
				} else if(bigDecimalFrvc3.compareTo(maxFrvc3)>0) {
					maxFrvc3Index = i;
					maxFrvc3 = bigDecimalFrvc3;
				}
			}
		}
		int formatLen = 1;
		for(int initSize = bgImpSpecListViewEntityList.size(),initTimes = 10;initSize>initTimes;initTimes*=10) {
			formatLen++;
		}
		String idxFormat = "%0"+formatLen+"d";
		for(int i=0,len_i=bgImpSpecListViewEntityList.size();i<len_i;i++) {
			BgImpSpecListViewEntity bgImpSpecListViewEntity = bgImpSpecListViewEntityList.get(i);
			ImpapplybillorigindbtempEntity impapplybillorigindbtempEntity = createImpapplybillorigindbtempEntity(String.format(idxFormat, i),bgImpSpecListViewEntity,now);
			impapplybillorigindbtempEntity.setItemno(i+1);
			if(maxFrvc3 != null) {
				if(maxFrvc3Index == i) {
					impapplybillorigindbtempEntity.setPcs(maxFrvc3);
				} else {
					impapplybillorigindbtempEntity.setPcs(null);
				}
			}
			impapplybillorigindbtempEntityList.add(impapplybillorigindbtempEntity);
		}
		impapplybillorigindbtempService.replaceDataList(inv_no, impapplybillorigindbtempEntityList);
		QueryWrapper<ExportTimeTblEntity> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.eq("ODD_NO", inv_no);
		List<ExportTimeTblEntity> exportTimeTblEntityList = exportTimeTblService.list(queryWrapper2);
		List<ExpapplybilldbtempEntity> expapplybillorigindbtempEntityList = new ArrayList<>();
		for(int i=0,len_i=exportTimeTblEntityList.size();i<len_i;i++) {
			ExportTimeTblEntity exportTimeTblEntity = exportTimeTblEntityList.get(i);
			ExpapplybilldbtempEntity expapplybillorigindbtempEntity = createExpapplybillorigindbtempEntity(exportTimeTblEntity,now);
			expapplybillorigindbtempEntity.setItemno(i+1);
			expapplybillorigindbtempEntityList.add(expapplybillorigindbtempEntity);
		}
		expapplybillorigindbtempService.replaceDataList(inv_no, expapplybillorigindbtempEntityList);
	}

	/**
	 * 根据time2系统的装箱明细表数据创建一个云关通进口货物信息
	 * @param exportTimeTblEntity
	 * @param now
	 * @return
	 */
	private ExpapplybilldbtempEntity createExpapplybillorigindbtempEntity(ExportTimeTblEntity exportTimeTblEntity,Date now) {
		ExpapplybilldbtempEntity expapplybillorigindbtempEntity = new ExpapplybilldbtempEntity();
		expapplybillorigindbtempEntity.setZextenditemfield1(exportTimeTblEntity.getBoardNo());
		expapplybillorigindbtempEntity.setZextenditemfield6(exportTimeTblEntity.getTotalboxQty());
		expapplybillorigindbtempEntity.setCreatedate(now);
		expapplybillorigindbtempEntity.setOutbillno(exportTimeTblEntity.getOddNo());
		expapplybillorigindbtempEntity.setSectionno("20190511002");
		expapplybillorigindbtempEntity.setCompanycode(COMPANY_CODE);
		expapplybillorigindbtempEntity.setUploaddate(now);
		expapplybillorigindbtempEntity.setId(exportTimeTblEntity.getOddNo()+"||||"+String.format("%04d", exportTimeTblEntity.getRowindex()));
		expapplybillorigindbtempEntity.setOptlock(0);
		expapplybillorigindbtempEntity.setImgexgflag(exportTimeTblEntity.getItmTyp());
		expapplybillorigindbtempEntity.setMaterial(exportTimeTblEntity.getCopgno());
		expapplybillorigindbtempEntity.setZextendheadfield1(exportTimeTblEntity.getTransport());
		
		expapplybillorigindbtempEntity.setDeclarekey(exportTimeTblEntity.getRe());
		
		expapplybillorigindbtempEntity.setPrice(exportTimeTblEntity.getPrice());
		expapplybillorigindbtempEntity.setCurr(exportTimeTblEntity.getCurDesc());
		expapplybillorigindbtempEntity.setVersionno(String.valueOf(exportTimeTblEntity.getVer()));
		expapplybillorigindbtempEntity.setOrderno(exportTimeTblEntity.getOddNo());
		expapplybillorigindbtempEntity.setItemorderno(exportTimeTblEntity.getOddNo());
		expapplybillorigindbtempEntity.setOverallunitptno(exportTimeTblEntity.getCopgno());
		expapplybillorigindbtempEntity.setOverallunitprice(exportTimeTblEntity.getPrice());
		if(exportTimeTblEntity.getQty().intValue()!=0) {
//			expapplybillorigindbtempEntity.setUnitnetweight(new BigDecimal(exportTimeTblEntity.getTotalnetWgt())
//					.divide(new BigDecimal(exportTimeTblEntity.getQty()),RoundingMode.CEILING).floatValue());
//			expapplybillorigindbtempEntity.setUnitgrossweight(new BigDecimal(exportTimeTblEntity.getTotalgrossWgt())
//					.divide(new BigDecimal(exportTimeTblEntity.getQty()),RoundingMode.CEILING).floatValue());
			if(exportTimeTblEntity.getTotalboxQty() == null) {
				expapplybillorigindbtempEntity.setSinglecount(new BigDecimal(1));
			} else {
				expapplybillorigindbtempEntity.setSinglecount(exportTimeTblEntity.getTotalboxQty().divide(exportTimeTblEntity.getQty(),RoundingMode.CEILING));
			}
		}
		expapplybillorigindbtempEntity.setBrandtype(exportTimeTblEntity.getBrandType());
		expapplybillorigindbtempEntity.setTradingqty(exportTimeTblEntity.getQty());
		expapplybillorigindbtempEntity.setInvoicedate(exportTimeTblEntity.getPlDt().toInstant().atZone(SYSTEM_DEFAULT_ZONE_ID).toLocalDate().format(FORMATTER1));
		expapplybillorigindbtempEntity.setTransf(exportTimeTblEntity.getRemark());
		expapplybillorigindbtempEntity.setPalletnum(exportTimeTblEntity.getBoardQty());
		expapplybillorigindbtempEntity.setPcs(exportTimeTblEntity.getTotalboxQty());
		
		expapplybillorigindbtempEntity.setWrap(exportTimeTblEntity.getPackageTyp());
		expapplybillorigindbtempEntity.setQty(exportTimeTblEntity.getQty());
		expapplybillorigindbtempEntity.setNetweight(exportTimeTblEntity.getTotalnetWgt());
		expapplybillorigindbtempEntity.setGrossweight(exportTimeTblEntity.getTotalgrossWgt());
//		expapplybillorigindbtempEntity.setFeecurr(exportTimeTblEntity.getCurId());
		expapplybillorigindbtempEntity.setNote(exportTimeTblEntity.getRemark());
		expapplybillorigindbtempEntity.setNoteitem(exportTimeTblEntity.getRemark());

		expapplybillorigindbtempEntity.setTrainno(exportTimeTblEntity.getSlOddNo());
		expapplybillorigindbtempEntity.setZextendheadfield2(exportTimeTblEntity.getChLicensePlate());
		expapplybillorigindbtempEntity.setZextendheadfield3(exportTimeTblEntity.getHgLicensePlate());
		expapplybillorigindbtempEntity.setZextendheadfield4(exportTimeTblEntity.getVeCustNo());
		expapplybillorigindbtempEntity.setZextendheadfield5(exportTimeTblEntity.getTransitPort());
		expapplybillorigindbtempEntity.setContainersno(exportTimeTblEntity.getCounterInfo());
		return expapplybillorigindbtempEntity;
	}

	/**
	 * 根据time2系统的货物累总表数据创建一个云关通进口货物信息
	 * @param bgImpSpecListViewEntity
	 * @param now
	 * @return
	 */
	private ImpapplybillorigindbtempEntity createImpapplybillorigindbtempEntity(String idx,BgImpSpecListViewEntity bgImpSpecListViewEntity,Date now) {
		ImpapplybillorigindbtempEntity impapplybillorigindbtempEntity = new ImpapplybillorigindbtempEntity();
		impapplybillorigindbtempEntity.setZextenditemfield11(bgImpSpecListViewEntity.getCstmKsq().toString());
		impapplybillorigindbtempEntity.setZextenditemfield1(String.valueOf(bgImpSpecListViewEntity.getFrvc3()));
		impapplybillorigindbtempEntity.setCreatedate(now);
		impapplybillorigindbtempEntity.setInvoiceno(bgImpSpecListViewEntity.getImpInvNo());
		impapplybillorigindbtempEntity.setImgexgflag(bgImpSpecListViewEntity.getItemTyp());
		impapplybillorigindbtempEntity.setSectionno("20190511002");
		impapplybillorigindbtempEntity.setCompanycode(COMPANY_CODE);
		impapplybillorigindbtempEntity.setUploaddate(now);
		impapplybillorigindbtempEntity.setMaterial(bgImpSpecListViewEntity.getItemCd());
		impapplybillorigindbtempEntity.setId(idx+bgImpSpecListViewEntity.getRcvKsq());
		impapplybillorigindbtempEntity.setOptlock(0);
		impapplybillorigindbtempEntity.setUpdateuser(bgImpSpecListViewEntity.getUserId());
		impapplybillorigindbtempEntity.setDeclarekey(bgImpSpecListViewEntity.getSpec());
		impapplybillorigindbtempEntity.setPrice(bgImpSpecListViewEntity.getUPric());
		impapplybillorigindbtempEntity.setAmount(bgImpSpecListViewEntity.getRcvAmnt());
		impapplybillorigindbtempEntity.setCurr(bgImpSpecListViewEntity.getImpCurId());
		impapplybillorigindbtempEntity.setOrderno(bgImpSpecListViewEntity.getPOdrNo());
		impapplybillorigindbtempEntity.setItemorderno(bgImpSpecListViewEntity.getPOdrNo());
		impapplybillorigindbtempEntity.setCutoffdate(bgImpSpecListViewEntity.getEntDt().toInstant().atZone(SYSTEM_DEFAULT_ZONE_ID).toLocalDate().format(FORMATTER1));
		impapplybillorigindbtempEntity.setOverallunitprice(bgImpSpecListViewEntity.getUPric());
		impapplybillorigindbtempEntity.setMarkname(bgImpSpecListViewEntity.getFreeTxt6());
		impapplybillorigindbtempEntity.setBrandtype(bgImpSpecListViewEntity.getItmSpecNm());
		impapplybillorigindbtempEntity.setBusinessdept(bgImpSpecListViewEntity.getDivCd());
		impapplybillorigindbtempEntity.setOrigplace(bgImpSpecListViewEntity.getCyNm());
		impapplybillorigindbtempEntity.setTradingqty(bgImpSpecListViewEntity.getCstmQty());
		if(bgImpSpecListViewEntity.getCrtnQty()!=null && bgImpSpecListViewEntity.getCrtnQty().intValue()!=0) {
			impapplybillorigindbtempEntity.setSinglecount(bgImpSpecListViewEntity.getCstmQty()
					.divide(bgImpSpecListViewEntity.getCrtnQty(),RoundingMode.CEILING));
		}
		impapplybillorigindbtempEntity.setInvoicedate(bgImpSpecListViewEntity.getEntDt().toInstant().atZone(SYSTEM_DEFAULT_ZONE_ID).toLocalDate().format(FORMATTER1));
		impapplybillorigindbtempEntity.setQty(bgImpSpecListViewEntity.getCstmQty());
		impapplybillorigindbtempEntity.setTradingqty(bgImpSpecListViewEntity.getRcvQty());
		impapplybillorigindbtempEntity.setNetweight(bgImpSpecListViewEntity.getNw());
		impapplybillorigindbtempEntity.setGrossweight(bgImpSpecListViewEntity.getFrnm4());
		impapplybillorigindbtempEntity.setOrigincountry(bgImpSpecListViewEntity.getCyNm());
		impapplybillorigindbtempEntity.setZextenditemfield7(new BigDecimal(bgImpSpecListViewEntity.getInspFlg()));
		impapplybillorigindbtempEntity.setZextenditemfield6(bgImpSpecListViewEntity.getCrtnQty());
		return impapplybillorigindbtempEntity;
	}
}