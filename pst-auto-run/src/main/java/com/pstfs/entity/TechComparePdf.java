package com.pstfs.entity;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 技术部需要对比的Pdf
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-06-21 16:20:22
 */
@Data
@TableName("tech_compare_pdf")
public class TechComparePdf implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * pdf文件的id
	 */
	private Integer uploadFileListId;
	
	/**
	 * pdf文件的类型
	 */
	private Integer type;
	
	/**
	 * pdf文件类型的描述
	 */
	private String typeDesc;
	
	/**
	 * pdf文件名
	 */
	private String pdfName;
	/**
	 * 创建日期
	 */
	private Date createTime;
	/**
	 * 修改日期
	 */
	private Date updateTime;
	/**
	 * 由java程序异步读取pdf内容并写入
	 */
	private String pdfContent;

	/**
	 * 由java程序异步读取pdf内容并把pdf页码信息保存
	 */
	private String pdfPageInfo;
	
	
}