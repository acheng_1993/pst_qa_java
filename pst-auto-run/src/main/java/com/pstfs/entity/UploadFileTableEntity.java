package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 涉及到文件上传的表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-13 17:15:53
 */
@Data
@TableName("upload_file_table")
public class UploadFileTableEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 涉及文件上传的表名
	 */
	private String tableName;
	/**
	 * 涉及文件上传的表字段
	 */
	private String fileIdField;

}
