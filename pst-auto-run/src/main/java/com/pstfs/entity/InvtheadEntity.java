package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-03-20 09:49:42
 */
@Data
@TableName("InvtHead")
public class InvtheadEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId
	private String id;
	/**
	 * $column.comments
	 */
	private Date createdate;
	/**
	 * $column.comments
	 */
	private String createuser;
	/**
	 * $column.comments
	 */
	private Date modifydate;
	/**
	 * $column.comments
	 */
	private String modifyuser;
	/**
	 * $column.comments
	 */
	private Integer optlock;
	/**
	 * $column.comments
	 */
	private Date updatedate;
	/**
	 * $column.comments
	 */
	private String updateuser;
	/**
	 * $column.comments
	 */
	private String zextendfield1;
	/**
	 * $column.comments
	 */
	private String zextendfield2;
	/**
	 * $column.comments
	 */
	private String zextendfield3;
	/**
	 * $column.comments
	 */
	private Float zextendfield6;
	/**
	 * $column.comments
	 */
	private Date zextendfield8;
	/**
	 * $column.comments
	 */
	private String zextendfield10;
	/**
	 * $column.comments
	 */
	private String zextendfield11;
	/**
	 * $column.comments
	 */
	private String zextendfield12;
	/**
	 * $column.comments
	 */
	private String zextendfield13;
	/**
	 * $column.comments
	 */
	private String zextendfield14;
	/**
	 * $column.comments
	 */
	private String zextendfield15;
	/**
	 * $column.comments
	 */
	private String zextendfield4;
	/**
	 * $column.comments
	 */
	private String zextendfield5;
	/**
	 * $column.comments
	 */
	private Float zextendfield7;
	/**
	 * $column.comments
	 */
	private Date zextendfield9;
	/**
	 * $column.comments
	 */
	private String agentcode;
	/**
	 * $column.comments
	 */
	private String agentname;
	/**
	 * $column.comments
	 */
	private String agentsccd;
	/**
	 * $column.comments
	 */
	private String appno;
	/**
	 * $column.comments
	 */
	private String attatchuploadstatus;
	/**
	 * $column.comments
	 */
	private Date beforededucdate;
	/**
	 * $column.comments
	 */
	private String billimpexpstatus;
	/**
	 * $column.comments
	 */
	private String billno;
	/**
	 * $column.comments
	 */
	private String billtype;
	/**
	 * $column.comments
	 */
	private String cbssumbatchno;
	/**
	 * $column.comments
	 */
	private Date checkdate;
	/**
	 * $column.comments
	 */
	private String checkname;
	/**
	 * $column.comments
	 */
	private String circulatype;
	/**
	 * $column.comments
	 */
	private String copinvtno;
	/**
	 * $column.comments
	 */
	private String corresponddeclarecode;
	/**
	 * $column.comments
	 */
	private String corresponddeclarename;
	/**
	 * $column.comments
	 */
	private String corresponddeclaresccd;
	/**
	 * $column.comments
	 */
	private String correspondentryno;
	/**
	 * $column.comments
	 */
	private String coverdateexistdiff;
	/**
	 * $column.comments
	 */
	private String cusrecvmsg;
	/**
	 * $column.comments
	 */
	private String cusrecvstatus;
	/**
	 * $column.comments
	 */
	private Date customsdeletedate;
	/**
	 * $column.comments
	 */
	private String customsdeleteuser;
	/**
	 * $column.comments
	 */
	private String dcltypecd;
	/**
	 * $column.comments
	 */
	private String dclcustypecd;
	/**
	 * $column.comments
	 */
	private Date decdeclaredate;
	/**
	 * $column.comments
	 */
	private String decflag;
	/**
	 * $column.comments
	 */
	private String decheadid;
	/**
	 * $column.comments
	 */
	private String decnote;
	/**
	 * $column.comments
	 */
	private String decseqno;
	/**
	 * $column.comments
	 */
	private Integer decserialno;
	/**
	 * $column.comments
	 */
	private String dectype;
	/**
	 * $column.comments
	 */
	private Date declaredate;
	/**
	 * $column.comments
	 */
	private String declarestatus;
	/**
	 * $column.comments
	 */
	private Date deducdate;
	/**
	 * $column.comments
	 */
	private String deducmark;
	/**
	 * $column.comments
	 */
	private String delmodreason;
	/**
	 * $column.comments
	 */
	private String downloadtype;
	/**
	 * $column.comments
	 */
	private String emsno;
	/**
	 * $column.comments
	 */
	private String espdeclarestatus;
	/**
	 * $column.comments
	 */
	private String esptaskid;
	/**
	 * $column.comments
	 */
	private String feemark;
	/**
	 * $column.comments
	 */
	private Float feerate;
	/**
	 * $column.comments
	 */
	private String gendecflag;
	/**
	 * $column.comments
	 */
	private String iccardno;
	/**
	 * $column.comments
	 */
	private String imgexgflag;
	/**
	 * $column.comments
	 */
	private Date impexpdate;
	/**
	 * $column.comments
	 */
	private String impexpflag;
	/**
	 * $column.comments
	 */
	private String inoroutno;
	/**
	 * $column.comments
	 */
	private String inputcode;
	/**
	 * $column.comments
	 */
	private Date inputdate;
	/**
	 * $column.comments
	 */
	private String inputname;
	/**
	 * $column.comments
	 */
	private String inputsccd;
	/**
	 * $column.comments
	 */
	private String insurmark;
	/**
	 * $column.comments
	 */
	private Float insurrate;
	/**
	 * $column.comments
	 */
	private String invtheadparamid;
	/**
	 * $column.comments
	 */
	private Boolean isespfreeze;
	/**
	 * $column.comments
	 */
	private Boolean isfused;
	/**
	 * $column.comments
	 */
	private Boolean isfuseduploadesp;
	/**
	 * $column.comments
	 */
	private Boolean ismakedec;
	/**
	 * $column.comments
	 */
	private Boolean isrecheck;
	/**
	 * $column.comments
	 */
	private Boolean istodc;
	/**
	 * $column.comments
	 */
	private String itemcuscurrnames;
	/**
	 * $column.comments
	 */
	private Float levyblamt;
	/**
	 * $column.comments
	 */
	private String needentrymodified;
	/**
	 * $column.comments
	 */
	private String note;
	/**
	 * $column.comments
	 */
	private String othermark;
	/**
	 * $column.comments
	 */
	private Float otherrate;
	/**
	 * $column.comments
	 */
	private String ownercode;
	/**
	 * $column.comments
	 */
	private String ownername;
	/**
	 * $column.comments
	 */
	private String ownersccd;
	/**
	 * $column.comments
	 */
	private String portheadid;
	/**
	 * $column.comments
	 */
	private Date recheckdate;
	/**
	 * $column.comments
	 */
	private String recheckname;
	/**
	 * $column.comments
	 */
	private String recheckopinion;
	/**
	 * $column.comments
	 */
	private String recheckstatus;
	/**
	 * $column.comments
	 */
	private String relativebillno;
	/**
	 * $column.comments
	 */
	private String relativedeclarecode;
	/**
	 * $column.comments
	 */
	private String relativedeclarename;
	/**
	 * $column.comments
	 */
	private String relativedeclaresccd;
	/**
	 * $column.comments
	 */
	private String relativeemsno;
	/**
	 * $column.comments
	 */
	private String relativeentryno;
	/**
	 * $column.comments
	 */
	private String relativeownercode;
	/**
	 * $column.comments
	 */
	private String relativeownername;
	/**
	 * $column.comments
	 */
	private String relativeownersccd;
	/**
	 * $column.comments
	 */
	private String relativetradecode;
	/**
	 * $column.comments
	 */
	private String relativetradename;
	/**
	 * $column.comments
	 */
	private String relativetradesccd;
	/**
	 * $column.comments
	 */
	private String releasemark;
	/**
	 * $column.comments
	 */
	private String secondlinesign;
	/**
	 * $column.comments
	 */
	private String seqno;
	/**
	 * $column.comments
	 */
	private Integer serialno;
	/**
	 * $column.comments
	 */
	private Date signdate;
	/**
	 * $column.comments
	 */
	private String signstatus;
	/**
	 * $column.comments
	 */
	private String signer;
	/**
	 * $column.comments
	 */
	private String taskid;
	/**
	 * $column.comments
	 */
	private Float totalamount;
	/**
	 * $column.comments
	 */
	private String tradecode;
	/**
	 * $column.comments
	 */
	private String tradename;
	/**
	 * $column.comments
	 */
	private String tradesccd;
	/**
	 * $column.comments
	 */
	private String trainno;
	/**
	 * $column.comments
	 */
	private String company;
	/**
	 * $column.comments
	 */
	private String brokercorp;
	/**
	 * $column.comments
	 */
	private String country;
	/**
	 * $column.comments
	 */
	private String custommaster;
	/**
	 * $column.comments
	 */
	private String drivercorp;
	/**
	 * $column.comments
	 */
	private String espbrokercorp;
	/**
	 * $column.comments
	 */
	private String feecurr;
	/**
	 * $column.comments
	 */
	private String ieport;
	/**
	 * $column.comments
	 */
	private String insurcurr;
	/**
	 * $column.comments
	 */
	private String othercurr;
	/**
	 * $column.comments
	 */
	private String scmcoc;
	/**
	 * $column.comments
	 */
	private String trade;
	/**
	 * $column.comments
	 */
	private String tradearea;
	/**
	 * $column.comments
	 */
	private String transac;
	/**
	 * $column.comments
	 */
	private String transf;

}
