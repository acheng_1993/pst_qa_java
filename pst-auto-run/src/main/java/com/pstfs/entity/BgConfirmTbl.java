package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
@Data
@TableName("tkr.bg_confirm_tbl")
public class BgConfirmTbl {

	/**
	 * 锁定状态
	 */
	private Integer efctTyp;
	
	/**
	 * 传票号
	 */
	private String invNo;
}