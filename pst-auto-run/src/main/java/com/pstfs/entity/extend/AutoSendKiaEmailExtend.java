package com.pstfs.entity.extend;
import com.pstfs.entity.AutoSendKiaEmailEntity;
import lombok.Data;

/**
 * 技联网自动发送邮件的扩展类
 * @author jiajian
 */
@Data
public class AutoSendKiaEmailExtend extends AutoSendKiaEmailEntity{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 加工商名称
	 */
	private String makerName;
	/**
	 * 加工商编码
	 */
	private String makerCd;
	
	/**
	 * 整数扩展列1
	 */
	private Integer extendIntCol1;
}