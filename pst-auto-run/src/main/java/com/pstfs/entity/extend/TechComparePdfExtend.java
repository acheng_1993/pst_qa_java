package com.pstfs.entity.extend;
import com.pstfs.entity.TechComparePdf;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TechComparePdfExtend extends TechComparePdf{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * pdf文件的相对路径
	 */
	private String path;
}