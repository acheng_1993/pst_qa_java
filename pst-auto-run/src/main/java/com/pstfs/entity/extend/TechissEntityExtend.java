package com.pstfs.entity.extend;
import com.pstfs.entity.TechissEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TechissEntityExtend extends TechissEntity{
	
	/**
	 * 
	 */
	private String drawingno;
	
	/**
	 * 文件大小
	 */
	private Long fileSize;
	
	public TechissEntityExtend(TechissEntity techissEntity) {
		setId(techissEntity.getId());
		setCtrlno(techissEntity.getCtrlno());
		setCategory(techissEntity.getCategory());
		setDoctype(techissEntity.getDoctype());
		setPnpa(techissEntity.getPnpa());
		setPn(techissEntity.getPn());
		setStatus(techissEntity.getStatus());
		setIssno(techissEntity.getIssno());
		setIssTy(techissEntity.getIssTy());
		setIsdate(techissEntity.getIsdate());
		setMakercd(techissEntity.getMakercd());
		setRemark(techissEntity.getRemark());
		setUpload(techissEntity.getUpload());
		setIssto(techissEntity.getIssto());
		setIdent(techissEntity.getIdent());
		setDtent(techissEntity.getDtent());
		setViewable(techissEntity.getViewable());
	}
}
