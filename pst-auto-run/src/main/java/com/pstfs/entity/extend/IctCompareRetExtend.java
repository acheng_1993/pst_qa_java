package com.pstfs.entity.extend;
import com.pstfs.entity.IctCompareRetEntity;
import lombok.Data;

@Data
public class IctCompareRetExtend extends IctCompareRetEntity{

	private static final long serialVersionUID = 1L;
	
	private String compareSrcName1;
	
	private String compareSrcName2;
}
