package com.pstfs.entity.extend;
import java.util.Date;
import java.util.List;
import com.pstfs.entity.InqSyukoView;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class InqSyukoViewExtend extends InqSyukoView{
	private Double pacSz;
	private Date invDt;
	private Integer pacQty;
	
	/**
	 * 分单号列表
	 */
	private List<String> copinvtnoList;
	/**
	 * 分单序号列表
	 */
	private List<Integer> itemNo2List;
	/**
	 * 合同番号（手册号）
	 */
	private List<String> contractnoList;
	/**
	 * 报关单号
	 */
	private List<String> corresPondEntryNoList;
	/**
	 * 核注清单号
	 */
	private List<String> billNoList;
	/**
	 * 申报日期
	 */
	private List<Date> declaredateList;
	/**
	 * 申报数量
	 */
	private List<Float> qtyList;
	/**
	 * 申报总价
	 */
	private List<Float> amountList;
	
	public InqSyukoViewExtend() {
		super();
	}
	
	/**
	 * 
	 * @param inqSyukoView
	 */
	public InqSyukoViewExtend(InqSyukoView inqSyukoView) {
		super();
		setInvno(inqSyukoView.getInvno());
		setCustInstNo(inqSyukoView.getCustInstNo());
		setClntNm(inqSyukoView.getClntNm());
		setCustOdrNo(inqSyukoView.getCustOdrNo());
		setCustItemCd(inqSyukoView.getCustItemCd());
		setPkgCrtnNo(inqSyukoView.getPkgCrtnNo());
		setPkgCrtnQty(inqSyukoView.getPkgCrtnQty());
		setSOdrNo(inqSyukoView.getSOdrNo());
		setEuCustItemCd(inqSyukoView.getEuCustItemCd());
		setItmNm(inqSyukoView.getItmNm());
		setCustCd(inqSyukoView.getCustCd());
		setPkgQty(inqSyukoView.getPkgQty());
		setShpQty(inqSyukoView.getShpQty());
		setPkgNw(inqSyukoView.getPkgNw());
		setPkgGw(inqSyukoView.getPkgGw());
		setPkgSz(inqSyukoView.getPkgSz());
		setShpDt(inqSyukoView.getShpDt());
		setFcId(inqSyukoView.getFcId());
		setShpKsq(inqSyukoView.getShpKsq());
		setShpTyp(inqSyukoView.getShpTyp());
		setSOdrTyp(inqSyukoView.getSOdrTyp());
		setDivCd(inqSyukoView.getDivCd());
		setClntSnm(inqSyukoView.getClntSnm());
		setCustWhseTyp(inqSyukoView.getCustWhseTyp());
		setCustGPTyp(inqSyukoView.getCustGPTyp());
		setDlvNo(inqSyukoView.getDlvNo());
		setCorrTyp(inqSyukoView.getCorrTyp());
		setCustDueDt(inqSyukoView.getCustDueDt());
		setClsfCd(inqSyukoView.getClsfCd());
		setItemCd(inqSyukoView.getItemCd());
		setShpAmnt(inqSyukoView.getShpAmnt());
		setPrdNo(inqSyukoView.getPrdNo());
		setSOdrQty(inqSyukoView.getSOdrQty());
		setShpInstNo(inqSyukoView.getShpInstNo());
		setDDlvFlg(inqSyukoView.getDDlvFlg());
		setCustDlvCd(inqSyukoView.getCustDlvCd());
		setCustDlvPlcNm(inqSyukoView.getCustDlvPlcNm());
		setCmpsgTyp(inqSyukoView.getCmpsgTyp());
		setPicCd(inqSyukoView.getPicCd());
		setFrctTyp(inqSyukoView.getFrctTyp());
		setFrctTyp2(inqSyukoView.getFrctTyp2());
		setCurId(inqSyukoView.getCurId());
		setUPricTyp(inqSyukoView.getUPricTyp());
		setSuPric(inqSyukoView.getSuPric());
		setTrdTrm(inqSyukoView.getTrdTrm());
		setTrdTrm2(inqSyukoView.getTrdTrm2());
		setFrctTyp3(inqSyukoView.getFrctTyp3());
		setTaxId(inqSyukoView.getTaxId());
		setTaxRt(inqSyukoView.getTaxRt());
		setTaxAmnt(inqSyukoView.getTaxAmnt());
		setUPricChgDt(inqSyukoView.getUPricChgDt());
		setClCurId(inqSyukoView.getClCurId());
		setClUPricTyp(inqSyukoView.getClUPricTyp());
		setClUPric(inqSyukoView.getClUPric());
		setClShpAmnt(inqSyukoView.getClShpAmnt());
		setClTaxAmnt(inqSyukoView.getClTaxAmnt());
		setExRt(inqSyukoView.getExRt());
		setStCurId(inqSyukoView.getStCurId());
		setStUPric(inqSyukoView.getStUPric());
		setStShpAmnt(inqSyukoView.getStShpAmnt());
		setStTaxAmnt(inqSyukoView.getStTaxAmnt());
		setUSimeym(inqSyukoView.getUSimeym());
		setArTyp(inqSyukoView.getArTyp());
		setVerfFlg(inqSyukoView.getVerfFlg());
		setVerfDt(inqSyukoView.getVerfDt());
		setDatTyp(inqSyukoView.getDatTyp());
		setMsclTyp(inqSyukoView.getMsclTyp());
		setMsclCorrFlg(inqSyukoView.getMsclCorrFlg());
		setCustClCd(inqSyukoView.getCustClCd());
		setSInpTyp(inqSyukoView.getSInpTyp());
		setPlanTyp(inqSyukoView.getPlanTyp());
		setCompFlg(inqSyukoView.getCompFlg());
		setWhseCd(inqSyukoView.getWhseCd());
		setWhseTyp(inqSyukoView.getWhseTyp());
		setGPTyp(inqSyukoView.getGPTyp());
		setNote1(inqSyukoView.getNote1());
		setNote2(inqSyukoView.getNote2());
		setEuPodrTyp(inqSyukoView.getEuPodrTyp());
		setEuPrdNo(inqSyukoView.getEuPrdNo());
		setIdTyp(inqSyukoView.getIdTyp());
		setSlipId(inqSyukoView.getSlipId());
		setTrdTyp(inqSyukoView.getTrdTyp());
		setTrckNo(inqSyukoView.getTrckNo());
		setTrnsfctCustCd(inqSyukoView.getTrnsfctCustCd());
		setCrtnQty(inqSyukoView.getCrtnQty());
		setTrnsfctDt(inqSyukoView.getTrnsfctDt());
		setCstmSlsNo(inqSyukoView.getCstmSlsNo());
		setTrnsfctQty(inqSyukoView.getTrnsfctQty());
		setPkgPalltNo(inqSyukoView.getPkgPalltNo());
		setPkgPalltQty(inqSyukoView.getPkgPalltQty());
	}
}