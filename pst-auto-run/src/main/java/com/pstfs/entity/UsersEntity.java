package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-09-30 15:57:13
 */
@Data
@TableName("users")
public class UsersEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String logid;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String password;
	/**
	 * 
	 */
	private String email;
	/**
	 * 
	 */
	private Integer level;
	/**
	 * 
	 */
	private String deptid;
	/**
	 * 
	 */
	private String parentid;
	/**
	 * 
	 */
	private Date ymd8cg;
	/**
	 * 
	 */
	private Date ymd8ent;

}
