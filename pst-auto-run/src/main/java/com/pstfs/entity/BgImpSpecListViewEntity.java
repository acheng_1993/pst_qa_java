package com.pstfs.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * time2系统货物累总表
 * 
 * @author jiajian
 */

@Data
@TableName("tkr.bg_imp_spec_list_view")
public class BgImpSpecListViewEntity {

	/**
	 * 入库单号
	 */
	private String refNo;

	/**
	 * 物料标记
	 */
	private String itemTyp;

	/**
	 * 项号
	 */
	private Integer freeSu1;
	
	/**
	 * 板号
	 */
	@TableField("FRVC_3")
	private String frvc3;

	/**
	 * 品番
	 */
	private String itemCd;

	/**
	 * 入库号码
	 */
	private String rcvKsq;

	/**
	 * 用户ID
	 */
	private String userId;

	/**
	 * 报关规格
	 */
	private String spec;

	/**
	 * 单价
	 */
	private BigDecimal uPric;

	/**
	 * 金额
	 */
	private BigDecimal rcvAmnt;

	/**
	 * 货币
	 */
	private String curId;

	/**
	 * 订单号
	 */
	private String pOdrNo;

	/**
	 * 票据日期
	 */
	private Date entDt;

	/**
	 * 净重
	 */
	private BigDecimal nw;

	/**
	 * 单重
	 */
	private BigDecimal wgt;

	/**
	 * 品牌
	 */
	private String freeTxt6;

	/**
	 * 品牌类型名称
	 */
	private String itmSpecNm;

	/**
	 * 部门
	 */
	private String divCd;

	/**
	 * 原产国
	 */
	private String cyNm;

	/**
	 * 报关数
	 */
	private BigDecimal cstmQty;

	/**
	 * 箱数
	 */
	private BigDecimal crtnQty;

	/**
	 * 工厂数
	 */
	private BigDecimal rcvQty;

	/**
	 * 毛重
	 */
	@TableField("FRNM_4")
	private BigDecimal frnm4;

	/**
	 * 传票号码
	 */
	private String impInvNo;

	/**
	 * 报关货币
	 */
	private String impCurId;

	/**
	 * 法检类别
	 */
	private String inspTyp;
	
	/**
	 * 法检标记
	 */
	private String inspFlg;
	
	/**
	 * 序列号
	 */
	private BigDecimal cstmKsq;
}