package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 计划发送消息中已经发送的消息
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-08 10:40:36
 */
@Data
@TableName("scheduled_sent_msg")
public class ScheduledSentMsgEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 消息主题
	 */
	private String title;
	/**
	 * 消除样式标签后的消息内容。
	 */
	private String contentText;
	/**
	 * 含有样式标签的消息内容。
	 */
	private String contentHtml;
	/**
	 * 关联计划发送消息表 scheduled_send_msg.id
	 */
	private Integer scheduledSendMsgId;
	
	/**
	 * 是否发送到微信，未发送：0，已发送：1
	 */
	private Integer sendWechat;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	
	/**
	 * 跳转的url，当该字段不为空时，则点击"详细请点击这里" 时直接跳转该url
	 */
	private String redirectUrl;
}
