package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-12-17 11:39:53
 */
@Data
@TableName("MEntrydb_2111")
public class Mentrydb2111 implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	private String meid;
	/**
	 * $column.comments
	 */
	private String product;
	/**
	 * $column.comments
	 */
	private String sNo;
	/**
	 * $column.comments
	 */
	private String sNo1;
	/**
	 * $column.comments
	 */
	private String sNo2;
	/**
	 * $column.comments
	 */
	private String sNo3;
	/**
	 * $column.comments
	 */
	private String process;
	/**
	 * $column.comments
	 */
	private String machine;
	/**
	 * $column.comments
	 */
	private String member;
	/**
	 * $column.comments
	 */
	private Integer processpattern;
	/**
	 * $column.comments
	 */
	private Date entrytime;
	/**
	 * $column.comments
	 */
	private String nogood1;
	/**
	 * $column.comments
	 */
	private String nogood2;
	/**
	 * $column.comments
	 */
	private String note;
	/**
	 * $column.comments
	 */
	private String judgment;
	/**
	 * $column.comments
	 */
	private String packing;
	/**
	 * $column.comments
	 */
	private String factorycode;
	/**
	 * $column.comments
	 */
	private String pl;

}
