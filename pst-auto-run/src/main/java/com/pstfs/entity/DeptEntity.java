package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * Departments
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-09-30 15:57:13
 */
@Data
@TableName("dept")
public class DeptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String deptid;
	/**
	 * 
	 */
	private String deptname;
	/**
	 * 
	 */
	private String deptdesc;

}
