package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 自动发送邮件到加工商的各个加工商的信息
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-21 15:08:30
 */
@Data
@TableName("auto_send_email_to_makercd_info")
public class AutoSendEmailToMakercdInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 加工商名称
	 */
	private String makerName;
	/**
	 * 加工商编码
	 */
	private String makerCd;
	/**
	 * 邮件标题模板，模板中附带有 {{pnpa}}：品番、{{pnpaCount}}：品番数、
	 */
	private String title;
	/**
	 * 邮件模板，模板中附带有 {{pnpa}}：品番、{{pnpaCount}}：品番数、{{table}}：表格
	 */
	private String template;
	/**
	 * 收件人，格式：收件人1 ; 收件人2 ; 收件人3
	 */
	private String recipient;
	/**
	 * 抄送人，格式：抄送人1 ; 抄送人2 ; 抄送人3
	 */
	private String cc;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
