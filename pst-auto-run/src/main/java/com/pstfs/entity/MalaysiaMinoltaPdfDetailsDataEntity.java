package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 马来西亚柯尼卡美能达的pdf详细数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-03 17:19:55
 */
@Data
@TableName("malaysia_minolta_pdf_details_data")
public class MalaysiaMinoltaPdfDetailsDataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 文件id，关联 upload_file.id
	 */
	private Integer uploadFileId;
	/**
	 * 页码
	 */
	private Integer pageNum;
	/**
	 * 
	 */
	private String poNo;
	/**
	 * 
	 */
	private String vendor;
	/**
	 * 
	 */
	private String storageLocation;
	/**
	 * 
	 */
	private String partsNo;
	/**
	 * 
	 */
	private String partsDescription;
	/**
	 * 
	 */
	private String itemNo;
	/**
	 * 
	 */
	private String orderQty;
	/**
	 * 
	 */
	private String unit;
	/**
	 * 
	 */
	private String qualityInspection;
	/**
	 * 
	 */
	private String deliveryDate;
	/**
	 * 
	 */
	private String bottomText;

}
