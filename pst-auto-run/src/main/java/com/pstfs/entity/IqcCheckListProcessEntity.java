package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * iqc_check_list表的检索进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-07-25 17:16:12
 */
@Data
@TableName("iqc_check_list_process")
public class IqcCheckListProcessEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 进度id，关联 iqc_check_list.id 字段
	 */
	private Integer processId;

}
