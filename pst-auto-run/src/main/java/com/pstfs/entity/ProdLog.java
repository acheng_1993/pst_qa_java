package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 产线的生产日志
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-12-17 11:25:34
 */
@Data
@TableName("prod_log")
public class ProdLog implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 日志文件名称
	 */
	private String srffFile;
	/**
	 * 单位
	 */
	private String units;
	/**
	 * 面板id
	 */
	private String panelId;
	/**
	 * 测试开始时间
	 */
	private String startDate;
	/**
	 * 测试结束时间
	 */
	private String endDate;
	/**
	 * 测试状态，N：不良品，P：良品
	 */
	private String testStatus;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * sha1串，用于区分日志
	 */
	private String sha1;
	/**
	 * 产生该数据的机器ip地址
	 */
	private String ipAddress;
	/**
	 * 日志文件名
	 */
	private String filename;
	/**
	 * 是否已经进入了mes系统的数据库，0：未进入，1：已进入
	 */
	private Integer intoMes;
	/**
	 * 是否已经进入了新mes系统的数据库，0：未进入，1：已进入
	 */
	private Integer intoNewMes;
	/**
	 * mo单号
	 */
	private String mo;
	/**
	 * 未进入新mes系统的原因
	 */
	private String notIntoNewMesReason;
}