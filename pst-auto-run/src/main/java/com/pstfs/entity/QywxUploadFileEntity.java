package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 企业微信的文件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-18 14:55:16
 */
@Data
@TableName("qywx_upload_file")
public class QywxUploadFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 文件的sha1
	 */
	private String sha1;
	/**
	 * 实际文件路径
	 */
	private String path;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
