package com.pstfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-10-19 13:01:04
 */
@Data
@TableName("A01")
public class A01Entity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	private String a0191;
	/**
	 * $column.comments
	 */
	@TableId
	private Integer a0188;
	/**
	 * $column.comments
	 */
	private Integer deleted;
	/**
	 * $column.comments
	 */
	private Integer a0199;
	/**
	 * $column.comments
	 */
	private String pydm;
	/**
	 * $column.comments
	 */
	private String a0190;
	/**
	 * $column.comments
	 */
	private String a0101;
	/**
	 * $column.comments
	 */
	private String email;
	/**
	 * $column.comments
	 */
	private String cardno;
	/**
	 * $column.comments
	 */
	private String iskq;
	/**
	 * $column.comments
	 */
	private Integer signed;
	/**
	 * $column.comments
	 */
	private String a0177;
	/**
	 * $column.comments
	 */
	private String a0104;
	/**
	 * $column.comments
	 */
	private String a0107;
	/**
	 * $column.comments
	 */
	private Date a0111;
	/**
	 * $column.comments
	 */
	private String a0117;
	/**
	 * $column.comments
	 */
	private String a0121;
	/**
	 * $column.comments
	 */
	private String a0127;
	/**
	 * $column.comments
	 */
	private String a0131;
	/**
	 * $column.comments
	 */
	private Date a0141;
	/**
	 * $column.comments
	 */
	private BigDecimal a0151;
	/**
	 * $column.comments
	 */
	private BigDecimal a01101;
	/**
	 * $column.comments
	 */
	private String a01102;
	/**
	 * $column.comments
	 */
	private String a01103;
	/**
	 * $column.comments
	 */
	private String a0154;
	/**
	 * $column.comments
	 */
	private BigDecimal a0152;
	/**
	 * $column.comments
	 */
	private String c0103;
	/**
	 * $column.comments
	 */
	private Date a0144;
	/**
	 * $column.comments
	 */
	private String c0104;
	/**
	 * $column.comments
	 */
	private String c0105;
	/**
	 * $column.comments
	 */
	private String c0106;
	/**
	 * $column.comments
	 */
	private String c0107;
	/**
	 * $column.comments
	 */
	private String deptCode;
	/**
	 * $column.comments
	 */
	private String e0101;
	/**
	 * $column.comments
	 */
	private String shiftName;
	/**
	 * $column.comments
	 */
	private Integer spflag;
	/**
	 * $column.comments
	 */
	private String kcarono;
	/**
	 * $column.comments
	 */
	private String kposition;
	/**
	 * $column.comments
	 */
	private String checkYn;
	/**
	 * $column.comments
	 */
	private String a01107;
	/**
	 * $column.comments
	 */
	private Date a01109;
	/**
	 * $column.comments
	 */
	private BigDecimal a01124;
	/**
	 * $column.comments
	 */
	private String a01273;
	/**
	 * $column.comments
	 */
	private String a0162;
	/**
	 * $column.comments
	 */
	private String a01112;
	/**
	 * $column.comments
	 */
	private String a01115;
	/**
	 * $column.comments
	 */
	private String a01120;
	/**
	 * $column.comments
	 */
	private String a01122;
	/**
	 * $column.comments
	 */
	private String a01139;
	/**
	 * $column.comments
	 */
	private String a01140;
	/**
	 * $column.comments
	 */
	private String loginothername;
	/**
	 * $column.comments
	 */
	private Integer px;
	/**
	 * $column.comments
	 */
	private String a0174;
	/**
	 * $column.comments
	 */
	private String a0175;
	/**
	 * $column.comments
	 */
	private Date a01bysj;
	/**
	 * $column.comments
	 */
	private String a01gzz;
	/**
	 * $column.comments
	 */
	private Date a0180;
	/**
	 * $column.comments
	 */
	private String a0181;
	/**
	 * $column.comments
	 */
	private Date a0183;
	/**
	 * $column.comments
	 */
	private String a0184;
	/**
	 * $column.comments
	 */
	private String a0185;
	/**
	 * $column.comments
	 */
	private String a01yyfs;
	/**
	 * $column.comments
	 */
	private String a01yypj;
	/**
	 * $column.comments
	 */
	private String a01hkxz;
	/**
	 * $column.comments
	 */
	private String a01jjlxr;
	/**
	 * $column.comments
	 */
	private BigDecimal a0192;
	/**
	 * $column.comments
	 */
	private BigDecimal a0193;
	/**
	 * $column.comments
	 */
	private BigDecimal a0194;
	/**
	 * $column.comments
	 */
	private BigDecimal a0195;
	/**
	 * $column.comments
	 */
	private BigDecimal a0196;
	/**
	 * $column.comments
	 */
	private BigDecimal a01zzjt;
	/**
	 * $column.comments
	 */
	private BigDecimal a01ryjt;
	/**
	 * $column.comments
	 */
	private BigDecimal a01yyjt1;
	/**
	 * $column.comments
	 */
	private BigDecimal a01gdqjt;
	/**
	 * $column.comments
	 */
	private BigDecimal a01tsgwjt;
	/**
	 * $column.comments
	 */
	private String a01sbh;
	/**
	 * $column.comments
	 */
	private Date a01htks;
	/**
	 * $column.comments
	 */
	private Date a01htjs;
	/**
	 * $column.comments
	 */
	private String a01htzt;
	/**
	 * $column.comments
	 */
	private String a01126;
	/**
	 * $column.comments
	 */
	private String a01x;
	/**
	 * $column.comments
	 */
	private String a01lzyy;
	/**
	 * $column.comments
	 */
	private String a01xxzy;
	/**
	 * $column.comments
	 */
	private String a0114;
	/**
	 * $column.comments
	 */
	private String a01jjlxrdh;
	/**
	 * $column.comments
	 */
	private String a01jtdh;
	/**
	 * $column.comments
	 */
	private Date a01pqrzrq;
	/**
	 * $column.comments
	 */
	private Date a01lzsqrq;
	/**
	 * $column.comments
	 */
	private String a01157;
	/**
	 * $column.comments
	 */
	private String a01158;
	/**
	 * $column.comments
	 */
	private BigDecimal a01ylbxjfjs;
	/**
	 * $column.comments
	 */
	private BigDecimal a01ylbxbldw;
	/**
	 * $column.comments
	 */
	private BigDecimal a01ylbxblgr;
	/**
	 * $column.comments
	 */
	private BigDecimal a01gsbxjfjs;
	/**
	 * $column.comments
	 */
	private BigDecimal a01gsbxbldw;
	/**
	 * $column.comments
	 */
	private BigDecimal a01sybxjfjs;
	/**
	 * $column.comments
	 */
	private BigDecimal a01sybxbldw;
	/**
	 * $column.comments
	 */
	private BigDecimal a01sybxblgr;
	/**
	 * $column.comments
	 */
	private BigDecimal a01jbylbxjfjs;
	/**
	 * $column.comments
	 */
	private BigDecimal a01jbylbxbldw;
	/**
	 * $column.comments
	 */
	private BigDecimal a01jbylbxblgr;
	/**
	 * $column.comments
	 */
	private BigDecimal a01sybxjfjs1;
	/**
	 * $column.comments
	 */
	private BigDecimal a01sybxbldw1;
	/**
	 * $column.comments
	 */
	private String a01z;
	/**
	 * $column.comments
	 */
	private BigDecimal a01176;
	/**
	 * $column.comments
	 */
	private BigDecimal a01177;
	/**
	 * $column.comments
	 */
	private String a01178;
	/**
	 * $column.comments
	 */
	private BigDecimal a01179;
	/**
	 * $column.comments
	 */
	private Date a01180;
	/**
	 * $column.comments
	 */
	private Date a01181;
	/**
	 * $column.comments
	 */
	private Date a01182;
	/**
	 * $column.comments
	 */
	private Date a01183;
	/**
	 * $column.comments
	 */
	private String a01xzdz;
	/**
	 * $column.comments
	 */
	private Date a01185;
	/**
	 * $column.comments
	 */
	private Date a01186;
	/**
	 * $column.comments
	 */
	private String a01187;
	/**
	 * $column.comments
	 */
	private String a01k;
	/**
	 * $column.comments
	 */
	private String a01jtjt;
	/**
	 * $column.comments
	 */
	private String a01cxfs;
	/**
	 * $column.comments
	 */
	private Date a01rzpxrq;
	/**
	 * $column.comments
	 */
	private String a01nxjts;
	/**
	 * $column.comments
	 */
	private String a01sjspr;
	/**
	 * $column.comments
	 */
	private String a01195;
	/**
	 * $column.comments
	 */
	private String a01197;
	/**
	 * $column.comments
	 */
	private String a01lbt;
	/**
	 * $column.comments
	 */
	private String a01199;
	/**
	 * $column.comments
	 */
	private String a01hkcbzx;
	/**
	 * $column.comments
	 */
	private String a01201;
	/**
	 * $column.comments
	 */
	private Date a01202;
	/**
	 * $column.comments
	 */
	private BigDecimal a01203;
	/**
	 * $column.comments
	 */
	private String a01204;
	/**
	 * $column.comments
	 */
	private String a01205;
	/**
	 * $column.comments
	 */
	private Date a01206;
	/**
	 * $column.comments
	 */
	private String a01zrz;
	/**
	 * $column.comments
	 */
	private String a01208;
	/**
	 * $column.comments
	 */
	private String a01209;
	/**
	 * $column.comments
	 */
	private Date a01210;
	/**
	 * $column.comments
	 */
	private String a01211;
	/**
	 * $column.comments
	 */
	private String a01212;
	/**
	 * $column.comments
	 */
	private String a01213;
	/**
	 * $column.comments
	 */
	private String a01214;
	/**
	 * $column.comments
	 */
	private String a01216;
	/**
	 * $column.comments
	 */
	private BigDecimal a01217;
	/**
	 * $column.comments
	 */
	private String wxuserid;
	/**
	 * $column.comments
	 */
	private String a01219;
	/**
	 * $column.comments
	 */
	private Date a01221;
	/**
	 * $column.comments
	 */
	private BigDecimal a01zwgzjt;
	/**
	 * $column.comments
	 */
	private BigDecimal a01zrzjt;
	/**
	 * $column.comments
	 */
	private BigDecimal a01224;
	/**
	 * $column.comments
	 */
	private BigDecimal a01225;
	/**
	 * $column.comments
	 */
	private BigDecimal a01226;
	/**
	 * $column.comments
	 */
	private String a01bxjh;

}
