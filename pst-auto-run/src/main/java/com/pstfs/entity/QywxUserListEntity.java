package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 企业微信用户表，这是从企业微信接口中获取的用户数据，每天同步一次，用于降低调用企业微信接口的频率
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-10-25 16:00:20
 */
@Data
@TableName("qywx_user_list")
public class QywxUserListEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户id
	 */
	private String userid;
	/**
	 * 真实姓名
	 */
	private String name;
	/**
	 * 缩略图
	 */
	private String avatar;
	/**
	 * 头像缩略图
	 */
	private String thumbAvatar;
	/**
	 * 别名
	 */
	private String alias;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
