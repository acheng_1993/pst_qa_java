package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-09 09:37:10
 */
@Data
@TableName("sys_user")
public class SysUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private String mobilephone;
	/**
	 * 
	 */
	private String sex;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 真实姓名
	 */
	private String realname;
	/**
	 * 是否可用
	 */
	private Boolean isenabled;
	/**
	 * 最后登录时间
	 */
	private Date lastaccesstime;
	/**
	 * 登陆成功次数
	 */
	private Long logincount;
	/**
	 * 登录错误次数
	 */
	private Long errorlogincount;
	/**
	 * 是否初始密码
	 */
	private Boolean isorignalpassword;
	/**
	 * 创建人
	 */
	private Long createrid;
	/**
	 * 创建人名称
	 */
	private String creatername;
	/**
	 * 创建时间
	 */
	private Date createtime;
	/**
	 * 更新人
	 */
	private Long modifierid;
	/**
	 * 修改人名称
	 */
	private String modifiername;
	/**
	 * 更新时间
	 */
	private Date modifytime;
	/**
	 * 是否删除
	 */
	private Boolean isdeleted;
	/**
	 * 
	 */
	private String remark;
	/**
	 * 
	 */
	private String jobstatus;
	/**
	 * 最后密码修改
	 */
	private Date lastupdatepwdtime;

}
