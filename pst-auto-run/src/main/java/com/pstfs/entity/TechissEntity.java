package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-09-30 14:53:28
 */
@Data
@TableName("techiss")
public class TechissEntity implements Serializable, Cloneable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String ctrlno;
	/**
	 * 
	 */
	private String category;
	/**
	 * 
	 */
	private String doctype;
	/**
	 * 
	 */
	private String pnpa;
	/**
	 * 
	 */
	private String pn;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private String issno;
	/**
	 * 
	 */
	private String issTy;
	/**
	 * 
	 */
	private Date isdate;
	/**
	 * 
	 */
	private String makercd;
	/**
	 * 
	 */
	private String remark;
	/**
	 * 
	 */
	private String upload;
	/**
	 * 
	 */
	private String issto;
	/**
	 * 
	 */
	private String ident;
	/**
	 * 
	 */
	private Date dtent;
	/**
	 * 
	 */
	private String viewable;
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
