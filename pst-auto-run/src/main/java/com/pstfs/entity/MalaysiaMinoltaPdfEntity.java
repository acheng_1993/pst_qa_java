package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 马来西亚美能达的pdf二维码解析功能
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-01 14:00:28
 */
@Data
@TableName("malaysia_minolta_pdf")
public class MalaysiaMinoltaPdfEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 文件id，关联 upload_file.id
	 */
	private Integer uploadFileId;
	/**
	 * 处理进度 0: 待处理，1: 已处理，2: 处理失败
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 合并后的文件id，关联 upload_file.id
	 */
	private Integer mergeFileId;
	/**
	 * 指定订单号
	 */
	private String invno;
	/**
	 * 指定订单号后导出的文件id
	 */
	private Integer invnoUploadFileId;
	/**
	 * 订单处理状态，0:待处理，1:已处理，2:处理失败
	 */
	private Integer invnoStatus;
}
