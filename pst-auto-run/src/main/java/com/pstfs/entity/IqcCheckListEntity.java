package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * iqc的进料通知检验单
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-07-25 16:48:10
 */
@Data
@TableName("iqc_check_list")
public class IqcCheckListEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 进料日期，格式：Y-m-d
	 */
	private String feedDate;
	/**
	 * 进料通知检验单的单号
	 */
	private String orderNum;
	/**
	 * 部品品番
	 */
	private String itmCode;

}
