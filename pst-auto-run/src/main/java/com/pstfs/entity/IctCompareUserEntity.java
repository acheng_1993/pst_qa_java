package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * ict对比相关的用户，用于控制指定用户具有的操作权限
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-07-11 09:56:51
 */
@Data
@TableName("ict_compare_user")
public class IctCompareUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 关联user.id字段
	 */
	private Integer userId;
	/**
	 * 用户所属角色，0：革新课用户、1：生技用户、2：QA用户
	 */
	private Integer roleType;
	/**
	 * 用户邮箱
	 */
	private String mail;

}
