package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-09-26 10:29:05
 */
@Data
@TableName("form_forminstance")
public class FormForminstanceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 模板ID
	 */
	private Long templateid;
	/**
	 * 表单编码
	 */
	private String formcode;
	/**
	 * 表单实体名称
	 */
	private String formname;
	/**
	 * 表单JSON
	 */
	private String formjson;
	/**
	 * 
	 */
	private String formdatajson;
	/**
	 * 表单描述
	 */
	private String formdesc;
	/**
	 * 表单类型
	 */
	private Long formtype;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 审核状态
	 */
	private Long approvestatus;
	/**
	 * 部门ID
	 */
	private Long departmentid;
	/**
	 * 创建人
	 */
	private Long createrid;
	/**
	 * 创建时间
	 */
	private Date createtime;
	/**
	 * 编辑人
	 */
	private Long modifierid;
	/**
	 * 编辑时间
	 */
	private Date modifytime;
	/**
	 * 是否删除
	 */
	private Boolean isdeleted;
	/**
	 * 创建人
	 */
	private String creatername;
	/**
	 * 编辑人
	 */
	private String modifiername;
	/**
	 * 是否同步复制
	 */
	private Boolean issynccopy;
	/**
	 * 单据流水号
	 */
	private String docnum;
	/**
	 * 是否同步ERP
	 */
	private Boolean issyncerp;
	/**
	 * ERP单号
	 */
	private String erpdocnumber;

}
