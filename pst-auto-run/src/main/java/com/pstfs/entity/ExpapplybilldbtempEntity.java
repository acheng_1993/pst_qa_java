package com.pstfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 云关通出口申请单数据表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-08-22 10:50:29
 */
@Data
@TableName("ExpApplyBillDBTemp")
public class ExpapplybilldbtempEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId
	private String id;
	/**
	 * $column.comments
	 */
	private Date createdate;
	/**
	 * $column.comments
	 */
	private String createuser;
	/**
	 * $column.comments
	 */
	private Date modifydate;
	/**
	 * $column.comments
	 */
	private String modifyuser;
	/**
	 * $column.comments
	 */
	private Integer optlock;
	/**
	 * $column.comments
	 */
	private Date updatedate;
	/**
	 * $column.comments
	 */
	private String updateuser;
	/**
	 * $column.comments
	 */
	private String zextendfield1;
	/**
	 * $column.comments
	 */
	private String zextendfield2;
	/**
	 * $column.comments
	 */
	private String zextendfield3;
	/**
	 * $column.comments
	 */
	private Float zextendfield6;
	/**
	 * $column.comments
	 */
	private Date zextendfield8;
	/**
	 * $column.comments
	 */
	private String companycode;
	/**
	 * $column.comments
	 */
	private String deletemark;
	/**
	 * $column.comments
	 */
	private Date downloaddate;
	/**
	 * $column.comments
	 */
	private String errmsg;
	/**
	 * $column.comments
	 */
	private Boolean failorsucce;
	/**
	 * $column.comments
	 */
	private String sectionno;
	/**
	 * $column.comments
	 */
	private Date uploaddate;
	/**
	 * $column.comments
	 */
	private String warnmsg;
	/**
	 * $column.comments
	 */
	private String zextendfield10;
	/**
	 * $column.comments
	 */
	private String zextendfield11;
	/**
	 * $column.comments
	 */
	private String zextendfield12;
	/**
	 * $column.comments
	 */
	private String zextendfield13;
	/**
	 * $column.comments
	 */
	private String zextendfield14;
	/**
	 * $column.comments
	 */
	private String zextendfield15;
	/**
	 * $column.comments
	 */
	private String zextendfield4;
	/**
	 * $column.comments
	 */
	private String zextendfield5;
	/**
	 * $column.comments
	 */
	private Float zextendfield7;
	/**
	 * $column.comments
	 */
	private Date zextendfield9;
	/**
	 * $column.comments
	 */
	private Float amount;
	/**
	 * $column.comments
	 */
	private String brandtype;
	/**
	 * $column.comments
	 */
	private String businessdept;
	/**
	 * $column.comments
	 */
	private String cabinettype;
	/**
	 * $column.comments
	 */
	private String companymergerkey;
	/**
	 * $column.comments
	 */
	private String containerno;
	/**
	 * $column.comments
	 */
	private String containersno;
	/**
	 * $column.comments
	 */
	private String country;
	/**
	 * $column.comments
	 */
	private String curr;
	/**
	 * $column.comments
	 */
	private String cutoffdate;
	/**
	 * $column.comments
	 */
	private String declarekey;
	/**
	 * $column.comments
	 */
	private String feecurr;
	/**
	 * $column.comments
	 */
	private String feemark;
	/**
	 * $column.comments
	 */
	private Float feerate;
	/**
	 * $column.comments
	 */
	private String goodsattr;
	/**
	 * $column.comments
	 */
	private BigDecimal grossweight;
	/**
	 * $column.comments
	 */
	private String hsversionno;
	/**
	 * $column.comments
	 */
	private String insurcurr;
	/**
	 * $column.comments
	 */
	private String insurmark;
	/**
	 * $column.comments
	 */
	private Float insurrate;
	/**
	 * $column.comments
	 */
	private String invoicedate;
	/**
	 * $column.comments
	 */
	private String itemorderno;
	/**
	 * $column.comments
	 */
	private String markname;
	/**
	 * $column.comments
	 */
	private String material;
	/**
	 * $column.comments
	 */
	private BigDecimal netweight;
	/**
	 * $column.comments
	 */
	private String orderno;
	/**
	 * $column.comments
	 */
	private String origplace;
	/**
	 * $column.comments
	 */
	private String othercurr;
	/**
	 * $column.comments
	 */
	private String othermark;
	/**
	 * $column.comments
	 */
	private Float otherrate;
	/**
	 * $column.comments
	 */
	private String outerpackage;
	/**
	 * $column.comments
	 */
	private BigDecimal overallunitprice;
	/**
	 * $column.comments
	 */
	private String overallunitptno;
	/**
	 * $column.comments
	 */
	private Integer palletnum;
	/**
	 * $column.comments
	 */
	private BigDecimal pcs;
	/**
	 * $column.comments
	 */
	private BigDecimal price;
	/**
	 * $column.comments
	 */
	private BigDecimal qty;
	/**
	 * $column.comments
	 */
	private String scmcoc;
	/**
	 * $column.comments
	 */
	private String shipcompany;
	/**
	 * $column.comments
	 */
	private String shipname;
	/**
	 * $column.comments
	 */
	private BigDecimal singlecount;
	/**
	 * $column.comments
	 */
	private String trade;
	/**
	 * $column.comments
	 */
	private String tradearea;
	/**
	 * $column.comments
	 */
	private BigDecimal tradingqty;
	/**
	 * $column.comments
	 */
	private String trainno;
	/**
	 * $column.comments
	 */
	private String transac;
	/**
	 * $column.comments
	 */
	private String transf;
	/**
	 * $column.comments
	 */
	private String transferparametername;
	/**
	 * $column.comments
	 */
	private Float unitgrossweight;
	/**
	 * $column.comments
	 */
	private Float unitnetweight;
	/**
	 * $column.comments
	 */
	private String versionno;
	/**
	 * $column.comments
	 */
	private String voyageno;
	/**
	 * $column.comments
	 */
	private String wrap;
	/**
	 * $column.comments
	 */
	private String destcity;
	/**
	 * $column.comments
	 */
	private String destinationcountry;
	/**
	 * $column.comments
	 */
	private String distinateport;
	/**
	 * $column.comments
	 */
	private String district;
	/**
	 * $column.comments
	 */
	private String ieport;
	/**
	 * $column.comments
	 */
	private String imgexgflag;
	/**
	 * $column.comments
	 */
	private Integer itemno;
	/**
	 * $column.comments
	 */
	private String note;
	/**
	 * $column.comments
	 */
	private String noteitem;
	/**
	 * $column.comments
	 */
	private String origincountry;
	/**
	 * $column.comments
	 */
	private String outbillno;
	/**
	 * $column.comments
	 */
	private String zextendheadfield1;
	/**
	 * $column.comments
	 */
	private String zextendheadfield10;
	/**
	 * $column.comments
	 */
	private String zextendheadfield11;
	/**
	 * $column.comments
	 */
	private String zextendheadfield12;
	/**
	 * $column.comments
	 */
	private String zextendheadfield13;
	/**
	 * $column.comments
	 */
	private String zextendheadfield14;
	/**
	 * $column.comments
	 */
	private String zextendheadfield15;
	/**
	 * $column.comments
	 */
	private String zextendheadfield2;
	/**
	 * $column.comments
	 */
	private String zextendheadfield3;
	/**
	 * $column.comments
	 */
	private String zextendheadfield4;
	/**
	 * $column.comments
	 */
	private String zextendheadfield5;
	/**
	 * $column.comments
	 */
	private Float zextendheadfield6;
	/**
	 * $column.comments
	 */
	private Float zextendheadfield7;
	/**
	 * $column.comments
	 */
	private Date zextendheadfield8;
	/**
	 * $column.comments
	 */
	private Date zextendheadfield9;
	/**
	 * $column.comments
	 */
	private String zextenditemfield1;
	/**
	 * $column.comments
	 */
	private String zextenditemfield10;
	/**
	 * $column.comments
	 */
	private String zextenditemfield11;
	/**
	 * $column.comments
	 */
	private String zextenditemfield12;
	/**
	 * $column.comments
	 */
	private String zextenditemfield13;
	/**
	 * $column.comments
	 */
	private String zextenditemfield14;
	/**
	 * $column.comments
	 */
	private String zextenditemfield15;
	/**
	 * $column.comments
	 */
	private String zextenditemfield2;
	/**
	 * $column.comments
	 */
	private String zextenditemfield3;
	/**
	 * $column.comments
	 */
	private String zextenditemfield4;
	/**
	 * $column.comments
	 */
	private String zextenditemfield5;
	/**
	 * $column.comments
	 */
	private BigDecimal zextenditemfield6;
	/**
	 * $column.comments
	 */
	private Float zextenditemfield7;
	/**
	 * $column.comments
	 */
	private Date zextenditemfield8;
	/**
	 * $column.comments
	 */
	private Date zextenditemfield9;

}
