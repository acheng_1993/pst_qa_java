package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 云关通报关单返填功能标识id，用于标识当且返填进度，以区分哪些单是返填过的，哪些单是未返填过的。
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-02 10:39:41
 */
@Data
@TableName("apply_bill_item_current_id")
public class ApplyBillItemCurrentIdEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Integer id;
	
	/**
	 * 返填进度id，是一个32位的uuid
	 */
	private String currentId;
	
	/**
	 * 返填进度对应的传票号码
	 */
	private String currentInvoiceno;
	
	/**
	 * 上次处理的年月日:YYYYMMDD
	 */
	private String handleDate;
}
