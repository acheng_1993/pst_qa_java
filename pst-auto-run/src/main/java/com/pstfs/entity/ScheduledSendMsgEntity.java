package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 计划发送消息设置表，用于记录将要发送消息的时间，策略等内容...
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-07 17:02:24
 */
@Data
@TableName("scheduled_send_msg")
public class ScheduledSendMsgEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 消息主题
	 */
	private String title;
	/**
	 * 消除样式标签后的消息内容。
	 */
	private String contentText;
	/**
	 * 含有样式标签的消息内容。
	 */
	private String contentHtml;
	/**
	 * 触发自动发送的时间，一旦到了这个时间就会触发。
	 */
	private Integer autoSendMinuteOfDay;
	/**
	 * 最近的一次发送时间
	 */
	private Date lastSendDate;
	/**
	 * 最近的一次发送年，截取自 last_send_date
	 */
	private Integer lastSendYear;
	/**
	 * 最近的一次发送月，截取自 last_send_date
	 */
	private Integer lastSendMonthOfYear;
	/**
	 * 最近的一次发送日，截取自 last_send_date
	 */
	private Integer lastSendDayOfYear;
	/**
	 * 最近的一次发送周，截取自 last_send_date
	 */
	private Integer lastSendWeekOfYear;
	/**
	 * 最近的一次发送在周几，截取自 last_send_date
	 */
	private Integer lastSendDayOfWeek;
	/**
	 * 发送策略，0：一年一次，1：一月一次，2：一周一次，3：一天一次
	 */
	private Integer sendStrategy;
	/**
	 * 发送策略，相关的日设置\\n发送策略是一年一次时，指这一年的第几天。\\n发送策略是一月一次时，指这一月的第几天。\\n发送策略是一周一次时，指这一周的第几天。

	 */
	private Integer sendDayOfStrategy;
	/**
	 * 发送策略，相关的候选日1设置(用于解决某日是假日，或者是某日在月份中不存在)
	 * 发送策略是一年一次时，指这一年的第几天。
	 * 发送策略是一月一次时，指这一月的第几天。
	 * 发送策略是一周一次时，指这一周的第几天。
	 */
	private Integer sendCandidateDayOfStrategy1;
	/**
	 * 发送策略，相关的候选日2设置(用于解决某日是假日，或者是某日在月份中不存在)
	 * 发送策略是一年一次时，指这一年的第几天。
	 * 发送策略是一月一次时，指这一月的第几天。
	 * 发送策略是一周一次时，指这一周的第几天。
	 */
	private Integer sendCandidateDayOfStrategy2;
	/**
	 * 发送策略，相关的候选日3设置(用于解决某日是假日，或者是某日在月份中不存在)
	 * 发送策略是一年一次时，指这一年的第几天。
	 * 发送策略是一月一次时，指这一月的第几天。
	 * 发送策略是一周一次时，指这一周的第几天。
	 */
	private Integer sendCandidateDayOfStrategy3;
	/**
	 * 开关状态，0：已开启自动发送，1：已关闭自动发送
	 */
	private Integer switchStatus;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	
	/**
	 * 跳转的url，当该字段不为空时，则点击"详细请点击这里" 时直接跳转该url
	 */
	private String redirectUrl;
}