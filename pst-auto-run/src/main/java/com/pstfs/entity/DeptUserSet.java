package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
@Data
@TableName("dept_user_set")
public class DeptUserSet {
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	
	/**
	 * 学历
	 */
	@TableField("a22")
	private String a22;
	
	/**
	 * 联系电话
	 */
	@TableField("a24")
	private String a24;
	
	/**
	 * 现住地址
	 */
	@TableField("a26")
	private String a26;
	
	/**
	 * 紧急联系人
	 */
	@TableField("a28")
	private String a28;
	
	/**
	 * 紧急联系电话
	 */
	@TableField("a29")
	private String a29;
	
	/**
	 * 出行方式
	 */
	@TableField("a33")
	private String a33;
	
	/**
	 * 姓名
	 */
	@TableField("user_name")
	private String userName;
}