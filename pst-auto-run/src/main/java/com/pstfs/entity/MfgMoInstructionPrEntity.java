package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * MO工程指示预定表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-03-15 15:17:55
 */
@Data
@TableName("mfg_mo_instruction_pr")
public class MfgMoInstructionPrEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private String id;
	/**
	 * 工厂ID
	 */
	private String plantId;
	/**
	 * 工厂编码
	 */
	private String plantName;
	/**
	 * MO工单号（TIMES)
	 */
	private String moNo;
	/**
	 * 枝番1
	 */
	private String ban1No;
	/**
	 * 枝番2
	 */
	private String ban2No;
	/**
	 * 机种编码
	 */
	private String modelNo;
	/**
	 * 机种编码名称
	 */
	private String modelName;
	/**
	 * 单位
	 */
	private String uomCode;
	/**
	 * 单位数
	 */
	private String uomName;
	/**
	 * MO工单号
	 */
	private String operationWoNo;
	/**
	 * 物料代码
	 */
	private String productCode;
	/**
	 * 物料名称
	 */
	private String productName;
	/**
	 * 物料单位
	 */
	private String productUomCode;
	/**
	 * 物料单位名称
	 */
	private String productUomName;
	/**
	 * MO出库指示数量
	 */
	private BigDecimal operationWoQty;
	/**
	 * MO计划数量
	 */
	private BigDecimal moQty;
	/**
	 * 最大数量
	 */
	private BigDecimal maxQty;
	/**
	 * 存储场所代码
	 */
	private String storageLocation;
	/**
	 * 存储场所名称
	 */
	private String storageLocationName;
	/**
	 * 生产场所代码
	 */
	private String productLocation;
	/**
	 * 生产场所名称
	 */
	private String productLocationName;
	/**
	 * 拣货日期
	 */
	private Date pickingDate;
	/**
	 * 生产日期（亲计划日）
	 */
	private Date productionDate;
	/**
	 * 产线代码
	 */
	private String workLine;
	/**
	 * 产线名称
	 */
	private String workLineName;
	/**
	 * 工序序号
	 */
	private Integer operationSeq;
	/**
	 * 工程代码
	 */
	private String operationCode;
	/**
	 * 工程名称
	 */
	private String operationName;
	/**
	 * 打印状态   N：未打印    Y：已打印
	 */
	private String printStatus;
	/**
	 * 是否需要打印工单 Y:是 N:否
	 */
	private String isPrintMo;
	/**
	 * 1：实装 0：手插  null：未设置
	 */
	private String operationStationType;
	/**
	 * 是否实绩登录工序 
	 */
	private String isDetResult;
	/**
	 * 
	 */
	private String isDel;
	/**
	 * 
	 */
	private String createdByName;
	/**
	 * 
	 */
	private Date createdWhen;
	/**
	 * 
	 */
	private String lastModifiedByName;
	/**
	 * 
	 */
	private Date lastModifiedWhen;
	/**
	 * 领料单号
	 */
	private String pickNo;
	/**
	 * 
	 */
	private String mcCode;
	/**
	 * 
	 */
	private String mourl;
	/**
	 * 
	 */
	private String picurl;

}
