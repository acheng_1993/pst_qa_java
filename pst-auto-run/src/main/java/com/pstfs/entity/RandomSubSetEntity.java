package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 随机子集表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-13 10:31:08
 */
@Data
@TableName("random_sub_set")
public class RandomSubSetEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 项目号id，关联random_section_no.id
	 */
	private Integer randomSectionNoId;
	/**
	 * 随机数字子集合
	 */
	private String randomString;
	/**
	 * 出现次数
	 */
	private Integer times;

}
