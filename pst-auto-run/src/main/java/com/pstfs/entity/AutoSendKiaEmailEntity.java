package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
/**
 * 自动发送给KIA的邮件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 12:10:12
 */
@Data
@TableName("auto_send_kia_email")
public class AutoSendKiaEmailEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 邮件标题
	 */
	private String title;
	/**
	 * 邮件标题模板备份，模板中附带有 {{pnpa}}：品番、{{pnpaCount}}：品番数、
	 */
	private String titleBak;
	/**
	 * 对应的品番
	 */
	private String pnpa;
	/**
	 * 邮件内容
	 */
	private String content;
	/**
	 * 邮件内容模板备份，模板中附带有 {{pnpa}}：品番、{{pnpaCount}}：品番数、{{table}}：表格
	 */
	private String contentBak;
	/**
	 * 收件人，格式：收件人1 ; 收件人2 ; 收件人3
	 */
	private String recipient;
	/**
	 * 抄送人，格式：抄送人1 ; 抄送人2 ; 抄送人3
	 */
	private String cc;
	/**
	 * 收件人备份，格式：收件人1 ; 收件人2 ; 收件人3
	 */
	private String recipientBak;
	/**
	 * 抄送人备份，格式：抄送人1 ; 抄送人2 ; 抄送人3
	 */
	private String ccBak;
	/**
	 * 最近发送日期
	 */
	private Date lastSendDate;
	/**
	 * 确认状态，0：kia未确认、1：kia已确认
	 */
	private Integer confirmStatus;
	
	/**
	 * 中宝确认状态：0：中宝未确认、1：中宝已确认
	 */
	private Integer pstfsConfirmStatus;
	
	/**
	 * 发送邮件提醒中宝用户确认，0：未发送、1：已发送
	 */
	private Integer sendEmailRemind;
	/**
	 * 文件总大小
	 */
	private Long fileSize;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 关联的加工商的主键id
	 */
	private Integer autoSendEmailToMakercdInfoId;
	/**
	 * 最小的技术文档id
	 */
	private Integer minTechissId;
}