package com.pstfs.entity;
import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 免检部品表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-21 10:48:03
 */
@Data
@TableName("zprod_exem_from_insp")
public class ZProdExemFromInspEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 品番
	 */
	private String productCode;
	/**
	 * 产品描述
	 */
	private String productDesc;
	/**
	 * 区分
	 */
	private String distinguish;
	
	/**
	 * 导入日期
	 */
	private Date importDate;
	
	/**
	 * 作为免检部品的结束期限
	 */
	private Date finishDate;
	
	/**
	 * 最近一次签收日期
	 */
	private Date lastCargReceiptDate;
	
	/**
	 * 最近一次发送邮件的时间
	 */
	private Date lastSendMailDate;
	
	/**
	 * 版本标识，格式是：YYYY-MM-DD
	 */
	private String version;
}