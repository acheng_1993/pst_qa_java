package com.pstfs.entity;
import java.util.Date;

import lombok.Data;
@Data
public class MfgMoItemAllEntity {
	
	private String id;
	
	private String ModelNo;
	
	private Date createdWhen;
}