package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ICT程序核对的文件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-10 17:21:20
 */
@Data
@NoArgsConstructor
@TableName("upload_ict_file_list")
public class UploadIctFileListEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id，对于其他需要上传文件的业务代码，需要把记录uploadict_file_list.id 的字段设置为外键，方便日后文件维护。
	 */
	@TableId
	private Integer id;
	/**
	 * 原始文件名称，含后缀
	 */
	private String srcName;
	/**
	 * 文件后缀
	 */
	private String suffix;
	/**
	 * 文件id，关联upload_file.id
	 */
	private Integer fileId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 上传的excel种类：-1：待处理，0：不明类型，1：表示有表格那种（partlist），2：表示没有表格那种（ict程序）
	 */
	private Integer type;
	/**
	 * excel sheet数据，格式是[[],[],[],[],...]
	 */
	private String sheetData;
	/**
	 * 字符串数据，格式是[,,,,....]
	 */
	private String shareString;
	/**
	 * 上传文件的用户名
	 */
	private String username;

	public UploadIctFileListEntity(Integer id, String srcName, String suffix, Integer fileId,Date createTime,
			Date updateTime, Integer type, String sheetData, String shareString, String username) {
		this.id = id;
		this.srcName = srcName;
		this.suffix = suffix;
		this.fileId = fileId;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.type = type;
		this.sheetData = sheetData;
		this.shareString = shareString;
		this.username = username;
	}
}
