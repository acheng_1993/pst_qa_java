package com.pstfs.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
/**
 * 后台保存的文件表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-08-17 08:28:53
 */
@Data
@TableName("upload_file")
public class UploadFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 文件的sha1
	 */
	private String sha1;
	/**
	 * 实际文件路径，含文件夹、文件名；相对路径。
	 */
	private String path;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
