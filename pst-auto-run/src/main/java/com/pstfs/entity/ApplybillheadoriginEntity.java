package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-02 11:16:34
 */
@Data
@TableName("ApplyBillHeadOrigin")
public class ApplybillheadoriginEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId
	private String id;
	/**
	 * $column.comments
	 */
	private Date createdate;
	/**
	 * $column.comments
	 */
	private String createuser;
	/**
	 * $column.comments
	 */
	private Date modifydate;
	/**
	 * $column.comments
	 */
	private String modifyuser;
	/**
	 * $column.comments
	 */
	private Integer optlock;
	/**
	 * $column.comments
	 */
	private Date updatedate;
	/**
	 * $column.comments
	 */
	private String updateuser;
	/**
	 * $column.comments
	 */
	private String zextendfield1;
	/**
	 * $column.comments
	 */
	private String zextendfield2;
	/**
	 * $column.comments
	 */
	private String zextendfield3;
	/**
	 * $column.comments
	 */
	private Float zextendfield6;
	/**
	 * $column.comments
	 */
	private Date zextendfield8;
	/**
	 * $column.comments
	 */
	private String zextendfield10;
	/**
	 * $column.comments
	 */
	private String zextendfield11;
	/**
	 * $column.comments
	 */
	private String zextendfield12;
	/**
	 * $column.comments
	 */
	private String zextendfield13;
	/**
	 * $column.comments
	 */
	private String zextendfield14;
	/**
	 * $column.comments
	 */
	private String zextendfield15;
	/**
	 * $column.comments
	 */
	private String zextendfield4;
	/**
	 * $column.comments
	 */
	private String zextendfield5;
	/**
	 * $column.comments
	 */
	private Float zextendfield7;
	/**
	 * $column.comments
	 */
	private Date zextendfield9;
	/**
	 * $column.comments
	 */
	private String applybillheadoriginid;
	/**
	 * $column.comments
	 */
	private String carno;
	/**
	 * $column.comments
	 */
	private String consignmentno;
	/**
	 * $column.comments
	 */
	private Date effectivedate;
	/**
	 * $column.comments
	 */
	private String effectiveperson;
	/**
	 * $column.comments
	 */
	private String feemark;
	/**
	 * $column.comments
	 */
	private Float feerate;
	/**
	 * $column.comments
	 */
	private String imgexgflag;
	/**
	 * $column.comments
	 */
	private Date impexpdate;
	/**
	 * $column.comments
	 */
	private String impexpflag;
	/**
	 * $column.comments
	 */
	private String insurmark;
	/**
	 * $column.comments
	 */
	private Float insurrate;
	/**
	 * $column.comments
	 */
	private Date invoicedate;
	/**
	 * $column.comments
	 */
	private String invoiceno;
	/**
	 * $column.comments
	 */
	private Boolean iseffective;
	/**
	 * $column.comments
	 */
	private Float itemcount;
	/**
	 * $column.comments
	 */
	private Float makeitemcount;
	/**
	 * $column.comments
	 */
	private Integer makewmsstockcount;
	/**
	 * $column.comments
	 */
	private String note;
	/**
	 * $column.comments
	 */
	private String orderno;
	/**
	 * $column.comments
	 */
	private String othermark;
	/**
	 * $column.comments
	 */
	private Float otherrate;
	/**
	 * $column.comments
	 */
	private String outbillno;
	/**
	 * $column.comments
	 */
	private String producebatchno;
	/**
	 * $column.comments
	 */
	private Integer serialno;
	/**
	 * $column.comments
	 */
	private String stockbillno;
	/**
	 * $column.comments
	 */
	private Float totalamount;
	/**
	 * $column.comments
	 */
	private Float totalgrossweight;
	/**
	 * $column.comments
	 */
	private Float totalnetweight;
	/**
	 * $column.comments
	 */
	private Float totalpalletcount;
	/**
	 * $column.comments
	 */
	private Float totalpcscount;
	/**
	 * $column.comments
	 */
	private Float totalqty;
	/**
	 * $column.comments
	 */
	private String trainno;
	/**
	 * $column.comments
	 */
	private String trucktype;
	/**
	 * $column.comments
	 */
	private String zextendfield101;
	/**
	 * $column.comments
	 */
	private String zextendfield102;
	/**
	 * $column.comments
	 */
	private String zextendfield103;
	/**
	 * $column.comments
	 */
	private String zextendfield104;
	/**
	 * $column.comments
	 */
	private String zextendfield105;
	/**
	 * $column.comments
	 */
	private String zextendfield106;
	/**
	 * $column.comments
	 */
	private String zextendfield107;
	/**
	 * $column.comments
	 */
	private String zextendfield108;
	/**
	 * $column.comments
	 */
	private String zextendfield109;
	/**
	 * $column.comments
	 */
	private String zextendfield110;
	/**
	 * $column.comments
	 */
	private Boolean ismakecbs;
	/**
	 * $column.comments
	 */
	private Boolean ismakedec;
	/**
	 * $column.comments
	 */
	private Boolean ismakeinvt;
	/**
	 * $column.comments
	 */
	private String company;
	/**
	 * $column.comments
	 */
	private String country;
	/**
	 * $column.comments
	 */
	private String distinateport;
	/**
	 * $column.comments
	 */
	private String feecurr;
	/**
	 * $column.comments
	 */
	private String ieport;
	/**
	 * $column.comments
	 */
	private String insurcurr;
	/**
	 * $column.comments
	 */
	private String othercurr;
	/**
	 * $column.comments
	 */
	private String portlin;
	/**
	 * $column.comments
	 */
	private String scmcoc;
	/**
	 * $column.comments
	 */
	private String trade;
	/**
	 * $column.comments
	 */
	private String tradearea;
	/**
	 * $column.comments
	 */
	private String transac;
	/**
	 * $column.comments
	 */
	private String transf;
	/**
	 * $column.comments
	 */
	private Date zextendfield111;
	/**
	 * $column.comments
	 */
	private Date zextendfield112;
	/**
	 * $column.comments
	 */
	private String iscreatefee;

}
