package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 物品基础信息主表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-21 14:53:42
 */
@Data
@TableName("mst_itm_all")
public class MstItmAllEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private String id;
	/**
	 * 工厂ID
	 */
	private String plantId;
	/**
	 * 工厂名称
	 */
	private String plantName;
	/**
	 * 产品编码
	 */
	private String itmCode;
	/**
	 * 产品名称
	 */
	private String itmName;
	/**
	 * 全球编码
	 */
	private String universalProductCode;
	/**
	 * 客户物品编码
	 */
	private String customItmCode;
	/**
	 * 单位
	 */
	private String uomCode;
	/**
	 * 单位名称
	 */
	private String uomName;
	/**
	 * 品目类型

	 */
	private String itmTypeCode;
	/**
	 * 品目类型名称
	 */
	private String itmTypeName;
	/**
	 * 物品区分
	 */
	private String itmCategoryCode;
	/**
	 * 物品区分名称
	 */
	private String itmCategoryName;
	/**
	 * 物品规格型号
	 */
	private String itmSpec;
	/**
	 * 是否IQC检验

	 */
	private String isIqc;
	/**
	 * 是否ROHS检验
	 */
	private String isRohs;
	/**
	 * 
IQC检验等级
	 */
	private String iqcLevel;
	/**
	 * RoHS检测周期
	 */
	private Integer rohsCycle;
	/**
	 * 上次RoHS检测日期
	 */
	private Date lastRohsDate;
	/**
	 * 是否保税品
	 */
	private String taxCode;
	/**
	 * 检验类型代码
	 */
	private String iqcType;
	/**
	 * 检验类型名称
	 */
	private String iqcTypeName;
	/**
	 * 供应商代码
	 */
	private String suppCode;
	/**
	 * 供应商名称
	 */
	private String suppName;
	/**
	 * 供应商物品代码
	 */
	private String suppItmCode;
	/**
	 * 陈村收货场所
	 */
	private String ccReceivingLocation;
	/**
	 * 陈村收货场所名称
	 */
	private String ccReceivingLocationName;
	/**
	 * 收货场所
	 */
	private String receivingLocation;
	/**
	 * 收货场所名
	 */
	private String receivingLocationName;
	/**
	 * 制造场所
	 */
	private String productionLocation;
	/**
	 * 制造场所名称
	 */
	private String productionLocationName;
	/**
	 * 存储场所
	 */
	private String storageLocation;
	/**
	 * 存储场所名称
	 */
	private String storageLocationName;
	/**
	 * 存储货架名称
	 */
	private String storageLocatorName;
	/**
	 * 存储货架
	 */
	private String storageLocator;
	/**
	 * SMD数料机间距
	 */
	private BigDecimal smdSpacing;
	/**
	 * 最小包装
	 */
	private BigDecimal moq;
	/**
	 * 产品描述
	 */
	private String itmComment;
	/**
	 * 批号管理/序列号管理
	 */
	private String isLot;
	/**
	 * 采购提前期
	 */
	private Long poCycle;
	/**
	 * 批号有效期
	 */
	private Long expCycle;
	/**
	 * 制造提前期
	 */
	private Long moCycle;
	/**
	 * 
	 */
	private String isDel;
	/**
	 * 
	 */
	private String createdByName;
	/**
	 * 
	 */
	private Date createdWhen;
	/**
	 * 
	 */
	private String lastModifiedByName;
	/**
	 * 
	 */
	private Date lastModifiedWhen;

}
