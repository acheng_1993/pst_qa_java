package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 10:46:23
 */
@Data
@TableName("chklist")
public class ChklistEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String cfmdep;
	/**
	 * 
	 */
	private String cfmid;
	/**
	 * 
	 */
	private Date cfmdatetime;
	/**
	 * 
	 */
	private String cfmfg;
	/**
	 * 
	 */
	private String viewable;

}
