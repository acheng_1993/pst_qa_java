package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 自动发送给KIA的邮件，对应的 techiss 的详细数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 12:10:12
 */
@Data
@TableName("auto_send_kia_email_details")
public class AutoSendKiaEmailDetailsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 关联 techiss.id
	 */
	private Integer techissId;
	/**
	 * 关联auto_send_kia_email.id
	 */
	private Integer autoSendKiaEmailId;
	
	/**
	 * 实际需要发送的标记：0：被取消，1：需要发送
	 */
	private Integer actualSendStatus;

	/**
	 * 文件大小
	 */
	private Long fileSize;
	
	private String doctype;

	private String ctrlno;

	private String remark;

	private String statusDesc;
}
