package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 制品品番前缀表检测进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-08-22 15:36:17
 */
@Data
@TableName("model_no_prefix_mgr_process")
public class ModelNoPrefixMgrProcessEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String idProcess;
	/**
	 * 
	 */
	private Date createdWhenProcess;
	/**
	 * 
	 */
	private String idProcess2;
	/**
	 * 
	 */
	private Date createdWhenProcess2;

}
