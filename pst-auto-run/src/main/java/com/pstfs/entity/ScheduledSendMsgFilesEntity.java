package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 计划发送消息设置表的文件id
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-18 14:42:43
 */
@Data
@TableName("scheduled_send_msg_files")
public class ScheduledSendMsgFilesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 关联 scheduled_send_msg.id
	 */
	private Integer scheduledSendMsgId;
	/**
	 * 关联 upload_file.id
	 */
	private Integer uploadFileId;
	/**
	 * 是否为附件，0：不是附件，出现在文本中，1：是附件，出现在附件栏位
	 */
	private Integer isAttachment;
	/**
	 * 原始文件名称，含后缀
	 */
	private String srcName;
	/**
	 * 文件后缀
	 */
	private String suffix;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
