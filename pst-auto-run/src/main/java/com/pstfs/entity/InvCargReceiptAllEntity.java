package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 物料签收单表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-21 10:55:42
 */
@Data
@TableName("inv_carg_receipt_all")
public class InvCargReceiptAllEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private String id;
	/**
	 * 工厂ID
	 */
	private String plantId;
	/**
	 * 场所代码
	 */
	private String locationCode;
	/**
	 * 场所名称
	 */
	private String locationName;
	/**
	 * 签收单编号
	 */
	private String receiptNo;
	/**
	 * 检验单号
	 */
	private String checkNo;
	/**
	 * 物品代码
	 */
	private String itmCode;
	/**
	 * 物品名称
	 */
	private String itmName;
	/**
	 * 应收数量
	 */
	private BigDecimal qtyPlan;
	/**
	 * 实收数量
	 */
	private BigDecimal qtyAct;
	/**
	 * 
	 */
	private BigDecimal qtyDiff;
	/**
	 * 单位
	 */
	private String uomCode;
	/**
	 * 单位名称
	 */
	private String uomName;
	/**
	 * 净重
	 */
	private BigDecimal netWeight;
	/**
	 * 毛重
	 */
	private BigDecimal grossWeight;
	/**
	 * 原产国
	 */
	private String origin;
	/**
	 * 供应商物品代码
	 */
	private String suppItmCode;
	/**
	 * 供应商代码
	 */
	private String suppCode;
	/**
	 * 供应商名称
	 */
	private String suppName;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 发票号
	 */
	private String invoiceNo;
	/**
	 * 签收单日期
	 */
	private Date receiptDate;
	/**
	 * 途径(序号)
	 */
	private String channelCode;
	/**
	 * 途径名称
	 */
	private String channelName;
	/**
	 * 物品区分
	 */
	private String itmCategoryCode;
	/**
	 * 物品区分名称
	 */
	private String itmCategoryName;
	/**
	 * 箱号
	 */
	private String cartonNo;
	/**
	 * 是否IQC检验

	 */
	private String isIqc;
	/**
	 * 检验类型代码
	 */
	private String iqcType;
	/**
	 * 检验类型名称
	 */
	private String iqcTypeName;
	/**
	 * 
	 */
	private BigDecimal minPack;
	/**
	 * 
	 */
	private String isDel;
	/**
	 * 
	 */
	private String createdByName;
	/**
	 * 
	 */
	private Date createdWhen;
	/**
	 * 
	 */
	private String lastModifiedByName;
	/**
	 * 
	 */
	private Date lastModifiedWhen;

}
