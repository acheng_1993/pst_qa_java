package com.pstfs.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-10-19 11:47:42
 */
@Data
@TableName("K_Day")
public class KDayEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId
	private Integer kId;
	/**
	 * $column.comments
	 */
	private Integer a0188;
	/**
	 * $column.comments
	 */
	private Date dutyDate;
	/**
	 * $column.comments
	 */
	private String shiftName;
	/**
	 * $column.comments
	 */
	private Date cardBegin;
	/**
	 * $column.comments
	 */
	private Date cardEnd;
	/**
	 * $column.comments
	 */
	private Integer lateMin;
	/**
	 * $column.comments
	 */
	private Integer earlyMin;
	/**
	 * $column.comments
	 */
	private Integer absentCount;
	/**
	 * $column.comments
	 */
	private Integer kDone;
	/**
	 * $column.comments
	 */
	private String leaveType1;
	/**
	 * $column.comments
	 */
	private BigDecimal leaveTime1;
	/**
	 * $column.comments
	 */
	private String leaveType2;
	/**
	 * $column.comments
	 */
	private BigDecimal leaveTime2;
	/**
	 * $column.comments
	 */
	private String outType1;
	/**
	 * $column.comments
	 */
	private BigDecimal outTime1;
	/**
	 * $column.comments
	 */
	private String outType2;
	/**
	 * $column.comments
	 */
	private BigDecimal outTime2;
	/**
	 * $column.comments
	 */
	private BigDecimal ondutyTime;
	/**
	 * $column.comments
	 */
	private Integer shiftNo;
	/**
	 * $column.comments
	 */
	private BigDecimal absentTime;
	/**
	 * $column.comments
	 */
	private Integer leaveCount;
	/**
	 * $column.comments
	 */
	private BigDecimal leaveTime;
	/**
	 * $column.comments
	 */
	private Integer outCount;
	/**
	 * $column.comments
	 */
	private BigDecimal outTime;
	/**
	 * $column.comments
	 */
	private String kActiona0188;
	/**
	 * $column.comments
	 */
	private Date kActiontime;
	/**
	 * $column.comments
	 */
	private String kMemory;
	/**
	 * $column.comments
	 */
	private String kDaystate;
	/**
	 * $column.comments
	 */
	private String kOutType;
	/**
	 * $column.comments
	 */
	private Integer kLeaveType;
	/**
	 * $column.comments
	 */
	private BigDecimal xjsj;
	/**
	 * $column.comments
	 */
	private Integer overId;
	/**
	 * $column.comments
	 */
	private String iskg;
}