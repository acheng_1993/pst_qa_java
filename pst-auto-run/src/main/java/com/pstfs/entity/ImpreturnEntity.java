package com.pstfs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * invoiceno --原单号, 
itemNo1 --传输序号,
zextendField11 --总表序号, 
contractno --手册号,
copInvtNo --分单号, 
billNo --核注清单号, 
itemNo2 --流水号,
declaredate --申报日期,
corresPondEntryNo --报关单号, 
ljptno --品番,
qty --数量,
amount --金额
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-02 11:01:30
 */
@Data
@TableName("impreturn")
public class ImpreturnEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId
	private String id;
	/**
	 * $column.comments
	 */
	private String invoiceno;
	/**
	 * $column.comments
	 */
	private Integer itemno1;
	/**
	 * $column.comments
	 */
	private String zextendfield11;
	/**
	 * $column.comments
	 */
	private String contractno;
	/**
	 * $column.comments
	 */
	private String copinvtno;
	/**
	 * $column.comments
	 */
	private String billno;
	/**
	 * $column.comments
	 */
	private Integer itemno2;
	/**
	 * $column.comments
	 */
	private Date declaredate;
	/**
	 * $column.comments
	 */
	private String correspondentryno;
	/**
	 * $column.comments
	 */
	private String ljptno;
	/**
	 * $column.comments
	 */
	private BigDecimal qty;
	/**
	 * $column.comments
	 */
	private BigDecimal amount;
	/**
	 * $column.comments
	 */
	private BigDecimal Netweight;
}
