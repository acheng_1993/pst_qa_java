package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-09-26 10:29:19
 */
@Data
@TableName("approveflow_processinstance")
public class ApproveflowProcessinstanceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * zj
	 */
	@TableId
	private Long id;
	/**
	 * 表单ID
	 */
	private Long formid;
	/**
	 * 流程名称
	 */
	private String processname;
	/**
	 * 流程模板ID
	 */
	private Long processtemplateid;
	/**
	 * 实例状态（1：审批中，2：审批通过，3：审批拒绝，4作废）
	 */
	private Integer status;
	/**
	 * 完成时间
	 */
	private Date finishtime;
	/**
	 * 审核状态
	 */
	private Long approvestatus;
	/**
	 * 表单内容Json
	 */
	private String formjson;
	/**
	 * 创建人
	 */
	private Long createrid;
	/**
	 * 创建时间
	 */
	private Date createtime;
	/**
	 * 编辑人
	 */
	private Long modifierid;
	/**
	 * 编辑时间
	 */
	private Date modifytime;
	/**
	 * 是否删除
	 */
	private Boolean isdeleted;
	/**
	 * 创建人
	 */
	private String creatername;
	/**
	 * 编辑人
	 */
	private String modifiername;

}
