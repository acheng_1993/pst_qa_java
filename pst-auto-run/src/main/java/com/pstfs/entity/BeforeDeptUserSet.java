package com.pstfs.entity;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
@Data
@TableName("before_dept_user_set")
public class BeforeDeptUserSet {
	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	
	/**
	 * 学历
	 */
	@TableField("a22")
	private String a22;
	
	/**
	 * 联系电话
	 */
	@TableField("a24")
	private String a24;
	
	/**
	 * 现住地址
	 */
	@TableField("a26")
	private String a26;
	
	/**
	 * 紧急联系人
	 */
	@TableField("a28")
	private String a28;
	
	/**
	 * 紧急联系电话
	 */
	@TableField("a29")
	private String a29;
	
	/**
	 * 出行方式
	 */
	@TableField("a33")
	private String a33;
	
	/**
	 * 姓名
	 */
	@TableField("user_name")
	private String userName;
	
	/**
	 * 创建日期
	 */
	@TableField("create_time")
	private Date createTime;
	
	/**
	 * 修改日期
	 */
	@TableField("update_time")
	private Date updateTime;
	
	/**
	 * 删除日期
	 */
	@TableField("delete_time")
	private Date deleteTime;
}