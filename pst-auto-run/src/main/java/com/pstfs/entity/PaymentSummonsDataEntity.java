package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 支付传票数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-09-26 11:34:53
 */
@Data
@TableName("payment_summons_data")
public class PaymentSummonsDataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type=IdType.AUTO)
	private Integer id;
	/**
	 * 借方科目
	 */
	private String accountSubject;
	/**
	 * 成本中心
	 */
	private String costCenter;
	/**
	 * 金额(RMB)
	 */
	private BigDecimal money;
	/**
	 * 实例状态（1：审批中，2：审批通过，3：审批拒绝，4作废）
	 */
	private Integer status;
	/**
	 * 申請人
	 */
	private String createrName;
	/**
	 * 付款日期
	 */
	private Date applyDate;
	/**
	 * 创建日期
	 */
	private Date docDate;
	/**
	 * 编号
	 */
	private String docNum;
	/**
	 * 数据源的主键id
	 */
	private Long formForminstanceId;
	/**
	 * 支付传票明细的下标
	 */
	private Integer detailIndex;
	/**
	 * 摘要
	 */
	private String memo;
	/**
	 * 申请人工号
	 */
	private String createrId;
}