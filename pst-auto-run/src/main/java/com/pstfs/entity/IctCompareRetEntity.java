package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ict和partList文件的对比结果
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-07-11 09:56:51
 */
@Data
@TableName("ict_compare_ret")
public class IctCompareRetEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 对比数据
	 */
	private String compareData;
	/**
	 * 对比的原始文件名1
	 */
	private Integer uploadIctFileListId1;
	/**
	 * 对比的原始文件名2
	 */
	private Integer uploadIctFileListId2;
	/**
	 * 对比类型，0：partList和ict对比，1：ict和ict对比，2：partList和partList对比。
	 */
	private Integer compareType;
	/**
	 * 确认进展，0：暂存，未提交，1：已提交，革新课未确认、2：革新课已确认，生技未确认、3：生技已确认，QA未确认、4：QA已确认
	 */
	private Integer confirmProgress;
	/**
	 * 邮件发送状态，0：未发送、1：已发送
	 */
	private Integer sendMailStatus;
	/**
	 * 最近一次邮件发送时间
	 */
	private Date lastSendMailDate;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
