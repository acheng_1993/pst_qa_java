package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 上传文件的删除进度表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-14 09:18:51
 */
@Data
@TableName("upload_file_delete_progress")
public class UploadFileDeleteProgressEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 进度id
	 */
	private Integer progressId;

}
