package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 随机子集项目
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-13 10:31:08
 */
@Data
@TableName("random_section_no")
public class RandomSectionNoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 项目名
	 */
	private String sectionName;
	/**
	 * 开始时间
	 */
	private Date startDate;
	/**
	 * 结束时间
	 */
	private Date endDate;
	/**
	 * 项目类型，0：DCB
	 */
	private Integer sectionType;
}
