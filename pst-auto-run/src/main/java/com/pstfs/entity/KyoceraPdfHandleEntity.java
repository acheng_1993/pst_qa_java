package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 京瓷的“交货指示”pdf处理情况表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-01-22 10:33:01
 */
@Data
@TableName("kyocera_pdf_handle")
public class KyoceraPdfHandleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 文件id，关联 upload_file.id
	 */
	private Integer uploadFileId;
	/**
	 * 上传的文件名
	 */
	private String uploadFileName;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * pdf处理状态，0:待处理，1:已处理，2:处理失败
	 */
	private Integer handleStatus;
	/**
	 * 处理完毕后产生的文件id
	 */
	private Integer handleUploadFileId;

}
