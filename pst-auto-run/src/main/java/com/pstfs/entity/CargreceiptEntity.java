package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-21 11:07:33
 */
@Data
@TableName("CargReceipt")
public class CargreceiptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId
	private String cargreceiptid;
	/**
	 * $column.comments
	 */
	private String docno;
	/**
	 * $column.comments
	 */
	private String docdate;
	/**
	 * $column.comments
	 */
	private String docmodel;
	/**
	 * $column.comments
	 */
	private String cartonno;
	/**
	 * $column.comments
	 */
	private String productcode;
	/**
	 * $column.comments
	 */
	private String productdesc;
	/**
	 * $column.comments
	 */
	private Float qty;
	/**
	 * $column.comments
	 */
	private String unit;
	/**
	 * $column.comments
	 */
	private Float nw;
	/**
	 * $column.comments
	 */
	private Float gw;
	/**
	 * $column.comments
	 */
	private String origin;
	/**
	 * $column.comments
	 */
	private String invoiceno;
	/**
	 * $column.comments
	 */
	private String vendor;
	/**
	 * $column.comments
	 */
	private String remark;
	/**
	 * $column.comments
	 */
	private Date createdate;
	/**
	 * $column.comments
	 */
	private String vendorid;
	/**
	 * $column.comments
	 */
	private Integer sqty;
	/**
	 * $column.comments
	 */
	private Date sdate;
	/**
	 * $column.comments
	 */
	private String serialno;
	/**
	 * $column.comments
	 */
	private String productcodeshow;

}
