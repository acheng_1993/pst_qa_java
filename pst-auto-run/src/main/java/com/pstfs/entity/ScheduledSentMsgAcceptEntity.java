package com.pstfs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 已经发送的消息接收者列表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-10 10:29:35
 */
@Data
@TableName("scheduled_sent_msg_accept")
public class ScheduledSentMsgAcceptEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 用户id，关联 qywx_user_list.userid
	 */
	private String userid;
	/**
	 * 已发送的消息id，关联 scheduled_sent_msg.id
	 */
	private Integer scheduledSentMsgId;
	/**
	 * 是否已读，0：未读，1：已读
	 */
	private Integer read;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

}
