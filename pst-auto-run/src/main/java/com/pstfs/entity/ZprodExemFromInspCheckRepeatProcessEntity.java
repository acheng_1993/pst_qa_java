package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 对zprod_exem_from_insp表的重复数据校验进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-26 09:38:34
 */
@Data
@TableName("zprod_exem_from_insp_check_repeat_process")
public class ZprodExemFromInspCheckRepeatProcessEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Integer id;
	/**
	 * 品番
	 */
	private String productCode;
	/**
	 * 导入日期
	 */
	private Date importDate;
	/**
	 * 最新版本
	 */
	private String lastVersion;
	
	/**
	 * 最近提醒日期
	 */
	private Date remindDate;
}
