package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 最大发送id，用于记录techiss已经发送的内容中的最大id
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-09-30 14:53:28
 */
@Data
@TableName("max_send_techiss_id")
public class MaxSendTechissIdEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	
	private Integer techissId;
}