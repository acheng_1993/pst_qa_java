package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 支付传票读取进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-09-26 11:35:12
 */
@Data
@TableName("form_forminstance_process")
public class FormForminstanceProcessEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 支付传票进度id
	 */
	private Long formForminstanceId;

}
