package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import org.springframework.data.annotation.Id;

import lombok.Data;

/**
 * 制品品番前缀表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-08-22 15:01:20
 */
@Data
@TableName("model_no_prefix_mgr")
public class ModelNoPrefixMgrEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 
	 */
	private String modelNo;
	/**
	 * 
	 */
	private String modelNoPrefix;

}
