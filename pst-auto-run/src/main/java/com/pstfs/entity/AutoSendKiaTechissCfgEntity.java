package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 自动发送到 kia 的进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 11:09:49
 */
@Data
@TableName("auto_send_kia_techiss_cfg")
public class AutoSendKiaTechissCfgEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(type = IdType.NONE)
	private Integer id;
	/**
	 * 发布日期，格式是：YYYY-MM-DD
	 */
	private String isdate;
	/**
	 * 关联：techiss.id
	 */
	private String techissId;

	/**
	 * 执行确认操作的用户id
	 */
	private String userId;
	
	/**
	 * 更新时间
	 */
	private Date updateTime;
}
