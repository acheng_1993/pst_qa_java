package com.pstfs.entity;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * time2系统装箱单明细
 * @author jiajian
 */

@Data
@TableName("tkr.export_time_tbl")
public class ExportTimeTblEntity {

	/**
	 * 单号
	 */
	private String oddNo;
	
	/**
	 * 板号
	 */
	private String boardNo;
	
	/**
	 * 物料标记
	 */
	private String itmTyp;

	/**
	 * 项号
	 */
	private Integer exgno;
	
	/**
	 * 产品型号
	 */
	private String copgno;
	
	/**
	 * 规范申报要素
	 */
	private String re;
	
	/**
	 * 单价
	 */
	private BigDecimal price;
	
	/**
	 * 货币名称
	 */
	private String curDesc;
	
	/**
	 * 版本号
	 */
	private Integer ver;
	
	/**
	 * 数量
	 */
	private BigDecimal qty;
	
	/**
	 * 总净重
	 */
	private BigDecimal totalnetWgt;
	
	/**
	 * 总毛重
	 */
	private BigDecimal totalgrossWgt;
	
	/**
	 * 品牌类型
	 */
	private String brandType;
	
	/**
	 * 箱数
	 */
	private BigDecimal totalboxQty;
	
	/**
	 * 日期
	 */
	private Date plDt;
	
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 板数
	 */
	private Integer boardQty;
	
	/**
	 * 包装类型
	 */
	private String packageTyp;
	
	/**
	 * 货币
	 */
	private String curId;
	
	/**
	 * 装箱单上传时顺序
	 */
	private Integer rowindex;
	
	/**
	 * 运输方式
	 */
	private String transport;
	
	/**
	 * 六联单号
	 */
	private String slOddNo;
	
	/**
	 * 大陆车牌
	 */
	private String chLicensePlate;
	
	/**
	 * 香港车牌
	 */
	private String hgLicensePlate;
	
	/**
	 * 车辆海关编码
	 */
	private String veCustNo;
	
	/**
	 * 司机海关编码
	 */
	private String drCustNo;
	
	/**
	 * 过境口岸
	 */
	private String transitPort;
	
	/**
	 * 货柜信息
	 */
	private String counterInfo;
}