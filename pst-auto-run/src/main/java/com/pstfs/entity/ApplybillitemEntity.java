package com.pstfs.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-02 11:01:40
 */
@Data
@TableName("ApplyBillItem")
public class ApplybillitemEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId(type = IdType.AUTO)
	private String id;
	/**
	 * $column.comments
	 */
	private Date createdate;
	/**
	 * $column.comments
	 */
	private String createuser;
	/**
	 * $column.comments
	 */
	private Date modifydate;
	/**
	 * $column.comments
	 */
	private String modifyuser;
	/**
	 * $column.comments
	 */
	private Integer optlock;
	/**
	 * $column.comments
	 */
	private Date updatedate;
	/**
	 * $column.comments
	 */
	private String updateuser;
	/**
	 * $column.comments
	 */
	private String zextendfield1;
	/**
	 * $column.comments
	 */
	private String zextendfield2;
	/**
	 * $column.comments
	 */
	private String zextendfield3;
	/**
	 * $column.comments
	 */
	private Float zextendfield6;
	/**
	 * $column.comments
	 */
	private Date zextendfield8;
	/**
	 * $column.comments
	 */
	private String zextendfield10;
	/**
	 * $column.comments
	 */
	private String zextendfield11;
	/**
	 * $column.comments
	 */
	private String zextendfield12;
	/**
	 * $column.comments
	 */
	private String zextendfield13;
	/**
	 * $column.comments
	 */
	private String zextendfield14;
	/**
	 * $column.comments
	 */
	private String zextendfield15;
	/**
	 * $column.comments
	 */
	private String zextendfield4;
	/**
	 * $column.comments
	 */
	private String zextendfield5;
	/**
	 * $column.comments
	 */
	private Float zextendfield7;
	/**
	 * $column.comments
	 */
	private Date zextendfield9;
	/**
	 * $column.comments
	 */
	private Float amount;
	/**
	 * $column.comments
	 */
	private String applybillitemoriginid;
	/**
	 * $column.comments
	 */
	private String brandtype;
	/**
	 * $column.comments
	 */
	private String cabinettype;
	/**
	 * $column.comments
	 */
	private Integer cbsserialno;
	/**
	 * $column.comments
	 */
	private String ciqcode;
	/**
	 * $column.comments
	 */
	private String companymergerkey;
	/**
	 * $column.comments
	 */
	private String containerno;
	/**
	 * $column.comments
	 */
	private String containersno;
	/**
	 * $column.comments
	 */
	private Date cutoffdate;
	/**
	 * $column.comments
	 */
	private Float decqty;
	/**
	 * $column.comments
	 */
	private Integer decserialno;
	/**
	 * $column.comments
	 */
	private String declarekey;
	/**
	 * $column.comments
	 */
	private String goodsattr;
	/**
	 * $column.comments
	 */
	private Float grossweight;
	/**
	 * $column.comments
	 */
	private String hscode;
	/**
	 * $column.comments
	 */
	private String hsmodel;
	/**
	 * $column.comments
	 */
	private String hsname;
	/**
	 * $column.comments
	 */
	private Integer hsno;
	/**
	 * $column.comments
	 */
	private String hsversionno;
	/**
	 * $column.comments
	 */
	private Integer invtserialno;
	/**
	 * $column.comments
	 */
	private Boolean ismakecbs;
	/**
	 * $column.comments
	 */
	private Boolean ismakedec;
	/**
	 * $column.comments
	 */
	private Boolean ismakeinvt;
	/**
	 * $column.comments
	 */
	private Integer itemno;
	/**
	 * $column.comments
	 */
	private String itemorderno;
	/**
	 * $column.comments
	 */
	private String markname;
	/**
	 * $column.comments
	 */
	private Integer mergerno;
	/**
	 * $column.comments
	 */
	private Float netweight;
	/**
	 * $column.comments
	 */
	private String note;
	/**
	 * $column.comments
	 */
	private String operatetype;
	/**
	 * $column.comments
	 */
	private Float overallunitprice;
	/**
	 * $column.comments
	 */
	private String overallunitptno;
	/**
	 * $column.comments
	 */
	private Integer palletnum;
	/**
	 * $column.comments
	 */
	private Integer pcs;
	/**
	 * $column.comments
	 */
	private Float price;
	/**
	 * $column.comments
	 */
	private Float qty;
	/**
	 * $column.comments
	 */
	private String shipcompany;
	/**
	 * $column.comments
	 */
	private String shipname;
	/**
	 * $column.comments
	 */
	private Float singlecount;
	/**
	 * $column.comments
	 */
	private Float tariffamount;
	/**
	 * $column.comments
	 */
	private String todecemsno;
	/**
	 * $column.comments
	 */
	private Float tradingqty;
	/**
	 * $column.comments
	 */
	private String transferparametername;
	/**
	 * $column.comments
	 */
	private String unit;
	/**
	 * $column.comments
	 */
	private Float unitgrossweight;
	/**
	 * $column.comments
	 */
	private Float unitnetweight;
	/**
	 * $column.comments
	 */
	private Float vatamount;
	/**
	 * $column.comments
	 */
	private String versionno;
	/**
	 * $column.comments
	 */
	private String voyageno;
	/**
	 * $column.comments
	 */
	private String wmsstockheadid;
	/**
	 * $column.comments
	 */
	private String wmsstockitemid;
	/**
	 * $column.comments
	 */
	private String zextendfield101;
	/**
	 * $column.comments
	 */
	private String zextendfield102;
	/**
	 * $column.comments
	 */
	private String zextendfield103;
	/**
	 * $column.comments
	 */
	private String zextendfield104;
	/**
	 * $column.comments
	 */
	private String zextendfield105;
	/**
	 * $column.comments
	 */
	private String zextendfield106;
	/**
	 * $column.comments
	 */
	private String zextendfield107;
	/**
	 * $column.comments
	 */
	private String zextendfield108;
	/**
	 * $column.comments
	 */
	private String zextendfield109;
	/**
	 * $column.comments
	 */
	private String zextendfield110;
	/**
	 * $column.comments
	 */
	private Integer orderbyoriginconfirm;
	/**
	 * $column.comments
	 */
	private String businessdept;
	/**
	 * $column.comments
	 */
	private String country;
	/**
	 * $column.comments
	 */
	private String curr;
	/**
	 * $column.comments
	 */
	private String destcity;
	/**
	 * $column.comments
	 */
	private String destinationcountry;
	/**
	 * $column.comments
	 */
	private String district;
	/**
	 * $column.comments
	 */
	private String material;
	/**
	 * $column.comments
	 */
	private String mbsmaintentype;
	/**
	 * $column.comments
	 */
	private String origplace;
	/**
	 * $column.comments
	 */
	private String origincountry;
	/**
	 * $column.comments
	 */
	private String outerpackage;
	/**
	 * $column.comments
	 */
	private String wrap;
	/**
	 * $column.comments
	 */
	private String applybillhead;
	/**
	 * $column.comments
	 */
	private String zextendfield111;
	/**
	 * $column.comments
	 */
	private String zextendfield112;
	/**
	 * $column.comments
	 */
	private String zextendfield113;
	/**
	 * $column.comments
	 */
	private String zextendfield114;
	/**
	 * $column.comments
	 */
	private String zextendfield115;
	/**
	 * $column.comments
	 */
	private String zextendfield116;
	/**
	 * $column.comments
	 */
	private String zextendfield117;
	/**
	 * $column.comments
	 */
	private String zextendfield118;
	/**
	 * $column.comments
	 */
	private String zextendfield119;
	/**
	 * $column.comments
	 */
	private String zextendfield120;
	/**
	 * $column.comments
	 */
	private String origininvtid;

}
