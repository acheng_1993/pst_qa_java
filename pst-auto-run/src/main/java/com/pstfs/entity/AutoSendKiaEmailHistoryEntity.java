package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 自动发送邮件到KIA历史记录。
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-19 10:45:07
 */
@Data
@TableName("auto_send_kia_email_history")
public class AutoSendKiaEmailHistoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.AUTO)
	private Integer id;
	/**
	 * 关联auto_send_kia_email.id
	 */
	private Integer autoSendKiaEmailId;
	/**
	 * 发送时间
	 */
	private Date sendDate;

}
