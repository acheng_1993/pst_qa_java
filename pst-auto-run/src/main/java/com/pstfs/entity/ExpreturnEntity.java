package com.pstfs.entity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-10 14:37:52
 */
@Data
@TableName("expreturn")
public class ExpreturnEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * $column.comments
	 */
	@TableId
	private String id;
	/**
	 * $column.comments
	 */
	private String outbillno;
	/**
	 * $column.comments
	 */
	private Integer itemno1;
	/**
	 * $column.comments
	 */
	private String contractno;
	/**
	 * $column.comments
	 */
	private String copinvtno;
	/**
	 * $column.comments
	 */
	private String billno;
	/**
	 * $column.comments
	 */
	private Integer itemno2;
	/**
	 * $column.comments
	 */
	private Date declaredate;
	/**
	 * $column.comments
	 */
	private String correspondentryno;
	/**
	 * $column.comments
	 */
	private String ptno;
	/**
	 * $column.comments
	 */
	private Float qty;
	/**
	 * $column.comments
	 */
	private Float amount;
	/**
	 * $column.comments
	 */
	private Date updatedate;

}
