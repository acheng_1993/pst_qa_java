package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.MalaysiaMinoltaPdfDetailsDataEntity;
import com.pstfs.mapper.MalaysiaMinoltaPdfDetailsDataMapper;

@Service("malaysiaMinoltaPdfDetailsDataService")
public class MalaysiaMinoltaPdfDetailsDataService extends ServiceImpl<MalaysiaMinoltaPdfDetailsDataMapper, MalaysiaMinoltaPdfDetailsDataEntity> {


}