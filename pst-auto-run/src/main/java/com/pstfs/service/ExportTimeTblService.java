package com.pstfs.service;
import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ExportTimeTblEntity;
import com.pstfs.mapper.ExportTimeTblMapper;

/**
 * time2系统装箱单明细业务类
 * @author jiajian
 */
@DS("time2")
@Service
public class ExportTimeTblService extends ServiceImpl<ExportTimeTblMapper, ExportTimeTblEntity>{

	private Page<ExportTimeTblEntity> hundredRowPage = new Page<>(1, 100);

	/**
	 * 获取time2系统装箱单信息
	 * @return
	 */
	public List<ExportTimeTblEntity> getExportTimeTblList(){
		return getExportTimeTblList(null,null);
	}

	/**
	 * 获取time2系统装箱单信息
	 * @param oddNo
	 * @param rowindex
	 * @return
	 */
	public List<ExportTimeTblEntity> getExportTimeTblList(String oddNo,Integer rowindex){
		QueryWrapper<ExportTimeTblEntity> queryWrapper = new QueryWrapper<ExportTimeTblEntity>();
		queryWrapper.eq(StringUtils.isNotEmpty(oddNo),"ODD_NO", oddNo);
		queryWrapper.gt(rowindex!=null,"ROWINDEX", rowindex);
		queryWrapper.or().gt(StringUtils.isNotEmpty(oddNo),"ODD_NO", oddNo);
		queryWrapper.orderByAsc("ODD_NO,ROWINDEX");
		queryWrapper.select("ODD_NO","BOARD_NO","ITM_TYP","EXGNO","COPGNO","RE","PRICE","CUR_DESC","VER","QTY","TOTALNET_WGT",
				"TOTALGROSS_WGT","BRAND_TYPE","TOTALBOX_QTY","PL_DT","REMARK","BOARD_QTY","PACKAGE_TYP","CUR_ID","ROWINDEX");
		return page(hundredRowPage,queryWrapper).getRecords();
	}
}