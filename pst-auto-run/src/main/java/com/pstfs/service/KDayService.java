package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.mapper.KDayMapper;
import com.pstfs.entity.KDayEntity;
@Service
@DS("zhongbao_hr")
public class KDayService extends ServiceImpl<KDayMapper, KDayEntity>{

}