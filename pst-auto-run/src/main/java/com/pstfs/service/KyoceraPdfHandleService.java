package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.KyoceraPdfHandleEntity;
import com.pstfs.mapper.KyoceraPdfHandleMapper;

@Service("kyoceraPdfHandleService")
public class KyoceraPdfHandleService extends ServiceImpl<KyoceraPdfHandleMapper, KyoceraPdfHandleEntity> {

}