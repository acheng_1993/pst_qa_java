package com.pstfs.service;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ImpapplybilldbtempEntity;
import com.pstfs.mapper.ImpapplybilldbtempMapper;

@DS("yun_guan_tong_idb")
@Service("impapplybilldbtempService")
public class ImpapplybilldbtempService extends ServiceImpl<ImpapplybilldbtempMapper, ImpapplybilldbtempEntity>{
	
	/**
	 * 获取最大的id
	 * @return
	 */
	public String maxId() {
		QueryWrapper<ImpapplybilldbtempEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("max(id) id");
		ImpapplybilldbtempEntity impapplybilldbtempEntity = getOne(queryWrapper);
		return impapplybilldbtempEntity == null?null:impapplybilldbtempEntity.getId();
	}
	
	/**
	 * 把传票号码 invoiceNo 相关的数据替换成 time2 系统中最新的数据
	 * @param invoiceNo
	 * @param impapplybilldbtempEntityList
	 */
	@Transactional(rollbackFor = Throwable.class)
	public void replaceDataList(String invoiceNo,List<ImpapplybilldbtempEntity> impapplybilldbtempEntityList) {
		QueryWrapper<ImpapplybilldbtempEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("invoiceNo", invoiceNo);
		remove(queryWrapper);
		saveBatch(impapplybilldbtempEntityList);
	}
}