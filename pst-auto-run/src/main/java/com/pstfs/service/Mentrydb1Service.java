package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.Mentrydb1;
import com.pstfs.mapper.Mentrydb1Mapper;
@DS("mes")
@Service("mentrydb1Service")
public class Mentrydb1Service extends ServiceImpl<Mentrydb1Mapper, Mentrydb1> {

}