package com.pstfs.service;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.UploadFileDeleteProgressEntity;
import com.pstfs.entity.UploadFileEntity;
import com.pstfs.entity.UploadFileTableEntity;
import com.pstfs.mapper.UploadFileDeleteProgressMapper;
import com.pstfs.mapper.UploadFileMapper;

@DS("pst-wxhr-app")
@Service("uploadFileService")
public class UploadFileService extends ServiceImpl<UploadFileMapper, UploadFileEntity> {
	Page<UploadFileEntity> hundredRowPage = new Page<>(1, 100);
	
	@Autowired
	private UploadFileTableService uploadFileTableService;
	
	/**
	 * 保存上传文件
	 * @param sha1
	 * @param path
	 * @return 返回主键id
	 */
	public Integer save(String sha1, String path) {
		UploadFileEntity uploadFileEntity = new UploadFileEntity();
		uploadFileEntity.setSha1(sha1);
		uploadFileEntity.setPath(path);
		uploadFileEntity.setCreateTime(new Date());
		uploadFileEntity.setUpdateTime(uploadFileEntity.getCreateTime());
		save(uploadFileEntity);
		return uploadFileEntity.getId();
	}
	
	/**
	 * 按照 sha1 获取上传文件信息
	 * @param sha1
	 * @param field
	 * @return
	 */
	public UploadFileEntity getBySha1(String sha1,String field) {
		QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("sha1", sha1);
		queryWrapper.select(field);
		return getOne(queryWrapper);
	}
	
	/**
	 * 获取一个新的进度id
	 * @param lastProgressId
	 * @param limit
	 * @return 如果有则获取新进度 id，否则返回 null
	 */
	public Integer newLastProgressId(int lastProgressId,int limit) {
		return baseMapper.newLastProgressId(lastProgressId, limit);
	}
	
	/**
	 * 未使用的文件列表
	 * @return
	 */
    public List<UploadFileEntity> notUseUploadFileList(int lastProgressId,int newLastProgressId) {
    	List<UploadFileTableEntity> uploadFileTableEntityList = uploadFileTableService.list();
    	QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
    	queryWrapper.select("id,path");
    	StringBuilder sb = new StringBuilder("where id > ").append(lastProgressId).append(" and id <= ").append(newLastProgressId).append(" and datediff(now(),create_time) >= 1 ");
    	if(uploadFileTableEntityList.isEmpty()) {
    		throw new RuntimeException("upload_file_table 表没有数据，停止删除！");
    	}
    	for (UploadFileTableEntity uploadFileTableEntity:uploadFileTableEntityList) {
    		sb.append(" and not exists(select id from ").append(uploadFileTableEntity.getTableName())
    		.append(" where ").append(uploadFileTableEntity.getTableName()).append(".").append(uploadFileTableEntity.getFileIdField())
    		.append(" = upload_file.id)");
    	}
    	sb.append("order by id asc");
    	queryWrapper.last(sb.toString());
    	return list(queryWrapper);
    }
    
    @Autowired
    private UploadFileDeleteProgressMapper uploadFileDeleteProgressMapper;
    
    /**
     * 删除未被使用的上传文件，并更新进度id
     * @param ids
     */
    @Transactional
    public void deleteNotUseUploadFile(List<Integer> ids,int newLastProgressId) {
    	this.removeByIds(ids);
    	UploadFileDeleteProgressEntity uploadFileDeleteProgressEntity = new UploadFileDeleteProgressEntity();
    	uploadFileDeleteProgressEntity.setId(0);
    	uploadFileDeleteProgressEntity.setProgressId(newLastProgressId);
    	uploadFileDeleteProgressMapper.updateById(uploadFileDeleteProgressEntity);
    }
    
    /*
     * 根据id获取对应的path
     */
    public UploadFileEntity getPathById(int id){
		QueryWrapper<UploadFileEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("id", id);
		queryWrapper.select("path,sha1");
		UploadFileEntity file = getOne(queryWrapper);
		if(file == null) {
			return null;
		}else {
			return file;
		}
	}
}