package com.pstfs.service;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.RandomSectionNoEntity;
import com.pstfs.mapper.RandomSectionNoMapper;

@DS("machine-learning")
@Service("randomSectionNoService")
public class RandomSectionNoService extends ServiceImpl<RandomSectionNoMapper, RandomSectionNoEntity> {

	private static DateTimeFormatter DATE_TIME_FORMATTER1 = DateTimeFormatter.ofPattern("YYYY-MM-dd EEEE");

	/**
	 * 获取最新项目
	 * @return
	 */
	public RandomSectionNoEntity getMaxStartDateData() {
		QueryWrapper<RandomSectionNoEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByDesc("start_date");
		queryWrapper.last("limit 1");
		return getOne(queryWrapper);
	}
	
	/**
	 * 生成随机子集项目
	 * @param sectionType
	 */
	public RandomSectionNoEntity genRandomSectionNoDCB(int sectionType) {
		LocalDateTime localDateTime = LocalDateTime.now();
		Date startDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
		int dayOfWeek = localDateTime.getDayOfWeek().getValue();
		LocalDateTime endLocalDateTime = null;
		int hour = localDateTime.getHour();
		int minute = localDateTime.getMinute();
		int second = localDateTime.getSecond();
		int overHour = 20-hour;
		if (dayOfWeek > 4) {
			endLocalDateTime = localDateTime.plusDays(2).plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
		} else if(dayOfWeek == 4) {
			if (overHour >= 0) {
				endLocalDateTime = localDateTime.plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
			} else {
				endLocalDateTime = localDateTime.plusDays(3).plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
			}
		} else if(dayOfWeek > 2) {
			endLocalDateTime = localDateTime.plusDays(1).plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
		} else if(dayOfWeek == 2) {
			if (overHour >= 0) {
				endLocalDateTime = localDateTime.plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
			} else {
				endLocalDateTime = localDateTime.plusDays(1).plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
			}
		} else if(dayOfWeek > 0) {
			endLocalDateTime = localDateTime.plusDays(1).plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
		} else if(dayOfWeek == 0) {
			if (overHour >= 0) {
				endLocalDateTime = localDateTime.plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
			} else {
				endLocalDateTime = localDateTime.plusDays(1).plusMinutes(30-minute).plusHours(overHour).minusSeconds(second);
			}
		}
		RandomSectionNoEntity randomSectionNoEntity = new RandomSectionNoEntity();
		randomSectionNoEntity.setStartDate(startDate);
		randomSectionNoEntity.setEndDate(Date.from(endLocalDateTime.atZone(ZoneId.systemDefault()).toInstant()));
		randomSectionNoEntity.setSectionType(sectionType);
		randomSectionNoEntity.setSectionName(DATE_TIME_FORMATTER1.format(localDateTime));
		save(randomSectionNoEntity);
		return randomSectionNoEntity;
	}
}