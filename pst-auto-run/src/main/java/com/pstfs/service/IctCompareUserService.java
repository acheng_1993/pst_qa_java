package com.pstfs.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.IctCompareUserEntity;
import com.pstfs.mapper.IctCompareUserMapper;

@DS("pst-wxhr-app")
@Service("ictCompareUserService")
public class IctCompareUserService extends ServiceImpl<IctCompareUserMapper, IctCompareUserEntity> {

	/**
	 * 根据角色类型获取邮箱地址
	 * 
	 * @param roleType
	 * @return
	 */
	public List<String> getMailByRoleType(int roleType) {
		QueryWrapper<IctCompareUserEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("role_type", roleType);
		queryWrapper.select("distinct mail");
		List<IctCompareUserEntity> list =list(queryWrapper);
		List<String> ret = new ArrayList<>();
		for (int i=0,len_i=list.size();i<len_i;i++) {
			ret.add(list.get(i).getMail());
		}
		return ret;
	}
}