package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.MaxSendTechissIdEntity;
import com.pstfs.mapper.MaxSendTechissIdMapper;

@Service("maxSendTechissIdService")
@DS("techpl")
public class MaxSendTechissIdService extends ServiceImpl<MaxSendTechissIdMapper, MaxSendTechissIdEntity>{

}