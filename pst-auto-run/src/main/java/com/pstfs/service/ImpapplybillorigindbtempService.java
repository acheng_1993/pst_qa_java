package com.pstfs.service;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ImpapplybillorigindbtempEntity;
import com.pstfs.mapper.ImpapplybillorigindbtempMapper;
@DS("yun_guan_tong_idb")
@Service("impapplybillorigindbtempService")
public class ImpapplybillorigindbtempService extends ServiceImpl<ImpapplybillorigindbtempMapper, ImpapplybillorigindbtempEntity> {

	/**
	 * 把传票号码 invoiceNo 相关的数据替换成 time2 系统中最新的数据
	 * @param invoiceNo
	 * @param impapplybilldbtempEntityList
	 */
	@Transactional(rollbackFor = Throwable.class)
	public void replaceDataList(String invoiceNo,List<ImpapplybillorigindbtempEntity> impapplybillorigindbtempEntityList) {
		QueryWrapper<ImpapplybillorigindbtempEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("invoiceNo", invoiceNo);
		remove(queryWrapper);
		saveBatch(impapplybillorigindbtempEntityList);
	}
}