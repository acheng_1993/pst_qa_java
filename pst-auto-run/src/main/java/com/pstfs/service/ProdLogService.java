package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ProdLog;
import com.pstfs.mapper.ProdLogMapper;

@DS("pst-wxhr-app")
@Service("prodLogService")
public class ProdLogService extends ServiceImpl<ProdLogMapper, ProdLog> {

}