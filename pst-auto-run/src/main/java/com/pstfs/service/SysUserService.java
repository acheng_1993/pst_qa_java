package com.pstfs.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.SysUserEntity;
import com.pstfs.mapper.SysUserMapper;

@DS("oaauth")
@Service("sysUserService")
public class SysUserService extends ServiceImpl<SysUserMapper, SysUserEntity> {

	/**
	 * 根据用户 id 列表获取用户名
	 * @param idList
	 * @return
	 */
	public Map<Long,String> getUsernameByIdList(List<Long> idList){
		QueryWrapper<SysUserEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("id", idList);
		queryWrapper.select("username,id");
		List<SysUserEntity> sysUserEntityList = list(queryWrapper);
		Map<Long,String> ret = new HashMap<>();
		for(int i=0,len_i=sysUserEntityList.size();i<len_i;i++) {
			SysUserEntity sysUserEntity = sysUserEntityList.get(i);
			ret.put(sysUserEntity.getId(), sysUserEntity.getUsername());
		}
		return ret;
	}
}