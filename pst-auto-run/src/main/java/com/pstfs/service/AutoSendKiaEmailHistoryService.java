package com.pstfs.service;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.AutoSendKiaEmailHistoryEntity;
import com.pstfs.mapper.AutoSendKiaEmailHistoryMapper;

@Service("autoSendKiaEmailHistoryService")
public class AutoSendKiaEmailHistoryService extends ServiceImpl<AutoSendKiaEmailHistoryMapper, AutoSendKiaEmailHistoryEntity> {


}