package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.UploadFileDeleteProgressEntity;
import com.pstfs.mapper.UploadFileDeleteProgressMapper;
@DS("pst-wxhr-app")
@Service("uploadFileDeleteProgressService")
public class UploadFileDeleteProgressService extends ServiceImpl<UploadFileDeleteProgressMapper, UploadFileDeleteProgressEntity> {

	/**
	 * 更新进度
	 * @param progressId
	 */
	public void updateProgress(Integer progressId) {
		UploadFileDeleteProgressEntity uploadFileDeleteProgressEntity = new UploadFileDeleteProgressEntity();
		uploadFileDeleteProgressEntity.setId(0);
		uploadFileDeleteProgressEntity.setProgressId(progressId);
		updateById(uploadFileDeleteProgressEntity);
	}
	
	/**
	 * 重置进度
	 */
	public void resetProgress() {
		updateProgress(0);
	}
}