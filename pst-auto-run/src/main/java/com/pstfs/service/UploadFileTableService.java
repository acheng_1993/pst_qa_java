package com.pstfs.service;

import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.UploadFileTableEntity;
import com.pstfs.mapper.UploadFileTableMapper;

@DS("pst-wxhr-app")
@Service("uploadFileTableService")
public class UploadFileTableService extends ServiceImpl<UploadFileTableMapper, UploadFileTableEntity> {

}