package com.pstfs.service;

import org.springframework.stereotype.Service;
import com.pstfs.entity.A01Entity;
import com.pstfs.mapper.A01Mapper;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
@DS("zhongbao_hr")
public class A01Service extends ServiceImpl<A01Mapper, A01Entity>{

}