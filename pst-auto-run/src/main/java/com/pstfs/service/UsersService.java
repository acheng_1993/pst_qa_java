package com.pstfs.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.AutoSendKiaTechissCfgEntity;
import com.pstfs.entity.UsersEntity;
import com.pstfs.mapper.UsersMapper;

@Service("usersService")
@DS("techpl")
public class UsersService extends ServiceImpl<UsersMapper, UsersEntity> {
	
	@Autowired
	private AutoSendKiaTechissCfgService autoSendKiaTechissCfgService;
	
	/**
	 * 获取操作员的邮箱
	 * @return
	 */
	public List<String> getOperEmail() {
		AutoSendKiaTechissCfgEntity autoSendKiaTechissCfgEntity = autoSendKiaTechissCfgService.getOne(null);
		List<String> logidList = new ArrayList<String>();
		String userId = autoSendKiaTechissCfgEntity.getUserId();
		if (StringUtils.isNotEmpty(userId)) {
			String[] userIdList = userId.split(";");
			for(int i=0,len_i=userIdList.length;i<len_i;i++) {
				logidList.add(userIdList[i]);
			}
		}
		List<String> ret = new ArrayList<>();
		if(!logidList.isEmpty()) {
			QueryWrapper<UsersEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.in("logid", logidList);
			queryWrapper.select("email");
			List<UsersEntity> usersEntityList = list(queryWrapper);
			for (int i=0,len_i= usersEntityList.size();i<len_i;i++) {
				ret.add(usersEntityList.get(i).getEmail());
			}
		}
		return ret;
	}
}