package com.pstfs.service;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.FormForminstanceProcessEntity;
import com.pstfs.entity.PaymentSummonsDataEntity;
import com.pstfs.mapper.PaymentSummonsDataMapper;

@DS("pst-wxhr-app")
@Service("paymentSummonsDataService")
public class PaymentSummonsDataService extends ServiceImpl<PaymentSummonsDataMapper, PaymentSummonsDataEntity> {

	@Autowired
	private FormForminstanceProcessService formForminstanceProcessService;
	
	/**
	 * 批量更新支付传票信息
	 * @param paymentSummonsDataEntityList
	 * @return
	 */
	public boolean updateBatch(List<PaymentSummonsDataEntity> paymentSummonsDataEntityList) {
		List<PaymentSummonsDataEntity> updateList = new ArrayList<>();
		for(int i=0,len_i = paymentSummonsDataEntityList.size();i<len_i;i++) {
			PaymentSummonsDataEntity tempEntity = paymentSummonsDataEntityList.get(i);
			PaymentSummonsDataEntity paymentSummonsDataEntity = new PaymentSummonsDataEntity();
			paymentSummonsDataEntity.setId(tempEntity.getId());
			paymentSummonsDataEntity.setAccountSubject(tempEntity.getAccountSubject());
			paymentSummonsDataEntity.setApplyDate(tempEntity.getApplyDate());
			paymentSummonsDataEntity.setDocDate(tempEntity.getDocDate());
			paymentSummonsDataEntity.setCostCenter(tempEntity.getCostCenter());
			paymentSummonsDataEntity.setDocNum(tempEntity.getDocNum());
			paymentSummonsDataEntity.setStatus(tempEntity.getStatus());
			paymentSummonsDataEntity.setMoney(tempEntity.getMoney());
			paymentSummonsDataEntity.setMemo(tempEntity.getMemo());
			updateList.add(paymentSummonsDataEntity);
		}
		return updateBatchById(updateList);
	}
	
	/**
	 * 批量保存支付传票信息
	 * @param paymentSummonsDataEntityList
	 * @return
	 */
	@Transactional
	public boolean saveBatch(List<PaymentSummonsDataEntity> paymentSummonsDataEntityList) {
		List<PaymentSummonsDataEntity> saveList = new ArrayList<>();
		long maxFormForminstanceId = Long.MIN_VALUE;
		for(int i=0,len_i = paymentSummonsDataEntityList.size();i<len_i;i++) {
			PaymentSummonsDataEntity tempEntity = paymentSummonsDataEntityList.get(i);
			PaymentSummonsDataEntity paymentSummonsDataEntity = new PaymentSummonsDataEntity();
			paymentSummonsDataEntity.setStatus(tempEntity.getStatus());
			paymentSummonsDataEntity.setMoney(tempEntity.getMoney());
			paymentSummonsDataEntity.setFormForminstanceId(tempEntity.getFormForminstanceId());
			paymentSummonsDataEntity.setDocNum(tempEntity.getDocNum());
			paymentSummonsDataEntity.setDocDate(tempEntity.getDocDate());
			paymentSummonsDataEntity.setCreaterName(tempEntity.getCreaterName());
			paymentSummonsDataEntity.setCostCenter(tempEntity.getCostCenter());
			paymentSummonsDataEntity.setApplyDate(tempEntity.getApplyDate());
			paymentSummonsDataEntity.setAccountSubject(tempEntity.getAccountSubject());
			paymentSummonsDataEntity.setDetailIndex(tempEntity.getDetailIndex());
			paymentSummonsDataEntity.setMemo(tempEntity.getMemo());
			paymentSummonsDataEntity.setCreaterId(tempEntity.getCreaterId());
			saveList.add(paymentSummonsDataEntity);
			if(tempEntity.getFormForminstanceId()>maxFormForminstanceId) {
				maxFormForminstanceId = tempEntity.getFormForminstanceId(); 
			}
		}
		FormForminstanceProcessEntity tempEntity = formForminstanceProcessService.getOne(null);
		if(tempEntity == null) {
			tempEntity = new FormForminstanceProcessEntity();
			tempEntity.setId(0);
			tempEntity.setFormForminstanceId(maxFormForminstanceId);
			formForminstanceProcessService.save(tempEntity);
		} else {
			tempEntity.setFormForminstanceId(maxFormForminstanceId);
			formForminstanceProcessService.updateById(tempEntity);
		}
		return super.saveBatch(saveList);
	}
}