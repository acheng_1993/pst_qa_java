package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.AutoSendEmailToMakercdInfoEntity;
import com.pstfs.mapper.AutoSendEmailToMakercdInfoMapper;

@Service("autoSendEmailToMakercdInfoService")
public class AutoSendEmailToMakercdInfoService extends ServiceImpl<AutoSendEmailToMakercdInfoMapper, AutoSendEmailToMakercdInfoEntity> {


}