package com.pstfs.service;
import java.util.List;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.BgImpSpecListViewEntity;
import com.pstfs.mapper.BgImpSpecListViewMapper;

/**
 * time2系统货物累总表业务类
 * @author jiajian
 */
@DS("time2")
@Service
public class BgImpSpecListViewService extends ServiceImpl<BgImpSpecListViewMapper, BgImpSpecListViewEntity>{
	
	/**
	 * 根据传票号码获取货物累总信息
	 * @param impInvNo
	 * @return
	 */
	public List<BgImpSpecListViewEntity> getBgImpSpecListViewEntityList(String impInvNo) {
		QueryWrapper<BgImpSpecListViewEntity> queryWrapper = new QueryWrapper<BgImpSpecListViewEntity>();
		queryWrapper.eq("IMP_INV_NO", impInvNo);
		queryWrapper.select("REF_NO,ITEM_TYP,FREE_SU1,FRVC_3,ITEM_CD,RCV_KSQ,USER_ID,SPEC,U_PRIC,RCV_AMNT,"
		+ "CUR_ID,P_ODR_NO,ENT_DT,NW,WGT,FREE_TXT6,ITM_SPEC_NM,DIV_CD,CY_NM,CSTM_QTY,CSTM_KSQ,CRTN_QTY,RCV_QTY,"
		+ "IMP_INV_NO,IMP_CUR_ID");
		return list(queryWrapper);
	}
}