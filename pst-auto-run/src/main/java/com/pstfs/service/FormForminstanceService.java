package com.pstfs.service;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.FormForminstanceEntity;
import com.pstfs.mapper.FormForminstanceMapper;

@DS("oa")
@Service("formForminstanceService")
public class FormForminstanceService extends ServiceImpl<FormForminstanceMapper, FormForminstanceEntity> {

	/**
	 * 获取已经删除的支付传票
	 * @param idList
	 * @return
	 */
	public List<Long> getIsDeleted(List<Long> idList){
		QueryWrapper<FormForminstanceEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("IsDeleted", 1);
		queryWrapper.in("id", idList);
		queryWrapper.select("id");
		List<FormForminstanceEntity> tempList = list(queryWrapper);
		List<Long> ret = new ArrayList<Long>(tempList.size());
		for(int i=0,len_i=tempList.size();i<len_i;i++) {
			ret.add(tempList.get(i).getId());
		}
		return ret;
	}
	
	/**
	 * 获取 lastId 后面的数据
	 * @param lastId
	 * @return
	 */
	public List<FormForminstanceEntity> getNextList(long lastId){
		QueryWrapper<FormForminstanceEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("IsDeleted", 0);
		queryWrapper.gt("id", lastId);
		queryWrapper.eq("templateId", 4071379870737895424L);
		queryWrapper.select("id,docNum,formDataJson,createrName,createrId");
		queryWrapper.last(" limit 100");
		return list(queryWrapper);
	}
	
	/**
	 * 获取 lastId 后面的数据
	 * @param lastId
	 * @return
	 */
	public List<FormForminstanceEntity> getNextList(){
		return getNextList(-1);
	}
}