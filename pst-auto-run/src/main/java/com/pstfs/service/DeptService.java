package com.pstfs.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.DeptEntity;
import com.pstfs.mapper.DeptMapper;

@Service("deptService")
@DS("techpl")
public class DeptService extends ServiceImpl<DeptMapper, DeptEntity> {

	private Map<String,DeptEntity> deptMap = new HashMap<>();

	/**
	 * 根据 deptid 获取 Dept 信息
	 * @param deptid
	 * @return
	 */
	public DeptEntity getByDeptId(String deptid) {
		if(deptMap.isEmpty()) {
			synchronized (this) {
				if(deptMap.isEmpty()) {
					List<DeptEntity> deptEntityList = list();
					Map<String,DeptEntity> deptMap = new HashMap<>();
					for(int i=0,len_i = deptEntityList.size();i<len_i;i++) {
						DeptEntity deptEntity = deptEntityList.get(i);
						deptMap.put(deptEntity.getDeptid(), deptEntity);
					}
					this.deptMap = deptMap;
				}
				return deptMap.get(deptid);
			}
		}
		return deptMap.get(deptid);
	}
}