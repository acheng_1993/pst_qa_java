package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ApplyBillItemCurrentIdEntity;
import com.pstfs.mapper.ApplyBillItemCurrentIdMapper;

@DS("pst-wxhr-app")
@Service("applyBillItemCurrentIdService")
public class ApplyBillItemCurrentIdService extends ServiceImpl<ApplyBillItemCurrentIdMapper, ApplyBillItemCurrentIdEntity> {
	
	/**
	 * 更新进度 id
	 * @param currentId
	 * @return
	 */
	public boolean update(int id,String currentId,String currentInvoiceno,String handleDate) {
		ApplyBillItemCurrentIdEntity applyBillItemCurrentIdEntity=new ApplyBillItemCurrentIdEntity();
		applyBillItemCurrentIdEntity.setId(id);
		applyBillItemCurrentIdEntity.setCurrentId(currentId);
		applyBillItemCurrentIdEntity.setCurrentInvoiceno(currentInvoiceno);
		applyBillItemCurrentIdEntity.setHandleDate(handleDate);
		return updateById(applyBillItemCurrentIdEntity);
	}
}