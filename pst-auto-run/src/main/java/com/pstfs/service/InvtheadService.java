package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.InvtheadEntity;
import com.pstfs.mapper.InvtheadMapper;

@DS("yun_guan_tong_cmp")
@Service("invtheadService")
public class InvtheadService extends ServiceImpl<InvtheadMapper, InvtheadEntity> {

	/**
	 * 获取下一份要处理的报关单号
	 * @param currentInvoiceno
	 * @return
	 */
	public String getNextCopInvtNo(String currentInvoiceno) {
		return baseMapper.getNextCopInvtNo(currentInvoiceno);
	}
}