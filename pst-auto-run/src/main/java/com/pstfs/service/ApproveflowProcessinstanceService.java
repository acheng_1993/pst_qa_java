package com.pstfs.service;
import java.util.List;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ApproveflowProcessinstanceEntity;
import com.pstfs.mapper.ApproveflowProcessinstanceMapper;

@DS("oa")
@Service("approveflowProcessinstanceService")
public class ApproveflowProcessinstanceService extends ServiceImpl<ApproveflowProcessinstanceMapper, ApproveflowProcessinstanceEntity>{

	/**
	 * 按照表单 id 获取表单数据详情
	 * @param formForminstanceIdList
	 * @return
	 */
	public List<ApproveflowProcessinstanceEntity> getListByFormIdList(List<Long> formForminstanceIdList,String select){
		QueryWrapper<ApproveflowProcessinstanceEntity> queryWrapper = new QueryWrapper<>();
		StringBuilder sb = new StringBuilder();
		String connChar = "";
		for(int i=0,len_i=formForminstanceIdList.size();i<len_i;i++) {
			sb.append(connChar).append(formForminstanceIdList.get(i));
			connChar = ",";
		}
		queryWrapper.eq("isDeleted", 0);
		queryWrapper.select(select);
		queryWrapper.inSql("(id,formId)", "select max(id) id,formId from approveflow_processinstance where formId in("+
				sb.toString()+") group by formId");
		return list(queryWrapper);
	}
}