package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.CargreceiptEntity;
import com.pstfs.mapper.CargreceiptMapper;

@DS("mes")
@Service("cargreceiptService")
public class CargreceiptService extends ServiceImpl<CargreceiptMapper, CargreceiptEntity> {

}