package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.IqcCheckListProcessEntity;
import com.pstfs.mapper.IqcCheckListProcessMapper;

@DS("pst-wxhr-app")
@Service("iqcCheckListProcessService")
public class IqcCheckListProcessService extends ServiceImpl<IqcCheckListProcessMapper, IqcCheckListProcessEntity> {

}