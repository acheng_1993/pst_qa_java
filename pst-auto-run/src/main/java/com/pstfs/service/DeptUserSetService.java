package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.DeptUserSet;
import com.pstfs.mapper.DeptUserSetMapper;

@Service
@DS("zhongbao_msg")
public class DeptUserSetService extends ServiceImpl<DeptUserSetMapper, DeptUserSet>{

}
