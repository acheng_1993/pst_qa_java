package com.pstfs.service;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.InqSyukoView;
import com.pstfs.entity.extend.InqSyukoViewExtend;
import com.pstfs.mapper.InqSyukoViewMapper;

@DS("time2")
@Service
public class InqSyukoViewService extends ServiceImpl<InqSyukoViewMapper, InqSyukoView>{

	/**
	 * 根据订单号获取出货日期
	 * @param invno
	 * @return
	 */
	public Date shpDt(String invno) {
		QueryWrapper<InqSyukoView> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("INVNO", invno);
		queryWrapper.eq("FC_ID", "PF");
		queryWrapper.eq("PKG_CRTN_NO", "1");
		queryWrapper.select("SHP_DT");
		InqSyukoView inqSyukoView = getOne(queryWrapper);
		return inqSyukoView == null? null : inqSyukoView.getShpDt();
	}
	
	/**
	 * 检查订单号是否存在
	 * @param invno
	 * @return
	 */
	public boolean invnoExist(String invno) {
		QueryWrapper<InqSyukoView> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("INVNO", invno);
		queryWrapper.eq("FC_ID", "PF");
		queryWrapper.select("INVNO");
		Page<InqSyukoView> page = new Page<>(0, 1);
		return page(page, queryWrapper).getRecords().size() > 0;
	}
	
	/**
	 * 根据订单号获取出货信息
	 * @param invno
	 * @return
	 */
	public List<InqSyukoViewExtend> getInqSyukoViewListByInvno(String invno) {
		QueryWrapper<InqSyukoViewExtend> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("ISV.INVNO", invno);
		queryWrapper.eq("ISV.FC_ID", "PF");
		queryWrapper.eq("IHT.FC_ID", "PF");
		queryWrapper.eq("KB.FC_ID", "PF");
		queryWrapper.orderByAsc("ISV.PKG_CRTN_NO");
		queryWrapper.select(
				"ISV.INVNO INVNO",
				"ISV.CLNT_NM CLNT_NM",
				"ISV.CUST_ODR_NO CUST_ODR_NO",
				"ISV.CUST_ITEM_CD CUST_ITEM_CD",
				"ISV.PKG_CRTN_NO PKG_CRTN_NO",
				"ISV.PKG_CRTN_QTY PKG_CRTN_QTY",
				"ISV.CUST_ITEM_CD CUST_ITEM_CD",
				"ISV.S_ODR_NO S_ODR_NO",
				"ISV.EU_CUST_ITEM_CD EU_CUST_ITEM_CD",
				"ISV.CUST_ODR_NO CUST_ODR_NO",
				"ISV.ITM_NM ITM_NM",
				"ISV.PKG_QTY PKG_QTY",
				"ISV.SHP_QTY SHP_QTY",
				"ISV.PKG_NW PKG_NW",
				"ISV.PKG_GW PKG_GW",
				"ISV.PKG_SZ PKG_SZ",
				"ISV.SHP_DT SHP_DT",
				"ISV.CUST_CD CUST_CD",
				"IHT.INV_DT INV_DT",
				"ISV.CUST_INST_NO CUST_INST_NO",
				"KB.PAC_SZ PAC_SZ",
				"KB.PAC_QTY PAC_QTY");
		return baseMapper.selectInqSyukoViewExtendList(queryWrapper);
	}
}