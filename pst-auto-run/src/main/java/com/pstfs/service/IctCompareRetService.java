package com.pstfs.service;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.IctCompareRetEntity;
import com.pstfs.entity.extend.IctCompareRetExtend;
import com.pstfs.mapper.IctCompareRetMapper;

@DS("pst-wxhr-app")
@Service("ictCompareRetService")
public class IctCompareRetService extends ServiceImpl<IctCompareRetMapper, IctCompareRetEntity> {

	/**
	 * 获取待发送邮件的列表
	 * @return
	 */
	public List<IctCompareRetExtend> toBeSendEmailList() {
		return baseMapper.toBeSendEmailList();
	}
	
	/**
	 * 批量更新最近发送邮件的日期
	 * @param idList
	 * @return
	 */
	public boolean updateLastSendMailDate(List<Integer> idList) {
		UpdateWrapper<IctCompareRetEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.set("last_send_mail_date", new Date());
		updateWrapper.in("id", idList);
		return update(updateWrapper);
	}
}