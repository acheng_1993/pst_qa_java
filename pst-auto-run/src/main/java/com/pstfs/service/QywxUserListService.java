package com.pstfs.service;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.QywxUserListEntity;
import com.pstfs.mapper.QywxUserListMapper;

@Service
@DS("pst-wxhr-app")
public class QywxUserListService extends ServiceImpl<QywxUserListMapper, QywxUserListEntity>{

}