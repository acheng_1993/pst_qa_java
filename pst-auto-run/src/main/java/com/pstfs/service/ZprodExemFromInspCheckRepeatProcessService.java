package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ZprodExemFromInspCheckRepeatProcessEntity;
import com.pstfs.mapper.ZprodExemFromInspCheckRepeatProcessMapper;

@DS("pst-wxhr-app")
@Service("zprodExemFromInspCheckRepeatProcessService")
public class ZprodExemFromInspCheckRepeatProcessService extends ServiceImpl<ZprodExemFromInspCheckRepeatProcessMapper, ZprodExemFromInspCheckRepeatProcessEntity> {

}