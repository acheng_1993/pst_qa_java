package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ScheduledSentMsgFilesEntity;
import com.pstfs.mapper.ScheduledSentMsgFilesMapper;

@Service("scheduledSentMsgFilesService")
public class ScheduledSentMsgFilesService extends ServiceImpl<ScheduledSentMsgFilesMapper, ScheduledSentMsgFilesEntity> {

}