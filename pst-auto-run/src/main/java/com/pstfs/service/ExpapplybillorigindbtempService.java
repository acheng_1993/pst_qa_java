package com.pstfs.service;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ExpapplybillorigindbtempEntity;
import com.pstfs.mapper.ExpapplybillorigindbtempMapper;
@DS("yun_guan_tong_idb")
@Service("expapplybillorigindbtempService")
public class ExpapplybillorigindbtempService extends ServiceImpl<ExpapplybillorigindbtempMapper, ExpapplybillorigindbtempEntity> {

	/**
	 * 把装箱单号 outBillNo 相关的数据替换成 time2 系统中最新的数据
	 * @param outbillno
	 * @param expapplybilldbtempEntityList
	 */
	@Transactional(rollbackFor = Throwable.class)
	public void replaceDataList(String outBillNo,List<ExpapplybillorigindbtempEntity> expapplybillorigindbtempEntityList) {
		QueryWrapper<ExpapplybillorigindbtempEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("outBillNo", outBillNo);
		remove(queryWrapper);
		saveBatch(expapplybillorigindbtempEntityList);
	}
}