package com.pstfs.service;
import java.time.LocalDate;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ScheduledSendMsgEntity;
import com.pstfs.entity.ScheduledSentMsgEntity;
import com.pstfs.mapper.ScheduledSentMsgAcceptMapper;
import com.pstfs.mapper.ScheduledSentMsgFilesMapper;
import com.pstfs.mapper.ScheduledSentMsgMapper;

@DS("pst-wxhr-app")
@Service("scheduledSentMsgService")
public class ScheduledSentMsgService extends ServiceImpl<ScheduledSentMsgMapper, ScheduledSentMsgEntity> {
	
	@Autowired
	private ScheduledSendMsgService scheduledSendMsgService;
	
	@Autowired
	private ScheduledSentMsgAcceptMapper scheduledSentMsgAcceptMapper;
	
	@Autowired
	private ScheduledSentMsgFilesMapper scheduledSentMsgFilesMapper;
	
	@Value("${uploadFilePath}")
	private String uploadFilePath;
	
	@Value("${qywxUploadFilePath}")
	private String qywxUploadFilePath;

	@Transactional
	public int add(ScheduledSentMsgEntity scheduledSentMsgEntity) {
		Date now = new Date();
		LocalDate nowLocalDate = LocalDate.now();
		LocalDate currentYear = LocalDate.of(nowLocalDate.getDayOfYear(), 1, 1);
		int restDay = nowLocalDate.getDayOfYear()-currentYear.getDayOfWeek().getValue();
		int weekOfYear = restDay%7>0?restDay/7+1:restDay/7;
		ScheduledSentMsgEntity saveData = new ScheduledSentMsgEntity();
		saveData.setTitle(scheduledSentMsgEntity.getTitle());
		saveData.setContentHtml(scheduledSentMsgEntity.getContentHtml());
		saveData.setContentText(scheduledSentMsgEntity.getContentText());
		saveData.setScheduledSendMsgId(scheduledSentMsgEntity.getScheduledSendMsgId());
		saveData.setSendWechat(0);
		saveData.setCreateTime(now);
		saveData.setUpdateTime(now);
		saveData.setRedirectUrl(scheduledSentMsgEntity.getRedirectUrl());

		ScheduledSendMsgEntity updateData = new ScheduledSendMsgEntity();
		updateData.setId(scheduledSentMsgEntity.getScheduledSendMsgId());
		updateData.setLastSendDate(now);
		updateData.setUpdateTime(now);
		updateData.setLastSendDayOfWeek(nowLocalDate.getDayOfWeek().getValue());
		updateData.setLastSendDayOfYear(nowLocalDate.getDayOfYear());
		updateData.setLastSendMonthOfYear(nowLocalDate.getMonthValue());
		updateData.setLastSendWeekOfYear(weekOfYear);
		updateData.setLastSendYear(nowLocalDate.getYear());

		scheduledSendMsgService.updateById(updateData);
		save(saveData);
		scheduledSentMsgFilesMapper.insertBatch(saveData.getId(), scheduledSentMsgEntity.getScheduledSendMsgId());
		scheduledSentMsgAcceptMapper.insertBatch(saveData.getId());
		return saveData.getId();
	}
	
	/**
	 * 把发送到微信的状态标记为已发送
	 * @param id
	 */
	public void updateSendWechat(int id) {
		ScheduledSentMsgEntity updateData = new ScheduledSentMsgEntity();
		updateData.setId(id);
		updateData.setSendWechat(1);
		updateById(updateData);
	}
	
//	@Transactional
//	public void saveBatch(List<ScheduledSentMsgEntity> scheduledSentMsgEntityList) {
//		List<ScheduledSentMsgEntity> saveList = new ArrayList<>(scheduledSentMsgEntityList.size());
//		List<ScheduledSendMsgEntity> updateList = new ArrayList<>(scheduledSentMsgEntityList.size());
//		Date now = new Date();
//		LocalDate nowLocalDate = LocalDate.now();
//		LocalDate currentYear = LocalDate.of(nowLocalDate.getDayOfYear(), 1, 1);
//		int restDay = nowLocalDate.getDayOfYear()-currentYear.getDayOfWeek().getValue();
//		int weekOfYear = restDay%7>0?restDay/7+1:restDay/7;
//		for(ScheduledSentMsgEntity scheduledSentMsgEntity:scheduledSentMsgEntityList) {
//			ScheduledSentMsgEntity saveData = new ScheduledSentMsgEntity();
//			saveData.setTitle(scheduledSentMsgEntity.getTitle());
//			saveData.setContentHtml(scheduledSentMsgEntity.getContentHtml());
//			saveData.setContentText(scheduledSentMsgEntity.getContentText());
//			saveData.setScheduledSendMsgId(scheduledSentMsgEntity.getScheduledSendMsgId());
//			saveData.setCreateTime(now);
//			saveData.setUpdateTime(now);
//			saveList.add(saveData);
//
//			ScheduledSendMsgEntity updateData = new ScheduledSendMsgEntity();
//			updateData.setId(scheduledSentMsgEntity.getScheduledSendMsgId());
//			updateData.setLastSendDate(now);
//			updateData.setLastSendDayOfWeek(nowLocalDate.getDayOfWeek().getValue());
//			updateData.setLastSendDayOfYear(nowLocalDate.getDayOfYear());
//			updateData.setLastSendMonthOfYear(nowLocalDate.getMonthValue());
//			updateData.setLastSendWeekOfYear(weekOfYear);
//			updateData.setLastSendYear(nowLocalDate.getYear());
//
//			updateList.add(updateData);
//		}
//		saveBatch(saveList);
//		scheduledSendMsgService.updateBatchById(updateList);
//	}
}