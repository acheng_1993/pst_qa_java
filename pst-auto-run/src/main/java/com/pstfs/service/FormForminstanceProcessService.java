package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.FormForminstanceProcessEntity;
import com.pstfs.mapper.FormForminstanceProcessMapper;

@DS("pst-wxhr-app")
@Service("formForminstanceProcessService")
public class FormForminstanceProcessService extends ServiceImpl<FormForminstanceProcessMapper, FormForminstanceProcessEntity> {


}