package com.pstfs.service;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.IqcCheckListEntity;
import com.pstfs.entity.IqcCheckListProcessEntity;
import com.pstfs.entity.ZProdExemFromInspEntity;
import com.pstfs.entity.ZprodExemFromInspCheckRepeatProcessEntity;
import com.pstfs.mapper.ZProdExemFromInspMapper;

@DS("new_mes")
@Service("prodExemFromInspService")
public class ZProdExemFromInspService extends ServiceImpl<ZProdExemFromInspMapper, ZProdExemFromInspEntity> {

	/**
	 * 检测是否存在表 mst_itm_all 和 zprod_exem_from_insp 表的免检信息不一致的数据
	 * @return
	 */
	public boolean diffIsIqc() {
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.last(" zefi where exists(select ID from mst_itm_all mia where IS_IQC ='Y' and mia.ITM_CODE = zefi.product_code)"+
		"and (product_code,last_carg_receipt_date)in(select product_code,max(last_carg_receipt_date) last_carg_receipt_date from zprod_exem_from_insp where finish_date is null group by product_code)limit 1");
		queryWrapper.select("id");
		Object obj = getOne(queryWrapper);
		return obj != null;
	}
	
	@Autowired
	private ZprodExemFromInspCheckRepeatProcessService zprodExemFromInspCheckRepeatProcessService;
	
	/**
	 * 校验是否存在重复数据
	 * @return
	 */
	public boolean repeatDataExists() {
		//获取检验重复数据的进度
		ZprodExemFromInspCheckRepeatProcessEntity zprodExemFromInspCheckRepeatProcessEntity = zprodExemFromInspCheckRepeatProcessService.getOne(null);
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper = new QueryWrapper<>();
		if (zprodExemFromInspCheckRepeatProcessEntity != null && StringUtils.isNotEmpty(zprodExemFromInspCheckRepeatProcessEntity.getProductCode())) {
			queryWrapper.ge("import_date", zprodExemFromInspCheckRepeatProcessEntity.getImportDate());
		}
		queryWrapper.groupBy("product_code,version");
		queryWrapper.having("count(*)>1");
		queryWrapper.select("product_code,version");
		queryWrapper.last("limit 1");
		//如果发现有重复数据，则 zProdExemFromInspEntity!=null
		ZProdExemFromInspEntity zProdExemFromInspEntity = getOne(queryWrapper);
		return zProdExemFromInspEntity != null;
	}

	/**
	 * 检测新版本，如果有新版本返回 true，否则返回 false
	 * @return
	 */
	public boolean discoverNewVersion() {
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByAsc("version");
		queryWrapper.select("version");
		queryWrapper.last("limit 1");
		ZprodExemFromInspCheckRepeatProcessEntity zprodExemFromInspCheckRepeatProcessEntity = zprodExemFromInspCheckRepeatProcessService.getOne(null);
		String lastVersion = null;
		if(zprodExemFromInspCheckRepeatProcessEntity == null || StringUtils.isEmpty(lastVersion = zprodExemFromInspCheckRepeatProcessEntity.getLastVersion())) {
			ZProdExemFromInspEntity zProdExemFromInspEntity = getOne(queryWrapper);
			return zProdExemFromInspEntity != null;
		} else {
			queryWrapper.gt("version", lastVersion);
			ZProdExemFromInspEntity zProdExemFromInspEntity = getOne(queryWrapper);
			return zProdExemFromInspEntity != null;
		}
	}
	
	/**
	 * 按照版本号处理数据
	 * @return
	 */
	public boolean handleDataByVersion() {
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByDesc("version");
		queryWrapper.select("distinct version");
		queryWrapper.last("limit 2");
		List<ZProdExemFromInspEntity> zprodExemFromInspEntityList = list(queryWrapper);
		if (zprodExemFromInspEntityList.size() == 2) {
			String prevVersion = zprodExemFromInspEntityList.get(1).getVersion();
			String currentVersion = zprodExemFromInspEntityList.get(0).getVersion();
			QueryWrapper<ZProdExemFromInspEntity> queryWrapper2 = new QueryWrapper<>();
			queryWrapper2.or(query->{
				query.eq("version", prevVersion);
				query.isNull("finish_date");
			});
			queryWrapper2.or(query->{
				query.eq("version", currentVersion);
			});
			List<ZProdExemFromInspEntity> compareDataList = list(queryWrapper2);
			//整理出上个版本的数据
			Map<String,ZProdExemFromInspEntity> prevVerDataMap = new HashMap<>();
			//整理出当前版本的数据
			List<ZProdExemFromInspEntity> currentVerDataList = new ArrayList<>();
			for (int i=0,len_i=compareDataList.size();i<len_i;i++) {
				ZProdExemFromInspEntity entity = compareDataList.get(i);
				if(prevVersion.equals(entity.getVersion())) {
					prevVerDataMap.put(entity.getProductCode(), entity);
				} else {
					currentVerDataList.add(entity);
				}
			}
			Date now = new Date();
			List<ZProdExemFromInspEntity> updateBatchById = new ArrayList<>();
			for (int i=0,len_i=currentVerDataList.size();i<len_i;i++) {
				ZProdExemFromInspEntity currentVerData = currentVerDataList.get(i);
				ZProdExemFromInspEntity prevVerData = prevVerDataMap.get(currentVerData.getProductCode());
				if (prevVerData != null) {
					//移除上一版本的数据
					prevVerDataMap.remove(currentVerData.getProductCode());
					//设置结束日期
					prevVerData.setFinishDate(now);
					updateBatchById.add(prevVerData);
					//新旧版本都有的情况下，让新版本的最近收货日期和旧版本的一致
					currentVerData.setLastCargReceiptDate(prevVerData.getLastCargReceiptDate());
					updateBatchById.add(currentVerData);
				}
			}
			//旧版本特有，新版本没有的情况下，视为作废
			for (ZProdExemFromInspEntity zprodExemFromInspEntity:prevVerDataMap.values()) {
				zprodExemFromInspEntity.setFinishDate(now);
				updateBatchById.add(zprodExemFromInspEntity);
			}
			boolean ret = updateBatchById.isEmpty()?false:updateBatchById(updateBatchById);
			ZprodExemFromInspCheckRepeatProcessEntity zprodExemFromInspCheckRepeatProcessEntity = zprodExemFromInspCheckRepeatProcessService.getOne(null);
			if(zprodExemFromInspCheckRepeatProcessEntity == null) {
				zprodExemFromInspCheckRepeatProcessEntity = new ZprodExemFromInspCheckRepeatProcessEntity();
				zprodExemFromInspCheckRepeatProcessEntity.setId(0);
				zprodExemFromInspCheckRepeatProcessEntity.setLastVersion(currentVersion);
				zprodExemFromInspCheckRepeatProcessService.save(zprodExemFromInspCheckRepeatProcessEntity);
			} else {
				zprodExemFromInspCheckRepeatProcessEntity.setLastVersion(currentVersion);
				zprodExemFromInspCheckRepeatProcessService.updateById(zprodExemFromInspCheckRepeatProcessEntity);
			}
			return ret;
		} else if(zprodExemFromInspEntityList.size() == 1) {
			ZprodExemFromInspCheckRepeatProcessEntity zprodExemFromInspCheckRepeatProcessEntity = zprodExemFromInspCheckRepeatProcessService.getOne(null);
			if(zprodExemFromInspCheckRepeatProcessEntity == null) {
				zprodExemFromInspCheckRepeatProcessEntity = new ZprodExemFromInspCheckRepeatProcessEntity();
				zprodExemFromInspCheckRepeatProcessEntity.setId(0);
				zprodExemFromInspCheckRepeatProcessEntity.setLastVersion(zprodExemFromInspEntityList.get(0).getVersion());
				zprodExemFromInspCheckRepeatProcessService.save(zprodExemFromInspCheckRepeatProcessEntity);
			} else {
				zprodExemFromInspCheckRepeatProcessEntity.setLastVersion(zprodExemFromInspEntityList.get(0).getVersion());
				zprodExemFromInspCheckRepeatProcessService.updateById(zprodExemFromInspCheckRepeatProcessEntity);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * 删除重复数据
	 * @return
	 */
	public boolean delRepeatData() {
		ZprodExemFromInspCheckRepeatProcessEntity zprodExemFromInspCheckRepeatProcessEntity = zprodExemFromInspCheckRepeatProcessService.getOne(null);
		String startProductCode = null;
		Date startImportDate = null;
		//如果没有进度则从免检部品清单的第一条开始检查
		if (zprodExemFromInspCheckRepeatProcessEntity == null || StringUtils.isEmpty(zprodExemFromInspCheckRepeatProcessEntity.getProductCode())) {
			//如果没有上次处理进度，则按照
			QueryWrapper<ZProdExemFromInspEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.select("min(import_date) import_date,min(product_code) product_code");
			ZProdExemFromInspEntity zProdExemFromInspEntity = getOne(queryWrapper);
			if (zProdExemFromInspEntity != null) {
				startImportDate = zProdExemFromInspEntity.getImportDate();
				startProductCode = zProdExemFromInspEntity.getProductCode();
			}
		} else {
			//如果有上次处理进度，则获取进度
			startImportDate = zprodExemFromInspCheckRepeatProcessEntity.getImportDate();
			startProductCode = zprodExemFromInspCheckRepeatProcessEntity.getProductCode();
		}
		if(startProductCode == null || startImportDate == null) {
			return false;
		}
		
		List<Integer> delIdList = new ArrayList<>();
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.ge("import_date", startImportDate);
		queryWrapper2.ge("product_code", startProductCode);
		queryWrapper2.orderByAsc("product_code,version,import_date");
		queryWrapper2.last("limit 500");
		queryWrapper2.select("id,version,product_code");
		List<ZProdExemFromInspEntity> list = list(queryWrapper2);
		if (list.size()<2 || list.get(0).getProductCode().equals(list.get(list.size()-1).getProductCode())) {
			queryWrapper2 = new QueryWrapper<>();
			queryWrapper2.gt("import_date", startImportDate);
			queryWrapper2.select("min(import_date) import_date");
			ZProdExemFromInspEntity newImportDate = getOne(queryWrapper2);
			queryWrapper2 = new QueryWrapper<>();
			queryWrapper2.select("min(product_code) product_code");
			ZProdExemFromInspEntity newProductCode = getOne(queryWrapper2);
			if (zprodExemFromInspCheckRepeatProcessEntity == null) {
				zprodExemFromInspCheckRepeatProcessEntity = new ZprodExemFromInspCheckRepeatProcessEntity();
				zprodExemFromInspCheckRepeatProcessEntity.setId(0);
				zprodExemFromInspCheckRepeatProcessEntity.setProductCode(newProductCode.getProductCode());
				zprodExemFromInspCheckRepeatProcessEntity.setImportDate(newImportDate.getImportDate());
				zprodExemFromInspCheckRepeatProcessService.save(zprodExemFromInspCheckRepeatProcessEntity);
			} else {
				zprodExemFromInspCheckRepeatProcessEntity.setProductCode(newProductCode.getProductCode());
				zprodExemFromInspCheckRepeatProcessEntity.setImportDate(newImportDate.getImportDate());
				zprodExemFromInspCheckRepeatProcessService.updateById(zprodExemFromInspCheckRepeatProcessEntity);
			}
			return false;
		} else {
			for(int i=1,len_i=list.size();i<len_i;i++) {
				ZProdExemFromInspEntity prevEntity = list.get(i - 1);
				ZProdExemFromInspEntity currentEntity = list.get(i);
				if (prevEntity.getProductCode().equals(currentEntity.getProductCode()) && 
						prevEntity.getVersion().equals(currentEntity.getVersion())) {
					delIdList.add(currentEntity.getId());
				}
			}
			ZProdExemFromInspEntity zProdExemFromInspEntity = list.get(list.size()-1);
			startProductCode = zProdExemFromInspEntity.getProductCode();
			if (zprodExemFromInspCheckRepeatProcessEntity == null) {
				zprodExemFromInspCheckRepeatProcessEntity = new ZprodExemFromInspCheckRepeatProcessEntity();
				zprodExemFromInspCheckRepeatProcessEntity.setId(0);
				zprodExemFromInspCheckRepeatProcessEntity.setProductCode(startProductCode);
				zprodExemFromInspCheckRepeatProcessEntity.setImportDate(startImportDate);
				zprodExemFromInspCheckRepeatProcessService.save(zprodExemFromInspCheckRepeatProcessEntity);
			} else {
				zprodExemFromInspCheckRepeatProcessEntity.setProductCode(startProductCode);
				zprodExemFromInspCheckRepeatProcessEntity.setImportDate(startImportDate);
				zprodExemFromInspCheckRepeatProcessService.updateById(zprodExemFromInspCheckRepeatProcessEntity);
			}
			if (!delIdList.isEmpty()) {
				removeByIds(delIdList);
			}
			return true;
		}
	}
	
	/**
	 * 检验最近三个月没有签收单的部品
	 * @return
	 */
	public List<ZProdExemFromInspEntity> getLastThreeMonthNotReceipt() {
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.select("id,product_code,last_carg_receipt_date");
		queryWrapper2.isNull("finish_date");
		queryWrapper2.inSql("(product_code,version)","select product_code,max(version) version from zprod_exem_from_insp group by product_code");
		//忽略长期免检的
		queryWrapper2.notInSql("product_code", "select product_code from zignore_product_code");
		queryWrapper2.last(" and last_carg_receipt_date<=date_sub(now(),interval 3 month) and (last_send_mail_date<=date_sub(now(),interval 1 day) or last_send_mail_date is null)");
		return list(queryWrapper2);
	}
	
	
	
	
	/**
	 * 批量更新最近发送邮件提醒的日期
	 * @param idList
	 * @return
	 */
	public boolean updateLastSendMailDate(List<Integer> idList) {
		UpdateWrapper<ZProdExemFromInspEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.in("id", idList);
		ZProdExemFromInspEntity zprodExemFromInspEntity = new ZProdExemFromInspEntity();
		zprodExemFromInspEntity.setLastSendMailDate(new Date());
		return update(zprodExemFromInspEntity, updateWrapper);
	}
	
	/**
	 * 更新表 mst_itm_all 和 zprod_exem_from_insp 表的免检信息不一致的数据
	 * 例如：在 mst_itm_all.IS_IQC='Y' 的数据中，对应的 zprod_exem_from_insp.finish_date is null
	 * 说明不一致，需要把 zprod_exem_from_insp 对应的数据的免检结束时间设置为当前时间。（相当于免检时间结束）
	 * @return
	 */
	public boolean updateDiffIsIqc() {
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.last(" zefi where exists(select ID from mst_itm_all mia where IS_IQC ='Y' and mia.ITM_CODE = zefi.product_code)"+
		"and (product_code,last_carg_receipt_date)in(select product_code,max(last_carg_receipt_date) last_carg_receipt_date from zprod_exem_from_insp where finish_date is null group by product_code)");
		queryWrapper.select("id");
		List<ZProdExemFromInspEntity> zProdExemFromInspEntityList = list(queryWrapper);
		List<Integer> idList = new ArrayList<>();
		for (int i=0,len_i=zProdExemFromInspEntityList.size();i<len_i;i++) {
			idList.add(zProdExemFromInspEntityList.get(i).getId());
		}
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.in("id", idList);
		ZProdExemFromInspEntity zProdExemFromInspEntity = new ZProdExemFromInspEntity();
		zProdExemFromInspEntity.setFinishDate(new Date());
		return update(zProdExemFromInspEntity, queryWrapper2);
	}
	
	/**
	 * 从有效免检清单中，获取最早一次签收单导入日期
	 * @return
	 */
	public Date getLastCargReceiptDate() {
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.last(" where (product_code,import_date,id) "
		+ "in(select product_code,max(import_date) import_date,max(id) id from zprod_exem_from_insp where finish_date is null group by product_code)");
		queryWrapper.select("min(last_carg_receipt_date) last_carg_receipt_date");
		ZProdExemFromInspEntity zProdExemFromInspEntity = getOne(queryWrapper);
		return zProdExemFromInspEntity == null ? null:zProdExemFromInspEntity.getLastCargReceiptDate();
	}
	
	@Autowired
	private IqcCheckListProcessService iqcCheckListProcessService;
	
	//日期格式
	private final static DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	/**
	 * 更新最近签收日期
	 * @param iqcCheckListEntityList
	 * @return
	 */
	public boolean updateLastCargReceiptDate(List<IqcCheckListEntity> iqcCheckListEntityList) {
		Map<String,String> iqcCheckListEntityMap = new HashMap<>();
		List<String> itmCodeList = new ArrayList<>();
		for(int i=0,len_i=iqcCheckListEntityList.size();i<len_i;i++) {
			IqcCheckListEntity iqcCheckListEntity = iqcCheckListEntityList.get(i);
			itmCodeList.add(iqcCheckListEntity.getItmCode());
			String feedDate = iqcCheckListEntityMap.get(iqcCheckListEntity.getItmCode());
			if (feedDate == null || iqcCheckListEntity.getFeedDate().compareTo(feedDate)>0) {
				iqcCheckListEntityMap.put(iqcCheckListEntity.getItmCode(), iqcCheckListEntity.getFeedDate());
			}
		}
		QueryWrapper<ZProdExemFromInspEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("product_code", itmCodeList);
		queryWrapper.inSql("(product_code,version)", "select product_code,max(version) from zprod_exem_from_insp group by product_code");
		queryWrapper.select("id,product_code,last_carg_receipt_date");
		List<ZProdExemFromInspEntity> updateList = list(queryWrapper);
		boolean ret = false;
		if(!updateList.isEmpty()) {
			for (int i=0,len_i=updateList.size();i<len_i;i++) {
				ZProdExemFromInspEntity zprodExemFromInspEntity = updateList.get(i);
				String feedDate = iqcCheckListEntityMap.get(zprodExemFromInspEntity.getProductCode());
				Date feedDate2 = Date.from(LocalDate.parse(feedDate, DATE_FORMAT).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
				Date lastCargReceiptDate = zprodExemFromInspEntity.getLastCargReceiptDate();
				if(feedDate2.compareTo(lastCargReceiptDate)>0) {
					zprodExemFromInspEntity.setLastCargReceiptDate(feedDate2);
				}
			}
			ret = updateBatchById(updateList);
		}
		IqcCheckListProcessEntity iqcCheckListProcessEntity = new IqcCheckListProcessEntity();
		iqcCheckListProcessEntity.setId(0);
		iqcCheckListProcessEntity.setProcessId(iqcCheckListEntityList.get(iqcCheckListEntityList.size()-1).getId());
		iqcCheckListProcessService.updateById(iqcCheckListProcessEntity);
		return ret;
	}
}