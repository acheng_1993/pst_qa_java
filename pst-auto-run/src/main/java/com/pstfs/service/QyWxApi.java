package com.pstfs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import cn.hutool.core.util.IdUtil;
import cn.hutool.http.HttpUtil;

/**
 * 企业微信api
 * @author jiajian
 */
@Service
public class QyWxApi {

	@Autowired
	private AccessToken accessToken;
	
	/**
	 * 根据部门id获取部门下的成员
	 * @param department_id
	 * @param fetch_child 是否获取部门下的子部门成员，1:是，0:否
	 * @return
	 */
	public String userList(Integer department_id,Integer fetch_child) {
		String access_token = accessToken.getAccessToken();
		return HttpUtil.get("https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token="+access_token+"&department_id="+department_id+"&fetch_child="+fetch_child);
	}
	
	/**
	 * 获取所有部门
	 * @return
	 */
	public String departmentList() {
		return departmentList(null);
	}
	
	/**
	 * 根据部门id，获取该部门下的所有子部门
	 * @param id
	 * @return
	 */
	public String departmentList(Integer id) {
		String access_token = accessToken.getAccessToken();
		if(id == null) {
			return HttpUtil.get("https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token="+access_token);
		}else {
			return HttpUtil.get("https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token="+access_token+"&id="+id);
		}
	}
	
	
	/**
	 * 根据用户id获取单个用户信息
	 * @param userid
	 * @return
	 */
	public String userGet(String userid) {
		String access_token = accessToken.getAccessToken();
		return HttpUtil.get("https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token="+access_token+"&userid="+userid);
	}
	
	/**
	 * 发起异步导出企业成员的详情信息的请求
	 * @return
	 */
	public String exportUser(String encoding_aeskey) {
		String access_token = accessToken.getAccessToken();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("encoding_aeskey", encoding_aeskey);
		return HttpUtil.post("https://qyapi.weixin.qq.com/cgi-bin/export/user?access_token="+access_token, jsonObject.toJSONString());
	}

	
	/**
	 * 导出异步数据 
	 * @param jobid
	 * @return
	 */
	public String exportGetResult(String jobid) {
		String access_token = accessToken.getAccessToken();
		return HttpUtil.get("https://qyapi.weixin.qq.com/cgi-bin/export/get_result?access_token="+access_token+"&jobid="+jobid);
	}
	
	/**
	 * 发送文本消息给企业微信用户
	 * @param userIdList
	 * @param content
	 * @return
	 */
	public String messageSendText(String content,String ...userIdList) {
		String access_token = accessToken.getAccessToken();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("touser", userIdList.length == 0?"@all":String.join("|", userIdList));
		jsonObject.put("msgtype", "text");
		jsonObject.put("agentid", 1000004);
		JSONObject contentObject = new JSONObject();
		contentObject.put("content", content);
		jsonObject.put("text", contentObject);
		return HttpUtil.post("https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token="+access_token, jsonObject.toJSONString());
	}
}