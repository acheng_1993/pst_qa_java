package com.pstfs.service;
import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.AutoSendEmailToMakercdInfoEntity;
import com.pstfs.entity.AutoSendKiaEmailDetailsEntity;
import com.pstfs.entity.AutoSendKiaEmailEntity;
import com.pstfs.entity.AutoSendKiaEmailHistoryEntity;
import com.pstfs.entity.AutoSendKiaTechissCfgEntity;
import com.pstfs.entity.ChklistEntity;
import com.pstfs.entity.TechissEntity;
import com.pstfs.entity.UsersEntity;
import com.pstfs.entity.extend.AutoSendKiaEmailExtend;
import com.pstfs.entity.extend.TechissEntityExtend;
import com.pstfs.mapper.AutoSendEmailToMakercdInfoMapper;
import com.pstfs.mapper.AutoSendKiaEmailMapper;
@Service("autoSendKiaEmailService")
@DS("techpl")
public class AutoSendKiaEmailService extends ServiceImpl<AutoSendKiaEmailMapper, AutoSendKiaEmailEntity> {
	@Autowired
	private AutoSendEmailToMakercdInfoMapper autoSendEmailToMakercdInfoMapper;
	@Autowired
	private AutoSendKiaEmailDetailsService autoSendKiaEmailDetailsService;
	@Autowired
	private AutoSendKiaTechissCfgService autoSendKiaTechissCfgService;
	@Autowired
	private StatusService statusService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private DeptService deptService;
	@Autowired
	private UsersService usersService;
	@Autowired
	private ChklistService chklistService;
	@Autowired
	private AutoSendKiaEmailMapper autoSendKiaEmailMapper;
	@Autowired
	private TechissService techissService;
	
	@Value("${spring.profiles.active}")
	private String springProfilesActive;
	
	/**
	 * 获取待发送到加工商的邮件
	 * @return
	 */
	public List<AutoSendKiaEmailEntity> getToBeSendEmailList() {
		QueryWrapper<AutoSendKiaEmailEntity> query = new QueryWrapper<>();
		query.eq("pstfs_confirm_status", 1); //必须由中宝的操作员确认后才能发送。
		query.eq("confirm_status", 0);
		query.last(" and (last_send_date is null or TimeStampDiff(DAY,last_send_date,now())>=1)");
		return list(query);
	}
	
	
	/**
	 * 获取需要发送给加工商，但未经过内部确认且未提醒内部确认的邮件
	 * @return
	 */
	public List<AutoSendKiaEmailExtend> getToBeSendEmailRemindList() {
		QueryWrapper<AutoSendKiaEmailExtend> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("ASKE.SEND_EMAIL_REMIND", 0);
		queryWrapper.isNull("LAST_SEND_DATE");
		return baseMapper.getExtendList(queryWrapper);
	}
	
	/**
	 * 更新提醒状态
	 * @param id
	 */
	public boolean updateSendEmailRemind(List<Integer> idList) {
		AutoSendKiaEmailEntity autoSendKiaEmailEntity = new AutoSendKiaEmailEntity();
		autoSendKiaEmailEntity.setSendEmailRemind(1);
		UpdateWrapper<AutoSendKiaEmailEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.in("id", idList);
		return update(autoSendKiaEmailEntity, updateWrapper);
	}
	
	//TechissEntity 的比较器
	private final static Comparator<TechissEntity> TECHISS_ENTITY_COMPARATOR = new Comparator<TechissEntity>() {
		@Override
		public int compare(TechissEntity o1, TechissEntity o2) {
			int makercdCompareRet = o1.getMakercd().compareTo(o2.getMakercd());
			if(makercdCompareRet == 0) {
				return o1.getPnpa().compareTo(o2.getPnpa());
			} else {
				return makercdCompareRet;
			}
		}
	};
	
	//日期格式
	private final static DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	//日期格式2
	private final static DateTimeFormatter DATE_FORMAT2 = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	
	/**
	 * 过滤技术资料，去除不需要的加工商，把多加工商的技术资料进行拆解
	 * （注意：过滤逻辑不会修改 techissList 数组）
	 * @param techissList
	 * @return
	 */
	private List<TechissEntity> filterTechissList(List<TechissEntity> techissList, Map<String,AutoSendEmailToMakercdInfoEntity> autoSendEmailToMakercdInfoMap) {
		//当天发送的加工商为 ALL、或斜杠分割的 TCN/KIA/PSE....  的急件
		List<AutoSendKiaEmailExtend> autoSendKiaEmailExtendList = autoSendKiaEmailMapper.getYestodayMultiMakecdDataList();
		Map<Integer,Set<String>> autoSendKiaEmailExtendMap = new HashMap<>();
		for (AutoSendKiaEmailExtend autoSendKiaEmailExtend:autoSendKiaEmailExtendList) {
			if(autoSendKiaEmailExtendMap.containsKey(autoSendKiaEmailExtend.getExtendIntCol1())) {
				autoSendKiaEmailExtendMap.get(autoSendKiaEmailExtend.getExtendIntCol1()).add(autoSendKiaEmailExtend.getMakerCd());
			} else {
				Set<String> makerCdSet = new HashSet<>();
				makerCdSet.add(autoSendKiaEmailExtend.getMakerCd());
				autoSendKiaEmailExtendMap.put(autoSendKiaEmailExtend.getExtendIntCol1(), makerCdSet);
			}
		}
		List<TechissEntity> ret = new ArrayList<TechissEntity>();
		for (TechissEntity techissEntity : techissList) {
			String makercd = techissEntity.getMakercd();
			if(StringUtils.isNotEmpty(makercd)) {
				makercd = makercd.trim().toUpperCase();
				//如果发送的目标加工商和配置的加工商完全一致，则发送
				if (autoSendEmailToMakercdInfoMap.containsKey(makercd)) {
					Set<String> makerCdSet = autoSendKiaEmailExtendMap.get(techissEntity.getId());
					//检查是否为急件，如果不是急件才会假如邮件列表中
					if(makerCdSet == null || !makerCdSet.contains(makercd)) {
						ret.add(techissEntity);
					}
				//如果是 ALL 的情况，则所有加工商都得发送一次
				} else if ("ALL".equals(makercd)) {
					for(String key:autoSendEmailToMakercdInfoMap.keySet()) {
						try {
							Set<String> makerCdSet = autoSendKiaEmailExtendMap.get(techissEntity.getId());
							//检查是否为急件，如果不是急件才会加入邮件列表中
							if (makerCdSet == null || !makerCdSet.contains(key)) {
								TechissEntity newTechissEntity = (TechissEntity)techissEntity.clone();
								newTechissEntity.setMakercd(key);
								ret.add(newTechissEntity);
							}								
						} catch (CloneNotSupportedException e) {
							e.printStackTrace();
						}
					}
				//如果是斜杠分割的加工商，例如：TCN/KIA/PSE.... 则根据分割内容的加工商发送
				} else {
					HashSet<String> tempMakercdStrSet = new HashSet<>();
					char[] tempMakercd = new char[10];
					int tempMakercdIndex = 0;
					final int INIT = 0;
					final int GET_MARKERCD = 1;
					final char[] makercdArray = makercd.toCharArray();
					int currentStaus = INIT;
					for (int i = 0,len_i = makercdArray.length; i < len_i;i++) {
						final char c = makercdArray[i];
						switch (currentStaus) {
						case INIT:
							if((c>='A' && c<='Z') || (c>='0' && c<='9')) {
								currentStaus = GET_MARKERCD;
								tempMakercd[tempMakercdIndex++] = c;
							}
							break;
						case GET_MARKERCD:
							if((c>='A' && c<='Z') || (c>='0' && c<='9')) {
								if(tempMakercdIndex == tempMakercd.length) {
									char[] newTempMakercd = new char[tempMakercd.length<<1];
									System.arraycopy(tempMakercd, 0, newTempMakercd, 0, tempMakercd.length);
									tempMakercd = newTempMakercd;
								}
								tempMakercd[tempMakercdIndex++] = c;
							} else {
								String tempMakercdStr = new String(tempMakercd, 0, tempMakercdIndex);
								if (autoSendEmailToMakercdInfoMap.containsKey(tempMakercdStr) && !tempMakercdStrSet.contains(tempMakercdStr)) {
									try {
										Set<String> makerCdSet = autoSendKiaEmailExtendMap.get(techissEntity.getId());
										//检查是否为急件，如果不是急件才会加入邮件列表中
										if (makerCdSet == null || !makerCdSet.contains(tempMakercdStr)) {
											TechissEntity newTechissEntity = (TechissEntity)techissEntity.clone();
											newTechissEntity.setMakercd(tempMakercdStr);
											ret.add(newTechissEntity);
											tempMakercdStrSet.add(tempMakercdStr);
										}
									} catch (CloneNotSupportedException e) {
										e.printStackTrace();
									}
								}
								tempMakercdIndex = 0;
								currentStaus = INIT;						
							}
							break;
						default:
							break;
						}
					}
					String tempMakercdStr = new String(tempMakercd, 0, tempMakercdIndex);
					if (autoSendEmailToMakercdInfoMap.containsKey(tempMakercdStr) && !tempMakercdStrSet.contains(tempMakercdStr)) {
						try {
							Set<String> makerCdSet = autoSendKiaEmailExtendMap.get(techissEntity.getId());
							//检查是否为急件，如果不是急件才会加入邮件列表中
							if (makerCdSet == null || !makerCdSet.contains(tempMakercdStr)) {
								TechissEntity newTechissEntity = (TechissEntity)techissEntity.clone();
								newTechissEntity.setMakercd(tempMakercdStr);
								ret.add(newTechissEntity);
								tempMakercdStrSet.add(tempMakercdStr);
							}
						} catch (CloneNotSupportedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return ret;
	}
	
	/**
	 * 批量新增要发送的邮件
	 * @param KIATechissList
	 */
	@Transactional
	public void saveBatch(List<TechissEntity> techissList) {
		//把邮件模板集合整理成 map
		Map<String,AutoSendEmailToMakercdInfoEntity> autoSendEmailToMakercdInfoMap = new HashMap<>();
		{
			//获取邮件模板集合
			QueryWrapper<AutoSendEmailToMakercdInfoEntity> queryWrapper = new QueryWrapper<>();
			queryWrapper.select("id,maker_cd,title,template,recipient,cc");
			List<AutoSendEmailToMakercdInfoEntity> autoSendEmailToMakercdInfoList = autoSendEmailToMakercdInfoMapper.selectList(queryWrapper);
			for (int i=0,len_i=autoSendEmailToMakercdInfoList.size();i<len_i;i++) {
				AutoSendEmailToMakercdInfoEntity temp = autoSendEmailToMakercdInfoList.get(i);
				autoSendEmailToMakercdInfoMap.put(temp.getMakerCd().toUpperCase(), temp);
			}
		}
		//过滤前的数据
		List<TechissEntity> beforeFilterTechissList = techissList; 
		//过滤掉不需要发送的内容
		techissList = filterTechissList(techissList, autoSendEmailToMakercdInfoMap);
		//当过滤后没有数据时使用过滤前的数据保存进度
		if (techissList.isEmpty()) {
			//保存处理进度
			updateSendProcess(beforeFilterTechissList);
			return;
		}
		techissList.sort(TECHISS_ENTITY_COMPARATOR);

		TechissEntity tempTechissEntity = techissList.get(0);
		String currentPnpa = tempTechissEntity.getPnpa();
		String currentMakercd = tempTechissEntity.getMakercd();
		List<TechissEntity> currentPnpaTechissList = new ArrayList<>();
		currentPnpaTechissList.add(tempTechissEntity);
		Date maxIsdate = tempTechissEntity.getIsdate();
		Integer maxId = tempTechissEntity.getId();		
		
		for (int i=1,len_i=techissList.size();i<len_i;i++) {
			tempTechissEntity = techissList.get(i);
			int isDateCompareRet = tempTechissEntity.getIsdate().compareTo(maxIsdate);
			if(isDateCompareRet == 0) {
				if (tempTechissEntity.getId().compareTo(maxId)>0) {
					maxIsdate = tempTechissEntity.getIsdate();
					maxId = tempTechissEntity.getId();
				}
			} else if(isDateCompareRet > 0) {
				maxIsdate = tempTechissEntity.getIsdate();
				maxId = tempTechissEntity.getId();
			}
			if (tempTechissEntity.getPnpa().equals(currentPnpa) && tempTechissEntity.getMakercd().equals(currentMakercd)) {
				currentPnpaTechissList.add(tempTechissEntity);
			} else {
				AutoSendEmailToMakercdInfoEntity autoSendEmailToMakercdInfoEntity = autoSendEmailToMakercdInfoMap.get(currentMakercd);
				long fileSize = 0;
				List<TechissEntityExtend> tempPnpaTechissList = new ArrayList<>();
				for(TechissEntity currentPnpaTechiss:currentPnpaTechissList) {
					File uploadFile = techissService.getUploadFile(currentPnpaTechiss.getUpload());
					if (uploadFile != null) {
						fileSize += uploadFile.length();
						TechissEntityExtend techissEntityExtend = new TechissEntityExtend(currentPnpaTechiss);
						techissEntityExtend.setFileSize(uploadFile.length());
						tempPnpaTechissList.add(techissEntityExtend);
					}
					//超过 10MB 生成一份邮件 
					if(fileSize > 1024*1024*10) {
						save(autoSendEmailToMakercdInfoEntity,currentPnpa, currentMakercd, tempPnpaTechissList,fileSize);
						fileSize = 0;
						tempPnpaTechissList.clear();
					}
				}
				if(!tempPnpaTechissList.isEmpty()) {
					save(autoSendEmailToMakercdInfoEntity,currentPnpa, currentMakercd, tempPnpaTechissList,fileSize);
				}
				currentPnpa = tempTechissEntity.getPnpa(); 
				currentMakercd = tempTechissEntity.getMakercd();
				currentPnpaTechissList.clear();
				currentPnpaTechissList.add(tempTechissEntity);
			}
		}
		
		{//处理剩余未处理的技术资料 tempPnpaTechissList
			AutoSendEmailToMakercdInfoEntity autoSendEmailToMakercdInfoEntity = autoSendEmailToMakercdInfoMap.get(currentMakercd);
			long fileSize = 0;
			List<TechissEntityExtend> tempPnpaTechissList = new ArrayList<>();
			for(TechissEntity currentPnpaTechiss:currentPnpaTechissList) {
				File uploadFile = techissService.getUploadFile(currentPnpaTechiss.getUpload());
				//技术文档存在的情况下才会发送附件
				if (uploadFile != null) {
					fileSize += uploadFile.length();
					TechissEntityExtend techissEntityExtend = new TechissEntityExtend(currentPnpaTechiss);
					techissEntityExtend.setFileSize(uploadFile.length());
					tempPnpaTechissList.add(techissEntityExtend);
				}
				//超过 10MB 生成一份邮件 
				if(fileSize > 1024*1024*10) {
					save(autoSendEmailToMakercdInfoEntity,currentPnpa, currentMakercd, tempPnpaTechissList,fileSize);
					fileSize = 0;
					tempPnpaTechissList.clear();
				}
			}
			//剩余部分。
			if(!tempPnpaTechissList.isEmpty()) {
				save(autoSendEmailToMakercdInfoEntity,currentPnpa,currentMakercd, tempPnpaTechissList,fileSize);
			}
		}
		
		//保存处理进度
		updateSendProcess(beforeFilterTechissList);
		
		{//把技联网对应的技术资料 techissIdSet 标记为已提交
			//把 techissList 标记为提交
			Set<Integer> techissIdSet = new HashSet<>();
			for(TechissEntity techissEntity:techissList) {
				techissIdSet.add(techissEntity.getId());
			}
			
			//实行该提交操作的操作员
			QueryWrapper<UsersEntity> usersEntityQueryWrapper = new QueryWrapper<>();
			usersEntityQueryWrapper.inSql("logid", "select case when INSTR(user_id,';')>0 then SUBSTR(user_id,1,INSTR(user_id,';')-1) else user_id END `user_id` from auto_send_kia_techiss_cfg");
			usersEntityQueryWrapper.select("logid,deptid");
			UsersEntity usersEntity = usersService.getOne(usersEntityQueryWrapper);
		
			ChklistEntity chklistEntity = new ChklistEntity();
			chklistEntity.setCfmid(usersEntity.getLogid().toUpperCase());
			chklistEntity.setCfmfg("T");
			chklistEntity.setCfmdatetime(new Date());
			
			UpdateWrapper<ChklistEntity> updateWrapper = new UpdateWrapper<>();
			updateWrapper.eq("CfmDep", usersEntity.getDeptid());
			updateWrapper.in("id", techissIdSet);
			chklistService.update(chklistEntity, updateWrapper);
		}
	}

	/**
	 * 更新保存进度
	 * @param beforeFilterTechissList 过滤前的数据
	 */
	private void updateSendProcess(List<TechissEntity> beforeFilterTechissList) {
		TechissEntity lastTechissEntity = beforeFilterTechissList.get(beforeFilterTechissList.size()-1);
		AutoSendKiaTechissCfgEntity autoSendKiaTechissCfgFromDb = autoSendKiaTechissCfgService.getOne(null);
		if(autoSendKiaTechissCfgFromDb == null) {
			AutoSendKiaTechissCfgEntity autoSendKiaTechissCfgEntity = new AutoSendKiaTechissCfgEntity();
			autoSendKiaTechissCfgEntity.setId(0);
			LocalDate localDate = lastTechissEntity.getIsdate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			autoSendKiaTechissCfgEntity.setIsdate(localDate.format(DATE_FORMAT));
			autoSendKiaTechissCfgEntity.setTechissId(lastTechissEntity.getId().toString());
			autoSendKiaTechissCfgEntity.setUpdateTime(new Date());
			autoSendKiaTechissCfgService.save(autoSendKiaTechissCfgEntity);
		} else {
			LocalDate localDate = lastTechissEntity.getIsdate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			String newIsDate = localDate.format(DATE_FORMAT);
			if (newIsDate.compareTo(autoSendKiaTechissCfgFromDb.getIsdate())>0) {
				autoSendKiaTechissCfgFromDb.setIsdate(newIsDate);
			}
			autoSendKiaTechissCfgFromDb.setTechissId(lastTechissEntity.getId().toString());
			autoSendKiaTechissCfgFromDb.setUpdateTime(new Date());
			autoSendKiaTechissCfgService.updateById(autoSendKiaTechissCfgFromDb);
		}
	}

	@Autowired
	private AutoSendKiaEmailHistoryService autoSendKiaEmailHistoryService;
	
	/**
	 * 发送成功后，记录发送记录
	 * @param id
	 */
	@Transactional
	public void recordSendKiaEmailHistory(Integer id) {
		Date now = new Date();
		AutoSendKiaEmailEntity autoSendKiaEmailEntity = new AutoSendKiaEmailEntity();
		autoSendKiaEmailEntity.setId(id);
		autoSendKiaEmailEntity.setLastSendDate(now);
		autoSendKiaEmailEntity.setUpdateTime(now);
		updateById(autoSendKiaEmailEntity);
		AutoSendKiaEmailHistoryEntity autoSendKiaEmailHistoryEntity = new AutoSendKiaEmailHistoryEntity();
		autoSendKiaEmailHistoryEntity.setAutoSendKiaEmailId(id);
		autoSendKiaEmailHistoryEntity.setSendDate(now);
		autoSendKiaEmailHistoryService.save(autoSendKiaEmailHistoryEntity);
	}
	
	/**
	 * 把技联网的技术资料整合成表格
	 * @param currentPnpaTechissList
	 * @return
	 */
	private String getTemplateTable(List<TechissEntityExtend> currentPnpaTechissList) {
		boolean showRemarkCol = false;
		for (int i=0,len_i=currentPnpaTechissList.size();i<len_i;i++) {
			TechissEntity techissEntity = currentPnpaTechissList.get(i);
			String remark = techissEntity.getRemark();
			if (remark!=null && StringUtils.isNotEmpty(remark.trim())) {
				showRemarkCol = true;
				break;
			}
		}
		StringBuilder sb = new StringBuilder();
		String tdStyle = "<td style=\"padding: 4px;border: 1px solid;border-color: #CCCCCC;\">";
		sb.append("<table style=\"width:auto;font-family: Verdana;font-size: xx-small;border: 0px outset;border-collapse: collapse;\">");
		sb.append("<tbody>");
		sb.append("<tr style=\"background-color: #666666;color: #FFFFFF;vertical-align: top;\">");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">id</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Doc No</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Doc Type</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Drawing#</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Confirm Dept</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Confirm?</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Model No</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">PN</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Status</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Issue No</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Version</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Issue date</a></td>");
		sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Subcon</a></td>");
		if(showRemarkCol) {
			sb.append(tdStyle).append("<a style=\"color: #fff;\" href=\"#\">Remark</a></td>");
		}
		sb.append("</tr>");
		QueryWrapper<UsersEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.inSql("logid", "select case when INSTR(user_id,';')>0 then SUBSTR(user_id,1,INSTR(user_id,';')-1) else user_id END `user_id` from auto_send_kia_techiss_cfg");
		queryWrapper.select("deptid");
		UsersEntity usersEntity = usersService.getOne(queryWrapper);
		for(int i=0,len_i=currentPnpaTechissList.size();i<len_i;i++) {
			TechissEntity techissEntity = currentPnpaTechissList.get(i);
			if(i%2 == 0) {
				sb.append("<tr style=\"background-color: #FFFFFF;\">");
			} else {
				sb.append("<tr style=\"background-color: #F5F5F5;\">");
			}
			sb.append(tdStyle).append(techissEntity.getId()).append("</td>");
			sb.append(tdStyle).append(techissEntity.getCtrlno()).append("</td>");
			sb.append(tdStyle).append(techissEntity.getCategory()).append("</td>");
			sb.append(tdStyle).append(categoryService.getByCategoryAndDoctype(techissEntity.getCategory(), techissEntity.getDoctype())).append("</td>");
			sb.append(tdStyle).append(deptService.getByDeptId(usersEntity.getDeptid()).getDeptname()).append("</td>");
			sb.append(tdStyle).append("</td>");
			sb.append(tdStyle).append(techissEntity.getPnpa()).append("</td>");
			sb.append(tdStyle).append(techissEntity.getPn()).append("</td>");
			sb.append(tdStyle).append(statusService.getByStatusId(techissEntity.getStatus())).append("</td>");
			sb.append(tdStyle).append(techissEntity.getIssno()).append("</td>");
			sb.append(tdStyle).append(techissEntity.getIssTy()).append("</td>");
			LocalDate localDate = techissEntity.getIsdate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			sb.append(tdStyle).append(localDate.format(DATE_FORMAT2)).append("</td>");
			sb.append(tdStyle).append(techissEntity.getMakercd()).append("</td>");
			if (showRemarkCol) {
				sb.append(tdStyle).append(techissEntity.getRemark()).append("</td>");
			}
			sb.append("</tr>");
		}
		sb.append("</tbody>");
		sb.append("</table>");
		return sb.toString();
	}
	
	/**
	 * 新增一个要发送的邮件
	 * @param pnpa 
	 * @param techissIdList 
	 */
	@Transactional
	public void save(AutoSendEmailToMakercdInfoEntity autoSendEmailToMakercdInfoEntity,String pnpa,String currentMakercd,List<TechissEntityExtend> currentPnpaTechissList,long fileSize) {
		AutoSendKiaEmailEntity saveEntity = new AutoSendKiaEmailEntity();
		String title = autoSendEmailToMakercdInfoEntity.getTitle();
		String pnpaCount = String.valueOf(currentPnpaTechissList.size());
		saveEntity.setTitle(title.replaceAll("\\{\\{pnpa\\}\\}", pnpa).replaceAll("\\{\\{pnpaCount\\}\\}", pnpaCount));
		String template = autoSendEmailToMakercdInfoEntity.getTemplate();
		String table = getTemplateTable(currentPnpaTechissList);
		saveEntity.setContent(template.replaceAll("\\{\\{pnpa\\}\\}", pnpa).replaceAll("\\{\\{pnpaCount\\}\\}", pnpaCount).replaceAll("\\{\\{table\\}\\}", table));
		Date now = new Date();
		saveEntity.setFileSize(fileSize);
		saveEntity.setTitleBak(title);
		saveEntity.setContentBak(template);
		saveEntity.setCreateTime(now);
		saveEntity.setUpdateTime(now);
		saveEntity.setPnpa(pnpa);
		saveEntity.setConfirmStatus(0);
		saveEntity.setPstfsConfirmStatus(0);
		saveEntity.setSendEmailRemind(0);
		saveEntity.setRecipient(autoSendEmailToMakercdInfoEntity.getRecipient());
		saveEntity.setRecipientBak(autoSendEmailToMakercdInfoEntity.getRecipient());
		saveEntity.setCc(autoSendEmailToMakercdInfoEntity.getCc());
		saveEntity.setCcBak(autoSendEmailToMakercdInfoEntity.getCc());
		saveEntity.setAutoSendEmailToMakercdInfoId(autoSendEmailToMakercdInfoEntity.getId());
		
		int minTechissId = Integer.MAX_VALUE;
		for (int i=0,len_i=currentPnpaTechissList.size();i<len_i;i++) {
			TechissEntityExtend techissEntityExtend = currentPnpaTechissList.get(i);
			if(techissEntityExtend.getId()<minTechissId) {
				minTechissId = techissEntityExtend.getId();
			}
		}
		saveEntity.setMinTechissId(minTechissId);
		
		save(saveEntity);
		List<AutoSendKiaEmailDetailsEntity> autoSendKiaEmailDetailsList = new ArrayList<>();
		for (int i=0,len_i=currentPnpaTechissList.size();i<len_i;i++) {
			TechissEntityExtend techissEntityExtend = currentPnpaTechissList.get(i);
			AutoSendKiaEmailDetailsEntity detailsEntity = new AutoSendKiaEmailDetailsEntity();
			detailsEntity.setAutoSendKiaEmailId(saveEntity.getId());
			detailsEntity.setTechissId(techissEntityExtend.getId());
			detailsEntity.setFileSize(techissEntityExtend.getFileSize());
			detailsEntity.setActualSendStatus(1);
			detailsEntity.setDoctype(techissEntityExtend.getDoctype());
			detailsEntity.setCtrlno(techissEntityExtend.getCtrlno());
			detailsEntity.setRemark(techissEntityExtend.getRemark());
			detailsEntity.setStatusDesc(statusService.getByStatusId(techissEntityExtend.getStatus()));
			autoSendKiaEmailDetailsList.add(detailsEntity);
		}
		autoSendKiaEmailDetailsService.saveBatch(autoSendKiaEmailDetailsList);
	}
}