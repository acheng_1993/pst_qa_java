package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.BeforeDeptUserSet;
import com.pstfs.mapper.BeforeDeptUserSetMapper;

@Service
@DS("pst-wxhr-app")
public class BeforeDeptUserSetService extends ServiceImpl<BeforeDeptUserSetMapper, BeforeDeptUserSet>{
}