package com.pstfs.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.IqcCheckListEntity;
import com.pstfs.entity.IqcCheckListProcessEntity;
import com.pstfs.mapper.IqcCheckListMapper;

@DS("pst-wxhr-app")
@Service("iqcCheckListService")
public class IqcCheckListService extends ServiceImpl<IqcCheckListMapper, IqcCheckListEntity> {
	
	@Autowired
	private IqcCheckListProcessService iqcCheckListProcessService;
	
	/**
	 * 获取待检查的送检记录
	 * @return
	 */
	public List<IqcCheckListEntity> getToBeHandleList() {
		IqcCheckListProcessEntity iqcCheckListProcessEntity = iqcCheckListProcessService.getOne(null);
		QueryWrapper<IqcCheckListEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByAsc("id");
		if(iqcCheckListProcessEntity != null) {
			queryWrapper.gt("id", iqcCheckListProcessEntity.getProcessId());
		}
		queryWrapper.last(" limit 500");
		return list(queryWrapper);
	}
}