package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ScheduledSendMsgFilesEntity;
import com.pstfs.mapper.ScheduledSendMsgFilesMapper;

@DS("pst-wxhr-app")
@Service("scheduledSendMsgFilesService")
public class ScheduledSendMsgFilesService extends ServiceImpl<ScheduledSendMsgFilesMapper, ScheduledSendMsgFilesEntity> {
	
}