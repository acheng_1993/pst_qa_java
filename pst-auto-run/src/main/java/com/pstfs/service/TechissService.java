package com.pstfs.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.AutoSendKiaEmailDetailsEntity;
import com.pstfs.entity.TechissEntity;
import com.pstfs.entity.extend.TechissEntityExtend;
import com.pstfs.mapper.AutoSendKiaEmailDetailsMapper;
import com.pstfs.mapper.TechissMapper;

import cn.hutool.core.io.IORuntimeException;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;

@Service("techissService")
@DS("techpl")
public class TechissService extends ServiceImpl<TechissMapper, TechissEntity> {

	@Autowired
	private AutoSendKiaEmailDetailsMapper autoSendKiaEmailDetailsMapper;

	@Autowired
	private CategoryService categoryService;

	/**
	 * 下载文件路径
	 */
	private final static String DOWNLOAD_FILE_PATH = "http://10.198.43.9:9090/techeno/uploads/";

	@Value("${mailAttachFilePath}")
	private String mailAttachFilePath;

	/**
	 * 获取上传的文件对象
	 * 
	 * @param upload
	 * @return
	 */
	public File getUploadFile(String upload) {
		File mailAttachFilePathObj = new File(mailAttachFilePath);
		// 检测附件路径是否存在，如果不存在则创建
		if (!mailAttachFilePathObj.exists()) {
			mailAttachFilePathObj.mkdirs();
		}
		// 文件缓存路径
		File uploadFile = new File(mailAttachFilePath + File.separator + upload);
		try {
			// 如果有缓存，则返回
			if (uploadFile.exists()) {
				return uploadFile;
				// 如果没有缓存，则到技联网下载文件
			} else {
				HttpResponse response = null;
				try {
					response = HttpUtil.createGet(DOWNLOAD_FILE_PATH + URLEncoder.encode(upload, "utf-8"), true)
							.timeout(30000).executeAsync();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (IORuntimeException e) {
					throw new RuntimeException("技联网服务器异常：", e);
				}
				// 如果从及联网获取到文件，则缓存
				if (response.isOk()) {
					byte[] fileByte = response.bodyBytes();
					try (FileOutputStream fileOutputStream = new FileOutputStream(uploadFile)) {
						fileOutputStream.write(fileByte, 0, fileByte.length);
						return uploadFile;
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				} else {
					// 如果没能获取文件，则检测是否为404错误，如果是则说明文件不存在
					if (response.getStatus() == HttpStatus.HTTP_NOT_FOUND) {
						return null;
						// 其他情况视为及联网服务器宕机
					} else {
						throw new RuntimeException("技联网服务器异常：" + response.getStatus());
					}
				}
			}
		} finally {
			// 检测文缓存件数是否超过100个，超过则删除部分以节省硬盘空间
			File[] listFiles = mailAttachFilePathObj.listFiles();
			int rest = listFiles.length - 100;
			if (rest > 0) {
				long currentTimeMillis = System.currentTimeMillis();
				for (int j = 0, len_j = listFiles.length; j < len_j; j++) {
					// 如果最近编辑时间超过1天则删除
					if (currentTimeMillis - listFiles[j].lastModified() > 86400000) {
						listFiles[j].deleteOnExit();
						rest--;
						if (rest == 0) {
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * 获取需要发送给加工商的技术文档
	 * 
	 * @param isdate
	 * @param techissId
	 * @return
	 */
	public List<TechissEntity> getTechissListByMakercd(String isdate, String techissId) {
		QueryWrapper<TechissEntity> query = new QueryWrapper<>();
		query.and((QueryWrapper<TechissEntity> t) -> {
			t.or((QueryWrapper<TechissEntity> t2) -> {
				t2.gt("id", techissId);
			});
			t.or((QueryWrapper<TechissEntity> t2) -> {
				t2.gt("isdate", isdate);
			});
		});
		query.eq("viewable", "Y");
		query.exists("select id from chklist where cfmID is null AND cfmDep="
				+ "(select DEPTID from users where logid=(select case when INSTR(user_id,';')>0 then SUBSTR(user_id,1,INSTR(user_id,';')-1) else user_id END `user_id` from auto_send_kia_techiss_cfg)) and chklist.id=techiss.id");
		query.orderByAsc("isdate,id");
		query.select("makercd,case when pnpa is null then ' ' when pnpa = '' then ' ' else pnpa end pnpa,isdate,id,category,doctype,status,ctrlno,issno,iss_ty,pn,remark,upload");
		query.last("limit 2000");
		return list(query);
	}

	/**
	 * 使用 auto_send_kia_email.id 获取对应的 techiss 表数据
	 * 
	 * @param autoSendKiaEmailIds
	 * @return
	 */
	public Map<Integer, List<TechissEntityExtend>> getTechissMapListByAutoSendKiaEmailIds(
			List<Integer> autoSendKiaEmailIds) {
		QueryWrapper<AutoSendKiaEmailDetailsEntity> query = new QueryWrapper<>();
		query.in("auto_send_kia_email_id", autoSendKiaEmailIds);
		query.select("auto_send_kia_email_id,techiss_id");
		List<AutoSendKiaEmailDetailsEntity> autoSendKiaEmailDetailsList = autoSendKiaEmailDetailsMapper
				.selectList(query);
		Map<Integer, List<Integer>> autoSendKiaEmailDetailsMap = new HashMap<>();
		for (int i = 0, len_i = autoSendKiaEmailDetailsList.size(); i < len_i; i++) {
			AutoSendKiaEmailDetailsEntity autoSendKiaEmailDetailsEntity = autoSendKiaEmailDetailsList.get(i);
			List<Integer> techissIdList = autoSendKiaEmailDetailsMap
					.get(autoSendKiaEmailDetailsEntity.getAutoSendKiaEmailId());
			if (techissIdList == null) {
				techissIdList = new ArrayList<>();
				autoSendKiaEmailDetailsMap.put(autoSendKiaEmailDetailsEntity.getAutoSendKiaEmailId(), techissIdList);
			}
			techissIdList.add(autoSendKiaEmailDetailsEntity.getTechissId());
		}

		QueryWrapper<TechissEntity> query2 = new QueryWrapper<>();
		StringBuilder sb = new StringBuilder();
		String split = "";
		for (int i = 0, len_i = autoSendKiaEmailIds.size(); i < len_i; i++) {
			sb.append(split).append(autoSendKiaEmailIds.get(i));
			split = ",";
		}
		query2.inSql("id", "select techiss_id from auto_send_kia_email_details where auto_send_kia_email_id in("
				+ sb.toString() + ")");
		query2.select("id,ctrlno,category,doctype,pnpa");
		List<TechissEntity> techissList = list(query2);
		Map<Integer, TechissEntity> techissMap = new HashMap<>();
		for (int i = 0, len_i = techissList.size(); i < len_i; i++) {
			TechissEntity techissEntity = techissList.get(i);
			techissMap.put(techissEntity.getId(), techissEntity);
		}
		Map<Integer, List<TechissEntityExtend>> ret = new HashMap<>();
		for (Integer key : autoSendKiaEmailDetailsMap.keySet()) {
			List<Integer> techissIdList = autoSendKiaEmailDetailsMap.get(key);
			List<TechissEntityExtend> techissEntityExtendList = new ArrayList<>();
			for (int i = 0, len_i = techissIdList.size(); i < len_i; i++) {
				Integer techissId = techissIdList.get(i);
				TechissEntity techissEntity = techissMap.get(techissId);
				if (techissEntity != null) {
					TechissEntityExtend techissEntityExtend = new TechissEntityExtend(techissEntity);
					techissEntityExtend.setDrawingno(categoryService.getByCategoryAndDoctype(
							techissEntityExtend.getCategory(), techissEntityExtend.getDoctype()));
					techissEntityExtendList.add(techissEntityExtend);
				}
			}
			ret.put(key, techissEntityExtendList);
		}
		return ret;
	}
}