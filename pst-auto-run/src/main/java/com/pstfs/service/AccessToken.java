package com.pstfs.service;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;

import cn.hutool.http.HttpUtil;

/**
 * 微信accessToken获取类
 * @author jiajian
 */
@Service
public class AccessToken {

	@Autowired
	private RedisTemplate redisTemplate;
	
	@Value("${corpid}")
	private String corpid;
	
	@Value("${corpsecret}")
	private String corpsecret;
	
	public String getAccessToken() {
		String access_token = (String) redisTemplate.opsForValue().get("access_token");
		if(access_token == null || "".equals(access_token)) {
			synchronized (this) {
				access_token = (String) redisTemplate.opsForValue().get("access_token");
				if(access_token == null || "".equals(access_token)) {
					String json = HttpUtil.get("https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid="+corpid+"&corpsecret="+corpsecret);
					JSONObject jsonObj = JSONObject.parseObject(json);
					access_token = jsonObj.getString("access_token");
					Long expires_in = jsonObj.getLong("expires_in");
					redisTemplate.opsForValue().set("access_token", access_token, expires_in, TimeUnit.SECONDS);
					return access_token;
				} else {
					return access_token;
				}
			}
		} else {
			return access_token;
		}
	}
}