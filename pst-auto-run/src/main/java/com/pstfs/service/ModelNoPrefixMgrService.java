package com.pstfs.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.MfgMoItemAllEntity;
import com.pstfs.entity.ModelNoPrefixMgrEntity;
import com.pstfs.mapper.ModelNoPrefixMgrMapper;

@DS("new_mes")
@Service("modelNoPrefixMgrService")
public class ModelNoPrefixMgrService extends ServiceImpl<ModelNoPrefixMgrMapper, ModelNoPrefixMgrEntity> {

	/**
	 * 获取 mfgMoItemAllEntityList 数据集的制品品番的前缀映射
	 * @param mfgMoItemAllEntityList
	 * @return
	 */
	public Map<String,String> loadModelNoPrefixMap(List<MfgMoItemAllEntity> mfgMoItemAllEntityList) {
		Set<String> modelNoSet = new HashSet<>();
		for (int i=0,len_i=mfgMoItemAllEntityList.size();i<len_i;i++) {
			modelNoSet.add(mfgMoItemAllEntityList.get(i).getModelNo());
		}
		QueryWrapper<ModelNoPrefixMgrEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("model_no", modelNoSet);
		queryWrapper.select("model_no,model_no_prefix");
		List<ModelNoPrefixMgrEntity> tempList = list(queryWrapper);
		Map<String,String> retMap = new HashMap<>();
		for (int i=0,len_i=tempList.size();i<len_i;i++) {
			ModelNoPrefixMgrEntity temp = tempList.get(i);
			retMap.put(temp.getModelNo(), temp.getModelNoPrefix());
		}
		return retMap;
	}
	
	/**
	 * 分配制品品番前缀
	 * @param modelNoSet
	 */
	public boolean distriPrefix(Set<String> modelNoSet) {
		QueryWrapper<ModelNoPrefixMgrEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("model_no", modelNoSet);
		queryWrapper.select("model_no");
		List<ModelNoPrefixMgrEntity> modelNoPrefixMgrEntityList = list(queryWrapper);
		List<String> modelNoList = new ArrayList<String>(modelNoPrefixMgrEntityList.size());
		for (int i=0,len_i=modelNoPrefixMgrEntityList.size();i<len_i;i++) {
			modelNoList.add(modelNoPrefixMgrEntityList.get(i).getModelNo());
		}
		modelNoSet.removeAll(modelNoList);
		if (modelNoSet.isEmpty()) {
			return true;
		} else {
			queryWrapper = new QueryWrapper<>();
			queryWrapper.select("max(reverse(model_no_prefix)) model_no_prefix");
			ModelNoPrefixMgrEntity modelNoPrefixMgrEntity = getOne(queryWrapper);
			String maxModelNoPrefix = modelNoPrefixMgrEntity == null? null : 
				new StringBuilder(modelNoPrefixMgrEntity.getModelNoPrefix()).reverse().toString();
			List<ModelNoPrefixMgrEntity> saveList = new ArrayList<>();
			if (StringUtils.isEmpty(maxModelNoPrefix)) {
				char[] prefix = {'0','0','0'};
				for(String modelNo:modelNoSet) {
					ModelNoPrefixMgrEntity saveData = new ModelNoPrefixMgrEntity();
					saveData.setModelNo(modelNo);
					saveData.setModelNoPrefix(new String(prefix));
					saveList.add(saveData);
					for(int i=0;i<3;i++) {
						char temp = prefix[i];
						if(temp>='0' && temp<'9') {
							prefix[i] = ++temp;
							break;
						} else if(temp == '9') {
							prefix[i] = 'a';
							break;
						} else if(temp>='a' && temp<'z') {
							prefix[i] = ++temp;
							break;
						} else if(temp == 'z') {
							prefix[i] = '0';
						}
					}
				}
			} else {
				char[] prefix = maxModelNoPrefix.toCharArray();
				for(String modelNo:modelNoSet) {
					for(int i=0;i<3;i++) {
						char temp = prefix[i];
						if(temp>='0' && temp<'9') {
							prefix[i] = ++temp;
							break;
						} else if(temp == '9') {
							prefix[i] = 'a';
							break;
						} else if(temp>='a' && temp<'z') {
							prefix[i] = ++temp;
							break;
						} else if(temp == 'z') {
							prefix[i] = '0';
						}
					}
					ModelNoPrefixMgrEntity saveData = new ModelNoPrefixMgrEntity();
					saveData.setModelNo(modelNo);
					saveData.setModelNoPrefix(new String(prefix));
					saveList.add(saveData);
				}
			}
			return saveBatch(saveList);
		}
	}
}