package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ScheduledSentMsgAcceptEntity;
import com.pstfs.mapper.ScheduledSentMsgAcceptMapper;

@DS("pst-wxhr-app")
@Service("scheduledSentMsgAcceptService")
public class ScheduledSentMsgAcceptService extends ServiceImpl<ScheduledSentMsgAcceptMapper, ScheduledSentMsgAcceptEntity> {

}