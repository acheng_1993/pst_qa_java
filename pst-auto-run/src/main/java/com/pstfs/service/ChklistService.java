package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ChklistEntity;
import com.pstfs.mapper.ChklistMapper;

@Service("chklistService")
@DS("techpl")
public class ChklistService extends ServiceImpl<ChklistMapper, ChklistEntity> {


}