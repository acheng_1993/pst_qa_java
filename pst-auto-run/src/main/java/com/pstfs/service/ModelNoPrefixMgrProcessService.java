package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ModelNoPrefixMgrProcessEntity;
import com.pstfs.mapper.ModelNoPrefixMgrProcessMapper;

@DS("new_mes")
@Service("modelNoPrefixMgrProcessService")
public class ModelNoPrefixMgrProcessService extends ServiceImpl<ModelNoPrefixMgrProcessMapper, ModelNoPrefixMgrProcessEntity> {


}