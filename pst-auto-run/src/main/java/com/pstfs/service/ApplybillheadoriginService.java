package com.pstfs.service;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ApplybillheadoriginEntity;
import com.pstfs.mapper.ApplybillheadoriginMapper;
@DS("yun_guan_tong_cmp")
@Service("applybillheadoriginService")
public class ApplybillheadoriginService extends ServiceImpl<ApplybillheadoriginMapper, ApplybillheadoriginEntity> {

	/**
	 * 获取下一个传票号
	 * @param lastInvoiceno
	 * @return
	 */
	public TreeSet<String> getAllNextInvoiceno(String lastInvoiceno) {
		QueryWrapper<ApplybillheadoriginEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("impexpflag", "I");
		queryWrapper.last("and createDate>(select createDate from ApplyBillHeadOrigin where impexpflag='I' and invoiceNo='"+lastInvoiceno.replaceAll("'", "\\'")+"') group by invoiceno");
		queryWrapper.select("invoiceno,max(createDate) createDate");
		List<ApplybillheadoriginEntity> applybillheadoriginEntityList = list(queryWrapper);
		applybillheadoriginEntityList.sort(new Comparator<ApplybillheadoriginEntity>() {
			@Override
			public int compare(ApplybillheadoriginEntity o1, ApplybillheadoriginEntity o2) {
				return o1.getCreatedate().compareTo(o2.getCreatedate());
			}
		});
		
		TreeSet<String> ret = new TreeSet<>();
		for(ApplybillheadoriginEntity applybillheadoriginEntity:applybillheadoriginEntityList) {
			ret.add(applybillheadoriginEntity.getInvoiceno());
		}
		return ret;
	}
	
	/**
	 * 获取传票号的创建日期
	 * @param invoiceno
	 * @return
	 */
//	public Date getInvoicenoCreateDate(String invoiceno) {
//		QueryWrapper<ApplybillheadoriginEntity> queryWrapper = new QueryWrapper<>();
//		queryWrapper.eq("impexpflag", "I");
//		queryWrapper.eq("invoiceno",invoiceno);
//		queryWrapper.select("top 1 createDate");
//		ApplybillheadoriginEntity applybillheadoriginEntity = getOne(queryWrapper);
//		return applybillheadoriginEntity == null?null:applybillheadoriginEntity.getCreatedate();
//	}
}