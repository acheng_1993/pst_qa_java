package com.pstfs.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.UploadIctFileListEntity;
import com.pstfs.mapper.UploadIctFileListMapper;


@DS("pst-wxhr-app")

@Service("uploadIctFileListService")
public class UploadIctFileListService extends ServiceImpl<UploadIctFileListMapper, UploadIctFileListEntity> {
	
	/*
	 * 获取uploadIctFileList表中所有type=-1的数据，返回成list
	 */
	public ArrayList<UploadIctFileListEntity> getType(){
		QueryWrapper<UploadIctFileListEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("type", -1);
		queryWrapper.select("file_id","suffix");
		ArrayList<UploadIctFileListEntity> list = (ArrayList<UploadIctFileListEntity>)list(queryWrapper);
		return list;
	}
	
	/*
	 * 根据指定的fileId插入shareString
	 */
	public boolean updateShareString(int fileId, String shareString) {
		UpdateWrapper<UploadIctFileListEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("file_id", fileId);
		return update(new UploadIctFileListEntity(null,null,null,null,null,null,null,null,shareString,null), updateWrapper);
	}
	
	/*
	 * 根据指定的fileId插入sheetData
	 */
	public boolean updateSheetData(int fileId, String sheetData) {
		UpdateWrapper<UploadIctFileListEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("file_id", fileId);
		return update(new UploadIctFileListEntity(null,null,null,null,null,null,null,sheetData,null,null), updateWrapper);
	}
	
	/*
	 * 根据指定的fileId修改type的值
	 */
	public boolean updateType(int type, int fileId) {
		UpdateWrapper<UploadIctFileListEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("file_id", fileId);
		return update(new UploadIctFileListEntity(null,null,null,null,null,null,type,null,null,null), updateWrapper);
	}

}
