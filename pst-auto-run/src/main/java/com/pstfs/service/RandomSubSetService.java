package com.pstfs.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.RandomSubSetEntity;
import com.pstfs.mapper.RandomSubSetMapper;

@DS("machine-learning")
@Service("randomSubSetService")
public class RandomSubSetService extends ServiceImpl<RandomSubSetMapper, RandomSubSetEntity> {

	/**
	 * 随机子集生成
	 * @param n 全集长度
	 * @param k 子集长度
	 * @param count 子集数
	 * @return
	 */
	public int[][] genRandomSubSet(int n,int k,int count) {
		int[][] ret = new int[count][k];
		for(int h=0; h<count; h++) {
			int[] ret2 = new int[k];
			int j=0;
			for (int i=0; i<n && j<k; i++) {
				if (Math.random() < ((double)(k-j))/(n-i)) {
					ret2[j] = i;
					j++;
				}
			}
			ret[h] = ret2;
		}
		return ret;
	}
	
	/**
	 * DCB子集翻译
	 * @param ret
	 * @return
	 */
	public HashMap<String, Integer> translateRandomSubSetForDCB(int[][] rret,int[][] bret) {
		HashMap<String, Integer> tempMap = new HashMap<>();
		for (int i=0,len_i=rret.length;i<len_i;i++) {
			int[] rret2 = rret[i];
			char[] subSet = new char[14];
			for (int j=0,len_j=rret2.length;j<len_j;j++) {
				int rret3 = rret2[j];
				if(rret3 < 9) {
					subSet[j*2] = '0';
					subSet[j*2+1] = (char) ('1' + rret3);
				} else {
					String temp = Integer.toString(rret3+1);
					subSet[j*2] = temp.charAt(0);
					subSet[j*2+1] = temp.charAt(1);
				}
			}
			int bret2 = bret[i][0];
			if(bret2 < 9) {
				subSet[12] = '0';
				subSet[13] = (char)('1' + bret2);
			} else {
				String temp = Integer.toString(bret2+1);
				subSet[12] = temp.charAt(0);
				subSet[13] = temp.charAt(1);
			}
			String subSetStr = new String(subSet);
			if(tempMap.containsKey(subSetStr)) {
				tempMap.put(subSetStr, tempMap.get(subSetStr)+1);
			} else {
				tempMap.put(subSetStr, 1);
			}
		}
		List<RandomSubSetEntity> ret = new ArrayList<>();
		for (Entry<String, Integer> entry:tempMap.entrySet()) {
			RandomSubSetEntity randomSubSetEntity = new RandomSubSetEntity();
			randomSubSetEntity.setRandomString(entry.getKey());
			randomSubSetEntity.setTimes(entry.getValue());
			ret.add(randomSubSetEntity);
		}
		return tempMap;
	}
	
	/**
	 * 生成 count 次 DCB 子集
	 * @param count
	 * @return
	 */
	public HashMap<String, Integer> genRandomSubSetDCBMap(int count) {
		int[][] rret = genRandomSubSet(33, 6, count);
		int[][] bret = genRandomSubSet(16, 1, count);
		HashMap<String, Integer> randomSubSetMap = translateRandomSubSetForDCB(rret, bret);
		return randomSubSetMap;
	}
	
	/**
	 * 批量保存
	 * @param randomSubSetEntityList
	 */
	@Transactional
	public void saveBatch(int randomSectionNoId,HashMap<String, Integer> randomSubSetMap) {
		List<String> randomStringList = new ArrayList<>();
		for (String randomString:randomSubSetMap.keySet()) {
			randomStringList.add(randomString);
		}
		QueryWrapper<RandomSubSetEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("random_section_no_id", randomSectionNoId);
		queryWrapper.in("random_string", randomStringList);
		List<RandomSubSetEntity> updateList = list(queryWrapper);
		for (int i=0,len_i=updateList.size();i<len_i;i++) {
			RandomSubSetEntity randomSubSetEntity = updateList.get(i);
			if(randomSubSetMap.containsKey(randomSubSetEntity.getRandomString())) {
				randomSubSetEntity.setTimes(randomSubSetEntity.getTimes()+randomSubSetMap.get(randomSubSetEntity.getRandomString()));
				randomSubSetMap.remove(randomSubSetEntity.getRandomString());
			}
		}
		List<RandomSubSetEntity> saveList = new ArrayList<>();
		for (Entry<String, Integer> entry :randomSubSetMap.entrySet()) {
			RandomSubSetEntity newEntity = new RandomSubSetEntity();
			newEntity.setRandomSectionNoId(randomSectionNoId);
			newEntity.setRandomString(entry.getKey());
			newEntity.setTimes(entry.getValue());
			saveList.add(newEntity);
		}
		if (!updateList.isEmpty()) {
			updateBatchById(updateList);
		}
		if (!saveList.isEmpty()) {
			saveBatch(saveList);
		}
	}
}