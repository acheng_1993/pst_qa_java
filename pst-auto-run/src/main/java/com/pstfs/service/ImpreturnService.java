package com.pstfs.service;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ImpreturnEntity;
import com.pstfs.mapper.ImpreturnMapper;

@DS("yun_guan_tong_cmp")
@Service("impreturnService")
public class ImpreturnService extends ServiceImpl<ImpreturnMapper, ImpreturnEntity> {
	
	/**
	 * 获取比指定 id 大的数据的前 limit 条
	 * @param id
	 * @param limit
	 * @return
	 */
	public List<ImpreturnEntity> list(String id,String declaredate,int limit) {
		if (limit<1 || limit>100) {
			throw new RuntimeException("参数 limit 异常，必须是 1-100 范围内的数字！");
		}
		QueryWrapper<ImpreturnEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.and(query->{
			query.gt("declaredate", declaredate);
			query.or(query1->{
				query1.eq("declaredate", declaredate);
				query1.gt("id", id);
			});
		});
		queryWrapper.orderByAsc("declaredate,id");
		queryWrapper.select("top "+limit+" id,invoiceno,itemNo1,zextendField11,contractno,copInvtNo,billNo,itemNo2,declaredate,corresPondEntryNo,netweight,ljptno,qty,amount");
		return list(queryWrapper);
	}
	
	/**
	 * 获取所有的未处理的传票号码
	 * @param id
	 * @return
	 */
	public TreeSet<String> getAllNextInvoiceno(String id,String handleDate){
		QueryWrapper<ImpreturnEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.and((query)->{
			query.gt("declaredate", handleDate);
			query.or((query2)->{
				query2.eq("declaredate", handleDate);
				query2.gt("id", id);
			});
		});
		queryWrapper.select("declaredate,invoiceno,max(id) id");
		queryWrapper.groupBy("declaredate,invoiceno");
		List<ImpreturnEntity> impreturnEntityList = list(queryWrapper);
		impreturnEntityList.sort(new Comparator<ImpreturnEntity>() {
			@Override
			public int compare(ImpreturnEntity o1, ImpreturnEntity o2) {
				return o1.getId().compareTo(o2.getId());
			}
		});
		TreeSet<String> ret = new TreeSet<>();
		for(ImpreturnEntity impreturnEntity:impreturnEntityList) {
			ret.add(impreturnEntity.getInvoiceno());
		}
		return ret;
	}
}