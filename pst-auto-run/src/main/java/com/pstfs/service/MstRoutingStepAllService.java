package com.pstfs.service;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.MstRoutingStepAllEntity;
import com.pstfs.mapper.MstRoutingStepAllMapper;

@DS("new_mes")
@Service("mstRoutingStepAllService")
public class MstRoutingStepAllService extends ServiceImpl<MstRoutingStepAllMapper, MstRoutingStepAllEntity> {

	/**
	 * 根据 id 获取下一组数据
	 * @param id
	 * @return
	 */
	public List<MstRoutingStepAllEntity> getNextDataForId(String id) {
		QueryWrapper<MstRoutingStepAllEntity> queryWrapper = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(id)) {
			queryWrapper.gt("id", id);
		}
		queryWrapper.orderByAsc("id");
		queryWrapper.select("itm_code,id");
		queryWrapper.last("limit 1000");
		return list(queryWrapper);
	}
	
	
	/**
	 * 根据 createdWhen 获取下一组数据
	 * @param id
	 * @return
	 */
	public List<MstRoutingStepAllEntity> getNextDataForCreatedWhen(Date createdWhen) {
		QueryWrapper<MstRoutingStepAllEntity> queryWrapper = new QueryWrapper<>();
		if (createdWhen != null) {
			queryWrapper.gt("created_when", createdWhen);
		}
		queryWrapper.orderByAsc("created_when");
		queryWrapper.select("itm_code,created_when");
		queryWrapper.last("limit 1000");
		return list(queryWrapper);
	}
}