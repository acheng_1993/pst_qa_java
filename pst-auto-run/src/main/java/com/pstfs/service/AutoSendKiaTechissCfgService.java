package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.AutoSendKiaTechissCfgEntity;
import com.pstfs.mapper.AutoSendKiaTechissCfgMapper;
@Service("autoSendKiaTechissProgressService")
@DS("techpl")
public class AutoSendKiaTechissCfgService extends ServiceImpl<AutoSendKiaTechissCfgMapper, AutoSendKiaTechissCfgEntity> {
	
}