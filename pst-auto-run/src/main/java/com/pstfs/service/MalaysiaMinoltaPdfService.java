package com.pstfs.service;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.MalaysiaMinoltaPdfDetailsDataEntity;
import com.pstfs.entity.MalaysiaMinoltaPdfEntity;
import com.pstfs.mapper.MalaysiaMinoltaPdfMapper;

@Service("malaysiaMinoltaPdfService")
public class MalaysiaMinoltaPdfService extends ServiceImpl<MalaysiaMinoltaPdfMapper, MalaysiaMinoltaPdfEntity> {

	@Autowired
	private MalaysiaMinoltaPdfDetailsDataService malaysiaMinoltaPdfDetailsDataService;

	@Transactional
	public boolean update(int uploadFileId,int mergeFileId,List<MalaysiaMinoltaPdfDetailsDataEntity> qrCodeContentList) {
		UpdateWrapper<MalaysiaMinoltaPdfEntity> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("upload_file_id", uploadFileId);
		updateWrapper.eq("status", 0);
		updateWrapper.set("status", 1);
		updateWrapper.set("merge_file_id", mergeFileId);
		updateWrapper.set("update_time", new Date());
		boolean ret = update(updateWrapper);
		
		QueryWrapper<MalaysiaMinoltaPdfDetailsDataEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("upload_file_id", uploadFileId);
		queryWrapper.last(" limit 1");
		queryWrapper.select("id");
		if(malaysiaMinoltaPdfDetailsDataService.getOne(queryWrapper) == null) {
			for(int i=0,len_i=qrCodeContentList.size();i<len_i;i++) {
				qrCodeContentList.get(i).setUploadFileId(uploadFileId);
			}
			malaysiaMinoltaPdfDetailsDataService.saveBatch(qrCodeContentList);
		}
		return ret;
	}
}