package com.pstfs.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.StatusEntity;
import com.pstfs.mapper.StatusMapper;

@Service("statusService")
@DS("techpl")
public class StatusService extends ServiceImpl<StatusMapper, StatusEntity> {

	private Map<String,String> statusMap = new HashMap<String, String>();
	
	/**
	 * 根据 statusId 获取 StatusDesc
	 * @param statusId
	 * @return
	 */
	public String getByStatusId(String statusId) {
		if(statusMap.isEmpty()) {
			synchronized (this) {
				if(statusMap.isEmpty()) {
					List<StatusEntity> statusEntityList = list();
					Map<String,String> statusMap = new HashMap<String, String>();
					for(int i=0,len_i = statusEntityList.size();i<len_i;i++) {
						StatusEntity statusEntity = statusEntityList.get(i);
						statusMap.put(statusEntity.getStatusId(), statusEntity.getStatusDesc());
					}
					this.statusMap = statusMap;
				}
				return statusMap.get(statusId);
			}
		}
		return statusMap.get(statusId);
	}
}