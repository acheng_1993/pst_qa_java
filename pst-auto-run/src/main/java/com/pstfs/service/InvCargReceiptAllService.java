package com.pstfs.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.InvCargReceiptAllEntity;
import com.pstfs.mapper.InvCargReceiptAllMapper;

/**
 * 新 mes 系统签收单表
 * @author jiajian
 */
@DS("new_mes")
@Service("invCargReceiptAllService")
public class InvCargReceiptAllService extends ServiceImpl<InvCargReceiptAllMapper, InvCargReceiptAllEntity> {
	
	@Autowired
	private ZProdExemFromInspService zprodExemFromInspService;
	
	
}