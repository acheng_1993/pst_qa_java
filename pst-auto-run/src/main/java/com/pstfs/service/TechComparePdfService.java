package com.pstfs.service;
import java.util.List;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ProdLog;
import com.pstfs.entity.TechComparePdf;
import com.pstfs.entity.extend.TechComparePdfExtend;
import com.pstfs.mapper.TechComparePdfMapper;

@Service
@DS("pst-wxhr-app")
public class TechComparePdfService extends ServiceImpl<TechComparePdfMapper, TechComparePdf>{

	/**
	 * 获取没有解析pdf内容的pdf文档列表
	 * @return
	 */
	public List<TechComparePdfExtend> getWithoutPdfContentList(){
		QueryWrapper<TechComparePdfExtend> queryWrapper = new QueryWrapper<>();
		queryWrapper.isNull("TCP.PDF_CONTENT");
		queryWrapper.or(true,(QueryWrapper<TechComparePdfExtend> t)-> {
			t.isNull("TCP.PDF_PAGE_INFO");
		});
		queryWrapper.select(
			"TCP.ID",
			"TCP.UPLOAD_FILE_LIST_ID",
			"TCP.PDF_NAME"
		);
		return baseMapper.selectTechComparePdfExtendList(queryWrapper);
	}
	
	private Page<TechComparePdf> firstRowPage = new Page<>(1, 1);
	/**
	 * 获取没有解析pdf内容的pdf文档数量
	 * @return
	 */
	public boolean getWithoutPdfContentExist(){
		QueryWrapper<TechComparePdf> queryWrapper = new QueryWrapper<>();
		queryWrapper.isNull("PDF_CONTENT");
		queryWrapper.or(true,(QueryWrapper<TechComparePdf> t)-> {
			t.isNull("PDF_PAGE_INFO");
		});
		queryWrapper.select("ID");
		return page(firstRowPage, queryWrapper).getSize() > 0;
	}
}