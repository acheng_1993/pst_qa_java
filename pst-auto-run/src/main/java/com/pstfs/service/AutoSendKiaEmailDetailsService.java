package com.pstfs.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.AutoSendKiaEmailDetailsEntity;
import com.pstfs.entity.TechissEntity;
import com.pstfs.mapper.AutoSendKiaEmailDetailsMapper;
import com.pstfs.mapper.TechissMapper;

@Service("autoSendKiaEmailDetailsService")
@DS("techpl")
public class AutoSendKiaEmailDetailsService extends ServiceImpl<AutoSendKiaEmailDetailsMapper, AutoSendKiaEmailDetailsEntity> {
	
	@Autowired
	private TechissMapper techissMapper;
	
	/**
	 * 使用 auto_send_kia_email.id 获取邮件附件内容
	 * @param autoSendKiaEmailIds
	 * @return
	 */
	public Map<Integer,List<String>> getMapListByAutoSendKiaEmailIds(List<Integer> autoSendKiaEmailIds){
		QueryWrapper<AutoSendKiaEmailDetailsEntity> query = new QueryWrapper<>();
		query.in("auto_send_kia_email_id", autoSendKiaEmailIds);
		query.eq("actual_send_status", 1);
		List<AutoSendKiaEmailDetailsEntity> autoSendKiaEmailDetailsList = list(query);
		List<Integer> techissIds = new ArrayList<>();
		Map<Integer,Integer> techissIdMapToAutoSendKiaEmailId = new HashMap<>(); 
		for (int i=0,len_i=autoSendKiaEmailDetailsList.size();i<len_i;i++) {
			AutoSendKiaEmailDetailsEntity autoSendKiaEmailDetailsEntity = autoSendKiaEmailDetailsList.get(i);
			techissIds.add(autoSendKiaEmailDetailsEntity.getTechissId());
			techissIdMapToAutoSendKiaEmailId.put(autoSendKiaEmailDetailsEntity.getTechissId(), autoSendKiaEmailDetailsEntity.getAutoSendKiaEmailId());
		}
		QueryWrapper<TechissEntity> query2 = new QueryWrapper<>();
		query2.in("id", techissIds);
		query2.select("id,upload");
		List<TechissEntity> techissEntityList = techissMapper.selectList(query2);
		Map<Integer,List<String>> ret = new HashMap<>();
		for (int i=0,len_i=techissEntityList.size();i<len_i;i++) {
			TechissEntity techissEntity = techissEntityList.get(i);
			Integer autoSendKiaEmailId = techissIdMapToAutoSendKiaEmailId.get(techissEntity.getId());
			List<String> uploadList = ret.get(autoSendKiaEmailId);
			if(uploadList == null) {
				ret.put(autoSendKiaEmailId, uploadList=new ArrayList<>());
			}
			uploadList.add(techissEntity.getUpload());
		}
		return ret;
	}
}