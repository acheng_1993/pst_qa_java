package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.MstItmAllEntity;
import com.pstfs.mapper.MstItmAllMapper;

@Service("mstItmAllService")
public class MstItmAllService extends ServiceImpl<MstItmAllMapper, MstItmAllEntity> {

}