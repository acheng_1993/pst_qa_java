package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.QywxUploadFileEntity;
import com.pstfs.mapper.QywxUploadFileMapper;

@Service("qywxUploadFileService")
public class QywxUploadFileService extends ServiceImpl<QywxUploadFileMapper, QywxUploadFileEntity> {

}