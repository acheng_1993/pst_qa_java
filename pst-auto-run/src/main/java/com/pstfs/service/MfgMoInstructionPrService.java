package com.pstfs.service;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.MfgMoInstructionPrEntity;
import com.pstfs.mapper.MfgMoInstructionPrMapper;

@DS("new_mes")
@Service("mfgMoInstructionPrService")
public class MfgMoInstructionPrService extends ServiceImpl<MfgMoInstructionPrMapper, MfgMoInstructionPrEntity> {

}