package com.pstfs.service;
import org.springframework.stereotype.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.BgConfirmTbl;
import com.pstfs.mapper.BgConfirmTblMapper;

/**
 * time2系统传票号码锁定表
 * @author jiajian
 */
@DS("time2")
@Service
public class BgConfirmTblService extends ServiceImpl<BgConfirmTblMapper, BgConfirmTbl>{

	
}