package com.pstfs.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.CategoryEntity;
import com.pstfs.mapper.CategoryMapper;
@Service("categoryService")
@DS("techpl")
public class CategoryService extends ServiceImpl<CategoryMapper, CategoryEntity> {

	private Map<CategoryEntity,String> categoryMap = new HashMap<>();
	
	/**
	 * 根据 category、doctype 获取 drawingno 数据
	 * @param doctype
	 * @return
	 */
	public String getByCategoryAndDoctype(String category,String doctype) {
		if(categoryMap.isEmpty()) {
			synchronized (this) {
				if(categoryMap.isEmpty()) {
					List<CategoryEntity> categoryEntityList = list();
					Map<CategoryEntity,String> categoryMap = new HashMap<>();
					for(int i=0,len_i=categoryEntityList.size();i<len_i;i++) {
						CategoryEntity categoryEntity = categoryEntityList.get(i);
						categoryMap.put(categoryEntity, categoryEntity.getDrawingno());
					}
					this.categoryMap = categoryMap;
				}
				CategoryEntity categoryEntity = new CategoryEntity();
				categoryEntity.setCategory(category);
				categoryEntity.setDoctype(doctype);
				return categoryMap.get(categoryEntity);
			}
		}
		CategoryEntity categoryEntity = new CategoryEntity();
		categoryEntity.setCategory(category);
		categoryEntity.setDoctype(doctype);
		return categoryMap.get(categoryEntity);
	}
}