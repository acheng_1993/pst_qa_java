package com.pstfs.service;
import java.util.List;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ScheduledSendMsgEntity;
import com.pstfs.mapper.ScheduledSendMsgMapper;

@DS("pst-wxhr-app")
@Service("scheduledSendMsgService")
public class ScheduledSendMsgService extends ServiceImpl<ScheduledSendMsgMapper, ScheduledSendMsgEntity> {

	/**
	 * 查找待推送的消息列表
	 * @return
	 */
	public List<ScheduledSendMsgEntity> findToBePushScheduledSendMsgList(int limit) {
		QueryWrapper<ScheduledSendMsgEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.select("id,title,content_text,content_html,redirect_url");
		queryWrapper.last("where (last_send_date is null and switch_status=1 and send_strategy=0 and auto_send_minute_of_day<minute(now())+60*hour(now()) and LEAST(send_day_of_strategy,send_candidate_day_of_strategy1,send_candidate_day_of_strategy2,send_candidate_day_of_strategy3)<=dayofyear(now())) "
				+ "or (last_send_date is null and switch_status=1 and send_strategy=1 and auto_send_minute_of_day<minute(now())+60*hour(now()) and LEAST(send_day_of_strategy,send_candidate_day_of_strategy1,send_candidate_day_of_strategy2,send_candidate_day_of_strategy3)<=dayofmonth(now())) "
				+ "or (last_send_date is null and switch_status=1 and send_strategy=2 and auto_send_minute_of_day<minute(now())+60*hour(now()) and LEAST(send_day_of_strategy,send_candidate_day_of_strategy1,send_candidate_day_of_strategy2,send_candidate_day_of_strategy3)<=dayofweek(now())) "
				+ "or (last_send_date is null and switch_status=1 and send_strategy=3 and auto_send_minute_of_day<minute(now())+60*hour(now())) "
				+ "or (switch_status=1 and send_strategy=0 and last_send_year<year(now()) and auto_send_minute_of_day<minute(now())+60*hour(now()) and LEAST(send_day_of_strategy,send_candidate_day_of_strategy1,send_candidate_day_of_strategy2,send_candidate_day_of_strategy3)<=dayofyear(now())) "
				+ "or (switch_status=1 and send_strategy=1 and last_send_year<=year(now()) and auto_send_minute_of_day<minute(now())+60*hour(now()) and LEAST(send_day_of_strategy,send_candidate_day_of_strategy1,send_candidate_day_of_strategy2,send_candidate_day_of_strategy3)<=dayofmonth(now()) and last_send_month_of_year<month(now())) "
				+ "or (switch_status=1 and send_strategy=2 and last_send_year<=year(now()) and auto_send_minute_of_day<minute(now())+60*hour(now()) and LEAST(send_day_of_strategy,send_candidate_day_of_strategy1,send_candidate_day_of_strategy2,send_candidate_day_of_strategy3)<=dayofweek(now()) and last_send_week_of_year<weekofyear(now()) and last_send_day_of_week<dayofweek(now())) "
				+ "or (switch_status=1 and send_strategy=3 and last_send_year<=year(now()) and auto_send_minute_of_day<minute(now())+60*hour(now()) and last_send_day_of_year<dayofyear(now())) limit "+limit);
		return list(queryWrapper);
	}
}