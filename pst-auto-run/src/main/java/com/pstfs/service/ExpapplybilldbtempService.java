package com.pstfs.service;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pstfs.entity.ExpapplybilldbtempEntity;
import com.pstfs.mapper.ExpapplybilldbtempMapper;

/**
 * 云关通出口申请单数据表业务类
 * @author jiajian
 */
@DS("yun_guan_tong_idb")
@Service("expapplybilldbtempService")
public class ExpapplybilldbtempService extends ServiceImpl<ExpapplybilldbtempMapper, ExpapplybilldbtempEntity> {
	
	/**
	 * 把装箱单号 outBillNo 相关的数据替换成 time2 系统中最新的数据
	 * @param outbillno
	 * @param expapplybilldbtempEntityList
	 */
	@Transactional(rollbackFor = Throwable.class)
	public void replaceDataList(String outBillNo,List<ExpapplybilldbtempEntity> expapplybilldbtempEntityList) {
		QueryWrapper<ExpapplybilldbtempEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("outBillNo", outBillNo);
		remove(queryWrapper);
		saveBatch(expapplybilldbtempEntityList);
	}
}