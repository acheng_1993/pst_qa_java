package com.pstfs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

import com.pstfs.scheduling.AutoFillLotNoForKyocera;
import com.pstfs.scheduling.AutoIctCheck;
import com.pstfs.scheduling.AutoMergeMalaysiaMinoltaPdf;
import com.pstfs.scheduling.AutoMoveAoiDataToMes;
import com.pstfs.scheduling.AutoMoveTime2DataToYunGuanTong;
import com.pstfs.scheduling.IctCompareProgressMail;
import com.pstfs.scheduling.ReadPdfContentToDatabase;

/**
 * 实时监听redis的键值对超时
 * @author jiajian
 */
@Configuration
public class RedisListenerConfig {
	
	/**
	 * 每次key超时后会触发事件
	 * @param connectionFactory
	 * @return
	 */
	@Bean
	RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
			ReadPdfContentToDatabase listenerAdapter1,
			AutoMoveAoiDataToMes listenerAdapter2,
			AutoMoveTime2DataToYunGuanTong listenerAdapter3,
			AutoIctCheck listenerAdapter4,
			IctCompareProgressMail listenerAdapter5,
			AutoMergeMalaysiaMinoltaPdf listenerAdapter6,
			AutoMergeMalaysiaMinoltaPdf listenerAdapter7,
			AutoFillLotNoForKyocera listenerAdapter8) {
		RedisMessageListenerContainer container = new RedisMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.addMessageListener(listenerAdapter1, new PatternTopic("techComparePdf:ReadContent"));
		container.addMessageListener(listenerAdapter2, new PatternTopic("AutoScanMoQrcode"));
		container.addMessageListener(listenerAdapter3, new PatternTopic("AutoMoveTime2DataToYunGuanTongLock"));
		container.addMessageListener(listenerAdapter3, new PatternTopic("AutoMoveTime2DataToYunGuanTongUnLock"));
		container.addMessageListener(listenerAdapter4, new PatternTopic("AutoCheckICTFileAndAnalysis"));
		container.addMessageListener(listenerAdapter5, new PatternTopic("IctCompareProgressMail"));
		container.addMessageListener(listenerAdapter6, new PatternTopic("AutoMergeMalaysiaMinoltaPdf"));
		container.addMessageListener(listenerAdapter7, new PatternTopic("AutoMergeMalaysiaMinoltaSubPdf"));
		container.addMessageListener(listenerAdapter8, new PatternTopic("AutoFillLotNoForKyocera"));
		return container;
	}
}