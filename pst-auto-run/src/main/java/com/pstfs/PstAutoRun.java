package com.pstfs;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.locks.Lock;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;
@EnableScheduling
@SpringBootApplication
@MapperScan("com.pstfs.mapper")
@Slf4j
public class PstAutoRun {
	public static final List<Lock> LOCK_LIST = new Vector<Lock>();
	public static void main(String[] args) {
		SpringApplication.run(PstAutoRun.class, args);
		Runtime run=Runtime.getRuntime();
        run.addShutdownHook(new Thread(){
            @Override
            public void run() {
            	for(Lock lock:LOCK_LIST) {
            		lock.lock();
            	}
            	log.info("程序结束调用...");
            }
        });
	}
}