package com.pstfs.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.entity.AutoSendKiaEmailEntity;
import com.pstfs.entity.extend.AutoSendKiaEmailExtend;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 自动发送给KIA的邮件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 12:10:12
 */
@Mapper
public interface AutoSendKiaEmailMapper extends BaseMapper<AutoSendKiaEmailEntity> {
	
	@Select("SELECT ASKE.ID ID,ASKE.TITLE TITLE,ASKE.PNPA PNPA,ASETMI.MAKER_NAME MAKER_NAME,ASETMI.MAKER_CD MAKER_CD FROM AUTO_SEND_KIA_EMAIL ASKE "+
	"LEFT JOIN AUTO_SEND_EMAIL_TO_MAKERCD_INFO ASETMI ON ASKE.AUTO_SEND_EMAIL_TO_MAKERCD_INFO_ID=ASETMI.ID WHERE ${"+ Constants.WRAPPER + ".sqlSegment}")
	public List<AutoSendKiaEmailExtend> getExtendList(@Param(Constants.WRAPPER) Wrapper<AutoSendKiaEmailExtend> queryWrapper);
	
	@Select("select asetmii.maker_cd maker_cd,asked.techiss_id extend_int_col1 from auto_send_kia_email aske "+
	"left join auto_send_email_to_makercd_info asetmii on aske.auto_send_email_to_makercd_info_id=asetmii.id "+
	"left join auto_send_kia_email_details asked on asked.auto_send_kia_email_id=aske.id "+
	"left join techiss t on t.id = asked.techiss_id "+
	"where aske.mail_type=1 and aske.create_time>=DATE_SUB(curdate(),INTERVAL 1 DAY) and t.makercd!=asetmii.maker_cd")
	public List<AutoSendKiaEmailExtend> getYestodayMultiMakecdDataList();
}
