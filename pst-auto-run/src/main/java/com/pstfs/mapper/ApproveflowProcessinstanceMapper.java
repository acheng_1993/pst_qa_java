package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ApproveflowProcessinstanceEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-09-26 10:29:19
 */
@Mapper
public interface ApproveflowProcessinstanceMapper extends BaseMapper<ApproveflowProcessinstanceEntity> {
	
}
