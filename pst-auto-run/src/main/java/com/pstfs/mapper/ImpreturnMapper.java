package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ImpreturnEntity;

/**
 * invoiceno --原单号, 
itemNo1 --传输序号,
zextendField11 --总表序号, 
contractno --手册号,
copInvtNo --分单号, 
billNo --核注清单号, 
itemNo2 --流水号,
declaredate --申报日期,
corresPondEntryNo --报关单号, 
ljptno --品番,
qty --数量,
amount --金额
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-02 11:01:30
 */
@Mapper
public interface ImpreturnMapper extends BaseMapper<ImpreturnEntity> {
	
}
