package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.KyoceraPdfHandleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 京瓷的“交货指示”pdf处理情况表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2024-01-22 10:33:01
 */
@Mapper
public interface KyoceraPdfHandleMapper extends BaseMapper<KyoceraPdfHandleEntity> {
	
}
