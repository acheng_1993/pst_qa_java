package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.DeptEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * Departments
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-09-30 15:57:13
 */
@Mapper
public interface DeptMapper extends BaseMapper<DeptEntity> {
	
}
