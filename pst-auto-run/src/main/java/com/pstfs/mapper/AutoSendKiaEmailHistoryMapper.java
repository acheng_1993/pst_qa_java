package com.pstfs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.AutoSendKiaEmailHistoryEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 自动发送邮件到KIA历史记录。
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-19 10:45:07
 */
@Mapper
public interface AutoSendKiaEmailHistoryMapper extends BaseMapper<AutoSendKiaEmailHistoryEntity> {
	
}
