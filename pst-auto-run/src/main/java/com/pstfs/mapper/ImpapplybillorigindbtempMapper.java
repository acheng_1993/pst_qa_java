package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ImpapplybillorigindbtempEntity;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-08-31 15:20:42
 */
@Mapper
public interface ImpapplybillorigindbtempMapper extends BaseMapper<ImpapplybillorigindbtempEntity> {
	
}