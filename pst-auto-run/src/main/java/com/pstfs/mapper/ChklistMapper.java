package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ChklistEntity;
/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 10:46:23
 */
@Mapper
public interface ChklistMapper extends BaseMapper<ChklistEntity> {
	
}
