package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ScheduledSentMsgEntity;

/**
 * 计划发送消息中已经发送的消息
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-08 10:40:36
 */
@Mapper
public interface ScheduledSentMsgMapper extends BaseMapper<ScheduledSentMsgEntity> {

}