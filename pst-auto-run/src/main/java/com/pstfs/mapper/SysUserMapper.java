package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-09 09:37:10
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {
	
}
