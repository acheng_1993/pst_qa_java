package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.IqcCheckListEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * iqc的进料通知检验单
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-07-25 16:48:10
 */
@Mapper
public interface IqcCheckListMapper extends BaseMapper<IqcCheckListEntity> {
	
}
