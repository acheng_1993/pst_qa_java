package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ProdLog;

/**
 * 产线的生产日志
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-12-17 11:25:34
 */
@Mapper
public interface ProdLogMapper extends BaseMapper<ProdLog> {
	
}
