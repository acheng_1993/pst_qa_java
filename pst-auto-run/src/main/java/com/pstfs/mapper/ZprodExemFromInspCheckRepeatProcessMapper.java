package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ZprodExemFromInspCheckRepeatProcessEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 对zprod_exem_from_insp表的重复数据校验进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-26 09:38:34
 */
@Mapper
public interface ZprodExemFromInspCheckRepeatProcessMapper extends BaseMapper<ZprodExemFromInspCheckRepeatProcessEntity> {
	
}
