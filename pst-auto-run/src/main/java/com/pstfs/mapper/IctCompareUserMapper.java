package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.IctCompareUserEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * ict对比相关的用户，用于控制指定用户具有的操作权限
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-07-11 09:56:51
 */
@Mapper
public interface IctCompareUserMapper extends BaseMapper<IctCompareUserEntity> {
	
}
