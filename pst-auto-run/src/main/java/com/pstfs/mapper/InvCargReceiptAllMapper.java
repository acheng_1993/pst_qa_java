package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.InvCargReceiptAllEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 物料签收单表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-21 10:55:42
 */
@Mapper
public interface InvCargReceiptAllMapper extends BaseMapper<InvCargReceiptAllEntity> {
	
}