package com.pstfs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.UploadIctFileListEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * ICT程序核对的文件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-10 17:21:20
 */
@Mapper
public interface UploadIctFileListMapper extends BaseMapper<UploadIctFileListEntity> {
	
}
