package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ApplybillheadoriginEntity;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-02 11:16:34
 */
@Mapper
public interface ApplybillheadoriginMapper extends BaseMapper<ApplybillheadoriginEntity> {
	
}
