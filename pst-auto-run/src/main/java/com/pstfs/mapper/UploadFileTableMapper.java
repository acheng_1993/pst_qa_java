package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.UploadFileTableEntity;

/**
 * 涉及到文件上传的表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-13 17:15:53
 */
@Mapper
public interface UploadFileTableMapper extends BaseMapper<UploadFileTableEntity> {
	
}
