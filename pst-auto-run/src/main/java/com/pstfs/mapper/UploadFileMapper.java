package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.UploadFileEntity;

/**
 * 后台保存的文件表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-08-17 08:28:53
 */
@Mapper
public interface UploadFileMapper extends BaseMapper<UploadFileEntity> {

	@Select("select max(t.id) id from (select id from upload_file where id > #{lastProgressId} order by id asc limit #{limit}) t")
	public Integer newLastProgressId(@Param("lastProgressId")int lastProgressId,@Param("limit")int limit);
}