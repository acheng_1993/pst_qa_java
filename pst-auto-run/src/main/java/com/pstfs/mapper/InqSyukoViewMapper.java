package com.pstfs.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.entity.InqSyukoView;
import com.pstfs.entity.extend.InqSyukoViewExtend;
public interface InqSyukoViewMapper extends BaseMapper<InqSyukoView>{

	@Select("SELECT ${" + Constants.WRAPPER + ".sqlSelect} FROM TKR.INQ_SYUKO_VIEW ISV "
			+ "LEFT JOIN TKR.KONPO_TBL KB ON ISV.CUST_ITEM_CD = KB.ITEM_CD AND ISV.FC_ID = KB.FC_ID "
			+ "LEFT JOIN TKR.INV_HEADER_TBL IHT ON ISV.INVNO = IHT.SLP_NO AND ISV.FC_ID = IHT.FC_ID "
			+ "WHERE ${"+ Constants.WRAPPER + ".sqlSegment}")
    List<InqSyukoViewExtend> selectInqSyukoViewExtendList(@Param(Constants.WRAPPER) Wrapper<InqSyukoViewExtend> queryWrapper);
	
	@Select("SELECT t.FC_ID,t.SHP_KSQ,t.SHP_TYP,t.S_ODR_TYP,t.S_ODR_NO,t.DIV_CD,t.CUST_CD,t.CLNT_SNM,"
			+ "t.CUST_WHSE_TYP,t.CUST_G_P_TYP,t.CUST_ODR_NO,t.DLV_NO,t.CORR_TYP,t.CUST_DUE_DT,t.CLSF_CD,"
			+ "t.SHP_DT,t.SHP_QTY,t.ITEM_CD,t.INVNO,t.SHP_AMNT,t.PRD_NO,t.S_ODR_QTY,t.SHP_INST_NO,t.D_DLV_FLG,"
			+ "t.CUST_DLV_CD,t.CUST_DLV_PLC_NM,t.CMPSG_TYP,t.PIC_CD,t.FRCT_TYP,t.FRCT_TYP2,t.CUR_ID,t.U_PRIC_TYP,"
			+ "t.SU_PRIC,t.TRD_TRM,t.TRD_TRM2,t.FRCT_TYP3,t.TAX_ID,t.TAX_RT,t.TAX_AMNT,t.U_PRIC_CHG_DT,"
			+ "t.CL_CUR_ID,t.CL_U_PRIC_TYP,t.CL_U_PRIC,t.CL_SHP_AMNT,t.CL_TAX_AMNT,t.EX_RT,t.ST_CUR_ID,"
			+ "t.ST_U_PRIC,t.ST_SHP_AMNT,t.ST_TAX_AMNT,t.U_SIMEYM,t.AR_TYP,t.VERF_FLG,t.VERF_DT,t.DAT_TYP,"
			+ "t.MSCL_TYP,t.MSCL_CORR_FLG,t.CUST_CL_CD,t.S_INP_TYP,t.PLAN_TYP,t.COMP_FLG,t.WHSE_CD,t.WHSE_TYP,"
			+ "t.G_P_TYP,t.NOTE1,t.NOTE2,t.EU_PODR_TYP,t.EU_PRD_NO,t.CUST_INST_NO,t.ID_TYP,t.SLIP_ID,t.TRD_TYP,"
			+ "t.TRCK_NO,t.TRNSFCT_CUST_CD,t.CRTN_QTY,t.TRNSFCT_DT,t.CSTM_SLS_NO,t.TRNSFCT_QTY,t.PKG_PALLT_NO,"
			+ "t.PKG_PALLT_QTY FROM ${"+ Constants.WRAPPER + ".sqlSegment}")
	List<InqSyukoView> scrollLoad(@Param(Constants.WRAPPER) Wrapper<InqSyukoView> queryWrapper);
}