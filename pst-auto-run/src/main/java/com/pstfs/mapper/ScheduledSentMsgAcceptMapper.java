package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ScheduledSentMsgAcceptEntity;

/**
 * 已经发送的消息接收者列表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-10 10:29:35
 */
@Mapper
public interface ScheduledSentMsgAcceptMapper extends BaseMapper<ScheduledSentMsgAcceptEntity> {

	@Select("insert into scheduled_sent_msg_accept(`userid`,`scheduled_sent_msg_id`,`read`,`create_time`,`update_time`)"
			+ "select userid,#{scheduledSentMsgId},0,now(),now() from qywx_user_list")
	void insertBatch(@Param("scheduledSentMsgId")Integer scheduledSentMsgId);
}