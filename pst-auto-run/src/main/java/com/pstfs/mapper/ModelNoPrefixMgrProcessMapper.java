package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ModelNoPrefixMgrProcessEntity;

/**
 * 制品品番前缀表检测进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-08-22 15:36:17
 */
@Mapper
public interface ModelNoPrefixMgrProcessMapper extends BaseMapper<ModelNoPrefixMgrProcessEntity> {
	
}