package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.MstRoutingStepAllEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 制品工艺节点追踪路线
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-08-22 15:01:31
 */
@Mapper
public interface MstRoutingStepAllMapper extends BaseMapper<MstRoutingStepAllEntity> {
	
}
