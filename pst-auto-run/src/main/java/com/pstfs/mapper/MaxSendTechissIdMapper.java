package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.entity.MaxSendTechissIdEntity;

/**
 * 最大发送id，用于记录techiss已经发送的内容中的最大id
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-09-30 14:53:28
 */
@Mapper
public interface MaxSendTechissIdMapper extends BaseMapper<MaxSendTechissIdEntity> {

}