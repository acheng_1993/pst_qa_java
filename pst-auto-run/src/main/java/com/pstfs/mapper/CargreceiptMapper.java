package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.CargreceiptEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * ${comments}
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-21 11:07:33
 */
@Mapper
public interface CargreceiptMapper extends BaseMapper<CargreceiptEntity> {
	
}
