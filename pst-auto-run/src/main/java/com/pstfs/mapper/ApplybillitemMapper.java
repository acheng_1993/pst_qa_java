package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ApplybillitemEntity;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-02 11:01:40
 */
@Mapper
public interface ApplybillitemMapper extends BaseMapper<ApplybillitemEntity> {
	
}
