package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.QywxUploadFileEntity;

/**
 * 企业微信的文件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-18 14:55:16
 */
@Mapper
public interface QywxUploadFileMapper extends BaseMapper<QywxUploadFileEntity> {
	
}
