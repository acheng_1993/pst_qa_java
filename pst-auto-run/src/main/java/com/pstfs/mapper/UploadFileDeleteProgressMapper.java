package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.UploadFileDeleteProgressEntity;

/**
 * 上传文件的删除进度表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-14 09:18:51
 */
@Mapper
public interface UploadFileDeleteProgressMapper extends BaseMapper<UploadFileDeleteProgressEntity> {
	
}
