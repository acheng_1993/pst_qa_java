package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.StatusEntity;
import org.apache.ibatis.annotations.Mapper;
/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 15:47:59
 */
@Mapper
public interface StatusMapper extends BaseMapper<StatusEntity> {
	
}
