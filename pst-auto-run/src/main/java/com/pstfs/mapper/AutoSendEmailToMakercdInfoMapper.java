package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.AutoSendEmailToMakercdInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 自动发送邮件到加工商的各个加工商的信息
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-21 15:08:30
 */
@Mapper
public interface AutoSendEmailToMakercdInfoMapper extends BaseMapper<AutoSendEmailToMakercdInfoEntity> {
	
}
