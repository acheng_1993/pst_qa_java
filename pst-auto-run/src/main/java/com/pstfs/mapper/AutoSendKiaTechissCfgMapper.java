package com.pstfs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.AutoSendKiaTechissCfgEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 自动发送到 kia 的进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 11:09:49
 */
@Mapper
public interface AutoSendKiaTechissCfgMapper extends BaseMapper<AutoSendKiaTechissCfgEntity> {
	
}
