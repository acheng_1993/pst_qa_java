package com.pstfs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.Mentrydb2111;
import org.apache.ibatis.annotations.Mapper;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-12-17 11:39:53
 */
@Mapper
public interface Mentrydb2111Mapper extends BaseMapper<Mentrydb2111> {
	
}
