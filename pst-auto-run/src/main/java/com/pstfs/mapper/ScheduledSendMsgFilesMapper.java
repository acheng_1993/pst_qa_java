package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ScheduledSendMsgFilesEntity;

/**
 * 计划发送消息设置表的文件id
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-18 14:42:43
 */
@Mapper
public interface ScheduledSendMsgFilesMapper extends BaseMapper<ScheduledSendMsgFilesEntity> {
	
}
