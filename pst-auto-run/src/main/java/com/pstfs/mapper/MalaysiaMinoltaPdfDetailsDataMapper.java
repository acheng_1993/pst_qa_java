package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.MalaysiaMinoltaPdfDetailsDataEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 马来西亚柯尼卡美能达的pdf详细数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-03 17:19:55
 */
@Mapper
public interface MalaysiaMinoltaPdfDetailsDataMapper extends BaseMapper<MalaysiaMinoltaPdfDetailsDataEntity> {
	
}
