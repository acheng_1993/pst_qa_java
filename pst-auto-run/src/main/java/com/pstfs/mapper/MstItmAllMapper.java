package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.MstItmAllEntity;

/**
 * 物品基础信息主表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-21 14:53:42
 */
@Mapper
public interface MstItmAllMapper extends BaseMapper<MstItmAllEntity> {
	
}
