package com.pstfs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ZProdExemFromInspEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 免检部品表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-21 10:48:03
 */
@Mapper
public interface ZProdExemFromInspMapper extends BaseMapper<ZProdExemFromInspEntity> {
	
}
