package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ModelNoPrefixMgrEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 制品品番前缀表
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-08-22 15:01:20
 */
@Mapper
public interface ModelNoPrefixMgrMapper extends BaseMapper<ModelNoPrefixMgrEntity> {
	
}
