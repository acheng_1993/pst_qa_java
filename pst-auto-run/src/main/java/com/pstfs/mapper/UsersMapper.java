package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.UsersEntity;

/**
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-09-30 15:57:13
 */
@Mapper
public interface UsersMapper extends BaseMapper<UsersEntity> {
	
}
