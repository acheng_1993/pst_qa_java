package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.PaymentSummonsDataEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 支付传票数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-09-26 11:34:53
 */
@Mapper
public interface PaymentSummonsDataMapper extends BaseMapper<PaymentSummonsDataEntity> {
	
}
