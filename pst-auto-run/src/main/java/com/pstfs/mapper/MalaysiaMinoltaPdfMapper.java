package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.MalaysiaMinoltaPdfEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * 马来西亚美能达的pdf二维码解析功能
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-11-01 14:00:28
 */
@Mapper
public interface MalaysiaMinoltaPdfMapper extends BaseMapper<MalaysiaMinoltaPdfEntity> {
	
}
