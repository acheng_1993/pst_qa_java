package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.FormForminstanceProcessEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 支付传票读取进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-09-26 11:35:12
 */
@Mapper
public interface FormForminstanceProcessMapper extends BaseMapper<FormForminstanceProcessEntity> {
	
}
