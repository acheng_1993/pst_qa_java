package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.IctCompareRetEntity;
import com.pstfs.entity.extend.IctCompareRetExtend;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * ict和partList文件的对比结果
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-07-11 09:56:51
 */
@Mapper
public interface IctCompareRetMapper extends BaseMapper<IctCompareRetEntity> {
	
	/**
	 * 连表查询
	 * @param queryWrapper
	 * @return
	 */
	@Select("select icr.id id,uifl1.src_name compare_src_name1,uifl2.src_name compare_src_name2,icr.compare_type compare_type,icr.confirm_progress confirm_progress,icr.last_send_mail_date last_send_mail_date from "+
	 "ict_compare_ret icr left join upload_ict_file_list uifl1 on icr.upload_ict_file_list_id1 = uifl1.id left join upload_ict_file_list uifl2 on icr.upload_ict_file_list_id2 = uifl2.id "+
	 "where icr.confirm_progress>0 and icr.confirm_progress<4 and (icr.last_send_mail_date is null or icr.last_send_mail_date<=date_sub(now(),interval 1 day))")
	public List<IctCompareRetExtend> toBeSendEmailList();
}
