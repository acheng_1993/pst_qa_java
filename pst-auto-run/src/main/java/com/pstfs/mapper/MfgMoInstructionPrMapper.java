package com.pstfs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.MfgMoInstructionPrEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * MO工程指示预定表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-03-15 15:17:55
 */
@Mapper
public interface MfgMoInstructionPrMapper extends BaseMapper<MfgMoInstructionPrEntity> {
	
}
