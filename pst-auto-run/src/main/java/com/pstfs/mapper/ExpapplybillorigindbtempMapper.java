package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ExpapplybillorigindbtempEntity;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-09-01 11:27:24
 */
@Mapper
public interface ExpapplybillorigindbtempMapper extends BaseMapper<ExpapplybillorigindbtempEntity> {
	
}
