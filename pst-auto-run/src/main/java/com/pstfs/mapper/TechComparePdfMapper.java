package com.pstfs.mapper;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.pstfs.entity.TechComparePdf;
import com.pstfs.entity.extend.TechComparePdfExtend;

/**
 * 技术部需要对比的Pdf
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-06-21 16:20:22
 */
public interface TechComparePdfMapper extends BaseMapper<TechComparePdf> {

	/**
	 * 查询技术部需要对比的Pdf的路径等信息
	 * @param queryWrapper
	 * @return
	 */
	@Select("SELECT ${" + Constants.WRAPPER + ".sqlSelect},(SELECT PATH FROM UPLOAD_FILE UF WHERE UF.ID=UFL.FILE_ID) PATH FROM "+
	 "TECH_COMPARE_PDF TCP LEFT JOIN UPLOAD_FILE_LIST UFL ON TCP.UPLOAD_FILE_LIST_ID=UFL.ID "+
	 "WHERE ${"+ Constants.WRAPPER + ".sqlSegment}")
	List<TechComparePdfExtend> selectTechComparePdfExtendList(@Param(Constants.WRAPPER) Wrapper<TechComparePdfExtend> queryWrapper);
	
}