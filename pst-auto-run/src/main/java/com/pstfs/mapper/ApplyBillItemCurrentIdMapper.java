package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ApplyBillItemCurrentIdEntity;

/**
 * 云关通报关单返填功能标识id，用于标识当且返填进度，以区分哪些单是返填过的，哪些单是未返填过的。
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-02 10:39:41
 */
@Mapper
public interface ApplyBillItemCurrentIdMapper extends BaseMapper<ApplyBillItemCurrentIdEntity> {
	
}
