package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ExpreturnEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-10-10 14:37:52
 */
@Mapper
public interface ExpreturnMapper extends BaseMapper<ExpreturnEntity> {
	
}
