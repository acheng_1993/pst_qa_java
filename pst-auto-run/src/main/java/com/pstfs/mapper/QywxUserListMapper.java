package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.QywxUserListEntity;

/**
 * 企业微信用户表，这是从企业微信接口中获取的用户数据，每天同步一次，用于降低调用企业微信接口的频率
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-10-25 16:00:20
 */
@Mapper
public interface QywxUserListMapper extends BaseMapper<QywxUserListEntity> {
	
}
