package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ScheduledSendMsgEntity;

/**
 * 计划发送消息设置表，用于记录将要发送消息的时间，策略等内容...
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-07 14:03:05
 */
@Mapper
public interface ScheduledSendMsgMapper extends BaseMapper<ScheduledSendMsgEntity> {
	
}
