package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ScheduledSentMsgFilesEntity;

/**
 * 已经发送的消息设置表的文件id
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-02-23 08:41:01
 */
@Mapper
public interface ScheduledSentMsgFilesMapper extends BaseMapper<ScheduledSentMsgFilesEntity> {

	@Select("insert into scheduled_sent_msg_files(scheduled_sent_msg_id,upload_file_id,is_attachment,src_name,suffix,create_time,update_time)"+
	"select #{scheduledSentMsgId},upload_file_id,is_attachment,src_name,suffix,now(),now() from scheduled_send_msg_files where scheduled_send_msg_id=#{scheduledSendMsgId}")
	public void insertBatch(@Param("scheduledSentMsgId")Integer scheduledSentMsgId,@Param("scheduledSendMsgId") Integer scheduledSendMsgId);
}
