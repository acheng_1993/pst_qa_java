package com.pstfs.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.ExpapplybilldbtempEntity;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-08-22 10:50:29
 */
@Mapper
public interface ExpapplybilldbtempMapper extends BaseMapper<ExpapplybilldbtempEntity> {
	
}
