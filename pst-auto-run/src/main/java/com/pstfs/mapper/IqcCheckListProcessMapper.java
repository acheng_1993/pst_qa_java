package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.IqcCheckListProcessEntity;

import org.apache.ibatis.annotations.Mapper;

/**
 * iqc_check_list表的检索进度
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-07-25 17:16:12
 */
@Mapper
public interface IqcCheckListProcessMapper extends BaseMapper<IqcCheckListProcessEntity> {
	
}
