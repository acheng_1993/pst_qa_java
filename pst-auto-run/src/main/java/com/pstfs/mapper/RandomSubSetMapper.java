package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.RandomSubSetEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 随机子集表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-06-13 10:31:08
 */
@Mapper
public interface RandomSubSetMapper extends BaseMapper<RandomSubSetEntity> {
	
}
