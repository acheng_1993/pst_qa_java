package com.pstfs.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.InvtheadEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * ${comments}
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-03-20 09:49:42
 */
@Mapper
public interface InvtheadMapper extends BaseMapper<InvtheadEntity> {

	@Select("select top 1 copInvtNo from InvtHead where "+
	"declareStatus='7' and createDate>(select max(createDate) from InvtHead where copInvtNo in"+
	"(select distinct copInvtNo from Impreturn where invoiceno=#{invoiceno} and declareStatus='7'))"+
	"order by createDate asc")
	public String getNextCopInvtNo(@Param("invoiceno")String invoiceno);
}