package com.pstfs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.AutoSendKiaEmailDetailsEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 自动发送给KIA的邮件，对应的 techiss 的详细数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-04-12 12:10:12
 */
@Mapper
public interface AutoSendKiaEmailDetailsMapper extends BaseMapper<AutoSendKiaEmailDetailsEntity> {
	
}