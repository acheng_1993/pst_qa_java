package com.pstfs.mapper;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pstfs.entity.FormForminstanceEntity;

/**
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-09-26 10:29:05
 */
@Mapper
public interface FormForminstanceMapper extends BaseMapper<FormForminstanceEntity> {
	
}
